﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Needs_Action_Editor.Classes;
using Needs_Action_Editor.Models;

namespace Needs_Action_Editor
{
    class NeedsActionEditorViewModel
    {
        public NeedsActionEditorViewModel()
        {
            Initialize_DataGrid_Actions();
        }

        ObservableCollection<StringWrapper> actions;
        public ObservableCollection<StringWrapper> Actions
        {
            get { return actions; }
            set { actions = value; }
        }
        void Initialize_DataGrid_Actions()
        {
            actions = new ObservableCollection<StringWrapper>();
        }
    }
}
