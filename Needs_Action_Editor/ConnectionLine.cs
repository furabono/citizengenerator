﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Media;

namespace Needs_Action_Editor
{
    class ConnectionLine
    {
        BindingPoint source;
        BindingPoint destination;

        Path path;
        PathFigure figure;
        BezierSegment bezier;

        public Path Path
        {
            get { return path; }
        }

        public ConnectionLine(BindingPoint source, BindingPoint destination)
        {
            this.source = source;
            this.destination = destination;

            //double center_x = (source.X + destination.X) / 2;
            double center_y = (source.Y + destination.Y) / 2;

            bezier = new BezierSegment()
            {
                Point1 = new Point(source.X, center_y),
                Point2 = new Point(destination.X, center_y),
                Point3 = destination.Point,
                IsStroked = true
            };

            figure = new PathFigure();
            figure.StartPoint = source.Point;
            figure.IsClosed = false;
            figure.Segments.Add(bezier);

            path = new Path();
            path.Stroke = Brushes.Black;
            path.StrokeThickness = 2;
            path.Data = new PathGeometry(new PathFigure[] { figure });

            path.MouseEnter += Path_MouseEnter;
            path.MouseLeave += Path_MouseLeave;

            this.source.PropertyChanged += Source_PropertyChanged;
            this.destination.PropertyChanged += Destination_PropertyChanged;
        }

        private void Path_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            path.Stroke = Brushes.Yellow;
        }

        private void Path_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            path.Stroke = Brushes.Black;
        }

        private void Source_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Update();
        }

        private void Destination_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Update();
        }

        void Update()
        {
            double center_y = (source.Y + destination.Y) / 2;

            figure.StartPoint = source.Point;
            bezier.Point1 = new Point(source.X, center_y);
            bezier.Point2 = new Point(destination.X, center_y);
            bezier.Point3 = destination.Point;
        }

        public void Clear()
        {
            source.PropertyChanged -= Source_PropertyChanged;
            destination.PropertyChanged -= Destination_PropertyChanged;
        }
    }
}
