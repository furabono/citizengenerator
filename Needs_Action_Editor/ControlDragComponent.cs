﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Needs_Action_Editor
{
    public class ControlDragComponent
    {
        private Control control;
        protected bool isDragging;
        private Point clickPosition;

        public event Action ControlDragged;

        public ControlDragComponent(Control control)
        {
            this.control = control;
            control.MouseLeftButtonDown += Control_MouseLeftButtonDown;
            control.MouseLeftButtonUp += Control_MouseLeftButtonUp;
            control.MouseMove += Control_MouseMove;
        }

        private void Control_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isDragging = true;
            var draggableControl = sender as Control;
            clickPosition = e.GetPosition(control);
            draggableControl.CaptureMouse();
        }

        private void Control_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isDragging = false;
            var draggable = sender as Control;
            draggable.ReleaseMouseCapture();
        }

        private void Control_MouseMove(object sender, MouseEventArgs e)
        {
            var draggableControl = sender as Control;

            if (isDragging && draggableControl != null)
            {
                Point currentPosition = e.GetPosition(control.Parent as UIElement);

                var transform = draggableControl.RenderTransform as TranslateTransform;
                if (transform == null)
                {
                    transform = new TranslateTransform();
                    draggableControl.RenderTransform = transform;
                }

                transform.X = currentPosition.X - clickPosition.X;
                transform.Y = currentPosition.Y - clickPosition.Y;

                ControlDragged();
            }
        }
    }
}
