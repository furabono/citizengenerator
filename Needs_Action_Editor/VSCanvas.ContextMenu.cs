﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Needs_Action_Editor
{
    public partial class VSCanvas
    {
        Point mousePos;

		void InitContextMenu()
        {
            var item1 = new MenuItem()
            {
				Header = "Label",
            };
            item1.Click += MenuItem_Click;

            var item2 = new MenuItem()
            {
                Header = "Goto"
            };
            item2.Click += MenuItem_Click;

            var menu = new ContextMenu();
            menu.Items.Add(item1);
            menu.Items.Add(item2);
            menu.Opened += Menu_Opened;

            this.ContextMenu = menu;
        }

        private void Menu_Opened(object sender, RoutedEventArgs e)
        {
            mousePos = Mouse.GetPosition(this);
            System.Diagnostics.Debug.Print(mousePos.ToString());
        }

        void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var item = (MenuItem)sender;
            var header = (string)item.Header;

			if (header == "Label")
            {
                var node = new VSLabelNode();
                node.Canvas = this;
                Children.Add(node);
                node.MoveTo(mousePos);
            }
			else if (header == "Goto")
            {
                var node = new VSGotoNode();
                node.Canvas = this;
                Children.Add(node);
                node.MoveTo(mousePos);
            }
        }
    }
}
