﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor
{
    class Connection
    {
        VSNodePort source;
        VSNodePort destination;
        ConnectionLine line;

        public VSNodePort Source
        {
            get { return source; }
        }

        public VSNodePort Destination
        {
            get { return destination; }
        }

        public ConnectionLine Line
        {
            get { return line; }
        }

        public Connection(VSNodePort source, VSNodePort destination)
        {
            this.source = source;
            this.destination = destination;

            this.line = new ConnectionLine(source.Position, destination.Position);

            //var menu = new System.Windows.Controls.ContextMenu();
            //menu.
        }

        
    }
}
