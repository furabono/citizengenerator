﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Needs_Action_Editor.Controls
{
    /// <summary>
    /// XAML 파일에서 이 사용자 지정 컨트롤을 사용하려면 1a 또는 1b단계를 수행한 다음 2단계를 수행하십시오.
    ///
    /// 1a단계) 현재 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor.Controls"
    ///
    ///
    /// 1b단계) 다른 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor.Controls;assembly=Needs_Action_Editor.Controls"
    ///
    /// 또한 XAML 파일이 있는 프로젝트의 프로젝트 참조를 이 프로젝트에 추가하고
    /// 다시 빌드하여 컴파일 오류를 방지해야 합니다.
    ///
    ///     솔루션 탐색기에서 대상 프로젝트를 마우스 오른쪽 단추로 클릭하고
    ///     [참조 추가]->[프로젝트]를 차례로 클릭한 다음 이 프로젝트를 찾아서 선택합니다.
    ///
    ///
    /// 2단계)
    /// 계속 진행하여 XAML 파일에서 컨트롤을 사용합니다.
    ///
    ///     <MyNamespace:TreeView/>
    ///
    /// </summary>
    public class DirectoryTreeView : System.Windows.Controls.TreeView
    {
        public static readonly DependencyProperty SelectedHItemProperty =
            DependencyProperty.Register("SelectedHItem", typeof(IHierarchicalItem), typeof(DirectoryTreeView));

        static DirectoryTreeView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DirectoryTreeView), new FrameworkPropertyMetadata(typeof(DirectoryTreeView)));
        }

        public IHierarchicalItem SelectedHItem
        {
            get { return (IHierarchicalItem)GetValue(SelectedHItemProperty); }
            set { SetValue(SelectedHItemProperty, value); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.SelectedItemChanged += TreeView_SelectedItemChanged;
            this.MouseDown += TreeView_MouseDown;
            this.MouseMove += TreeView_MouseMove;
            this.Drop += TreeView_Drop;
        }

        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            SelectedHItem = (IHierarchicalItem)e.NewValue;
        }

        Point startPosition;

        void TreeView_MouseDown(object sender, MouseEventArgs e)
        {
            startPosition = e.GetPosition(null);
        }

        void TreeView_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Vector delta = e.GetPosition(null) - startPosition;
                if (Math.Abs(delta.X) < SystemParameters.MinimumHorizontalDragDistance &&
                    Math.Abs(delta.Y) < SystemParameters.MinimumVerticalDragDistance)
                {
                    return;
                }

                if (e.OriginalSource is TextBlock)
                {
                    var dragItem = (e.OriginalSource as TextBlock).DataContext;
                    var dragData = new DataObject("IHierarchicalItem", dragItem);
                    DragDrop.DoDragDrop(this, dragData, DragDropEffects.Move);
                }
                else if (e.OriginalSource is Image)
                {
                    var dragItem = (e.OriginalSource as Image).DataContext;
                    var dragData = new DataObject("IHierarchicalItem", dragItem);
                    DragDrop.DoDragDrop(this, dragData, DragDropEffects.Move);
                }
            }
        }

        void TreeView_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("IHierarchicalItem"))
            {
                var dragItem = e.Data.GetData("IHierarchicalItem") as IHierarchicalItem;
                IParentItem dropItem = (e.OriginalSource as TextBlock)?.DataContext as IParentItem;
                if (e.OriginalSource is TextBlock)
                {
                    dropItem = (e.OriginalSource as TextBlock).DataContext as IParentItem;
                }
                else if (e.OriginalSource is Image)
                {
                    dropItem = (e.OriginalSource as Image).DataContext as IParentItem;
                }
                else if (e.OriginalSource is Grid)
                {
                    var parent = dragItem;
                    while (parent.Parent != null) { parent = parent.Parent; }
                    dropItem = parent as IParentItem;
                }

                if (dropItem == null) { return; }
                if (dragItem == dropItem) { return; }
                if (dragItem.Parent == dropItem) { return; }
                {
                    var parent = dropItem;
                    while (parent.Parent != null)
                    {
                        if (parent.Parent == dragItem) { return; }
                        parent = parent.Parent;
                    }
                }

                if (dragItem is Models.Directory)
                {
                    var dir = dragItem as Models.Directory;

                    System.IO.Directory.Move(
                        Models.Simulator.Instance.WorkingDirectory + dir.Path,
                        Models.Simulator.Instance.WorkingDirectory + (dropItem as Models.Directory).Path + dir.Name
                        );
                }
                else
                {
                    var name = (dragItem as Models.NeedSourceWrapper)?.Name + (dragItem as Models.ActionSourceWrapper)?.Name + ".json";
                    System.IO.File.Move(
                        Models.Simulator.Instance.WorkingDirectory + (dragItem.Parent as Models.Directory).Path + name,
                        Models.Simulator.Instance.WorkingDirectory + (dropItem as Models.Directory).Path + name
                        );
                }

                dragItem.Parent.RemoveChild(dragItem);
                dropItem.AddChild(dragItem);
            }
        }
    }

    public interface IHierarchicalItem
    {
        IParentItem Parent { get; set; }
    }

    public interface IParentItem : IHierarchicalItem
    {
        void AddChild(IHierarchicalItem item);
        bool RemoveChild(IHierarchicalItem item);
    }
}
