﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Needs_Action_Editor.Controls
{
    /// <summary>
    /// XAML 파일에서 이 사용자 지정 컨트롤을 사용하려면 1a 또는 1b단계를 수행한 다음 2단계를 수행하십시오.
    ///
    /// 1a단계) 현재 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor.Controls"
    ///
    ///
    /// 1b단계) 다른 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor.Controls;assembly=Needs_Action_Editor.Controls"
    ///
    /// 또한 XAML 파일이 있는 프로젝트의 프로젝트 참조를 이 프로젝트에 추가하고
    /// 다시 빌드하여 컴파일 오류를 방지해야 합니다.
    ///
    ///     솔루션 탐색기에서 대상 프로젝트를 마우스 오른쪽 단추로 클릭하고
    ///     [참조 추가]->[프로젝트]를 차례로 클릭한 다음 이 프로젝트를 찾아서 선택합니다.
    ///
    ///
    /// 2단계)
    /// 계속 진행하여 XAML 파일에서 컨트롤을 사용합니다.
    ///
    ///     <MyNamespace:PropertyListBox/>
    ///
    /// </summary>
    public class PropertyListBox : ListBox
    {
        public static readonly DependencyProperty IsDraggableProperty
            = DependencyProperty.Register(
                "IsDraggable",
                typeof(bool),
                typeof(PropertyListBox),
                new PropertyMetadata(false));

        public static readonly DependencyProperty IsDroppableProperty
            = DependencyProperty.Register(
                "IsDroppable",
                typeof(bool),
                typeof(PropertyListBox),
                new PropertyMetadata(false));

        static PropertyListBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PropertyListBox), new FrameworkPropertyMetadata(typeof(PropertyListBox)));
        }

        Point _mousePosition;

        public bool IsDraggable
        {
            get { return (bool)GetValue(IsDraggableProperty); }
            set { SetValue(IsDraggableProperty, value); }
        }

        public bool IsDroppable
        {
            get { return (bool)GetValue(IsDroppableProperty); }
            set { SetValue(IsDroppableProperty, value); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            MouseDown += PropertyListBox_MouseDown;
            MouseMove += PropertyListBox_MouseMove;
            DragEnter += PropertyListBox_DragEnter;
            Drop += PropertyListBox_Drop;

            RemovePropertyCommand = new DelegateCommand();
            RemovePropertyCommand.CanExecuteTargets += RemovePropertyCommand_CanExecuteTargets;
            RemovePropertyCommand.ExecuteTargets += RemovePropertyCommand_ExecuteTargets;
        }

        private void PropertyListBox_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _mousePosition = e.GetPosition(null);
        }

        private void PropertyListBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (!IsDraggable) { return; }
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Vector delta = e.GetPosition(null) - _mousePosition;
                if (Math.Abs(delta.X) < SystemParameters.MinimumHorizontalDragDistance &&
                    Math.Abs(delta.Y) < SystemParameters.MinimumVerticalDragDistance)
                {
                    return;
                }

                var property = (e.OriginalSource as FrameworkElement)?.DataContext as Models.Property;
                if (property != null)
                {
                    var data = new DataObject("Property", property);
                    DragDrop.DoDragDrop(this, data, DragDropEffects.Copy);
                }
            }
        }

        private void PropertyListBox_DragEnter(object sender, DragEventArgs e)
        {
            if (!IsDroppable || !e.Data.GetDataPresent("Property"))
            {
                e.Effects = DragDropEffects.None;
            }
        }

        private void PropertyListBox_Drop(object sender, DragEventArgs e)
        {
            if (!IsDroppable) { return; }
            if (e.Data.GetDataPresent("Property"))
            {
                var property = e.Data.GetData("Property") as Models.Property;
                //var properties = ItemsSource as ObservableCollection<Models.Property>;
                var meta = DataContext as Models.AttributeMeta;

                meta.AddProperty(property);
            }
        }

        public DelegateCommand RemovePropertyCommand
        {
            get; private set;
        }

        private bool RemovePropertyCommand_CanExecuteTargets()
        {
            return SelectedItem is Models.Property;
        }

        private void RemovePropertyCommand_ExecuteTargets()
        {
            var meta = (Models.AttributeMeta)DataContext;
            var property = SelectedItem as Models.Property;
            meta.OwnProperties.Remove(property);
        }
    }
}
