﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Needs_Action_Editor
{
    /// <summary>
    /// XAML 파일에서 이 사용자 지정 컨트롤을 사용하려면 1a 또는 1b단계를 수행한 다음 2단계를 수행하십시오.
    ///
    /// 1a단계) 현재 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor"
    ///
    ///
    /// 1b단계) 다른 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor;assembly=Needs_Action_Editor"
    ///
    /// 또한 XAML 파일이 있는 프로젝트의 프로젝트 참조를 이 프로젝트에 추가하고
    /// 다시 빌드하여 컴파일 오류를 방지해야 합니다.
    ///
    ///     솔루션 탐색기에서 대상 프로젝트를 마우스 오른쪽 단추로 클릭하고
    ///     [참조 추가]->[프로젝트]를 차례로 클릭한 다음 이 프로젝트를 찾아서 선택합니다.
    ///
    ///
    /// 2단계)
    /// 계속 진행하여 XAML 파일에서 컨트롤을 사용합니다.
    ///
    ///     <MyNamespace:VSCanvas/>
    ///
    /// </summary>
    public partial class VSCanvas : Canvas
    {
        static VSCanvas()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(VSCanvas), new FrameworkPropertyMetadata(typeof(VSCanvas)));
        }

        BindingPoint mousePoint;
        VSNodePort connectingSourcePort;
        ConnectionLine connectingLine;

        List<Connection> connections;

        public VSCanvas() :base()
        {
            mousePoint = new BindingPoint(0, 0);
            connections = new List<Connection>();

            this.MouseLeftButtonUp += VSCanvas_MouseLeftButtonUp;
            this.MouseMove += VSCanvas_MouseMove;

            InitContextMenu();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }

        public void MouseClickOnPort(VSNodePort port)
        {
            if (connectingLine == null)
            {
                connectingSourcePort = port;
                connectingLine = new ConnectionLine(port.Position, mousePoint);
                Children.Add(connectingLine.Path);
            }
            else
            {
                if (connectingSourcePort.Type != port.Type)
                {
                    var connection = new Connection(connectingSourcePort, port);
                    connections.Add(connection);
                    Children.Add(connection.Line.Path);
                }

                ClearConnecting();
            }
        }

        private void VSCanvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (connectingLine != null)
            {
                ClearConnecting();
            }
        }

        private void VSCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            Point mouse = e.GetPosition(this);
            mousePoint.X = mouse.X + 3;
            mousePoint.Y = mouse.Y + 3;
        }

        void ClearConnecting()
        {
            Children.Remove(connectingLine.Path);
            connectingLine.Clear();
            connectingLine = null;
            connectingSourcePort = null;
        }

        public void RemoveNode(VSNode node)
        {
            for (int i = 0; i < connections.Count; ++i)
            {
                if (connections[i].Source.ParentNode == node ||
                    connections[i].Destination.ParentNode == node)
                {
                    connections[i].Line.Clear();
                    Children.Remove(connections[i].Line.Path);

                    connections.RemoveAt(i);
                    --i;
                }
            }

            Children.Remove(node);
        }
    }
}
