﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows.Data;
using System.ComponentModel;

namespace Needs_Action_Editor.WPF.Converters
{
    public class Vector2Converter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                var str = (string)value;
                var split = str.Split(',');
                return new Models.Vector2(float.Parse(split[0]), float.Parse(split[1]));
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                var vec2 = (Models.Vector2)value;
                return string.Format("{0}, {1}", vec2.x, vec2.y);
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    public class Vector2ToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var vec2 = (Models.Vector2)value;
            return string.Format("{0}, {1}", vec2.x, vec2.y);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = (string)value;
            var split = str.Split(',');
            return new Models.Vector2(float.Parse(split[0]), float.Parse(split[1]));
        }
    }
}
