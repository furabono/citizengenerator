﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.UnityFeatures
{
    public class Random
    {
        static Random _instance;

        public static Random Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Random();
                }

                return _instance;
            }
        }

        MathNet.Numerics.Random.RandomSource _random;

        private Random()
        {
            _random = new MathNet.Numerics.Random.MersenneTwister();
        }

        public static float value
        {
            get
            {
                return (float)Instance._random.NextDouble();
            }
        }

        public static void InitState(int seed)
        {
            Instance._random = new MathNet.Numerics.Random.MersenneTwister(seed);
        }

        public static float Range(float min, float max)
        {
            return value * (max - min) + min;
        }

        public static int Range(int min, int max)
        {
            return Instance._random.Next(min, max);
        }
    }
}
