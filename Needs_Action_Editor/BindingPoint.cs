﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;

namespace Needs_Action_Editor
{
    public class BindingPoint : INotifyPropertyChanged
    {
        private Point point;

        public BindingPoint(double x, double y)
        {
            point = new Point(x, y);
        }

        public double X
        {
            get { return point.X;}
            set
            {
                point.X = value;
                OnPropertyChanged("Point");
            }
        }

        public double Y
        {
            get { return point.Y; }
            set
            {
                point.Y = value;
                OnPropertyChanged("Point");
            }
        }

        public Point Point
        {
            get { return point; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
