﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Needs_Action_Editor
{
    public class DelegateCommand : ICommand
    {
        public event Action ExecuteTargets;
        public event Func<bool> CanExecuteTargets;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            Delegate[] targets = CanExecuteTargets.GetInvocationList();

            foreach (Func<bool> target in targets)
            {
                if (target())
                {
                    return true;
                }
            }

            return false;
        }

        public void Execute(object parameter)
        {
            if (CanExecute(parameter))
            {
                ExecuteTargets();
            }
        }
    }

    public class DelegateCommand<T> : ICommand
    {
        public event Action<T> ExecuteTargets;
        public event Func<bool> CanExecuteTargets;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            Delegate[] targets = CanExecuteTargets.GetInvocationList();

            foreach (Func<bool> target in targets)
            {
                if (target())
                {
                    return true;
                }
            }

            return false;
        }

        public void Execute(object parameter)
        {
            if (CanExecute(parameter))
            {
                ExecuteTargets((T)parameter);
            }
        }
    }
}
