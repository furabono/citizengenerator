﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

using Random = UnityEngine.Random;

namespace BaseLib.Classes
{
    public class Citizen : CitizenBase
    {
        NavMeshAgent _agent;
        SpriteRenderer _actionImage;

        //protected int _id;
        Need[] _needs;
        Need _currentNeed;


        // __PARAMETERS__

        // __CHARACTERISTICS__

        Vector3 _position;
        public Vector3 Position
        {
            get
            {
                var pos = transform.position;
                _position.x = pos.x;
                _position.y = pos.z;
                _position.z = pos.y;
                return _position;
            }
        }

        public SpriteRenderer ActionImage
        {
            get { return _actionImage; }
        }

        public override void InitCharacteristics(Dictionary<string, int> nameToIndex, int[] values)
        {
            // __INITIALIZE_CHARACTERISTICS__
        }

        void Start()
        {
            _agent = GetComponent<NavMeshAgent>();
            _agent.Warp(new Vector3(/* __START_POSITION__ */));
            
            _actionImage = transform.Find("ActionBubble").transform.Find("ActionImage").GetComponent<SpriteRenderer>();
            _position = new Vector3();

            // __INITIALIZE_PARAMETERS__

             _needs = new Need[]
            {
                // __INITIALIZE_NEEDS__
            };
        }

        public override void ManualUpdate()
        {
            Metabolism();

            _currentNeed?.Update();

            for (int i = 0; i < _needs.Length; ++i)
            {
                if (_needs[i].UpdateActivation())
                {
                    _needs[i].UpdatePriority();
                }
            }

            Need tmp;
            for (int i = _needs.Length - 1; i > 0; --i)
            {
                if (_needs[i].Priority > _needs[i - 1].Priority)
                {
                    tmp = _needs[i];
                    _needs[i] = _needs[i - 1];
                    _needs[i - 1] = tmp;
                }
            }

            if (_currentNeed != _needs[0])
            {
                _currentNeed = _needs[0];
                _currentNeed.Start();
            }
        }

        void Metabolism()
        {
            // __METABOLISM__
        }

        public void MoveTo(float x, float y)
        {
            _agent.destination = new Vector3(x, 0f, y);
        }
    }
}
