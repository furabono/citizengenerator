﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Mathf = UnityEngine.Mathf;
using Random = UnityEngine.Random;

namespace BaseLib.Classes
{
    public class N__NAME__ : Need
    {
        public N__NAME__(Citizen citizen)
        {
            _citizen = citizen;

            _actions = new ActionMeta[]
            {
                // __INITIALIZE_ACTIONMETAS__
            };
        }

        public override bool CalcActivation()
        {
            // __ACTIVATION__
            // return
        }

        public override float CalcPriority()
        {
            // __PRIORITY__
            // return
        }
    }
}
