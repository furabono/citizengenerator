﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Needs_Action_Editor.Exporter
{
    using CitizenMetaData = Models.CitizenMetaData;
    using ActionSource = Classes.ActionSource;
    using NeedSource = Classes.NeedSource;
    using World = Models.World;

    public class Exporter
    {
        public static Task<CompilerResults> ExportAssemblyAsync(string path, CitizenMetaData citizenMetaData, ActionSource[] actionSources, NeedSource[] needSources, World world, string characteristicPath)
        {
            return Task.Run(() => ExportAssembly(path, citizenMetaData, actionSources, needSources, world, characteristicPath));
        }

        public static CompilerResults ExportAssembly(string path, CitizenMetaData citizenMetaData, ActionSource[] actionSources, NeedSource[] needSources, World world, string characteristicPath)
        {
            if (!Directory.Exists("Debug")) { Directory.CreateDirectory("Debug"); }
            foreach (var file in Directory.GetFiles("Debug"))
            {
                File.Delete(file);
            }
            foreach (var dir in Directory.GetDirectories("Debug"))
            {
                Directory.Delete(dir, true);
            }

            var template = new StringBuilder();
            List<string> sources = new List<string>(1 + (actionSources.Length * 2) + needSources.Length + 1);

            var src = Directory.GetFiles("src");
            List<string> files = new List<string>(sources.Capacity + src.Length);
            files.AddRange(src);
            string filename;

            Assembly assembly = Assembly.GetExecutingAssembly();

            Characteristic[] characteristics = ReadCharacteristicSchemas(characteristicPath);

            using (Stream stream = assembly.GetManifestResourceStream("Needs_Action_Editor.Exporter.Characteristics.cs"))
            using (var reader = new StreamReader(stream))
            {
                template.Append(reader.ReadToEnd());
                reader.Close();
                var characteristicsSource = GenerateCharacteristicsSource(template, characteristics);
                template.Clear();
                sources.Add(characteristicsSource);

                filename = @"Debug\Characteristics.cs";
                File.WriteAllText(filename, characteristicsSource);
                files.Add(filename);
            }

            using (Stream stream = assembly.GetManifestResourceStream("Needs_Action_Editor.Exporter.Citizen.cs"))
            using (var reader = new StreamReader(stream))
            {
                template.Append(reader.ReadToEnd());
                reader.Close();
                var citizenSource = GenerateCitizenSource(template, citizenMetaData, needSources, world, characteristics);
                template.Clear();
                sources.Add(citizenSource);

                filename = @"Debug\Citizen.cs";
                File.WriteAllText(filename, citizenSource);
                files.Add(filename);
            }

            Directory.CreateDirectory(@"Debug\ActionMeta");
            using (Stream stream = assembly.GetManifestResourceStream("Needs_Action_Editor.Exporter.AMDummy.cs"))
            using (var reader = new StreamReader(stream))
            {
                string t = reader.ReadToEnd();
                reader.Close();

                for (int i = 0; i < actionSources.Length; ++i)
                {
                    template.Append(t);
                    sources.Add(GenerateActionMetaSource(template, actionSources[i], citizenMetaData, world));
                    template.Clear();

                    filename = string.Format(@"Debug\ActionMeta\AM{0}.cs", actionSources[i].Name);
                    File.WriteAllText(filename, sources.Last());
                    files.Add(filename);
                }
            }

            Directory.CreateDirectory(@"Debug\Action");
            using (Stream stream = assembly.GetManifestResourceStream("Needs_Action_Editor.Exporter.ADummy.cs"))
            using (var reader = new StreamReader(stream))
            {
                string t = reader.ReadToEnd();
                reader.Close();

                for (int i = 0; i < actionSources.Length; ++i)
                {
                    template.Append(t);
                    sources.Add(GenerateActionSource(template, actionSources[i], citizenMetaData, world));
                    template.Clear();

                    filename = string.Format(@"Debug\Action\A{0}.cs", actionSources[i].Name);
                    File.WriteAllText(filename, sources.Last());
                    files.Add(filename);
                }
            }

            Directory.CreateDirectory(@"Debug\Need");
            using (Stream stream = assembly.GetManifestResourceStream("Needs_Action_Editor.Exporter.NDummy.cs"))
            using (var reader = new StreamReader(stream))
            {
                string t = reader.ReadToEnd();
                reader.Close();

                for (int i = 0; i < needSources.Length; ++i)
                {
                    template.Append(t);
                    sources.Add(GenerateNeedSource(template, needSources[i], citizenMetaData, world));
                    template.Clear();

                    filename = string.Format(@"Debug\Need\N{0}.cs", needSources[i].Name);
                    File.WriteAllText(filename, sources.Last());
                    files.Add(filename);
                }
            }

            using (Stream stream = assembly.GetManifestResourceStream("Needs_Action_Editor.Exporter.World.cs"))
            using (var reader = new StreamReader(stream))
            {
                template.Append(reader.ReadToEnd());
                reader.Close();
                var worldSource = GenerateWorldSource(template, world);
                template.Clear();
                sources.Add(worldSource);

                filename = @"Debug\World.cs";
                File.WriteAllText(filename, worldSource);
                files.Add(filename);
            }

            

            var options = new CompilerParameters();
            options.ReferencedAssemblies.Add("System.dll");
            options.ReferencedAssemblies.Add("System.Core.dll");
            options.ReferencedAssemblies.Add("System.Linq.dll");
            options.ReferencedAssemblies.Add("UnityEngine.dll");
            options.ReferencedAssemblies.Add("BaseLib.dll");
            options.GenerateExecutable = false;
            options.GenerateInMemory = false;
            options.IncludeDebugInformation = false;
            options.CompilerOptions = "/optimize";
            options.OutputAssembly = path;
            //var compiler = new CSharpCodeProvider();
            var compiler = new Microsoft.CodeDom.Providers.DotNetCompilerPlatform.CSharpCodeProvider();
            //var result = compiler.CompileAssemblyFromSource(options, sources.ToArray());
            var result = compiler.CompileAssemblyFromFile(options, files.ToArray());

            using (var writer = new StreamWriter(@"Debug\error.txt"))
            {
                foreach (CompilerError err in result.Errors)
                {
                    writer.WriteLine(err.ToString());
                }
            }

            return result;
        }

        static Characteristic[] ReadCharacteristicSchemas(string characteristicPath)
        {
            if (!File.Exists(characteristicPath)) { return null; }

            JObject jobject;

            using (StreamReader sr = File.OpenText(characteristicPath))
            using (JsonTextReader reader = new JsonTextReader(sr))
            {
                jobject = JObject.Load(reader);
            }

            var list = new List<Characteristic>();
            string type;
            var queue = new Queue<JObject>();
            queue.Enqueue((JObject)jobject["Root"]);
            while (queue.Any())
            {
                jobject = queue.Dequeue();

                type = jobject["Type"].Value<string>();
                if (type == "Category")
                {
                    foreach (JObject child in jobject["Children"])
                    {
                        queue.Enqueue(child);
                    }
                }
                else if (type == "Qualitative")
                {
                    list.Add(new Qualitative(
                        jobject["Name"].Value<string>(),
                        (from catetory in jobject["Categories"] select catetory.Value<string>()).ToArray())
                        );
                }
                else if (type == "Quantitative")
                {
                    list.Add(new Quantative(
                        jobject["Name"].Value<string>(),
                        jobject["Min"].Value<int>(),
                        jobject["Max"].Value<int>())
                        );
                }
                else
                {
                    // error
                }
            }

            return list.ToArray();
        }

        static string GenerateCharacteristicsSource(StringBuilder characteristicsTemplate, Characteristic[] characteristics)
        {
            if (characteristics == null) { return characteristicsTemplate.ToString(); }

            StringBuilder sb = new StringBuilder();
            Qualitative q;
            foreach (Characteristic c in characteristics)
            {
                if ((q = c as Qualitative) != null)
                {
                    sb.Append(GenerateQualitativeEnum(q));
                    sb.Append("\n    ");
                }
            }

            characteristicsTemplate.Replace("// __CHARACTERISTICS__", sb.ToString());
            return characteristicsTemplate.ToString();
        }

        static string GenerateQualitativeEnum(Qualitative qualitative)
        {
            StringBuilder enumTemplate = new StringBuilder("public enum {0}\n    {        {1}\n    }\n");

            StringBuilder sb = new StringBuilder();
            foreach (string category in qualitative.Categories)
            {
                sb.AppendFormat("{0}, ", category);
            }
            sb.Remove(sb.Length - 2, 2);

            enumTemplate.Replace("{0}", qualitative.InnerName);
            enumTemplate.Replace("{1}", sb.ToString());

            return enumTemplate.ToString();
        }

        static string GenerateCitizenSource(StringBuilder citizenTemplate, CitizenMetaData citizenMetaData, NeedSource[] needSources, World world, Characteristic[] characteristics)
        {
            var sb = new StringBuilder();

            for (int i = 0; i < citizenMetaData.Parameters.Count; ++i)
            {
                sb.AppendFormat("public float {0};\n", citizenMetaData.Parameters[i].Name);
                sb.Append("        ");
            }
            citizenTemplate.Replace("// __PARAMETERS__", sb.ToString());
            sb.Clear();

            if (characteristics != null)
            {
                foreach (Characteristic c in characteristics)
                {
                    if (c is Qualitative)
                    {
                        sb.AppendFormat("public Characteristics.{0} {0};\n", c.InnerName);
                    }
                    else
                    {
                        sb.AppendFormat("public float {0};\n", c.InnerName);
                    }
                    sb.Append("        ");
                }
                citizenTemplate.Replace("// __CHARACTERISTICS__", sb.ToString());
                sb.Clear();

                foreach (Characteristic c in characteristics)
                {
                    sb.AppendFormat("this.{0} = values[nameToIndex[{1}]];\n", c.InnerName, c.Name);
                    sb.Append("            ");
                }
                citizenTemplate.Replace("// __INITIALIZE_CHARACTERISTICS__", sb.ToString());
                sb.Clear();
            }

            citizenTemplate.Replace(
                "/* __START_POSITION__ */", 
                string.Format("{0}, {1}, {2}", citizenMetaData.StartPosition.x, 0, citizenMetaData.StartPosition.y));

            for (int i = 0; i < citizenMetaData.Parameters.Count; ++i)
            {
                sb.AppendFormat("this.{0} = {1};\n", citizenMetaData.Parameters[i].Name, citizenMetaData.Parameters[i].InitialValue);
                sb.Append("            ");
            }
            citizenTemplate.Replace("// __INITIALIZE_PARAMETERS__", sb.ToString());
            sb.Clear();

            for (int i = 0; i < needSources.Length - 1; ++i)
            {
                sb.AppendFormat("new N{0}(this),\n", needSources[i].Name);
                sb.Append("                ");
            }
            sb.AppendFormat("new N{0}(this)", needSources.Last().Name);
            citizenTemplate.Replace("// __INITIALIZE_NEEDS__", sb.ToString());
            sb.Clear();

            string metabolism = ReplaceCitizenParameter(citizenMetaData.MetabolismScript ?? "", "this", citizenMetaData);
            metabolism = ReplaceWorldParameter(metabolism, world);
            citizenTemplate.Replace("// __METABOLISM__", metabolism);

            return citizenTemplate.ToString();
        }

        static string GenerateActionMetaSource(StringBuilder actionMetaTemplate, ActionSource actionSource, CitizenMetaData citizenMetaData, World world)
        {
            actionMetaTemplate.Replace("__NAME__", actionSource.Name);

            string efficiency = actionSource.Efficiency.Code;
            efficiency = ReplaceCitizenParameter(efficiency, "citizen", citizenMetaData);
            efficiency = ReplaceWorldParameter(efficiency, world);
            if (actionSource.Efficiency.IsExpression)
            {
                efficiency = string.Format("return {0};", efficiency);
            }
            actionMetaTemplate.Replace("// __EFFICIENCY__", efficiency);

            return actionMetaTemplate.ToString();
        }

        static string GenerateActionSource(StringBuilder actionTemplate, ActionSource actionSource, CitizenMetaData citizenMetaData, World world)
        {
            var sb = new StringBuilder();

            actionTemplate.Replace("__NAME__", actionSource.Name);

            if (actionSource.Parameters.Any())
            {
                foreach (var parameter in actionSource.Parameters)
                {
                    sb.AppendFormat(", {0} {1}", parameter.Type, parameter.Name);
                }
                actionTemplate.Replace("/* __PARAMETERS_0__ */", sb.ToString());
                sb.Clear();

                foreach (var parameter in actionSource.Parameters)
                {
                    sb.AppendFormat("{0}, ", parameter.Name);
                }
                sb.Remove(sb.Length - 2, 2);
                actionTemplate.Replace("/* __PARAMETERS_1__ */", sb.ToString());
                sb.Clear();

                foreach (var parameter in actionSource.Parameters)
                {
                    sb.AppendFormat("{0} {1} = {2}, ", parameter.Type, parameter.Name, parameter.InitValue.Value);
                }
                sb.Remove(sb.Length - 2, 2);
                actionTemplate.Replace("/* __PARAMETERS_2__ */", sb.ToString());
                sb.Clear();

                foreach (var parameter in actionSource.Parameters)
                {
                    sb.AppendFormat("{0} {1};\n", parameter.Type, parameter.Name);
                    sb.Append("        ");
                }
                actionTemplate.Replace("// __PARAMETERS__", sb.ToString());
                sb.Clear();

                foreach (var parameter in actionSource.Parameters)
                {
                    sb.AppendFormat("this.{0} = {0};\n", parameter.Name);
                    sb.Append("            ");
                }
                actionTemplate.Replace("// __INITIALIZE_PARAMETERS__", sb.ToString());
                sb.Clear();

#if USE_DEFAULT
                foreach (var parameter in actionSource.Parameters)
                {
                    sb.AppendFormat("this.{0} = {1};\n", parameter.Name, parameter.InitialValue);
                    sb.Append("            ");
                }
                actionTemplate.Replace("/* __DEFAULT_INIT__", "");
                actionTemplate.Replace("__DEFAULT_INIT__ */", "");
                actionTemplate.Replace("// __INITIALIZE_PARAMETERS_DEFAULT__", sb.ToString());
                sb.Clear();
#endif

                foreach (var parameter in actionSource.Parameters)
                {
                    if (parameter.Type == "string")
                    {
                        sb.AppendFormat("this.{0} = null;\n", parameter.Name);
                        sb.Append("            ");
                    }
                }
                actionTemplate.Replace("// __SET_NULL_REFERENCES__", sb.ToString());
                sb.Clear();
            }

            string plan = actionSource.Plan.Code;
            plan = ReplaceCitizenParameter(plan, "_citizen", citizenMetaData);
            plan = ReplaceWorldParameter(plan, world);

            // Work(); => _pc = 1; return this
            plan = ReplaceOutOfQuotesWithRegex(plan, @"([^_a-zA-Z0-9]|^)Work\s*\(\s*\)\s*;", (match) =>
            {
                return match.Groups[1].Value + "{ _pc = 1; return this; }";
            });

            // (0)StartAction("(1)", => (0)return A(1).Start(this,
            /*
            plan = ReplaceOutOfQuotesWithRegex(
                plan,
                @"([^_a-zA-Z0-9]|^)StartAction\s*\(\s*" + "\"(.*)\"" + @"\s*[,)]",
                (match) =>
                {
                    return string.Format("{0}return A{1}.Start(this,", match.Groups[1].Value, match.Groups[2].Value);
                });
            */
            plan = Regex.Replace(plan, @"([^_a-zA-Z0-9]|^)StartAction\s*\(\s*" + "\"(.*)\"" + @"\s*([,)])",
                (match) =>
                {
                    return string.Format("{0}return A{1}.Start(this{2}", match.Groups[1].Value, match.Groups[2].Value, match.Groups[3].Value);
                });

            // Need.Activation (Optimization Required)
            plan = ReplaceOutOfQuotesWithRegex(plan, @"([^_a-zA-Z0-9]|^)Need.Activation([^_a-zA-Z0-9]|$)", (match) =>
            {
                return string.Format("{0}_need.UpdateActivation(){1}", match.Groups[1].Value, match.Groups[2].Value);
            });

            actionTemplate.Replace("// __PLAN__", plan);

            string work = actionSource.Work.Code;
            work = ReplaceCitizenParameter(work, "_citizen", citizenMetaData);
            work = ReplaceWorldParameter(work, world);

            // Finish(); => Release(); return Parent;
            work = ReplaceOutOfQuotesWithRegex(work, @"([^_a-zA-Z0-9]|^)Finish\s*\(\s*\)\s*;", (match) =>
            {
                return match.Groups[1].Value + "{ Release(); return Parent; }";
            });

            // Plan(); => _pc = 0; return this;
            work = ReplaceOutOfQuotesWithRegex(work, @"([^_a-zA-Z0-9]|^)Plan\s*\(\s*\)\s*;", (match) =>
            {
                return match.Groups[1].Value + "{ _pc = 0; return this; }";
            });

            // Need.Activation (Optimization Required)
            work = ReplaceOutOfQuotesWithRegex(work, @"([^_a-zA-Z0-9]|^)Need.Activation([^_a-zA-Z0-9]|$)", (match) =>
            {
                return string.Format("{0}_need.UpdateActivation(){1}", match.Groups[1].Value, match.Groups[2].Value);
            });

            actionTemplate.Replace("// __WORK__", work);

            return actionTemplate.ToString();
        }

        static string GenerateNeedSource(StringBuilder needTemplate, NeedSource needSource, CitizenMetaData citizenMetaData, World world)
        {
            needTemplate.Replace("__NAME__", needSource.Name);

            var sb = new StringBuilder();
            foreach (var action in needSource.ActionInfos)
            {
                sb.AppendFormat("AM{0}.Instance,\n", action);
                sb.Append("                ");
            }
            sb.Remove(sb.Length - 18, 18);
            needTemplate.Replace("// __INITIALIZE_ACTIONMETAS__", sb.ToString());
            sb.Clear();

            string activation = needSource.Activation.Code;
            activation = ReplaceCitizenParameter(activation, "_citizen", citizenMetaData);
            activation = ReplaceWorldParameter(activation, world);
            if (needSource.Activation.IsExpression)
            {
                activation = string.Format("return {0};", activation); ;
            }
            needTemplate.Replace("// __ACTIVATION__", activation);

            string priority = needSource.Priority.Code;
            priority = ReplaceCitizenParameter(priority, "_citizen", citizenMetaData);
            priority = ReplaceWorldParameter(priority, world);
            if (needSource.Priority.IsExpression)
            {
                priority = string.Format("return {0};", priority);
            }
            needTemplate.Replace("// __PRIORITY__", priority);

            return needTemplate.ToString();
        }

        static string GenerateWorldSource(StringBuilder worldTemplate, World world)
        {
            var sb = new StringBuilder();

            foreach (var variable in world.Variables)
            {
                sb.AppendFormat("public {0} {1};\n", variable.Type, variable.Name);
                sb.Append("        ");
            }
            worldTemplate.Replace("// __WORLD_VARIABLES__", sb.ToString());
            sb.Clear();

            foreach (var variable in world.Variables)
            {
                sb.AppendFormat("this.{0} = {1};\n", variable.Name, variable.Value.Value);
                sb.Append("            ");
            }
            worldTemplate.Replace("// __INITIALIZE_VARIABLES__", sb.ToString());
            sb.Clear();

            string update = world.Script ?? "";
            update = ReplaceOutOfQuotesWithRegex(
                update,
                @"([^_a-zA-Z0-9]|^)World.([_a-zA-Z]+[_a-zA-Z0-9]*)",
                (match)=>
                {
                    return string.Format("{0}this.{1}", match.Groups[1], match.Groups[2]);
                });
            worldTemplate.Replace("// __WORLD_UPDATE__", update);

            return worldTemplate.ToString();
        }

        static string ReplaceCitizenParameter(string script, string citizenPath, CitizenMetaData citizenMetaData)
        {
            return ReplaceOutOfQuotes(script, (str) =>
            {
                return Regex.Replace(str, @"([^_a-zA-Z0-9]|^)@([_a-zA-Z]+[_a-zA-Z0-9]*)", (match) =>
                {
                    return string.Format("{0}{1}.{2}", match.Groups[1], citizenPath, match.Groups[2]);
                });
            });
        }

        static string ReplaceWorldParameter(string script, World world)
        {
            return ReplaceOutOfQuotes(script, (str) =>
            {
                return Regex.Replace(str, @"([^_a-zA-Z0-9]|^)World.([_a-zA-Z]+[_a-zA-Z0-9]*)", (match) =>
                {
                    return string.Format("{0}World.Instance.{1}", match.Groups[1], match.Groups[2]);
                });
            });
        }

        static string ReplaceOutOfQuotesWithRegex(string script, string pattern, MatchEvaluator evaluator)
        {
            return ReplaceOutOfQuotes(script, (str) =>
            {
                return Regex.Replace(str, pattern, evaluator);
            });
        }

        static string ReplaceOutOfQuotesWithRegex(string script, string pattern, string replacement)
        {
            return ReplaceOutOfQuotes(script, (str) =>
            {
                return Regex.Replace(str, pattern, replacement);
            });
        }

        static string ReplaceOutOfQuotes(string script, Func<string, string> replace)
        {
            var replaced = new StringBuilder();

            int i;
            for (i = 0; i < script.Length; ++i)
            {
                if (script[i] == '"') { break; }
            }
            replaced.Append(replace(script.Substring(0, i)));

            int startIndex = i;
            while(i < script.Length)
            {
                while (script[++i] != '"') ;
                replaced.Append(script.Substring(startIndex, i - startIndex + 1));

                startIndex = i + 1;
                while (++i < script.Length && script[i] != '"') ;
                replaced.Append(replace(script.Substring(startIndex, i - startIndex)));

                startIndex = i;
            }

            return replaced.ToString();
        }
    }
}
