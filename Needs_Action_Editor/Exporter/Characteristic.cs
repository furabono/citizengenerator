﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Exporter
{
    public abstract class Characteristic
    {
        protected string _name;
        protected string _innerName;
        public string Name
        {
            get { return _name; }
        }
        public string InnerName
        {
            get { return _innerName; }
        }

        protected Characteristic(string name)
        {
            _name = name;
            _innerName = name.Replace(" ", "_");
        }
    }

    public class Qualitative : Characteristic
    {
        string[] _categories;
        public string[] Categories
        {
            get { return _categories; }
        }

        public Qualitative(string name, string[] categories) : base(name)
        {
            _categories = categories;   
        }
    }

    public class Quantative : Characteristic
    {
        int _min;
        int _max;

        public Quantative(string name, int min, int max) : base(name)
        {
            _min = min;
            _max = max;
        }
    }
}
