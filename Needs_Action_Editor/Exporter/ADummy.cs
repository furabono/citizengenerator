﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Mathf = UnityEngine.Mathf;
using Random = UnityEngine.Random;

namespace BaseLib.Classes
{
    public class A__NAME__ : Action
    {
        static LinkedList<A__NAME__> _pool;

        static A__NAME__()
        {
            _pool = new LinkedList<A__NAME__>();
        }

        public static A__NAME__ Start(Action parent/* __PARAMETERS_0__ */)
        {
            A__NAME__ obj;

            if (_pool.Any())
            {
                obj = _pool.First.Value;
                _pool.RemoveFirst();
            }
            else
            {
                obj = new A__NAME__();
            }

            obj.Parent = parent;
            obj._citizen = parent.Citizen;
            obj._need = parent.Need;
            obj.Init(/* __PARAMETERS_1__ */);

            obj._citizen.ActionImage.sprite = obj.Meta.Image;

            return obj;
        }

        public static A__NAME__ Start(Need need)
        {
            A__NAME__ obj;

            if (_pool.Any())
            {
                obj = _pool.First.Value;
                _pool.RemoveFirst();
            }
            else
            {
                obj = new A__NAME__();
            }

            obj.Parent = null;
            obj._citizen = need.Citizen;
            obj._need = need;
            obj.Init();

            obj._citizen.ActionImage.sprite = obj.Meta.Image;

            return obj;
        }

        public override ActionMeta Meta
        {
            get { return AM__NAME__.Instance; }
        }

        int _pc;

        // __PARAMETERS__

        // __VARIABLES__

        void Init(/* __PARAMETERS_2__ */)
        {
            _pc = 0;

            // __INITIALIZE_PARAMETERS__

            // __INITIALIZE_VARIABLES__
        }

        /* __DEFAULT_INIT__
        void Init()
        {
            _pc = 0;

            // __INITIALIZE_PARAMETERS_DEFAULT__
        }
        __DEFAULT_INIT__ */

        public override void Release()
        {
            _citizen = null;
            Parent = null;

            // set null reference type parameters, variables
            // __SET_NULL_REFERENCES__

            _pool.AddLast(this);
        }

        public override Action Do()
        {
            if (_pc == 0)
            {
                // __PLAN__
                // return StartAction
                // pc = 1, return this
                Release();
                return Parent;
            }
            else
            {
                // __WORK__
                // return Parent
                return this;
            }
        }
    }
}
