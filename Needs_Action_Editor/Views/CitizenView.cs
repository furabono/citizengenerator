﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Needs_Action_Editor.Views
{
    /// <summary>
    /// XAML 파일에서 이 사용자 지정 컨트롤을 사용하려면 1a 또는 1b단계를 수행한 다음 2단계를 수행하십시오.
    ///
    /// 1a단계) 현재 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor.Views"
    ///
    ///
    /// 1b단계) 다른 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor.Views;assembly=Needs_Action_Editor.Views"
    ///
    /// 또한 XAML 파일이 있는 프로젝트의 프로젝트 참조를 이 프로젝트에 추가하고
    /// 다시 빌드하여 컴파일 오류를 방지해야 합니다.
    ///
    ///     솔루션 탐색기에서 대상 프로젝트를 마우스 오른쪽 단추로 클릭하고
    ///     [참조 추가]->[프로젝트]를 차례로 클릭한 다음 이 프로젝트를 찾아서 선택합니다.
    ///
    ///
    /// 2단계)
    /// 계속 진행하여 XAML 파일에서 컨트롤을 사용합니다.
    ///
    ///     <MyNamespace:CitizenView/>
    ///
    /// </summary>
    public class CitizenView : Control
    {
        static CitizenView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CitizenView), new FrameworkPropertyMetadata(typeof(CitizenView)));
        }

        Xceed.Wpf.DataGrid.DataGridControl datagrid;
        DataGrid xDataGrid;

        DelegateCommand newCommand;
        public ICommand NewCommand
        {
            get { return newCommand; }
        }

        Xceed.Wpf.DataGrid.CellEditor xItemsCellEditor;
        DataTemplate xItemsCellTemplate;
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            newCommand = new DelegateCommand();
            newCommand.CanExecuteTargets += New_CanExecute;
            newCommand.ExecuteTargets += New_Execute;

            Models.Simulator.Instance.CitizenMetaDataPropertyChanged += CitizenMetaData_PropertyChanged;
            Models.Simulator.Instance.OnTick += Tick;
            Models.Simulator.Instance.OnPause += Pause;
            Models.Simulator.Instance.OnResume += Resume;

            datagrid = GetTemplateChild("_DataGrid") as Xceed.Wpf.DataGrid.DataGridControl;
            datagrid.ItemsSource = Models.Simulator.Instance.Citizens;
            datagrid.KeyDown += Datagrid_KeyDown;
            //datagrid.LayoutUpdated += Datagrid_LayoutUpdated;
            //xItemsCellEditor = (Xceed.Wpf.DataGrid.CellEditor)datagrid.FindResource("xItemsCellEditor");

            xDataGrid = GetTemplateChild("xDataGrid") as DataGrid;
            xDataGrid.ItemsSource = Models.Simulator.Instance.Citizens;

            xItemsCellTemplate = xDataGrid.FindResource("xItemsCellTemplate") as DataTemplate;

            Unloaded += CitizenView_Unloaded;

            UpdateColumns();
            InitContextMenu();
        }

        bool _columnUpdated;
        private void Datagrid_LayoutUpdated(object sender, EventArgs e)
        {
            if (!_columnUpdated) { return; }

            foreach (var col in datagrid.Columns)
            {
                col.Width = col.GetFittedWidth();
            }

            _columnUpdated = false;
        }
        
        private void Datagrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                if (datagrid.SelectedIndex >= 0)
                {
                    Models.Simulator.Instance.Citizens.RemoveAt(datagrid.SelectedIndex);
                }
            }
        }

        private void CitizenView_Unloaded(object sender, RoutedEventArgs e)
        {
            Models.Simulator.Instance.OnTick -= Tick;
            Models.Simulator.Instance.OnPause -= Pause;
            Models.Simulator.Instance.OnResume -= Resume;
        }

        bool New_CanExecute()
        {
            return true;
        }

        void New_Execute()
        {
            Models.Simulator.Instance.GenerateCitizen();
        }

        private void CitizenMetaData_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Parameters")
            {
                UpdateColumns();
            }
        }

        void UpdateColumns()
        {
            //var itemscol = datagrid.Columns.Last();
            datagrid.Columns.Clear();
            xDataGrid.Columns.Clear();

            var meta = Models.Simulator.Instance.CitizenMetaData;

            {
                var col = new DataGridTextColumn();
                col.Header = "ID";
                col.IsReadOnly = true;

                var binding = new Binding();
                binding.Path = new PropertyPath("ID");
                col.Binding = binding;

                xDataGrid.Columns.Add(col);
            }

            {
                foreach (var param in meta.Parameters)
                {
                    var col = new DataGridTextColumn();
                    col.Header = param.Name;

                    var binding = new Binding();
                    binding.Path = new PropertyPath("Parameters[(0)]", meta.ParameterIndexTable[param]);
                    binding.Mode = BindingMode.TwoWay;
                    col.Binding = binding;

                    xDataGrid.Columns.Add(col);
                }
            }

            {
                var col = new DataGridTextColumn();
                col.Header = "Pos.x";
                //col.IsReadOnly = true;

                var binding = new Binding();
                binding.Path = new PropertyPath("Position.x");
                col.Binding = binding;

                xDataGrid.Columns.Add(col);
            }

            {
                var col = new DataGridTextColumn();
                col.Header = "Pos.y";
                //col.IsReadOnly = true;

                var binding = new Binding();
                binding.Path = new PropertyPath("Position.y");
                col.Binding = binding;

                xDataGrid.Columns.Add(col);
            }

            {
                var col = new DataGridTextColumn();
                col.Header = "Need";
                col.IsReadOnly = true;

                var binding = new Binding();
                binding.Path = new PropertyPath("PriorNeed.Name");
                col.Binding = binding;

                xDataGrid.Columns.Add(col);
            }

            {
                var col = new DataGridTextColumn();
                col.Header = "Action";
                col.IsReadOnly = true;

                var binding = new Binding();
                binding.Path = new PropertyPath("Acting.Name");
                col.Binding = binding;

                xDataGrid.Columns.Add(col);
            }

            {
                var col = new DataGridTemplateColumn();
                col.Header = "Items";
                col.IsReadOnly = true;

                //var binding = new Binding();
                //binding.Path = new PropertyPath("Items");

                col.CellTemplate = xItemsCellTemplate;
                col.MinWidth = 150;
                col.Width = 150;

                xDataGrid.Columns.Add(col);
            }

            {
                var col = new Xceed.Wpf.DataGrid.Column();
                col.FieldName = "ID";
                col.Title = "ID";
                col.ReadOnly = true;
                col.DisplayMemberBindingInfo = new Xceed.Wpf.DataGrid.DataGridBindingInfo();
                col.DisplayMemberBindingInfo.Path = new PropertyPath("ID");
                col.DisplayMemberBindingInfo.ReadOnly = true;
                datagrid.Columns.Add(col);
            }

            foreach (var param in meta.Parameters)
            {
                var col = new Xceed.Wpf.DataGrid.Column();
                col.FieldName = param.Name;
                col.Title = param.Name;
                col.DisplayMemberBindingInfo = new Xceed.Wpf.DataGrid.DataGridBindingInfo();
                col.DisplayMemberBindingInfo.Path = new PropertyPath("Parameters[(0)]", meta.ParameterIndexTable[param]);
                datagrid.Columns.Add(col);
            }

            {
                var col = new Xceed.Wpf.DataGrid.Column();
                col.FieldName = "Pos.x";
                col.Title = "Pos.x";
                col.ReadOnly = false;
                col.DisplayMemberBindingInfo = new Xceed.Wpf.DataGrid.DataGridBindingInfo();
                col.DisplayMemberBindingInfo.Path = new PropertyPath("Position.x");
                //col.DisplayMemberBindingInfo.ReadOnly = true;
                datagrid.Columns.Add(col);
            }

            {
                var col = new Xceed.Wpf.DataGrid.Column();
                col.FieldName = "Pos.y";
                col.Title = "Pos.y";
                col.ReadOnly = false;
                col.DisplayMemberBindingInfo = new Xceed.Wpf.DataGrid.DataGridBindingInfo();
                col.DisplayMemberBindingInfo.Path = new PropertyPath("Position.y");
                //col.DisplayMemberBindingInfo.ReadOnly = true;
                datagrid.Columns.Add(col);
            }

            {
                var col = new Xceed.Wpf.DataGrid.Column();
                col.FieldName = "Need";
                col.Title = "Need";
                col.ReadOnly = true;
                col.DisplayMemberBindingInfo = new Xceed.Wpf.DataGrid.DataGridBindingInfo();
                col.DisplayMemberBindingInfo.Path = new PropertyPath("PriorNeed.Name");
                col.DisplayMemberBindingInfo.ReadOnly = true;
                datagrid.Columns.Add(col);
            }

            {
                var col = new Xceed.Wpf.DataGrid.Column();
                col.FieldName = "Action";
                col.Title = "Action";
                col.ReadOnly = true;
                col.DisplayMemberBindingInfo = new Xceed.Wpf.DataGrid.DataGridBindingInfo();
                col.DisplayMemberBindingInfo.Path = new PropertyPath("Acting.Name");
                col.DisplayMemberBindingInfo.ReadOnly = true;
                datagrid.Columns.Add(col);
            }

            {
                var col = new Xceed.Wpf.DataGrid.Column();
                col.FieldName = "Items";
                col.Title = "Items";
                col.ReadOnly = false;
                //col.CellEditor = xItemsCellEditor;
                col.DisplayMemberBindingInfo = new Xceed.Wpf.DataGrid.DataGridBindingInfo();
                col.DisplayMemberBindingInfo.Path = new PropertyPath("Items");
                //col.DisplayMemberBindingInfo.ReadOnly = true;
                datagrid.Columns.Add(col);
            }

            _columnUpdated = true;
        }

        void Tick()
        {
            //datagrid.ItemsSource = null;
            //datagrid.ItemsSource = Models.Simulator.Instance.Citizens;
        }

        void Pause()
        {
            xDataGrid.IsReadOnly = false;
            datagrid.ReadOnly = false;
        }

        void Resume()
        {
            xDataGrid.IsReadOnly = true;
            datagrid.ReadOnly = true;
        }

        #region ContextMenu
        ContextMenu _menu;
        void InitContextMenu()
        {
            if (_menu == null)
            {
                _menu = new ContextMenu();
            }
            _menu.Items.Clear();

            //foreach (var col in datagrid.Columns)
            //{
            //    var item = new MenuItem();
            //    item.Header = col.FieldName;
            //    item.IsCheckable = true;
            //    item.IsChecked = true;
            //    _menu.Items.Add(item);
            //}

            foreach (var col in xDataGrid.Columns)
            {
                var item = new MenuItem();
                item.Header = col.Header;
                item.IsCheckable = true;
                item.IsChecked = true;
                _menu.Items.Add(item);
            }

            _menu.AddHandler(MenuItem.ClickEvent, new RoutedEventHandler(MenuItem_Click));

            datagrid.ContextMenu = _menu;
            xDataGrid.ContextMenu = _menu;
        }

        void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var item = (MenuItem)e.Source;
            var header = (string)item.Header;

            foreach (var col in datagrid.Columns)
            {
                if (col.FieldName == header)
                {
                    col.Visible = !col.Visible;
                    break;
                }
            }

            foreach (var col in xDataGrid.Columns)
            {
                if (col.Header.ToString() == header)
                {
                    col.Visibility = (item.IsChecked) ? (Visibility.Visible) : (Visibility.Collapsed);
                    break;
                }
            }
        }
        #endregion
    }
}
