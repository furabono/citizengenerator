﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Needs_Action_Editor.Views
{
    /// <summary>
    /// MainView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainView : UserControl
    {
        public MainView()
        {
            InitializeComponent();

            Models.Simulator.Instance.OnStart += Instance_OnStart;
            Models.Simulator.Instance.OnStop += Instance_OnStop;
            Models.Simulator.Instance.OnPause += Instance_OnPause;
            Models.Simulator.Instance.OnResume += Instance_OnResume;

            //Models.Simulator.Instance.PropertyChanged += Simulator_PropertyChanged;
        }

        private void Instance_OnStart()
        {
            //xStartbutton.Content = "Stop";
            //xStartbutton.Style = xStartbutton.FindResource("")
            xStartbutton.Style = FindResource("Stop") as Style;
        }

        private void Instance_OnStop()
        {
            //xStartbutton.Content = "Start";
            xStartbutton.Style = FindResource("Start") as Style;
        }

        private void Instance_OnPause()
        {
            //xRunButton.Content = "Run";
            xRunButton.Style = FindResource("Update") as Style;
        }

        private void Instance_OnResume()
        {
            //xRunButton.Content = "Pause";
            xRunButton.Style = FindResource("Pause") as Style;
        }

        /*private void Simulator_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "VirtualWorld")
            {
                xPropertyGrid.SelectedObject = Models.Simulator.Instance.VirtualWorld;
            }
        }*/
    }
}
