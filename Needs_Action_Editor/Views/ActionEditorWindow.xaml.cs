﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Needs_Action_Editor.Views
{
    /// <summary>
    /// ActionEditorWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ActionEditorWindow : Window
    {
        public ActionEditorWindow()
        {
            InitializeComponent();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        public void SetSource(Classes.ActionSource source, BitmapImage image = null)
        {
            _ActionEditor.SetSource(source, image);
        }

        public Classes.ActionSource GetSource()
        {
            return _ActionEditor.GetSource();
        }
    }
}
