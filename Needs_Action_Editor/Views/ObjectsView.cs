﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Needs_Action_Editor.Views
{
    /// <summary>
    /// XAML 파일에서 이 사용자 지정 컨트롤을 사용하려면 1a 또는 1b단계를 수행한 다음 2단계를 수행하십시오.
    ///
    /// 1a단계) 현재 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor.Views"
    ///
    ///
    /// 1b단계) 다른 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor.Views;assembly=Needs_Action_Editor.Views"
    ///
    /// 또한 XAML 파일이 있는 프로젝트의 프로젝트 참조를 이 프로젝트에 추가하고
    /// 다시 빌드하여 컴파일 오류를 방지해야 합니다.
    ///
    ///     솔루션 탐색기에서 대상 프로젝트를 마우스 오른쪽 단추로 클릭하고
    ///     [참조 추가]->[프로젝트]를 차례로 클릭한 다음 이 프로젝트를 찾아서 선택합니다.
    ///
    ///
    /// 2단계)
    /// 계속 진행하여 XAML 파일에서 컨트롤을 사용합니다.
    ///
    ///     <MyNamespace:ObjectsView/>
    ///
    /// </summary>
    public class ObjectsView : Control
    {
        static ObjectsView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ObjectsView), new FrameworkPropertyMetadata(typeof(ObjectsView)));
        }

        Point _dragStartPosition;
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var xTreeView = GetTemplateChild("xTreeView") as TreeView;
            xTreeView.MouseDown += XTreeView_MouseDown;
            xTreeView.MouseMove += XTreeView_MouseMove;
            xTreeView.DragEnter += XTreeView_DragEnter;
            xTreeView.Drop += XTreeView_Drop;
        }

        private void XTreeView_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _dragStartPosition = e.GetPosition(null);
        }

        private void XTreeView_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Vector delta = e.GetPosition(null) - _dragStartPosition;
                if (Math.Abs(delta.X) < SystemParameters.MinimumHorizontalDragDistance &&
                    Math.Abs(delta.Y) < SystemParameters.MinimumVerticalDragDistance)
                {
                    return;
                }

                var xTreeView = sender as TreeView;
                if (xTreeView.SelectedItem == null || xTreeView.SelectedItem == Models.Simulator.Instance.Attributes[0]) { return; }

                var data = new DataObject("ObjectMeta", xTreeView.SelectedItem);
                DragDrop.DoDragDrop(xTreeView, data, DragDropEffects.Move);
            }
        }

        private void XTreeView_DragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("ObjectMeta") || !((e.OriginalSource as FrameworkElement).DataContext is Models.AttributeMeta))
            {
                e.Effects = DragDropEffects.None;
            }
        }

        private void XTreeView_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("ObjectMeta"))
            {
                var data = e.Data.GetData("ObjectMeta") as Models.IAttributeMeta;
                var target = (e.OriginalSource as FrameworkElement).DataContext as Models.AttributeMeta;

                if (target == null || target == data || target == data.Parent) { return; }

                {
                    var parent = target.Parent;
                    while (parent != null)
                    {
                        if (parent == data) { return; }
                        parent = parent.Parent;
                    }
                }

                data.Parent.Children.Remove(data);
                target.Children.Add(data);
                data.Parent = target;
            }
        }
    }

    public class ObjectsView_ObjectEditorView : Control
    {
        static ObjectsView_ObjectEditorView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ObjectsView_ObjectEditorView), new FrameworkPropertyMetadata(typeof(ObjectsView_ObjectEditorView)));
        }

        public object Proxy
        {
            get; set;
        }
    }
}
