﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Needs_Action_Editor
{
    public abstract partial class VSNode
    {
        void InitContextMenu()
        {
            var item1 = new MenuItem()
            {
                Header = "Duplicate",
            };
            item1.Click += MenuItem_Click;

            var item2 = new MenuItem()
            {
                Header = "Remove"
            };
            item2.Click += MenuItem_Click;

            var menu = new ContextMenu();
            menu.Items.Add(item1);
            menu.Items.Add(item2);

            this.ContextMenu = menu;
        }

        void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var item = (MenuItem)sender;
            var header = (string)item.Header;
            if (header == "Duplicate")
            {
                var clone = Clone();
                clone.Canvas = Canvas;
                Canvas.Children.Add(clone);
                clone.MoveTo(Position);
            }
            else if (header == "Remove")
            {
                Canvas.RemoveNode(this);
            }
        }
    }
}
