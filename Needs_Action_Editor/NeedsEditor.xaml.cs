﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ICSharpCode.AvalonEdit;
using Needs_Action_Editor.Models;

namespace Needs_Action_Editor
{
    /// <summary>
    /// NeedsEditor.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class NeedsEditor : UserControl
    {
        public static readonly DependencyProperty ActionsProperty
            = DependencyProperty.Register("Actions", 
                                          typeof(ObservableCollection<StringWrapper>), 
                                          typeof(NeedsEditor),
                                          new PropertyMetadata(new ObservableCollection<StringWrapper>()));

        public ObservableCollection<StringWrapper> Actions
        {
            get { return (ObservableCollection<StringWrapper>)GetValue(ActionsProperty); }
            set { SetValue(ActionsProperty, value); }
        }

        double[] rowHeight;
        Dictionary<int, GridSplitter> gridSplitters;

        public bool OK
        {
            get
            {
                var extend = xExpansion_ComboBox.SelectedItem as NeedSourceWrapper;

                return _Name_TextBox.Text != extend?.Name;
            }
        }

        public NeedsEditor()
        {
            InitializeComponent();

            rowHeight = new double[grid.RowDefinitions.Count];
            gridSplitters = new Dictionary<int, GridSplitter>();
            for (int i = 0; i < grid.Children.Count; ++i)
            {
                var gridSplitter = grid.Children[i] as GridSplitter;
                if (gridSplitter != null)
                {
                    gridSplitters[Grid.GetRow(gridSplitter)] = gridSplitter;
                }
            }

            {
                var binding = new Binding();
                binding.Source = Simulator.Instance;
                binding.Path = new PropertyPath("Needs");
                xExpansion_ComboBox.SetBinding(ComboBox.ItemsSourceProperty, binding);
                xExpansion_ComboBox.DisplayMemberPath = "Name";
            }
        }

        private void Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            if (!IsLoaded) { return; }

            var expander = (Expander)sender;
            var border = expander.Parent as Border;
            int rowIndex = Grid.GetRow(border);
            var row = grid.RowDefinitions[rowIndex];

            rowHeight[rowIndex] = row.Height.Value;
            row.Height = GridLength.Auto;
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            if (!IsLoaded) { return; }

            var expander = (Expander)sender;
            var se = expander.Content as ScriptEditor;
            var border = expander.Parent as Border;
            int rowIndex = Grid.GetRow(border);
            var row = grid.RowDefinitions[rowIndex];

            if (!se.IsExpression)
            {
                row.Height = new GridLength(rowHeight[rowIndex]);
            }
        }

        private void ScriptEditor_SingleLineChanged(object sender, RoutedPropertyChangedEventArgs<ScriptEditorMode> e)
        {
            var scriptEditor = (ScriptEditor)sender;
            var expander = scriptEditor.Parent as Expander;
            var border = expander.Parent as Border;
            int rowIndex = Grid.GetRow(border);
            var row = grid.RowDefinitions[rowIndex];

            if (scriptEditor.IsExpression)
            {
                row.Height = GridLength.Auto;
                gridSplitters[rowIndex + 1].IsEnabled = false;
            }
            else
            {
                scriptEditor.VerticalAlignment = VerticalAlignment.Stretch;
                row.Height = new GridLength(200);
                gridSplitters[rowIndex + 1].IsEnabled = true;
            }
        }

        public void SetSource(Classes.NeedSource source)
        {
            _Name_TextBox.Text = source.Name;

            if (source.Extend != null)
            {
                int index = 0;
                for (; index < Simulator.Instance.Needs.Count; ++index)
                {
                    if (Simulator.Instance.Needs[index].Name == source.Extend)
                    {
                        break;
                    }
                }
                xExpansion_ComboBox.SelectedIndex = index;
            }

            _Activation_ScriptEditor.Text = "";
            _Activation_ScriptEditor.Mode = (source.Activation.IsExpression) ? (ScriptEditorMode.Expression) : (ScriptEditorMode.Function);
            _Activation_ScriptEditor.Text = source.Activation.Code;

            _Priority_ScriptEditor.Text = "";
            _Priority_ScriptEditor.Mode = (source.Priority.IsExpression) ? (ScriptEditorMode.Expression) : (ScriptEditorMode.Function);
            _Priority_ScriptEditor.Text = source.Priority.Code;

            Actions = new ObservableCollection<StringWrapper>(from str in source.ActionInfos
                                                              select new StringWrapper(str));
        }

        public Classes.NeedSource GetSource()
        {
            var extend = xExpansion_ComboBox.SelectedItem as NeedSourceWrapper;
            var activation = new Classes.CodeSnippet(_Activation_ScriptEditor.Text, _Activation_ScriptEditor.IsExpression);
            var priority = new Classes.CodeSnippet(_Priority_ScriptEditor.Text, _Priority_ScriptEditor.IsExpression);

            return new Classes.NeedSource(_Name_TextBox.Text, 
                                          extend?.Name,
                                          activation, priority, 
                                          (from wrap in Actions select wrap.Text).ToArray());
        }
    }
}
