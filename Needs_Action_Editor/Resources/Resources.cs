﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows.Media.Imaging;

namespace Needs_Action_Editor.Resources
{
    public class Resources
    {
        static BitmapImage _Add_16x_png;
        static BitmapImage _Edit_16x_png;
        static BitmapImage _Metadata_16x_png;
        static BitmapImage _Method_16x_png;

        static Resources()
        {
            var assembly = Assembly.GetExecutingAssembly();

            using (var stream = new System.IO.MemoryStream())
            {
                Properties.Resources.Add_16x.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                stream.Position = 0;

                _Add_16x_png = new BitmapImage();
                _Add_16x_png.BeginInit();
                _Add_16x_png.StreamSource = stream;
                _Add_16x_png.CacheOption = BitmapCacheOption.OnLoad;
                _Add_16x_png.EndInit();
            }

            using (var stream = assembly.GetManifestResourceStream("Needs_Action_Editor.Resources.Icons.Metadata_16x.png"))
            {
                _Metadata_16x_png = new BitmapImage();
                _Metadata_16x_png.BeginInit();
                _Metadata_16x_png.StreamSource = stream;
                _Metadata_16x_png.EndInit();
            }

            using (var stream = assembly.GetManifestResourceStream("Needs_Action_Editor.Resources.Icons.Method_16x.png"))
            {
                _Method_16x_png = new BitmapImage();
                _Method_16x_png.BeginInit();
                _Method_16x_png.StreamSource = stream;
                _Method_16x_png.EndInit();
            }

            using (var stream = new System.IO.MemoryStream())
            {
                Properties.Resources.Edit_16x.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                stream.Position = 0;

                _Edit_16x_png = new BitmapImage();
                _Edit_16x_png.BeginInit();
                _Edit_16x_png.StreamSource = stream;
                _Edit_16x_png.CacheOption = BitmapCacheOption.OnLoad;
                _Edit_16x_png.EndInit();
            }
        }

        public BitmapImage Add_16x_png
        {
            get { return _Add_16x_png; }
        }

        public BitmapImage Edit_16x_png
        {
            get { return _Edit_16x_png; }
        }

        public BitmapImage Metadata_16x_png
        {
            get { return _Metadata_16x_png; }
        }

        public BitmapImage Method_16x_png
        {
            get { return _Method_16x_png; }
        }
    }
}
