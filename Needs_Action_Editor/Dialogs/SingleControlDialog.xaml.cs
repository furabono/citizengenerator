﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Needs_Action_Editor.Dialogs
{
    /// <summary>
    /// SingleControlDialog.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class SingleControlDialog : Window
    {
        /*public static readonly DependencyProperty ControlProperty
            = DependencyProperty.Register("ControlProperty", typeof(Control), typeof(SingleControlDialog));

        public Control Control
        {
            get { return (Control)GetValue(ControlProperty); }
            set { SetValue(ControlProperty, value); }
        }*/

        public SingleControlDialog(UIElement control)
        {
            InitializeComponent();

            xGrid.Children.Add(control);
            xOKButton.Click += XOKButton_Click;
            xCancelButton.Click += XCancelButton_Click;
        }

        private void XCancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void XOKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
