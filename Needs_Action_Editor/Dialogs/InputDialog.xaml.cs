﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Needs_Action_Editor.Dialogs
{
    /// <summary>
    /// InputDialog.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class InputDialog : Window
    {
        public InputDialog() : this(new ViewModels.InputDialogViewModel())
        {
        }

        public InputDialog(object viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        public InputDialog(string title, string message) : this(new ViewModels.InputDialogViewModel(title, message)) { }

        public string Message
        {
            get { return xMessage_TextBlock.Text; }
            set { xMessage_TextBlock.Text = value; }
        }

        public string Input
        {
            get { return xInput_TextBox.Text; }
            set { xInput_TextBox.Text = value; }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
