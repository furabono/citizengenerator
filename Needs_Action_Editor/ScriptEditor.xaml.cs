﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Needs_Action_Editor
{
    /// <summary>
    /// ScriptEditor.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ScriptEditor : UserControl
    {
        public static readonly DependencyProperty ModeProperty
            = DependencyProperty.Register(
                "Mode", 
                typeof(ScriptEditorMode), 
                typeof(ScriptEditor), 
                new PropertyMetadata(ScriptEditorMode.Expression, new PropertyChangedCallback(ModeChangedCallback)));

        public static readonly RoutedEvent ModeChangedEvent
            = EventManager.RegisterRoutedEvent(
                "ModeChanged",
                RoutingStrategy.Bubble,
                typeof(RoutedPropertyChangedEventHandler<ScriptEditorMode>),
                typeof(ScriptEditor));

        public static readonly DependencyProperty TextProperty
            = DependencyProperty.Register(
                "Text",
                typeof(string),
                typeof(ScriptEditor),
                new PropertyMetadata(new PropertyChangedCallback(TextChangedCallback)));

        static void ModeChangedCallback(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var se = (ScriptEditor)sender;
            var mode = (ScriptEditorMode)e.NewValue;

            se.RadioButton_Expression.IsChecked = (mode == ScriptEditorMode.Expression || mode == ScriptEditorMode.ExpressionOnly);
            se.RadioButton_Function.IsChecked = (mode == ScriptEditorMode.Function || mode == ScriptEditorMode.FunctionOnly);
            se.RadioButton_Expression.IsEnabled = (mode != ScriptEditorMode.FunctionOnly);
            se.RadioButton_Function.IsEnabled = (mode != ScriptEditorMode.ExpressionOnly);
            se.textEditor.SingleLine = (mode == ScriptEditorMode.Expression || mode == ScriptEditorMode.ExpressionOnly);

            se.RaiseEvent(new RoutedPropertyChangedEventArgs<ScriptEditorMode>((ScriptEditorMode)e.OldValue, (ScriptEditorMode)e.NewValue, ModeChangedEvent));
        }

        static void TextChangedCallback(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var se = (ScriptEditor)sender;
            
            if ((string)e.NewValue != se.textEditor.Text)
            {
                se.textEditor.Text = (string)e.NewValue;
            }
        }

        public ScriptEditorMode Mode
        {
            get { return (ScriptEditorMode)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }

        public event RoutedPropertyChangedEventHandler<ScriptEditorMode> ModeChanged
        {
            add { AddHandler(ModeChangedEvent, value); }
            remove { RemoveHandler(ModeChangedEvent, value); }
        }

        public bool IsExpression
        {
            get { return Mode == ScriptEditorMode.Expression || Mode == ScriptEditorMode.ExpressionOnly; }
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public ScriptEditor()
        {
            InitializeComponent();

            RadioButton_Expression.IsChecked = (Mode == ScriptEditorMode.Expression || Mode == ScriptEditorMode.ExpressionOnly);
            RadioButton_Function.IsChecked = (Mode == ScriptEditorMode.Function || Mode == ScriptEditorMode.FunctionOnly);
            RadioButton_Expression.IsEnabled = (Mode != ScriptEditorMode.FunctionOnly);
            RadioButton_Function.IsEnabled = (Mode != ScriptEditorMode.ExpressionOnly);
            textEditor.SingleLine = (Mode == ScriptEditorMode.Expression || Mode == ScriptEditorMode.ExpressionOnly);

            textEditor.Text = Text;
            textEditor.TextChanged += TextEditor_TextChanged;
        }

        private void TextEditor_TextChanged(object sender, EventArgs e)
        {
            Text = textEditor.Text;
        }

        private void RadioButton_Expression_Checked(object sender, RoutedEventArgs e)
        {
            if (!IsLoaded) { return; }

            if (string.IsNullOrWhiteSpace(textEditor.Text) || MessageBox.Show("작성한 내용은 모두 사라집니다", "Warning", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                textEditor.Clear();
                textEditor.Height = double.NaN;

                Mode = ScriptEditorMode.Expression;
            }
            else
            {
                string text = textEditor.Text;
                textEditor.Clear();
                RadioButton_Function.IsChecked = true;
                textEditor.Text = text;
            }
        }

        private void RadioButton_Function_Checked(object sender, RoutedEventArgs e)
        {
            if (!IsLoaded) { return; }

            if (string.IsNullOrWhiteSpace(textEditor.Text) || MessageBox.Show("작성한 내용은 모두 사라집니다", "Warning", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                textEditor.Clear();
                textEditor.VerticalAlignment = VerticalAlignment.Stretch;

                Mode = ScriptEditorMode.Function;
            }
            else
            {
                string text = textEditor.Text;
                textEditor.Clear();
                RadioButton_Expression.IsChecked = true;
                textEditor.Text = text;
            }
        }
    }

    public enum ScriptEditorMode
    {
        Expression, Function, ExpressionOnly, FunctionOnly
    }
}
