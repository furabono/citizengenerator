﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Needs_Action_Editor
{
    /// <summary>
    /// XAML 파일에서 이 사용자 지정 컨트롤을 사용하려면 1a 또는 1b단계를 수행한 다음 2단계를 수행하십시오.
    ///
    /// 1a단계) 현재 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor"
    ///
    ///
    /// 1b단계) 다른 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor;assembly=Needs_Action_Editor"
    ///
    /// 또한 XAML 파일이 있는 프로젝트의 프로젝트 참조를 이 프로젝트에 추가하고
    /// 다시 빌드하여 컴파일 오류를 방지해야 합니다.
    ///
    ///     솔루션 탐색기에서 대상 프로젝트를 마우스 오른쪽 단추로 클릭하고
    ///     [참조 추가]->[프로젝트]를 차례로 클릭한 다음 이 프로젝트를 찾아서 선택합니다.
    ///
    ///
    /// 2단계)
    /// 계속 진행하여 XAML 파일에서 컨트롤을 사용합니다.
    ///
    ///     <MyNamespace:VSNodePort/>
    ///
    /// </summary>
    public class VSNodePort : Control
    {
        public static readonly DependencyProperty PortTypeProperty
            = DependencyProperty.Register("PortType", typeof(PortType), typeof(VSNodePort));

        static VSNodePort()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(VSNodePort), new FrameworkPropertyMetadata(typeof(VSNodePort)));
        }

        BindingPoint position;
        Ellipse ellipse;

        public VSNode ParentNode
        {
            get; set;
        }

        public PortType Type
        {
            get { return (PortType)GetValue(PortTypeProperty); }
            set { SetValue(PortTypeProperty, value); }
        }

        public BindingPoint Position
        {
            get { return position; }
        }

        public bool EllipseMouseOver
        {
            get { return ellipse.IsMouseOver; }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            position = new BindingPoint(0, 0);
            ParentNode.ControlDragged += ParentNode_ControlDragged;

            ellipse = GetTemplateChild("ellipse") as Ellipse;
            ellipse.MouseLeftButtonUp += VSNodePort_MouseLeftButtonUp;
            ellipse.PreviewMouseLeftButtonUp += Ellipse_PreviewMouseLeftButtonUp;

            this.MouseLeftButtonUp += VSNodePort_MouseLeftButtonUp1;
        }

        private void VSNodePort_MouseLeftButtonUp1(object sender, MouseButtonEventArgs e)
        {
            ;
        }

        private void Ellipse_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ;
        }

        private void ParentNode_ControlDragged()
        {
            //System.Diagnostics.Debug.Print(TranslatePoint(new Point(ActualWidth / 2, ActualHeight / 2), this._Canvas).ToString());
            var point = TranslatePoint(new Point(ActualWidth / 2, ActualHeight / 2), ParentNode.Canvas);
            position.X = point.X;
            position.Y = point.Y;
        }

        void VSNodePort_MouseLeftButtonUp(object sender, MouseButtonEventArgs args)
        {
            if (this.IsMouseOver)
            {
                ParentNode.Canvas.MouseClickOnPort(this);
                args.Handled = true;
            }
        }
    }

    public enum PortType
    {
        Input, Output
    }
}
