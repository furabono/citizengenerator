﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Needs_Action_Editor.Models;

namespace Needs_Action_Editor.ViewModels
{
    class CitizenMetaDataEditorViewModel : ViewModelBase
    {
        ObservableCollection<CitizenMetaData.ParameterMeta> _parameters;
        public ObservableCollection<CitizenMetaData.ParameterMeta> Parameters
        {
            get { return _parameters; }
            set
            {
                _parameters = value;
                RaisePropertyChanged("Parameters");
            }
        }

        CitizenMetaData.ParameterMeta selectedParameter;
        public CitizenMetaData.ParameterMeta SelectedParameter
        {
            get { return selectedParameter; }
            set
            {
                selectedParameter = value;
                RaisePropertyChanged("SelectedParameter");
            }
        }

        public string MetabolismScript
        {
            get { return Simulator.Instance.CitizenMetaData.MetabolismScript; }
            set
            {
                Simulator.Instance.CitizenMetaData.MetabolismScript = value;
                //RaisePropertyChanged("MetabolismScript");
            }
        }

        public Vector2 StartPosition
        {
            get { return Simulator.Instance.CitizenMetaData.StartPosition; }
            set
            {
                Simulator.Instance.CitizenMetaData.StartPosition = value;
                RaisePropertyChanged("StartPosition");
            }
        }

        public ObservableCollection<AttributeMeta> RootAttribute
        {
            get; set;
        }

        public Classes.Inventory Items
        {
            get { return Simulator.Instance.CitizenMetaData.Items; }
        }

        public Classes.InventoryEntry SelectedObject
        {
            get; set;
        }

        DelegateCommand newCommand;
        public ICommand NewCommand
        {
            get { return newCommand; }
        }

        DelegateCommand deleteCommand;
        public ICommand DeleteCommand
        {
            get { return deleteCommand; }
        }

        public DelegateCommand<object> AddObjectCommand
        {
            get; set;
        }

        public DelegateCommand RemoveObjectCommand
        {
            get; set;
        }

        public CitizenMetaDataEditorViewModel()
        {
            //_parameters = new ObservableCollection<CitizenMetaData.ParameterMeta>(Simulator.Instance.CitizenMetaData.Parameters);

            RootAttribute = new ObservableCollection<Models.AttributeMeta>()
            {
                Models.Simulator.Instance.Attributes[0]
            };

            newCommand = new DelegateCommand();
            newCommand.CanExecuteTargets += New_CanExecute;
            newCommand.ExecuteTargets += New_Execute;

            deleteCommand = new DelegateCommand();
            deleteCommand.CanExecuteTargets += Delete_CanExecute;
            deleteCommand.ExecuteTargets += Delete_Execute;

            AddObjectCommand = new DelegateCommand<object>();
            AddObjectCommand.CanExecuteTargets += AddObjectCommand_CanExecuteTargets;
            AddObjectCommand.ExecuteTargets += AddObjectCommand_ExecuteTargets;

            RemoveObjectCommand = new DelegateCommand();
            RemoveObjectCommand.CanExecuteTargets += RemoveObjectCommand_CanExecuteTargets;
            RemoveObjectCommand.ExecuteTargets += RemoveObjectCommand_ExecuteTargets;
        }

        bool New_CanExecute()
        {
            return true;
        }

        void New_Execute()
        {
            Parameters.Add(new CitizenMetaData.ParameterMeta("NAME"));
        }

        bool Delete_CanExecute()
        {
            return selectedParameter != null;
        }

        void Delete_Execute()
        {
            Parameters.Remove(selectedParameter);
        }

        private bool AddObjectCommand_CanExecuteTargets()
        {
            return true;
        }

        private void AddObjectCommand_ExecuteTargets(object obj)
        {
            var objmeta = (ObjectMeta)obj;

            Items.AddItem(objmeta);
        }

        private bool RemoveObjectCommand_CanExecuteTargets()
        {
            return SelectedObject != null;
        }

        private void RemoveObjectCommand_ExecuteTargets()
        {
            Items.RemoveItem(SelectedObject.Object);
        }
    }
}
