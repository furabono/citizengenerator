﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Needs_Action_Editor.Classes;
using System.Windows.Input;
using Needs_Action_Editor.Models;

using Ookii.Dialogs.Wpf;

namespace Needs_Action_Editor.ViewModels
{
    class ActionsViewModel : ViewModelBase
    {
        public ObservableCollection<Controls.IHierarchicalItem> Actions
        {
            get { return Simulator.Instance.ActionsHierarchies; }
        }

        Controls.IHierarchicalItem selectedAction;
        public Controls.IHierarchicalItem SelectedAction
        {
            get { return selectedAction; }
            set
            {
                selectedAction = value;
                RaisePropertyChanged("SelectedAction");
            }
        }

        Classes.Cache<string, System.Windows.Media.Imaging.BitmapImage> _imageCache;

        DelegateCommand newCommand;
        public ICommand NewCommand
        {
            get { return newCommand; }
        }

        DelegateCommand deleteCommand;
        public ICommand DeleteCommand
        {
            get { return deleteCommand; }
        }

        DelegateCommand editCommand;
        public ICommand EditCommand
        {
            get { return editCommand; }
        }

        DelegateCommand _newFolderCommand;
        public ICommand NewFolderCommand
        {
            get { return _newFolderCommand; }
        }

        public DelegateCommand ItemDoubleClickEventCommand
        {
            get { return editCommand; }
        }

        public ActionsViewModel()
        {
            Simulator.Instance.PropertyChanged += Instance_PropertyChanged;

            _imageCache = new Cache<string, System.Windows.Media.Imaging.BitmapImage>(5);

            newCommand = new DelegateCommand();
            newCommand.CanExecuteTargets += New_CanExecute;
            newCommand.ExecuteTargets += New_Execute;

            deleteCommand = new DelegateCommand();
            deleteCommand.CanExecuteTargets += Delete_CanExecute;
            deleteCommand.ExecuteTargets += Delete_Execute;

            editCommand = new DelegateCommand();
            editCommand.CanExecuteTargets += Edit_CanExecute;
            editCommand.ExecuteTargets += Edit_Execute;

            _newFolderCommand = new DelegateCommand();
            _newFolderCommand.CanExecuteTargets += _newFolderCommand_CanExecuteTargets;
            _newFolderCommand.ExecuteTargets += _newFolderCommand_ExecuteTargets;
        }

        private void Instance_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ActionsHierarchies")
            {
                RaisePropertyChanged("Actions");
            }
        }

        bool New_CanExecute()
        {
            return true;
        }

        void New_Execute()
        {
            Directory parent;

            if (selectedAction == null)
            {
                parent = Simulator.Instance.ActionsRoot;
            }
            else if (selectedAction is Directory)
            {
                parent = (Directory)selectedAction;
            }
            else
            {
                var cosin = (ActionSourceWrapper)selectedAction;
                parent = cosin.Parent as Directory;
            }

            var dialog = new Views.ActionEditorWindow();
            while (dialog.ShowDialog() ?? false)
            {
                var newSrc = dialog.GetSource();

                bool nameAlreadyExists = false;
                foreach (var child in parent.Children)
                {
                    if ((child as ActionSourceWrapper)?.Name == newSrc.Name)
                    {
                        nameAlreadyExists = true;
                        break;
                    }
                }

                if (nameAlreadyExists)
                {
                    dialog = new Views.ActionEditorWindow();
                    dialog.SetSource(newSrc);
                    continue;
                }

                var wrapper = new ActionSourceWrapper(newSrc);

                parent.AddChild(wrapper);
                Simulator.Instance.Actions.Add(wrapper);

                string subDir = (wrapper.Parent as Directory)?.Path;
                Json.WriteActionSource(newSrc, Simulator.Instance.WorkingDirectory, subDir);

                if (dialog._ActionEditor.Image != null)
                {
                    var img = Simulator.Instance.WorkingDirectory + @"Resources\ActionImages\" + newSrc.Name + ".png";
                    var encoder = new System.Windows.Media.Imaging.PngBitmapEncoder();
                    encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(dialog._ActionEditor.Image));

                    using (var stream = new System.IO.FileStream(img, System.IO.FileMode.Create))
                    {
                        encoder.Save(stream);
                    }

                    CacheImage(newSrc.Name, dialog._ActionEditor.Image);
                }

                break;
            }
        }

        bool Delete_CanExecute()
        {
            return selectedAction != null;
        }

        void Delete_Execute()
        {
            if (this.selectedAction is ActionSourceWrapper)
            {
                ActionSourceWrapper selectedAction = (ActionSourceWrapper)this.selectedAction;
                System.IO.File.Delete(Simulator.Instance.WorkingDirectory + (selectedAction.Parent as Directory)?.Path + selectedAction.Name + ".json");

                var imageFile = Simulator.Instance.WorkingDirectory + @"Resources\ActionImages\" + selectedAction.Name + ".png";
                if (System.IO.File.Exists(imageFile))
                {
                    System.IO.File.Delete(imageFile);
                    RemoveFromCache(selectedAction.Name);
                }
                
                selectedAction.Parent.RemoveChild(selectedAction);
                Simulator.Instance.Actions.Remove(selectedAction);
            }
            else
            {
                //return;
                Directory dir = (Directory)this.selectedAction;
                System.IO.Directory.Delete(Simulator.Instance.WorkingDirectory + dir.Path.Substring(0, dir.Path.Length - 1), true);
                dir.Parent.RemoveChild(dir);

                {
                    Queue<Directory> queue = new Queue<Directory>();
                    queue.Enqueue(dir);

                    while (queue.Count > 0)
                    {
                        Directory d = queue.Dequeue();

                        foreach (var child in d.Children)
                        {
                            if (child is Directory)
                            {
                                queue.Enqueue((Directory)child);
                            }
                            else
                            {
                                var action = (ActionSourceWrapper)child;

                                Simulator.Instance.Actions.Remove(action);

                                var imageFile = Simulator.Instance.WorkingDirectory + @"Resources\ActionImages\" + action.Name + ".png";
                                if (System.IO.File.Exists(imageFile))
                                {
                                    System.IO.File.Delete(imageFile);
                                    RemoveFromCache(action.Name);
                                }
                            }
                        }
                    }
                }
            }
        }

        bool Edit_CanExecute()
        {
            return selectedAction != null;
        }

        void Edit_Execute()
        {
            if (SelectedAction is Directory)
            {
                var dialog = new Dialogs.InputDialog("Rename", "Input Name");

                if (dialog.ShowDialog() ?? false)
                {
                    var dir = selectedAction as Directory;

                    foreach (var child in (dir.Parent as Directory).Children)
                    {
                        if ((child as Directory)?.Name == dialog.Input) { return; }
                    }

                    System.IO.Directory.Move(
                        Simulator.Instance.WorkingDirectory + dir.Path,
                        Simulator.Instance.WorkingDirectory + (dir.Parent as Directory).Path + dialog.Input
                        );

                    dir.Name = dialog.Input;
                }
            }

            if (selectedAction is ActionSourceWrapper)
            {
                ActionSourceWrapper selectedAction = (ActionSourceWrapper)this.selectedAction;
                Directory parent = selectedAction.Parent as Directory;

                var image = GetImage(selectedAction.Name);

                var dialog = new Views.ActionEditorWindow();
                dialog.SetSource(selectedAction.Target, image);
                while (dialog.ShowDialog() ?? false)
                {
                    ActionSource newSrc = dialog.GetSource();

                    if (newSrc.Name != selectedAction.Name)
                    {
                        bool nameAlreadyExists = false;
                        foreach (var child in parent.Children)
                        {
                            if ((child as ActionSourceWrapper)?.Name == newSrc.Name)
                            {
                                nameAlreadyExists = true;
                                break;
                            }
                        }

                        if (nameAlreadyExists)
                        {
                            dialog = new Views.ActionEditorWindow();
                            dialog.SetSource(newSrc);
                            continue;
                        }

                        System.IO.File.Delete(Simulator.Instance.WorkingDirectory + (selectedAction.Parent as Directory)?.Path + selectedAction.Name + ".json");
                    }
                    Json.WriteActionSource(newSrc, Simulator.Instance.WorkingDirectory, (selectedAction.Parent as Directory)?.Path);

                    if (dialog._ActionEditor.Image != null)
                    {
                        if (image == dialog._ActionEditor.Image)
                        {
                            if (selectedAction.Name != newSrc.Name)
                            {
                                var oldimg = Simulator.Instance.WorkingDirectory + @"Resources\ActionImages\" + selectedAction.Name + ".png";
                                var newimg = Simulator.Instance.WorkingDirectory + @"Resources\ActionImages\" + newSrc.Name + ".png";
                                System.IO.File.Move(oldimg, newimg);
                            }
                        }
                        else
                        {
                            if (image != null)
                            {
                                string imageFile;
                                if (selectedAction.Name == newSrc.Name)
                                {
                                    imageFile = Simulator.Instance.WorkingDirectory + @"Resources\ActionImages\" + newSrc.Name + ".png";
                                }
                                else
                                {
                                    imageFile = Simulator.Instance.WorkingDirectory + @"Resources\ActionImages\" + selectedAction.Name + ".png";
                                }
                                System.IO.File.Delete(imageFile);
                            }

                            var img = Simulator.Instance.WorkingDirectory + @"Resources\ActionImages\" + newSrc.Name + ".png";
                            var encoder = new System.Windows.Media.Imaging.PngBitmapEncoder();
                            encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(dialog._ActionEditor.Image));

                            if (!System.IO.Directory.Exists(Simulator.Instance.WorkingDirectory + @"Resources\ActionImages"))
                            {
                                System.IO.Directory.CreateDirectory(Simulator.Instance.WorkingDirectory + @"Resources\ActionImages");
                            }

                            using (var stream = new System.IO.FileStream(img, System.IO.FileMode.Create))
                            {
                                encoder.Save(stream);
                            }

                            image = dialog._ActionEditor.Image;
                        }

                        //if (selectedAction.Name != newSrc.Name)
                        //{
                        //    var imageFile = Simulator.Instance.WorkingDirectory + @"Resources\ActionImages\" + selectedAction.Name + ".png";
                        //    if (System.IO.File.Exists(imageFile))
                        //    {
                        //        System.IO.File.Delete(imageFile);
                        //    }
                        //}

                        //var img = Simulator.Instance.WorkingDirectory + @"Resources\ActionImages\" + newSrc.Name + ".png";
                        //var encoder = new System.Windows.Media.Imaging.PngBitmapEncoder();
                        //encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(dialog._ActionEditor.Image));

                        //if (!System.IO.Directory.Exists(Simulator.Instance.WorkingDirectory + @"Resources\ActionImages"))
                        //{
                        //    System.IO.Directory.CreateDirectory(Simulator.Instance.WorkingDirectory + @"Resources\ActionImages");
                        //}

                        //using (var stream = new System.IO.FileStream(img, System.IO.FileMode.Create))
                        //{
                        //    encoder.Save(stream);
                        //}

                        //image = dialog._ActionEditor.Image;
                    }
                    else if (selectedAction.Name != newSrc.Name)
                    {
                        var oldimg = Simulator.Instance.WorkingDirectory + @"Resources\ActionImages\" + selectedAction.Name + ".png";
                        if (System.IO.File.Exists(oldimg))
                        {
                            var newimg = Simulator.Instance.WorkingDirectory + @"Resources\ActionImages\" + newSrc.Name + ".png";
                            System.IO.File.Move(oldimg, newimg);
                        }
                    }

                    CacheImage(newSrc.Name, image);

                    selectedAction.Target.Efficiency = newSrc.Efficiency;
                    selectedAction.Target.GlobalVariables = newSrc.GlobalVariables;
                    selectedAction.Target.Parameters = newSrc.Parameters;
                    selectedAction.Name = newSrc.Name;
                    selectedAction.Target.Plan = newSrc.Plan;
                    selectedAction.Target.Prepare = newSrc.Prepare;
                    selectedAction.Target.Work = newSrc.Work;

                    Simulator.Instance.ModifiedActionSources.Add(selectedAction.Target);
                    RaisePropertyChanged("SelectedAction");
                    break;
                }
            }
        }

        private bool _newFolderCommand_CanExecuteTargets()
        {
            return true;
        }

        private void _newFolderCommand_ExecuteTargets()
        {
            var dialog = new Dialogs.InputDialog("New Folder", "Input Name");

            if (dialog.ShowDialog() ?? false)
            {
                Directory parent = selectedAction as Directory;
                if (parent == null)
                {
                    if (selectedAction is ActionSourceWrapper) { parent = (selectedAction as ActionSourceWrapper).Parent as Directory; }
                    else { parent = Simulator.Instance.ActionsRoot; }
                }

                foreach (var child in parent.Children)
                {
                    if ((child as Directory)?.Name == dialog.Input) { return; }
                }

                System.IO.Directory.CreateDirectory(Simulator.Instance.WorkingDirectory + parent.Path + dialog.Input);

                var dir = new Directory(dialog.Input);
                parent.AddChild(dir);
            }
        }

        System.Windows.Media.Imaging.BitmapImage GetImage(string name)
        {
            System.Windows.Media.Imaging.BitmapImage image;
            if (_imageCache.ContainsKey(name))
            {
                image = _imageCache.Get(name);
            }
            else
            {
                var img = Simulator.Instance.WorkingDirectory + @"Resources\ActionImages\" + name + ".png";
                if (System.IO.File.Exists(img))
                {
                    image = new System.Windows.Media.Imaging.BitmapImage();
                    using (var stream = System.IO.File.OpenRead(img))
                    {
                        image.BeginInit();
                        image.StreamSource = stream;
                        image.CacheOption = System.Windows.Media.Imaging.BitmapCacheOption.OnLoad;
                        image.EndInit();
                    }

                    CacheImage(name, image);
                }
                else
                {
                    image = null;
                }
            }

            return image;
        }

        void CacheImage(string name, System.Windows.Media.Imaging.BitmapImage image)
        {
            _imageCache.Add(name, image);
        }

        void RemoveFromCache(string name)
        {
            _imageCache.Remove(name);
        }
    }
}
