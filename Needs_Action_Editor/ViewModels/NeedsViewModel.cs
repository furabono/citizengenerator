﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Needs_Action_Editor.Classes;
using System.Windows.Input;
using Needs_Action_Editor.Models;

namespace Needs_Action_Editor.ViewModels
{
    class NeedsViewModel : ViewModelBase
    {
        //ObservableCollection<Models.NeedSourceWrapper> needs;
        public ObservableCollection<Controls.IHierarchicalItem> Needs
        {
            get { return Simulator.Instance.NeedsHierarchies; }
        }

        Controls.IHierarchicalItem selectedNeed;
        public Controls.IHierarchicalItem SelectedNeed
        {
            get { return selectedNeed; }
            set
            {
                selectedNeed = value;
                RaisePropertyChanged("SelectedNeed");
            }
        }

        DelegateCommand newCommand;
        public ICommand NewCommand
        {
            get { return newCommand; }
        }

        DelegateCommand deleteCommand;
        public ICommand DeleteCommand
        {
            get { return deleteCommand; }
        }

        DelegateCommand editCommand;
        public ICommand EditCommand
        {
            get { return editCommand; }
        }

        DelegateCommand _newFolderCommand;
        public ICommand NewFolderCommand
        {
            get { return _newFolderCommand; }
        }

        //DelegateCommand _itemDoubleClickEventCommand;
        public ICommand ItemDoubleClickEventCommand
        {
            get { return editCommand; }
        }

        public NeedsViewModel()
        {
            Simulator.Instance.PropertyChanged += Instance_PropertyChanged;

            newCommand = new DelegateCommand();
            newCommand.CanExecuteTargets += New_CanExecute;
            newCommand.ExecuteTargets += New_Execute;

            deleteCommand = new DelegateCommand();
            deleteCommand.CanExecuteTargets += Delete_CanExecute;
            deleteCommand.ExecuteTargets += Delete_Execute;

            editCommand = new DelegateCommand();
            editCommand.CanExecuteTargets += Edit_CanExecute;
            editCommand.ExecuteTargets += Edit_Execute;

            _newFolderCommand = new DelegateCommand();
            _newFolderCommand.CanExecuteTargets += _newFolderCommand_CanExecuteTargets;
            _newFolderCommand.ExecuteTargets += _newFolderCommand_ExecuteTargets;
        }

        private void Instance_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "NeedsHierarchies")
            {
                RaisePropertyChanged("Needs");
            }
        }

        bool New_CanExecute()
        {
            return true;
        }

        void New_Execute()
        {
            Directory parent;
            if (selectedNeed == null)
            {
                parent = Simulator.Instance.NeedsRoot;
            }
            else if (selectedNeed is Directory)
            {
                parent = (Directory)selectedNeed;
            }
            else
            {
                var cosin = (NeedSourceWrapper)selectedNeed;
                parent = cosin.Parent as Directory;
            }

            var dialog = new Views.NeedEditorWindow();
            while (dialog.ShowDialog() ?? false)
            {
                var newSrc = dialog.GetSource();

                bool nameAlreadyExists = false;
                foreach (var child in parent.Children)
                {
                    if ((child as NeedSourceWrapper)?.Name == newSrc.Name)
                    {
                        nameAlreadyExists = true;
                        break;
                    }
                }

                if (nameAlreadyExists)
                {
                    dialog = new Views.NeedEditorWindow();
                    dialog.SetSource(newSrc);
                    continue;
                }

                var wrapper = new NeedSourceWrapper(newSrc);

                parent.AddChild(wrapper);
                Simulator.Instance.Needs.Add(wrapper);

                string subDir = (wrapper.Parent as Directory)?.Path;
                Json.WriteNeedSource(newSrc, Simulator.Instance.WorkingDirectory, subDir);
                break;
            }
        }

        bool Delete_CanExecute()
        {
            return selectedNeed != null;
        }

        void Delete_Execute()
        {
            if (selectedNeed is NeedSourceWrapper)
            {
                var wrapper = (NeedSourceWrapper)selectedNeed;

                System.IO.File.Delete(Simulator.Instance.WorkingDirectory + (wrapper.Parent as Directory)?.Path + wrapper.Name + ".json");

                wrapper.Parent.RemoveChild(wrapper);
                Simulator.Instance.Needs.Remove(wrapper);
            }
            else
            {
                Directory dir = (Directory)selectedNeed;
                System.IO.Directory.Delete(Simulator.Instance.WorkingDirectory + dir.Path.Substring(0, dir.Path.Length - 1), true);
                dir.Parent.RemoveChild(dir);

                {
                    Queue<Directory> queue = new Queue<Directory>();
                    queue.Enqueue(dir);

                    while (queue.Count > 0)
                    {
                        Directory d = queue.Dequeue();

                        foreach (var child in d.Children)
                        {
                            if (child is Directory)
                            {
                                queue.Enqueue((Directory)child);
                            }
                            else
                            {
                                Simulator.Instance.Needs.Remove((NeedSourceWrapper)child);
                            }
                        }
                    }
                }
            }
        }

        bool Edit_CanExecute()
        {
            return selectedNeed != null;
        }

        void Edit_Execute()
        {
            if (SelectedNeed is Directory)
            {
                var dialog = new Dialogs.InputDialog("Rename", "Input Name");

                if (dialog.ShowDialog() ?? false)
                {
                    var dir = selectedNeed as Directory;

                    foreach (var child in (dir.Parent as Directory).Children)
                    {
                        if ((child as Directory)?.Name == dialog.Input) { return; }
                    }

                    System.IO.Directory.Move(
                        Simulator.Instance.WorkingDirectory + dir.Path,
                        Simulator.Instance.WorkingDirectory + (dir.Parent as Directory).Path + dialog.Input
                        );

                    dir.Name = dialog.Input;
                }
            }

            if (selectedNeed is NeedSourceWrapper)
            {
                var selectedNeed = (NeedSourceWrapper)this.selectedNeed;
                var parent = selectedNeed.Parent as Directory;

                var dialog = new Views.NeedEditorWindow();
                dialog.SetSource(selectedNeed.Target);
                while (dialog.ShowDialog() ?? false)
                {
                    NeedSource newSrc = dialog.GetSource();

                    if (selectedNeed.Name != newSrc.Name)
                    {
                        bool nameAlreadyExists = false;
                        foreach (var child in parent.Children)
                        {
                            if ((child as NeedSourceWrapper)?.Name == newSrc.Name)
                            {
                                nameAlreadyExists = true;
                                break;
                            }
                        }

                        if (nameAlreadyExists)
                        {
                            dialog = new Views.NeedEditorWindow();
                            dialog.SetSource(newSrc);
                            continue;
                        }

                        System.IO.File.Delete(Simulator.Instance.WorkingDirectory + (selectedNeed.Parent as Directory)?.Path + selectedNeed.Name + ".json");

                        foreach (var need in Simulator.Instance.Needs)
                        {
                            if (need.Target.Extend == selectedNeed.Name)
                            {
                                need.Target.Extend = newSrc.Name;
                                Json.WriteNeedSource(need.Target, Simulator.Instance.WorkingDirectory, (need.Parent as Directory).Path);
                            }
                        }
                    }
                    Json.WriteNeedSource(newSrc, Simulator.Instance.WorkingDirectory, (selectedNeed.Parent as Directory)?.Path);

                    selectedNeed.Target.ActionInfos = newSrc.ActionInfos;
                    selectedNeed.Target.Activation = newSrc.Activation;
                    selectedNeed.Name = newSrc.Name;
                    selectedNeed.Target.Extend = newSrc.Extend;
                    selectedNeed.Target.Priority = newSrc.Priority;
                    RaisePropertyChanged("SelectedNeed");
                    break;
                }
            }
        }

        private bool _newFolderCommand_CanExecuteTargets()
        {
            return true;
        }

        private void _newFolderCommand_ExecuteTargets()
        {
            var dialog = new Dialogs.InputDialog("New Folder", "Input Name");

            if (dialog.ShowDialog() ?? false)
            {
                Directory parent = selectedNeed as Directory;
                if (parent == null)
                {
                    if (selectedNeed is NeedSourceWrapper) { parent = (selectedNeed as NeedSourceWrapper).Parent as Directory; }
                    else { parent = Simulator.Instance.NeedsRoot; }
                }

                foreach (var child in parent.Children)
                {
                    if ((child as Directory)?.Name == dialog.Input) { return; }
                }

                System.IO.Directory.CreateDirectory(Simulator.Instance.WorkingDirectory + parent.Path + dialog.Input);

                var dir = new Directory(dialog.Input);
                parent.AddChild(dir);
            }
        }
    }
}
