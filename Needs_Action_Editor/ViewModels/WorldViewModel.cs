﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.ViewModels
{
    public class WorldViewModel
    {
        Models.Simulator Simulator
        {
            get { return Models.Simulator.Instance; }
        }

        Models.World World
        {
            get { return Simulator.World; }
        }

        public ObservableCollection<Models.Variable> Variables
        {
            get { return World.Variables; }
        }

        public Models.WorldObjectCollection Objects
        {
            get { return World.Objects; }
        }

        public Models.WorldObject SelectedObject
        {
            get; set;
        }

        public DelegateCommand InitWorldCommand
        {
            get; set;
        }

        public DelegateCommand AddObjectCommand
        {
            get; set;
        }

        public DelegateCommand RemoveObjectCommand
        {
            get; set;
        }

        public WorldViewModel()
        {
            InitWorldCommand = new DelegateCommand();
            InitWorldCommand.CanExecuteTargets += InitWorldCommand_CanExecuteTargets;
            InitWorldCommand.ExecuteTargets += InitWorldCommand_ExecuteTargets;

            AddObjectCommand = new DelegateCommand();
            AddObjectCommand.CanExecuteTargets += AddObjectCommand_CanExecuteTargets;
            AddObjectCommand.ExecuteTargets += AddObjectCommand_ExecuteTargets;

            RemoveObjectCommand = new DelegateCommand();
            RemoveObjectCommand.CanExecuteTargets += RemoveObjectCommand_CanExecuteTargets;
            RemoveObjectCommand.ExecuteTargets += RemoveObjectCommand_ExecuteTargets;
        }

        private bool InitWorldCommand_CanExecuteTargets()
        {
            return true;
        }

        private void InitWorldCommand_ExecuteTargets()
        {
            World.Init();
        }

        private bool AddObjectCommand_CanExecuteTargets()
        {
            return true;
        }

        private void AddObjectCommand_ExecuteTargets()
        {
            var v = new Views.ObjectTreeView(Simulator.Attributes[0]);
            var dialog = new Dialogs.SingleControlDialog(v);
            dialog.xOKButton.Visibility = System.Windows.Visibility.Visible;
            dialog.SizeToContent = System.Windows.SizeToContent.Manual;
            dialog.Width = 400;
            dialog.Height = 300;
            dialog.Owner = App.Current.MainWindow;
            dialog.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;

            if (dialog.ShowDialog() ?? false)
            {
                var obj = v.SelectedItem;

                if (obj is Models.ObjectMeta)
                {
                    Objects.PutObject((Models.ObjectMeta)obj, new Models.Vector2());
                }
            }
        }

        private bool RemoveObjectCommand_CanExecuteTargets()
        {
            return SelectedObject != null;
        }

        private void RemoveObjectCommand_ExecuteTargets()
        {
            Objects.List.Remove(SelectedObject);
        }
    }
}
