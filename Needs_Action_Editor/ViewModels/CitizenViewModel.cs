﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.ViewModels
{
    public class CitizenViewModel : ViewModelBase
    {
        Models.Simulator Simulator
        {
            get { return Models.Simulator.Instance; }
        }

        public bool ReadOnly { get; set; }
        InventoryEditorViewModel _ievm;

        public Classes.Citizen SelectedCitizen { get; set; }

        public DelegateCommand EditItemsCommand { get; set; }

        public CitizenViewModel()
        {
            ReadOnly = false;

            var attr = new ObservableCollection<Models.AttributeMeta>()
            {
                Simulator.Attributes[0]
            };
            _ievm = new InventoryEditorViewModel(attr, null);

            EditItemsCommand = new DelegateCommand();
            EditItemsCommand.CanExecuteTargets += EditItemsCommand_CanExecuteTargets;
            EditItemsCommand.ExecuteTargets += EditItemsCommand_ExecuteTargets;
        }

        private bool EditItemsCommand_CanExecuteTargets()
        {
            return !ReadOnly && SelectedCitizen != null;
        }

        private void EditItemsCommand_ExecuteTargets()
        {
            _ievm.Items = SelectedCitizen.Items;
            var view = new Views.InventoryEditorView();
            view.DataContext = _ievm;
            var dialog = new Dialogs.SingleControlDialog(view);
            dialog.SizeToContent = System.Windows.SizeToContent.Manual;
            dialog.Width = 500;
            dialog.Height = 300;

            dialog.ShowDialog();
        }
    }
}
