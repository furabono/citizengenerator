﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using System.Windows;
using Gat.Controls;

namespace Needs_Action_Editor.ViewModels
{
    class MainViewModel : ViewModelBase
    {
        public int TPS
        {
            get { return Models.Simulator.Instance.TPS; }
            set
            {
                Models.Simulator.Instance.TPS = value;
                RaisePropertyChanged("TPS");
            }
        }

        bool _TPSChangeable;
        public bool TPSChangeable
        {
            get { return _TPSChangeable; }
            set
            {
                _TPSChangeable = value;
                RaisePropertyChanged("TPSChangeable");
            }
        }

        DelegateCommand _OpenFolderCommand;
        public ICommand OpenFolderCommand
        {
            get { return _OpenFolderCommand; }
        }

        DelegateCommand _RefreshCommand;
        public ICommand RefreshCommand
        {
            get { return _RefreshCommand; }
        }

        public DelegateCommand ExportCommand
        {
            get; private set;
        }

        DelegateCommand editCitizenMetaData;
        public ICommand EditCitizenMetaData
        {
            get { return editCitizenMetaData; }
        }

        DelegateCommand _editWorldCommand;
        public ICommand EditWorldCommand
        {
            get { return _editWorldCommand; }
        }

        public DelegateCommand EditObjectCommand
        {
            get; private set;
        }

        DelegateCommand _StartSimulationCommand;
        public ICommand StartSimulationCommand
        {
            get { return _StartSimulationCommand; }
        }

        DelegateCommand _TickCommand;
        public ICommand TickCommand
        {
            get { return _TickCommand; }
        }

        DelegateCommand _RunSimulationCommand;
        public ICommand RunSimulationCommand
        {
            get { return _RunSimulationCommand; }
        }

        public MainViewModel()
        {
            _OpenFolderCommand = new DelegateCommand();
            _OpenFolderCommand.CanExecuteTargets += _OpenFolderCommand_CanExecuteTargets;
            _OpenFolderCommand.ExecuteTargets += _OpenFolderCommand_ExecuteTargets;

            _RefreshCommand = new DelegateCommand();
            _RefreshCommand.CanExecuteTargets += _RefreshCommand_CanExecuteTargets;
            _RefreshCommand.ExecuteTargets += _RefreshCommand_ExecuteTargets;

            ExportCommand = new DelegateCommand();
            ExportCommand.CanExecuteTargets += ExportCommand_CanExecuteTargets;
            ExportCommand.ExecuteTargets += ExportCommand_ExecuteTargets;

            editCitizenMetaData = new DelegateCommand();
            editCitizenMetaData.CanExecuteTargets += EditCitizenMetaData_CanExecute;
            editCitizenMetaData.ExecuteTargets += EditCitizenMetaData_Execute;

            _editWorldCommand = new DelegateCommand();
            _editWorldCommand.CanExecuteTargets += _editWorldCommand_CanExecuteTargets;
            _editWorldCommand.ExecuteTargets += _editWorldCommand_ExecuteTargets;

            EditObjectCommand = new DelegateCommand();
            EditObjectCommand.CanExecuteTargets += EditObjectCommand_CanExecuteTargets;
            EditObjectCommand.ExecuteTargets += EditObjectCommand_ExecuteTargets;

            _StartSimulationCommand = new DelegateCommand();
            _StartSimulationCommand.CanExecuteTargets += _StartSimulationCommand_CanExecuteTargets;
            _StartSimulationCommand.ExecuteTargets += _StartSimulationCommand_ExecuteTargets;

            _TickCommand = new DelegateCommand();
            _TickCommand.CanExecuteTargets += _TickCommand_CanExecuteTargets;
            _TickCommand.ExecuteTargets += _TickCommand_ExecuteTargets;

            _RunSimulationCommand = new DelegateCommand();
            _RunSimulationCommand.CanExecuteTargets += _RunSimulationCommand_CanExecuteTargets;
            _RunSimulationCommand.ExecuteTargets += _RunSimulationCommand_ExecuteTargets;

            TPSChangeable = true;

            Models.Simulator.Instance.PropertyChanged += Simulator_PropertyChanged;
        }

        private bool ExportCommand_CanExecuteTargets()
        {
            return true;
        }

        string characteristicDirectory;
        private void ExportCommand_ExecuteTargets()
        {
            var simulator = Models.Simulator.Instance;
            var needs = (from nsw in simulator.Needs where nsw.Enable select nsw.Target).ToArray();
            var referencedActions = new HashSet<string>();
            foreach (var n in needs)
            {
                foreach (var a in n.ActionInfos)
                {
                    referencedActions.Add(a);
                }
            }
            var actions = (from asw in simulator.Actions
                           //where referencedActions.Contains(asw.Name)
                           select asw.Target).ToArray();


            if (string.IsNullOrWhiteSpace(this.characteristicDirectory))
            {
                this.characteristicDirectory = simulator.WorkingDirectory;
            }

            var filedialog = new Ookii.Dialogs.Wpf.VistaOpenFileDialog();
            filedialog.Multiselect = false;
            filedialog.InitialDirectory = this.characteristicDirectory;

            string characteristicPath = "";
            if (filedialog.ShowDialog() ?? false)
            {
                characteristicPath = filedialog.FileName;
                var fi = new System.IO.FileInfo(characteristicPath);
                characteristicDirectory = fi.DirectoryName;
            }


            var progressbar = new System.Windows.Controls.ProgressBar();
            progressbar.IsIndeterminate = true;
            progressbar.Minimum = 0;
            progressbar.Maximum = 100;
            progressbar.Width = 100;
            progressbar.Height = 20;
            progressbar.Margin = new Thickness(5);
            var dialog = new Dialogs.SingleControlDialog(progressbar);
            //dialog.Title = "Exporting";
            dialog.WindowStyle = WindowStyle.None;
            dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            dialog.ResizeMode = ResizeMode.NoResize;
            dialog.Owner = App.Current.MainWindow;

            System.CodeDom.Compiler.CompilerResults result = null;
            dialog.Loaded += async (s, e) => 
            {
                result = await Exporter.Exporter.ExportAssemblyAsync(
                    simulator.WorkingDirectory + "Export.mqd",
                    simulator.CitizenMetaData,
                    actions,
                    needs,
                    simulator.World,
                    characteristicPath);

                dialog.DialogResult = true;
            };
            dialog.ShowDialog();


            string msg;
            if (result.Errors.HasErrors)
            {
                msg = "Error";
            }
            else
            {
                msg = "Successfully Exported";
            }
            var textblock = new System.Windows.Controls.TextBlock();
            textblock.Text = msg;
            textblock.HorizontalAlignment = HorizontalAlignment.Center;
            textblock.VerticalAlignment = VerticalAlignment.Center;
            textblock.Margin = new Thickness(10);
            var dlg = new Dialogs.SingleControlDialog(textblock);
            dlg.Title = "Result";
            dlg.WindowStyle = WindowStyle.ToolWindow;
            dlg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            dlg.ResizeMode = ResizeMode.NoResize;
            dlg.xOKButton.Visibility = Visibility.Visible;
            dlg.Owner = App.Current.MainWindow;
            dlg.ShowDialog();
        }

        private bool EditObjectCommand_CanExecuteTargets()
        {
            return true;
        }

        private void EditObjectCommand_ExecuteTargets()
        {
            var simulator = Models.Simulator.Instance;

            var vm = new ObjectsViewModel(simulator.Attributes, simulator.Objects);
            var view = new Views.ObjectsView();
            view.DataContext = vm;

            var dialog = new Dialogs.SingleControlDialog(view);
            dialog.SizeToContent = SizeToContent.Manual;
            dialog.Width = 600;
            dialog.Height = 500;

            dialog.ShowDialog();

            {
                foreach (var attr in simulator.Attributes)
                {
                    attr.UpdateProperties();
                }
                foreach (var obj in simulator.Objects)
                {
                    obj.Update(simulator.Attributes.Count);
                }
            }

            Classes.Json.WriteObjectMetas(vm.Properties, vm.Attributes, vm.Objects, simulator.WorkingDirectory);
        }

        private bool _editWorldCommand_CanExecuteTargets()
        {
            return Models.Simulator.Instance.State != Models.Simulator.SimulationStates.Running;
        }

        private void _editWorldCommand_ExecuteTargets()
        {
            var WorldVariables = Models.Simulator.Instance.World.Variables;
            var worldeditor = new Views.WorldEditorView();

            while (true)
            {
                var window = new Dialogs.SingleControlDialog(worldeditor);
                window.SizeToContent = SizeToContent.Manual;
                window.Width = 600;
                window.Height = 500;
                window.ShowDialog();

                var names = new HashSet<string>(from v in WorldVariables select v.Name);
                if (names.Count == WorldVariables.Count) { break; }

                window.xGrid.Children.Remove(worldeditor);
            }

            foreach (var v in WorldVariables)
            {
                break;
                switch(v.Type)
                {
                    case "int":
                        v.Value = v.Cast<int>() ?? new Models.ValueHolder<int>("int", 0);
                        break;
                    case "float":
                        v.Value = v.Cast<float>() ?? new Models.ValueHolder<float>("float", 0f);
                        break;
                    case "bool":
                        v.Value = v.Cast<bool>() ?? new Models.ValueHolder<bool>("bool", false);
                        break;
                    case "string":
                        v.Value = v.Cast<string>() ?? new Models.ValueHolder<string>("string", "");
                        break;
                }
            }

            Models.Simulator.Instance.World.UpdateVariableTable();

            if (WorldVariables.Count > 0)
            {
                Classes.Json.WriteWorld(Models.Simulator.Instance.World, Models.Simulator.Instance.WorkingDirectory);
            }
        }

        private bool _OpenFolderCommand_CanExecuteTargets()
        {
            return Models.Simulator.Instance.State == Models.Simulator.SimulationStates.Stopped;
        }

        private void _OpenFolderCommand_ExecuteTargets()
        {
            /*
            var dialog = new OpenDialogView();
            var vm = (OpenDialogViewModel)dialog.DataContext;
            vm.IsDirectoryChooser = true;

            if (vm.Show() ?? false)
            {
                string dir = vm.SelectedFolder.Path;
                Models.Simulator.Instance.Load(dir + @"\");
            }*/

            var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            dialog.SelectedPath = Models.Simulator.Instance.WorkingDirectory;

            if (dialog.ShowDialog() ?? false)
            {
                string dir = dialog.SelectedPath;
                Models.Simulator.Instance.Load(dir + @"\");
            }
        }

        private bool _RefreshCommand_CanExecuteTargets()
        {
            return Models.Simulator.Instance.State == Models.Simulator.SimulationStates.Stopped;
        }

        private void _RefreshCommand_ExecuteTargets()
        {
            Models.Simulator.Instance.Refresh();
        }

        bool EditCitizenMetaData_CanExecute()
        {
            return Models.Simulator.Instance.State != Models.Simulator.SimulationStates.Running;
        }

        void EditCitizenMetaData_Execute()
        {
            var dialog = new Views.CitizenMetaEditorWindow();
            var vm = (dialog.xView.DataContext as CitizenMetaDataEditorViewModel);
            vm.Parameters = new ObservableCollection<Models.CitizenMetaData.ParameterMeta>(Models.Simulator.Instance.CitizenMetaData.Parameters);
            dialog.ShowDialog();

            //Models.Simulator.Instance.CitizenMetaData.UpdateParameterNameTable();
            var parameters = (dialog.xView.DataContext as CitizenMetaDataEditorViewModel).Parameters;
            Models.Simulator.Instance.UpdateCitizenParameters(parameters);
            //Models.Simulator.Instance.CitizenMetaData.UpdateParameters(parameters);

            Classes.Json.WriteCitizenMetaData(Models.Simulator.Instance.CitizenMetaData, Models.Simulator.Instance.WorkingDirectory);
        }

        private void _StartSimulationCommand_ExecuteTargets()
        {
            if (Models.Simulator.Instance.State == Models.Simulator.SimulationStates.Stopped)
            {
                try
                {
                    Models.Simulator.Instance.InitSimulation();
                }
                catch (Classes.CompilerException e)
                {
                    MessageBox.Show(e.InnerException.Message, "Compile Error on " + e.Message);
                }
            }
            else
            {
                Models.Simulator.Instance.StopSimulation();
            }
        }

        private bool _StartSimulationCommand_CanExecuteTargets()
        {
            return Models.Simulator.Instance.State != Models.Simulator.SimulationStates.Running;
        }

        private void _TickCommand_ExecuteTargets()
        {
            try
            {
                Models.Simulator.Instance.NextTick();
            }
            catch (Classes.CompilerException e)
            {
                MessageBox.Show(e.InnerException.Message, "Compile Error on " + e.Message);
            }
        }

        private bool _TickCommand_CanExecuteTargets()
        {
            return Models.Simulator.Instance.State == Models.Simulator.SimulationStates.Paused;
        }

        private void _RunSimulationCommand_ExecuteTargets()
        {
            if (Models.Simulator.Instance.State == Models.Simulator.SimulationStates.Paused)
            {
                TPSChangeable = false;

                try
                {
                    Models.Simulator.Instance.ResumeSimulation();
                }
                catch (Classes.CompilerException e)
                {
                    MessageBox.Show(e.InnerException.Message, "Compile Error on " + e.Message);
                }
            }
            else
            {
                TPSChangeable = true;
                Models.Simulator.Instance.PauseSimulation();
            }
        }

        private bool _RunSimulationCommand_CanExecuteTargets()
        {
            return Models.Simulator.Instance.State != Models.Simulator.SimulationStates.Stopped;
        }

        private void Simulator_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "World")
            {
                RaisePropertyChanged("WorldVariables");
            }
        }
    }
}
