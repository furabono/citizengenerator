﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.ViewModels
{
    public class InventoryEditorViewModel : ViewModelBase
    {
        ObservableCollection<Models.AttributeMeta> _rootAttribute;
        public ObservableCollection<Models.AttributeMeta> RootAttribute
        {
            get { return _rootAttribute; }
            set
            {
                _rootAttribute = value;
                RaisePropertyChanged("RootAttribute");
            }
        }

        Classes.InventoryEntry _selectedObject;
        public Classes.InventoryEntry SelectedObject
        {
            get { return _selectedObject; }
            set
            {
                _selectedObject = value;
                RaisePropertyChanged("SelectedObject");
            }
        }

        Classes.Inventory _items;
        public Classes.Inventory Items
        {
            get { return _items; }
            set
            {
                _items = value;
                RaisePropertyChanged("Items");
            }
        }

        public DelegateCommand<object> AddObjectCommand { get; set; }
        public DelegateCommand RemoveObjectCommand { get; set; }

        public InventoryEditorViewModel(ObservableCollection<Models.AttributeMeta> rootAtribute, Classes.Inventory inventory)
        {
            _rootAttribute = rootAtribute;
            _items = inventory;

            AddObjectCommand = new DelegateCommand<object>();
            AddObjectCommand.CanExecuteTargets += AddObjectCommand_CanExecuteTargets;
            AddObjectCommand.ExecuteTargets += AddObjectCommand_ExecuteTargets;

            RemoveObjectCommand = new DelegateCommand();
            RemoveObjectCommand.CanExecuteTargets += RemoveObjectCommand_CanExecuteTargets;
            RemoveObjectCommand.ExecuteTargets += RemoveObjectCommand_ExecuteTargets;
        }

        private bool AddObjectCommand_CanExecuteTargets()
        {
            return true;
        }

        private void AddObjectCommand_ExecuteTargets(object obj)
        {
            var objmeta = (Models.ObjectMeta)obj;

            Items.AddItem(objmeta);
        }

        private bool RemoveObjectCommand_CanExecuteTargets()
        {
            return SelectedObject != null;
        }

        private void RemoveObjectCommand_ExecuteTargets()
        {
            Items.RemoveItem(SelectedObject.Object);
        }
    }
}
