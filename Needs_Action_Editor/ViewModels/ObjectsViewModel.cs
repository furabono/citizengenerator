﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Property = Needs_Action_Editor.Models.Property;
using Attribute = Needs_Action_Editor.Models.Attribute;
using Object = Needs_Action_Editor.Models.Object;
using IAttribute = Needs_Action_Editor.Models.IAttribute;
using AttributeMeta = Needs_Action_Editor.Models.AttributeMeta;
using ObjectMeta = Needs_Action_Editor.Models.ObjectMeta;
using IAttributeMeta = Needs_Action_Editor.Models.IAttributeMeta;
using INamed = Needs_Action_Editor.Models.INamed;
using BitmapImage = System.Windows.Media.Imaging.BitmapImage;
using ImageCache = Needs_Action_Editor.Classes.Cache<Needs_Action_Editor.Models.ObjectMeta, System.Windows.Media.Imaging.BitmapImage>;

namespace Needs_Action_Editor.ViewModels
{
    class ObjectsViewModel : ViewModelBase
    {
        Models.Simulator Simulator
        {
            get { return Models.Simulator.Instance; }
        }

        public ObservableCollection<Property> Properties
        {
            get { return Property.Properties; }
        }

        public Property SelectedProperty
        {
            get; set;
        }

        public Property SelectedOwnProperty
        {
            get; set;
        }

        ObservableCollection<AttributeMeta> _rootAttribute;
        public ObservableCollection<AttributeMeta> RootAttribute
        {
            get { return _rootAttribute; }
            private set { _rootAttribute = value; }
        }

        IAttributeMeta _selectedItem;
        public IAttributeMeta SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                RaisePropertyChanged("SelectedItem");
            }
        }

        ImageCache _imageCache;
        BitmapImage _image;
        public BitmapImage Image
        {
            get { return _image; }
            set
            {
                _image = value;
                RaisePropertyChanged("Image");
            }
        }

        public ObservableCollection<AttributeMeta> Attributes { get; set; }
        public ObservableCollection<ObjectMeta> Objects { get; set; }

        #region Commands
        public DelegateCommand NewPropertyCommand
        {
            get; private set;
        }

        public DelegateCommand DeletePropertyCommand
        {
            get; private set;
        }

        public DelegateCommand AddAttributeCommand
        {
            get; private set;
        }

        public DelegateCommand RemovePropertyCommand
        {
            get; private set;
        }

        public DelegateCommand AddObjectCommand
        {
            get; private set;
        }

        public DelegateCommand DeleteItemCommand
        {
            get; private set;
        }

        public DelegateCommand ObjectLoadedCommand
        {
            get; private set;
        }

        public DelegateCommand OpenImageCommand
        {
            get; private set;
        }

        public DelegateCommand EditNameCommand
        {
            get; private set;
        }
        #endregion

        public bool Modified { get; set; }

        public ObjectsViewModel(ObservableCollection<AttributeMeta> attributes, ObservableCollection<ObjectMeta> objects, ImageCache imageCache = null)
        {
            Attributes = attributes;
            Objects = objects;

            RootAttribute = new ObservableCollection<AttributeMeta>()
            {
                Attributes[0]
            };

            _imageCache = imageCache;
            if (_imageCache == null)
            {
                _imageCache = new ImageCache(5);
            }

            NewPropertyCommand = new DelegateCommand();
            NewPropertyCommand.CanExecuteTargets += NewPropertyCommand_CanExecuteTargets;
            NewPropertyCommand.ExecuteTargets += NewPropertyCommand_ExecuteTargets;

            DeletePropertyCommand = new DelegateCommand();
            DeletePropertyCommand.CanExecuteTargets += DeletePropertyCommand_CanExecuteTargets;
            DeletePropertyCommand.ExecuteTargets += DeletePropertyCommand_ExecuteTargets;

            AddAttributeCommand = new DelegateCommand();
            AddAttributeCommand.CanExecuteTargets += AddAttributeCommand_CanExecuteTargets;
            AddAttributeCommand.ExecuteTargets += AddAttributeCommand_ExecuteTargets;

            RemovePropertyCommand = new DelegateCommand();
            RemovePropertyCommand.CanExecuteTargets += RemovePropertyCommand_CanExecuteTargets;
            RemovePropertyCommand.ExecuteTargets += RemovePropertyCommand_ExecuteTargets;

            AddObjectCommand = new DelegateCommand();
            AddObjectCommand.CanExecuteTargets += AddObjectCommand_CanExecuteTargets;
            AddObjectCommand.ExecuteTargets += AddObjectCommand_ExecuteTargets;

            DeleteItemCommand = new DelegateCommand();
            DeleteItemCommand.ExecuteTargets += DeleteItemCommand_ExecuteTargets;
            DeleteItemCommand.CanExecuteTargets += DeleteItemCommand_CanExecuteTargets;

            ObjectLoadedCommand = new DelegateCommand();
            ObjectLoadedCommand.CanExecuteTargets += ObjectLoadedCommand_CanExecuteTargets;
            ObjectLoadedCommand.ExecuteTargets += ObjectLoadedCommand_ExecuteTargets;

            OpenImageCommand = new DelegateCommand();
            OpenImageCommand.CanExecuteTargets += OpenImageCommand_CanExecuteTargets;
            OpenImageCommand.ExecuteTargets += OpenImageCommand_ExecuteTargets;

            EditNameCommand = new DelegateCommand();
            EditNameCommand.CanExecuteTargets += EditNameCommand_CanExecuteTargets;
            EditNameCommand.ExecuteTargets += EditNameCommand_ExecuteTargets;

            Modified = false;
        }

        private bool EditNameCommand_CanExecuteTargets()
        {
            return _selectedItem != null && _selectedItem != _rootAttribute[0];
        }

        private void EditNameCommand_ExecuteTargets()
        {
            var dialog = new Dialogs.InputDialog("Input Name", "");
            dialog.WindowStyle = System.Windows.WindowStyle.ToolWindow;
            dialog.Input = _selectedItem.Name;

            if (dialog.ShowDialog() ?? false)
            {
                var name = dialog.Input;

                if (_selectedItem.Name == name) { return; }

                if (_selectedItem is ObjectMeta)
                {
                    //var obj = (ObjectMeta)_selectedItem;

                    foreach (var obj in Objects)
                    {
                        if (obj.Name == _selectedItem.Name) { return; }
                    }

                    string path = Simulator.WorkingDirectory + @"Resources\ObjectImages\" + _selectedItem.Name + ".png";
                    if (System.IO.File.Exists(path))
                    {
                        string newpath = Simulator.WorkingDirectory + @"Resources\ObjectImages\" + name + ".png";
                        System.IO.File.Move(path, newpath);
                    }
                }
                else
                {
                    foreach (var attr in Attributes)
                    {
                        if (attr.Name == _selectedItem.Name) { return; }
                    }
                }

                _selectedItem.Name = name;
            }
        }

        public void ApplyChanges()
        {
            for (int i = 0; i < Properties.Count; ++i)
            {
                Properties[i].ID = i;
            }

            for (int i = 0; i < Attributes.Count; ++i)
            {
                Attributes[i].ID = i;
                Attributes[i].UpdateProperties();
            }

            for (int i = 0; i < Objects.Count; ++i)
            {
                if (Objects[i].ID != i)
                {
                    //ModifiedObjects.Add(new Tuple<int, int>(Objects[i].ID, i));
                    Objects[i].ID = i;
                    Objects[i].Update(Attributes.Count);
                }
            }
        }

        private bool NewPropertyCommand_CanExecuteTargets()
        {
            return true;
        }

        private void NewPropertyCommand_ExecuteTargets()
        {
            string name = "newproperty" + ((Properties.Any()) ? (Properties.Last().ID + 1) : (0));
            var p = new Property(Properties.Count, name);
            Properties.Add(p);
        }

        private bool DeletePropertyCommand_CanExecuteTargets()
        {
            return SelectedProperty != null;
        }

        private void DeletePropertyCommand_ExecuteTargets()
        {
            RemoveProperty(RootAttribute[0], SelectedProperty);
            Properties.Remove(SelectedProperty);

            //for (int i = 0; i < Properties.Count; ++i)
            //{
            //    Properties[i].ID = i;
            //}
        }

        void RemoveProperty(AttributeMeta attribute, Property property)
        {
            if (!attribute.RemoveProperty(property))
            {
                foreach (var c in attribute.Children)
                {
                    if (c is AttributeMeta)
                    {
                        RemoveProperty((AttributeMeta)c, property);
                    }
                }
            }
        }

        private bool AddAttributeCommand_CanExecuteTargets()
        {
            return _selectedItem != null;
        }

        private void AddAttributeCommand_ExecuteTargets()
        {
            var parent = _selectedItem as AttributeMeta ?? (_selectedItem as ObjectMeta).Parent;
            var attr = new AttributeMeta(Attributes.Count, "newattr" + Attributes.Count, parent);
            Attributes.Add(attr);
        }

        private bool RemovePropertyCommand_CanExecuteTargets()
        {
            return _selectedItem is AttributeMeta && SelectedOwnProperty != null;
        }

        private void RemovePropertyCommand_ExecuteTargets()
        {
            var attr = (AttributeMeta)_selectedItem;
            attr.OwnProperties.Remove(SelectedOwnProperty);
        }

        private bool AddObjectCommand_CanExecuteTargets()
        {
            return _selectedItem != null;
        }

        private void AddObjectCommand_ExecuteTargets()
        {
            var parent = _selectedItem as AttributeMeta ?? (_selectedItem as ObjectMeta).Parent;
            var obj = new ObjectMeta(Objects.Count, "newobj" + Objects.Count, parent, 1, 1);
            Objects.Add(obj);
        }

        private bool DeleteItemCommand_CanExecuteTargets()
        {
            return _selectedItem != null && _selectedItem.Parent != null;
        }

        private void DeleteItemCommand_ExecuteTargets()
        {
            if (_selectedItem is AttributeMeta)
            {
                DeleteAttribute((AttributeMeta)_selectedItem);
            }
            else
            {
                var obj = (ObjectMeta)_selectedItem;

                Objects.Remove(obj);
                _selectedItem.Parent.Children.Remove(_selectedItem);

                string path = Simulator.WorkingDirectory + @"Resources\ObjectImages\" + obj.Name + ".png";
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    _imageCache.Remove(obj);
                }

                Simulator.CitizenMetaData.Items.RemoveAll(obj);
                foreach (var citizen in Simulator.Citizens)
                {
                    citizen.Items.RemoveAll(obj);
                }

                Simulator.World.InitObjects.RemoveAll(obj);
                Simulator.World.Objects.RemoveAll(obj);
            }

            //if (Attributes.Last().ID != Attributes.Count)
            //{
            //    for (int i = 0; i < Attributes.Count; ++i)
            //    {
            //        Attributes[i].ID = i;
            //    }
            //}
        }

        void DeleteAttribute(AttributeMeta item)
        {
            foreach (var c in item.Children)
            {
                if (c is AttributeMeta)
                {
                    DeleteAttribute((AttributeMeta)c);
                }
                else
                {
                    var obj = (ObjectMeta)c;
                    Objects.Remove(obj);
                    _imageCache.Remove(obj);

                    Simulator.CitizenMetaData.Items.RemoveAll(obj);
                    foreach (var citizen in Simulator.Citizens)
                    {
                        citizen.Items.RemoveAll(obj);
                    }

                    Simulator.World.InitObjects.RemoveAll(obj);
                    Simulator.World.Objects.RemoveAll(obj);
                }
            }

            item.Children.Clear();
            Attributes.Remove(item);
            item.Parent.Children.Remove(item);
        }

        private bool ObjectLoadedCommand_CanExecuteTargets()
        {
            return _selectedItem is ObjectMeta;
        }

        private void ObjectLoadedCommand_ExecuteTargets()
        {
            var obj = (ObjectMeta)_selectedItem;
            
            if (_imageCache.ContainsKey(obj))
            {
                Image = _imageCache.Get(obj);
            }
            else
            {
                string path = Simulator.WorkingDirectory + @"Resources\ObjectImages\" + obj.Name + ".png";
                var image = LoadImage(path);
                _imageCache.Add(obj, image);
                Image = image;
            }
        }

        private bool OpenImageCommand_CanExecuteTargets()
        {
            return true;
        }

        string _directory;
        private void OpenImageCommand_ExecuteTargets()
        {
            var obj = (ObjectMeta)_selectedItem;

            var filedialog = new Ookii.Dialogs.Wpf.VistaOpenFileDialog();
            filedialog.Multiselect = false;
            filedialog.InitialDirectory = _directory ?? Simulator.WorkingDirectory;

            if (filedialog.ShowDialog() ?? false)
            {
                string path = filedialog.FileName;

                var image = LoadImage(path);
                _imageCache.Add(obj, image);
                Image = image;

                var encoder = new System.Windows.Media.Imaging.PngBitmapEncoder();
                encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(Image));

                if (!System.IO.Directory.Exists(Simulator.WorkingDirectory + @"Resources\ObjectImages"))
                {
                    System.IO.Directory.CreateDirectory(Simulator.WorkingDirectory + @"Resources\ObjectImages");
                }

                string import = Simulator.WorkingDirectory + @"Resources\ObjectImages\" + obj.Name + ".png";
                using (var stream = new System.IO.FileStream(import, System.IO.FileMode.Create))
                {
                    encoder.Save(stream);
                }

                var fileinfo = new System.IO.FileInfo(path);
                _directory = fileinfo.DirectoryName;
            }
        }

        BitmapImage LoadImage(string path)
        {
            if (System.IO.File.Exists(path))
            {
                var image = new BitmapImage();
                using (var stream = System.IO.File.OpenRead(path))
                {
                    image.BeginInit();
                    image.StreamSource = stream;
                    image.CacheOption = System.Windows.Media.Imaging.BitmapCacheOption.OnLoad;
                    image.EndInit();
                }

                return image;
            }

            return null;
        }
    }
}
