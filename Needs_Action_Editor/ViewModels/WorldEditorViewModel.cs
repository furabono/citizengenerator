﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.ViewModels
{
    class WorldEditorViewModel : ViewModelBase
    {
        //ObservableCollection<string> _types;
        public ObservableCollection<string> Types
        {
            get { return Models.Simulator.Instance.World.Types; }
        }

        public ObservableCollection<Models.Variable> Variables
        {
            get { return Models.Simulator.Instance.World.Variables; }
            set
            {
                Models.Simulator.Instance.World.Variables = value;
            }
        }

        Models.Variable _selectedVariable;
        public Models.Variable SelectedVariable
        {
            get { return _selectedVariable; }
            set
            {
                _selectedVariable = value;
                RaisePropertyChanged("SelectedVariable");
            }
        }

        public Models.WorldObjectCollection Objects
        {
            get { return World.InitObjects; }
        }

        public Models.WorldObject SelectedObject
        {
            get; set;
        }

        public ObservableCollection<Models.AttributeMeta> RootAttribute
        {
            get; set;
        }

        public string Script
        {
            get { return Models.Simulator.Instance.World.Script; }
            set
            {
                Models.Simulator.Instance.World.Script = value;
                //RaisePropertyChanged("Script");
            }
        }

        public Models.World World
        {
            get { return Models.Simulator.Instance.World; }
        }

        DelegateCommand _newCommand;
        public DelegateCommand NewCommand
        {
            get { return _newCommand; }
        }

        DelegateCommand _deleteCommand;
        public DelegateCommand DeleteCommand
        {
            get { return _deleteCommand; }
        }

        public DelegateCommand<object> AddObjectCommand
        {
            get; set;
        }

        public DelegateCommand RemoveObjectCommand
        {
            get; set;
        }

        public WorldEditorViewModel()
        {
            //_types = new ObservableCollection<string>
            //{
            //    "int", "float", "bool", "string"
            //};

            RootAttribute = new ObservableCollection<Models.AttributeMeta>()
            {
                Models.Simulator.Instance.Attributes[0]
            };

            _newCommand = new DelegateCommand();
            _newCommand.CanExecuteTargets += _newCommand_CanExecuteTargets;
            _newCommand.ExecuteTargets += _newCommand_ExecuteTargets;

            _deleteCommand = new DelegateCommand();
            _deleteCommand.CanExecuteTargets += _deleteCommand_CanExecuteTargets;
            _deleteCommand.ExecuteTargets += _deleteCommand_ExecuteTargets;

            AddObjectCommand = new DelegateCommand<object>();
            AddObjectCommand.CanExecuteTargets += AddObjectCommand_CanExecuteTargets;
            AddObjectCommand.ExecuteTargets += AddObjectCommand_ExecuteTargets;

            RemoveObjectCommand = new DelegateCommand();
            RemoveObjectCommand.CanExecuteTargets += RemoveObjectCommand_CanExecuteTargets;
            RemoveObjectCommand.ExecuteTargets += RemoveObjectCommand_ExecuteTargets;
        }

        private bool _newCommand_CanExecuteTargets()
        {
            return true;
        }

        private void _newCommand_ExecuteTargets()
        {
            Variables.Add(new Models.Variable("NAME", "int", null));
        }

        private bool _deleteCommand_CanExecuteTargets()
        {
            return SelectedVariable != null;
        }

        private void _deleteCommand_ExecuteTargets()
        {
            Variables.Remove(SelectedVariable);
        }

        private bool AddObjectCommand_CanExecuteTargets()
        {
            return true;
        }

        private void AddObjectCommand_ExecuteTargets(object obj)
        {
            var objmeta = (Models.ObjectMeta)obj;

            Objects.PutObject(objmeta, new Models.Vector2(0f, 0f));
        }

        private bool RemoveObjectCommand_CanExecuteTargets()
        {
            return SelectedObject != null;
        }

        private void RemoveObjectCommand_ExecuteTargets()
        {
            Objects.List.Remove(SelectedObject);
        }
    }
}
