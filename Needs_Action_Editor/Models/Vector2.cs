﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Models
{
    public class Vector2 : ModelBase
    {
        float _x;
        float _y;

        public float x
        {
            get { return _x; }
            set
            {
                _x = value;
                RaisePropertyChanged("x");
            }
        }

        public float y
        {
            get { return _y; }
            set
            {
                _y = value;
                RaisePropertyChanged("y");
            }
        }

        public float Length
        {
            get { return (float)Math.Sqrt(_x * _x + _y * _y); }
        }

        public Vector2(float x, float y)
        {
            _x = x;
            _y = y;
        }

        public Vector2() : this(0, 0) { }

        public Vector2(Vector2 v) : this(v._x, v._y) { }

        public static Vector2 operator -(Vector2 left, Vector2 right)
        {
            return new Vector2(left.x - right.x, left.y - right.y);
        }

        public static Vector2 operator +(Vector2 left, Vector2 right)
        {
            return new Vector2(left.x + right.x, left.y + right.y);
        }

        public static Vector2 operator *(Vector2 vector, int scalar)
        {
            return new Vector2(vector.x * scalar, vector.y * scalar);
        }

        public static Vector2 operator /(Vector2 vector, int scalar)
        {
            return new Vector2(vector.x / scalar, vector.y / scalar);
        }
    }
}
