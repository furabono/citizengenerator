﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Models
{
    public partial class CitizenMetaData
    {
        public class ParameterMeta : ModelBase
        {
            string _name;
            public string Name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged("Name");
                }
            }

            float _initialValue;
            public float InitialValue
            {
                get { return _initialValue; }
                set
                {
                    _initialValue = value;
                    RaisePropertyChanged("InitialValue");
                }
            }

            public ParameterMeta(string name) : this(name, 0f) { }

            public ParameterMeta(string name, float initialValue)
            {
                _name = name;
                _initialValue = initialValue;
            }
        }
    }
}
