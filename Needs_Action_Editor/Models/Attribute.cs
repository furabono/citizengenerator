﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Models
{
    public interface IAttribute
    {
        int ID { get; }
        string Name { get; }
        Attribute Parent { get; }
    }

    public class Attribute : IAttribute
    {
        #region static
        public static ObservableCollection<Attribute> Attributes
        {
            get; set;
        }

        static Attribute()
        {
            Attributes = new ObservableCollection<Attribute>();
        }
        #endregion

        public int ID
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public ObservableCollection<Property> OwnProperties
        {
            get; private set;
        }

        public BitArray Properties
        {
            get; private set;
        }

        public Attribute Parent
        {
            get; private set;
        }

        public ObservableCollection<IAttribute> Children
        {
            /*get
            {
                IEnumerable<object> attrs = (from attr in Attribute.Attributes
                                             where attr.Parent == this
                                             select (object)attr);
                IEnumerable<object> objs = (from obj in Object.Objects
                                            where obj.Parent == this
                                            select (object)obj);
                return new ObservableCollection<object>(attrs.Concat(objs));
            }*/
            get; private set;
        }

        public Attribute(int id, string name, Attribute parent, ObservableCollection<Property> ownProperties)
        {
            ID = id;
            Name = name;
            Parent = parent;
            OwnProperties = ownProperties;
            Children = new ObservableCollection<IAttribute>();

            UpdateProperties();
        }

        public Attribute(int id, string name, Attribute parent, IEnumerable<Property> ownProperties)
            : this(id, name, parent, new ObservableCollection<Property>(ownProperties)) { }

        public Attribute(int id, string name, Attribute parent)
            : this(id, name, parent, new ObservableCollection<Property>()) { }

        public void UpdateProperties()
        {
            if (Properties?.Length != Property.Properties.Count)
            {
                Properties = new BitArray(Property.Properties.Count);
            }

            Properties.SetAll(false);
            if (Parent != null)
            {
                Properties.Or(Parent.Properties);
            }
            
            foreach (var p in OwnProperties)
            {
                Properties.Set(p.ID, true);
            }
        }
    }
}
