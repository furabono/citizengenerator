﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Models
{
    public interface INamed
    {
        string Name { get; set; }
    }

    public interface IAttributeMeta : INamed
    {
        int ID { get; set; }
        AttributeMeta Parent { get; set; }
        bool Modified { get; set; }
    }

    public class AttributeMeta : ModelBase, IAttributeMeta
    {
        public int ID
        {
            get; set;
        }

        string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged("Name");

                Modified = true;
            }
        }

        public bool Modified { get; set; }

        public AttributeMeta Parent
        {
            get; set;
        }

        public ObservableCollection<Property> OwnProperties
        {
            get; private set;
        }

        public ObservableCollection<Property> InheritedProperties
        {
            get
            {
                var list = new List<Property>();
                AttributeMeta parent = Parent;

                while (parent != null)
                {
                    foreach (var p in parent.OwnProperties)
                    {
                        list.Add(p);
                    }

                    parent = parent.Parent;
                }

                list.Sort((x, y) => x.ID - y.ID);
                return new ObservableCollection<Property>(list);
            }
        }

        public BitArray Properties
        {
            get; private set;
        }

        public ObservableCollection<IAttributeMeta> Children
        {
            get; private set;
        }

        public AttributeMeta(int id, string name, AttributeMeta parent, ObservableCollection<Property> ownProperties)
        {
            ID = id;
            Name = name;
            Parent = parent;
            OwnProperties = ownProperties;

            Children = new ObservableCollection<IAttributeMeta>();

            //UpdateProperties();

            parent?.Children.Add(this);
        }

        public AttributeMeta(int id, string name, AttributeMeta parent)
            : this(id, name, parent, new ObservableCollection<Property>()) { }

        public void UpdateProperties()
        {
            if (Properties?.Length != Property.Properties.Count)
            {
                Properties = new BitArray(Property.Properties.Count);
            }

            Properties.SetAll(false);
            if (Parent != null)
            {
                Properties.Or(Parent.Properties);
            }

            foreach (var p in OwnProperties)
            {
                Properties.Set(p.ID, true);
            }
        }

        public void AddProperty(Property property)
        {
            if (OwnProperties.Contains(property))
            {
                return;
            }

            AttributeMeta parent = Parent;
            while (parent != null)
            {
                if (parent.OwnProperties.Contains(property))
                {
                    return;
                }
                parent = parent.Parent;
            }

            OwnProperties.Add(property);
            //Properties.Set(property.ID, true);
        }

        public bool RemoveProperty(Property property)
        {
            return OwnProperties.Remove(property);
            //Properties.Set(property.ID, false);
        }
    }
}
