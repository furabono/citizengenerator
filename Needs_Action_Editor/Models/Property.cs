﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Models
{
    public class Property : ModelBase
    {
        #region static
        public static ObservableCollection<Property> Properties
        {
            get; set;
        }

        static Property()
        {
            Properties = new ObservableCollection<Property>();
        }
        #endregion

        public int ID
        {
            get; set;
        }

        string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged("Name");
            }
        }

        public Property(int id, string name)
        {
            ID = id;
            _name = name;
        }
    }
}
