﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Needs_Action_Editor.Models
{
    public class ActionSourceWrapper : ModelBase, Controls.IHierarchicalItem
    {
        static ImageSource icon;

        static ActionSourceWrapper()
        {
            var assembly = Assembly.GetExecutingAssembly();

            BitmapImage src = new BitmapImage();
            src.BeginInit();
            src.StreamSource = assembly.GetManifestResourceStream("Needs_Action_Editor.Resources.Icons.Document_16x.png");
            src.EndInit();

            icon = src;
        }

        public ImageSource Icon
        {
            get { return icon; }
        }

        Classes.ActionSource target;
        public Classes.ActionSource Target
        {
            get { return target; }
            set
            {
                target = value;
                RaisePropertyChanged("Target");
                RaisePropertyChanged("Name");
            }
        }

        public string Name
        {
            get { return target.Name; }
            set
            {
                target.Name = value;
                RaisePropertyChanged("Name");
            }
        }

        public ActionSourceWrapper(Classes.ActionSource target)
        {
            this.target = target;
        }

        public Controls.IParentItem Parent
        {
            get; set;
        }
    }
}
