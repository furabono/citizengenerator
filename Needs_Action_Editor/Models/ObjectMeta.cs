﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Needs_Action_Editor.Models
{
    public class ObjectMeta : ModelBase, IAttributeMeta
    {
        public int ID { get; set; }

        string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged("Name");

                Modified = true;
            }
        }

        float _width;
        public float Width
        {
            get { return _width; }
            set
            {
                _width = value;
                RaisePropertyChanged("Width");
            }
        }

        float _height;
        public float Height
        {
            get { return _height; }
            set
            {
                _height = value;
                RaisePropertyChanged("Height");
            }
        }

        public bool Modified { get; set; }

        public AttributeMeta Parent
        {
            get; set;
        }

        public BitArray Attributes
        {
            get; private set;
        }

        public ObjectMeta(int id, string name, AttributeMeta parent, float width, float height)
        {
            ID = id;
            Name = name;
            _width = width;
            _height = height;

            Parent = parent;
            parent.Children.Add(this);
        }

        public void Update(int AttributeSize)
        {
            if (Attributes?.Count != AttributeSize)
            {
                Attributes = new BitArray(AttributeSize);
            }
            Attributes.SetAll(false);

            var parent = Parent;
            while (parent != null)
            {
                Attributes.Set(parent.ID, true);
                parent = parent.Parent;
            }
        }
    }
}
