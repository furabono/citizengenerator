﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Needs_Action_Editor.Classes;
using System.ComponentModel;

using System.Windows;
using System.Windows.Threading;
using System.IO;

namespace Needs_Action_Editor.Models
{
    public class Simulator : ModelBase
    {
        #region Singleton
        static Simulator instance;
        public static Simulator Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Simulator();
                }
                return instance;
            }
        }
        #endregion

        string workingDirectory;
        public string WorkingDirectory
        {
            get { return workingDirectory; }
        }

        ObservableCollection<NeedSourceWrapper> needs;
        public ObservableCollection<NeedSourceWrapper> Needs
        {
            get { return needs; }
            private set
            {
                needs = value;
                RaisePropertyChanged("Needs");
            }
        }

        Directory _needsRoot;
        public Directory NeedsRoot
        {
            get { return _needsRoot; }
            private set
            {
                _needsRoot = value;
                NeedsHierarchies = _needsRoot.Children;

                RaisePropertyChanged("NeedsRoot");
            }
        }

        ObservableCollection<Controls.IHierarchicalItem> _needsHierarchies;
        public ObservableCollection<Controls.IHierarchicalItem> NeedsHierarchies
        {
            get { return _needsHierarchies; }
            private set
            {
                _needsHierarchies = value;
                RaisePropertyChanged("NeedsHierarchies");
            }
        }

        ObservableCollection<ActionSourceWrapper> actions;
        public ObservableCollection<ActionSourceWrapper> Actions
        {
            get { return actions; }
            private set
            {
                actions = value;
                RaisePropertyChanged("Actions");
            }
        }

        Directory _actionsRoot;
        public Directory ActionsRoot
        {
            get { return _actionsRoot; }
            private set
            {
                _actionsRoot = value;
                ActionsHierarchies = _actionsRoot.Children;

                RaisePropertyChanged("ActionsRoot");
            }
        }

        ObservableCollection<Controls.IHierarchicalItem> _actionsHierarchies;
        public ObservableCollection<Controls.IHierarchicalItem> ActionsHierarchies
        {
            get { return _actionsHierarchies; }
            private set
            {
                _actionsHierarchies = value;
                RaisePropertyChanged("ActionsHierarchies");
            }
        }

        ObservableCollection<Citizen> citizens;
        public ObservableCollection<Citizen> Citizens
        {
            get { return citizens; }
            set
            {
                citizens = value;
                RaisePropertyChanged("Citizens");
            }
        }

        CitizenMetaData citizenMetaData;
        public CitizenMetaData CitizenMetaData
        {
            get { return citizenMetaData; }
            set
            {
                citizenMetaData = value;
                RaisePropertyChanged("CitizenMetaData");
            }
        }

        public event PropertyChangedEventHandler CitizenMetaDataPropertyChanged;

        //ObservableCollection<AttributeMeta> _attributes;
        public ObservableCollection<AttributeMeta> Attributes
        {
            get; set;
        }

        public ObservableCollection<ObjectMeta> Objects
        {
            get; set;
        }

        Simulator()
        {
            _needsHierarchies = new ObservableCollection<Controls.IHierarchicalItem>();
            _actionsHierarchies = new ObservableCollection<Controls.IHierarchicalItem>();
            needs = new ObservableCollection<NeedSourceWrapper>();
            actions = new ObservableCollection<ActionSourceWrapper>();
            citizens = new ObservableCollection<Citizen>();

            Load(AppDomain.CurrentDomain.BaseDirectory + @"Data\");
            needs.CollectionChanged += Needs_CollectionChanged;

            needInfos = new List<NeedInfo>();
            ModifiedActionSources = new HashSet<ActionSource>();

            //compiler = new Compiler(citizenMetaData);
            //compiler.CompileMetabolism();

            timer = new DispatcherTimer();
            timer.Tick += Timer_Tick;

            State = SimulationStates.Stopped;
            _TPS = 60;

            //World = new World();
        }

        public void Load(string workingDirectory)
        {
            this.workingDirectory = workingDirectory;

            NeedsHierarchies.Clear();
            needs.Clear();
            ActionsHierarchies.Clear();
            actions.Clear();

            if (System.IO.Directory.Exists(workingDirectory + Json.NeedSourcePath))
            {
                var di = new DirectoryInfo(workingDirectory + Json.NeedSourcePath);

                NeedsRoot = LoadNeedsRecursive(di);
                /*
                var d = LoadNeedsRecursive(di);
                d.Name = null;
                foreach (var child in d.Children)
                {
                    child.Parent = null;
                    _needsHierarchies.Add(child);
                }
                */
            }

            if (System.IO.Directory.Exists(workingDirectory + Json.ActionSourcePath))
            {
                var di = new DirectoryInfo(workingDirectory + Json.ActionSourcePath);

                ActionsRoot = LoadActionsRecursive(di);
                /*
                var d = LoadActionsRecursive(di);
                d.Name = null;
                foreach (var child in d.Children)
                {
                    child.Parent = null;
                    _actionsHierarchies.Add(child);
                }
                */
            }

            if (File.Exists(workingDirectory + "Metabolism.json"))
            {
                citizenMetaData = Json.ReadCitizenMetaData(workingDirectory);
            }
            else
            {
                citizenMetaData = new CitizenMetaData();
            }
            citizenMetaData.PropertyChanged += CitizenMetaDataPropertyChanged;

            compiler = new Compiler(citizenMetaData);

            //UpdateCitizenParameters();
            citizenMetaData.RaisePropertyChanged("Parameters");

            if (File.Exists(workingDirectory + "ObjectMeta.json"))
            {
                var objectmeta = Json.ReadObjectMetas(workingDirectory);
                Property.Properties = objectmeta.Item1;
                Attributes = objectmeta.Item2;
                Objects = objectmeta.Item3;
            }
            else
            {
                Attributes = new ObservableCollection<AttributeMeta>();
                Objects = new ObservableCollection<ObjectMeta>();

                Attributes.Add(new AttributeMeta(0, "Item", null));
            }

            if (File.Exists(workingDirectory + "World.json"))
            {
                World = Json.ReadWorld(workingDirectory, Objects);
                World.UpdateVariableTable();
                World.Init();
            }
            else
            {
                World = new World();
            }
        }

        Directory LoadNeedsRecursive(DirectoryInfo di)
        {
            Directory directory = new Directory(di.Name);

            foreach (var d in di.GetDirectories())
            {
                directory.AddChild(LoadNeedsRecursive(d));
            }

            foreach (var f in di.GetFiles())
            {
                if (f.Extension == ".json")
                {
                    var need = new NeedSourceWrapper(Json.ReadNeedSource(f.FullName));
                    directory.AddChild(need);
                    needs.Add(need);
                }
            }

            return directory;
        }

        Directory LoadActionsRecursive(DirectoryInfo di)
        {
            Directory directory = new Directory(di.Name);

            foreach (var d in di.GetDirectories())
            {
                directory.AddChild(LoadActionsRecursive(d));
            }

            foreach (var f in di.GetFiles())
            {
                if (f.Extension == ".json")
                {
                    var action = new ActionSourceWrapper(Json.ReadActionSource(f.FullName));
                    directory.AddChild(action);
                    actions.Add(action);
                }
            }

            return directory;
        }

        public void Refresh()
        {
            Load(workingDirectory);
        }

        public void Store()
        {
            for (int i = 0; i < needs.Count; ++i)
            {
                Json.WriteNeedSource(needs[i].Target, workingDirectory);
            }

            for (int i = 0; i < actions.Count; ++i)
            {
                Json.WriteActionSource(actions[i].Target, workingDirectory);
            }

            Json.WriteCitizenMetaData(citizenMetaData, workingDirectory);
        }

        public void GenerateCitizen(ObservableCollection<float> parameters = null)
        {
            if (parameters == null)
            {
                parameters = new ObservableCollection<float>();
                for (int i = 0; i < citizenMetaData.Parameters.Count; ++i)
                {
                    parameters.Add(citizenMetaData.Parameters[i].InitialValue);
                }
            }

            int id = (citizens.Count > 0) ? (from c in citizens select c.ID).Max() + 1 : 0;
            var citizen = new Citizen(id, null, parameters, citizenMetaData.Items, citizenMetaData.StartPosition);
            citizen.Metadata = citizenMetaData;
            citizens.Add(citizen);
        }

        public void UpdateCitizenParameters(ObservableCollection<CitizenMetaData.ParameterMeta> parameters)
        {
            if (citizens.Count > 0)
            {
                var meta = citizenMetaData;
                //var removed = meta.Parameters.Except(parameters).ToArray();
                //var added = parameters.Except(meta.Parameters).ToArray();

                float[] param = new float[meta.Parameters.Count];
                int idx;

                if (meta.Parameters.Count < parameters.Count)
                {
                    for (int i = 0; i < citizens.Count; ++i)
                    {
                        for (int j = 0; j < meta.Parameters.Count; ++j)
                        {
                            param[j] = citizens[i].Parameters[j];
                        }
                        for (int j = 0; j < meta.Parameters.Count; ++j)
                        {
                            if (meta.ParameterIndexTable.TryGetValue(parameters[j], out idx))
                            {
                                citizens[i].Parameters[j] = param[idx];
                            }
                            else
                            {
                                citizens[i].Parameters[j] = parameters[j].InitialValue;
                            }
                        }
                        for (int j = meta.Parameters.Count; j < parameters.Count; ++j)
                        {
                            citizens[i].Parameters.Add(parameters[j].InitialValue);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < citizens.Count; ++i)
                    {
                        for (int j = 0; j < meta.Parameters.Count; ++j)
                        {
                            param[j] = citizens[i].Parameters[j];
                        }
                        int cnt = meta.Parameters.Count;
                        for (int j = 0; j < meta.Parameters.Count - parameters.Count; ++j)
                        {
                            citizens[i].Parameters.RemoveAt(cnt - j - 1);
                        }
                        for (int j = 0; j < parameters.Count; ++j)
                        {
                            if (meta.ParameterIndexTable.TryGetValue(parameters[j], out idx))
                            {
                                citizens[i].Parameters[j] = param[idx];
                            }
                            else
                            {
                                citizens[i].Parameters[j] = parameters[j].InitialValue;
                            }
                        }
                    }
                }
            }

            CitizenMetaData.UpdateParameters(parameters);
            citizenMetaData.RaisePropertyChanged("Parameters");
        }

        public enum SimulationStates
        {
            Running, Paused, Stopped
        }

        public event System.Action OnStart;
        public event System.Action OnStop;
        public event System.Action OnTick;
        public event System.Action OnPause;
        public event System.Action OnResume;

        public SimulationStates State
        {
            get; private set;
        }
        Compiler compiler;
        List<NeedInfo> needInfos;
        public HashSet<ActionSource> ModifiedActionSources
        {
            get; set;
        }

        DispatcherTimer timer;
        int _TPS;
        public int TPS
        {
            get { return _TPS; }
            set
            {
                _TPS = value;
                timer.Interval = TimeSpan.FromSeconds(1.0 / _TPS);
            }
        }

        World _world;
        public World World
        {
            get { return _world; }
            set
            {
                _world = value;
                RaisePropertyChanged("World");
            }
        }

        object _virtualWorld;
        public object VirtualWorld
        {
            get { return _virtualWorld; }
            set
            {
                _virtualWorld = value;
                RaisePropertyChanged("VritualWorld");
            }
        }

        public void InitSimulation()
        {
            World.SetTick(0);
            World.SetTime(0);

            ModifiedActionSources.Clear();

            //citizenMetaData.UpdateParameterNameTable();
            //citizenMetaData.RaisePropertyChanged("Parameters");

            UpdateNeedsAndActions();
            UpdateCitizens();

            UpdateWorld();

            State = SimulationStates.Paused;
            OnStart?.Invoke();
        }

        public void PauseSimulation()
        {
            timer.Stop();
            State = SimulationStates.Paused;
            OnPause?.Invoke();
        }

        public void ResumeSimulation()
        {
            UpdateNeedsAndActions();
            UpdateCitizens();
            UpdateWorld();
            timer.Start();

            State = SimulationStates.Running;
            OnResume?.Invoke();
        }

        public void StopSimulation()
        {
            if (State == SimulationStates.Running)
            {
                PauseSimulation();
            }

            for (int i = 0; i < citizens.Count; ++i)
            {
                citizens[i].Needs = null;
                citizens[i].ActiveNeeds.Clear();
                citizens[i].PriorNeed = null;
                citizens[i].ClearAction();
            }

            ActionInfo.UnRegisterAll();
            needInfos.Clear();

            State = SimulationStates.Stopped;
            OnStop?.Invoke();
        }

        void Tick()
        {
            try
            {
                _world.Update(_world);

                for (int i = 0; i < citizens.Count; ++i)
                {
                    citizens[i].Update();
                }
            }
            catch (RuntimeException e)
            {
                MessageBox.Show(e.Message);
            }

            World.NextTick();

            OnTick?.Invoke();
        }

        public void NextTick()
        {
            UpdateNeedsAndActions();
            UpdateCitizens();

            Tick();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Tick();
        }

        private void Needs_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //if (Simulating)
            //{
            //    var source = (NeedSource)e.NewItems[0];

            //    if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            //    {
            //        NewNeedSource(source);
            //    }
            //    else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            //    {
            //        RemoveNeedSource(source);
            //    }
            //}
        }

        public void NewNeedSource(NeedSource source)
        {
            var need = new NeedInfo();
            need.Source = source;
            need.Update(compiler);
            needInfos.Add(need);
        }

        //public void ModifyNeedSource(NeedSource source)
        //{
        //    for (int i = 0; i < needInfos.Count; ++i)
        //    {
        //        if (needInfos[i].Source == source)
        //        {
        //            needInfos[i].Update(compiler);
        //        }
        //    }
        //}

        public void RemoveNeedSource(NeedSource source)
        {
            for (int i = 0; i < needInfos.Count; ++i)
            {
                if (needInfos[i].Source == source)
                {
                    needInfos.RemoveAt(i);
                    break;
                }
            }

            for (int i = 0; i < citizens.Count; ++i)
            {
                if (citizens[i].PriorNeed.Source == source)
                {
                    citizens[i].PriorNeed = null;
                    citizens[i].ClearAction();
                }
                for (int j = 0; j < citizens[i].ActiveNeeds.Count; ++j)
                {
                    if (citizens[i].ActiveNeeds[j].Source == source)
                    {
                        citizens[i].ActiveNeeds.RemoveAt(j);
                        break;
                    }
                }
            }
        }

        public void UpdateNeedsAndActions()
        {
            // create new & modified need's actions
            var enableSources = (from need in needs
                                 where need.Enable
                                 select need.Target).ToArray();

            var removedSources = (from ni in needInfos
                                  where !enableSources.Contains(ni.Source)
                                  select ni.Source).ToArray();

            var newSources = enableSources.Except(from ni in needInfos
                                                  select ni.Source).ToArray();

            //for (int i = 0; i < actions.Count; ++i)
            //{
            //    for (int j = 0; j < enableSources.Length; ++j)
            //    {
            //        if (enableSources[j].ActionInfos.Contains(actions[i].Name) &&
            //            ActionInfo.Find(actions[i].Name) == null)
            //        {
            //            var action = new ActionInfo(actions[i].Name);
            //            compiler.Compile(actions[i].Target, action);
            //            action.Parameters = actions[i].Target.Parameters;
            //            ActionInfo.Register(action);
            //            break;
            //        }
            //    }
            //}

            for (int i = 0; i < actions.Count; ++i)
            {
                if (ActionInfo.Find(actions[i].Name) == null)
                {
                    var action = new ActionInfo(actions[i].Name);
                    compiler.Compile(actions[i].Target, action);
                    action.Parameters = actions[i].Target.Parameters;
                    ActionInfo.Register(action);
                }
            }

            // Remove removed needs
            for (int i = 0; i < removedSources.Length; ++i)
            {
                RemoveNeedSource(removedSources[i]);
            }

            // Update Modified needs
            for (int i = 0; i < needInfos.Count; ++i)
            {
                if (needInfos[i].Source.Modified)
                {
                    needInfos[i].Update(compiler);
                    needInfos[i].Source.Modified = false;
                }
            }

            // Add new needs
            for (int i = 0; i < newSources.Length; ++i)
            {
                NewNeedSource(newSources[i]);
            }

            // Update Modified Actions
            foreach (var action in ModifiedActionSources)
            {
                compiler.Compile(action, ActionInfo.Find(action.Name));
            }
        }

        public void UpdateCitizens()
        {
            for (int i = 0; i < citizens.Count; ++i)
            {
                citizens[i].Needs = needInfos;
            }

            if (citizenMetaData.MetabolismScriptChanged)
            {
                compiler.CompileMetabolism();
                citizenMetaData.MetabolismScriptChanged = false;
            }
        }

        public void UpdateWorld()
        {
            if (_world.ScriptUpdated || _world.Update == null)
            {
                compiler.CompileWorld(_world);
            }
        }
    }
}
