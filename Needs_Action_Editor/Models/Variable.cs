﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Models
{
    public class Variable : ModelBase
    {
        string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged("Name");
            }
        }

        string _type;
        public string Type
        {
            get { return _type; }
            set
            {
                SetType(value);
                RaisePropertyChanged("Type");
            }
        }

        IValueHolder _value;
        public IValueHolder Value
        {
            get { return _value; }
            set
            {
                _value = value;
                RaisePropertyChanged("Value");
            }
        }

        IValueHolder _initValue;
        public IValueHolder InitValue
        {
            get { return _initValue; }
            set
            {
                _initValue = value;
                RaisePropertyChanged("InitValue");
            }
        }

        public Variable(string name, string type, IValueHolder initValue, IValueHolder value)
        {
            _name = name;
            _type = type;
            _initValue = initValue;
            _value = value;
        }

        public Variable(string name, string type, IValueHolder initValue) : this(name, type, initValue, initValue.Clone()) { }

        public Variable(string name, string type)
        {
            _name = name;

            SetType(type);
        }

        public Variable(Variable variable) : this(variable._name, variable._type, variable._initValue.Clone(), variable._value.Clone()) { }

        public static Variable Create<T>(string name, string type, T value)
        {
            return new Variable(name, type, new ValueHolder<T>(type, value));
        }

        public static Variable Parse(string name, string type, string value)
        {
            IValueHolder InitValue;
            switch (type)
            {
                case "int":
                    {
                        int parse;
                        if (!int.TryParse(value, out parse)) { parse = 0; }
                        InitValue = new ValueHolder<int>("int", parse);
                    }
                    break;
                case "float":
                    {
                        float parse;
                        if (value.Length > 0 && value.Last() == 'f') { value = value.Substring(0, value.Length - 1); }
                        if (!float.TryParse(value, out parse)) { parse = 0f; }
                        InitValue = new ValueHolder<float>("float", parse);
                    }
                    break;
                case "bool":
                    {
                        bool parse;
                        if (!bool.TryParse(value, out parse)) { parse = false; }
                        InitValue = new ValueHolder<bool>("bool", parse);
                    }
                    break;
                case "char":
                    //{
                    //    char parse;
                    //    if (value.Length < 1) { parse = '\0'; }
                    //    else { parse = value[0]; }
                    //    InitValue = new ValueHolder<char>("char", parse);
                    //}
                    //break;
                case "string":
                    {
                        InitValue = new ValueHolder<string>("string", value);
                    }
                    break;
                default:
                    {
                        InitValue = new ValueHolder<string>("string", value);
                    }
                    break;
            }

            return new Variable(name, type, InitValue);
        }

        //public Variable(string name, string type) : this(name, type, null) { }

        public void SetType(string type)
        {
            if (_type == type) { return; }

            _type = type;

            switch (_type)
            {
                case "int":
                    InitValue = new ValueHolder<int>("int", 0);
                    break;
                case "float":
                    InitValue = new ValueHolder<float>("float", 0f);
                    break;
                case "bool":
                    InitValue = new ValueHolder<bool>("bool", false);
                    break;
                case "char":
                    InitValue = new ValueHolder<char>("char", '\0');
                    break;
                case "string":
                    InitValue = new ValueHolder<string>("string", "");
                    break;
            }

            Init();
        }

        public void Init()
        {
            Value = _initValue.Clone();
        }

        public ValueHolder<T> Cast<T>()
        {
            return _value as ValueHolder<T>;
        }
    }

    public interface IValueHolder
    {
        string Type { get; set; }
        object Value { get; }
        IValueHolder Clone();
    }

    public class ValueHolder<T> : ModelBase, IValueHolder
    {
        string _type;
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        T _value;
        public T Value
        {
            get { return _value; }
            set
            {
                _value = value;
                RaisePropertyChanged("Value");
            }
        }

        object IValueHolder.Value
        {
            get { return _value; }
        }

        public ValueHolder(string type)
        {
            _type = type;
        }

        public ValueHolder(string type, T value)
        {
            _type = type;
            _value = value;
        }

        public IValueHolder Clone()
        {
            return new ValueHolder<T>(_type, _value);
        }
    }
}
