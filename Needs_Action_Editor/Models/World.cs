﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Models
{
    public class World : ModelBase
    {
        int _Tick;
        int _Time;

        ObservableCollection<Variable> _variables;
        public ObservableCollection<Variable> Variables
        {
            get { return _variables; }
            set
            {
                _variables = value;
                RaisePropertyChanged("Variables");
            }
        }

        Dictionary<string, Variable> _variableTable;
        public Dictionary<string, Variable> VariableTable
        {
            get { return _variableTable; }
        }

        ObservableCollection<string> _types;
        public ObservableCollection<string> Types
        {
            get { return _types; }
            private set
            {
                _types = value;
                RaisePropertyChanged("Types");
            }
        }

        WorldObjectCollection _objects;
        public WorldObjectCollection Objects
        {
            get { return _objects; }
            set
            {
                _objects = value;
                RaisePropertyChanged("Objects");
            }
        }

        WorldObjectCollection _initObjects;
        public WorldObjectCollection InitObjects
        {
            get { return _initObjects; }
            set
            {
                _initObjects = value;
                RaisePropertyChanged("InitObjects");
            }
        }

        string _script;
        public string Script
        {
            get { return _script; }
            set
            {
                _scriptUpdated = (_script != value);
                _script = value;
                RaisePropertyChanged("Script");
            }
        }

        bool _scriptUpdated = true;
        public bool ScriptUpdated
        {
            get { return _scriptUpdated; }
        }

        public int Tick
        {
            get { return _Tick; }
            private set
            {
                _Tick = value;
                RaisePropertyChanged("Tick");
            }
        }

        public int Time
        {
            get { return _Time; }
            private set
            {
                _Time = value;
                RaisePropertyChanged("Time");
            }
        }

        Action<World> _update;
        public Action<World> Update
        {
            get { return _update; }
            set
            {
                _update = value;
                _scriptUpdated = false;
            }
        }

        public World(ObservableCollection<Variable> variables, string script, ObservableCollection<WorldObject> objects)
        {
            Tick = 0;
            Time = 0;

            _variables = variables;
            _types = new ObservableCollection<string>
            {
                "int", "float", "bool", "string"
            };

            _objects = new WorldObjectCollection();
            _initObjects = new WorldObjectCollection();
            _initObjects.List = objects;

            _script = script;
        }

        public World(ObservableCollection<Variable> variables) : this(variables, "", new ObservableCollection<WorldObject>()) { }

        public World() : this(new ObservableCollection<Variable>()) { }

        public void SetTick(int tick)
        {
            Tick = tick;
        }

        public void SetTime(int time)
        {
            Time = time;
        }

        public void NextTick()
        {
            ++Tick;
            Time = (++Time) % 3600;
        }

        public void UpdateVariableTable()
        {
            _variableTable = new Dictionary<string, Variable>();
            foreach (var v in Variables)
            {
                _variableTable.Add(v.Name, v);
            }
        }

        public void Init()
        {
            foreach (var v in Variables)
            {
                v.Init();
            }

            Objects.List.Clear();
            foreach (var o in InitObjects.List)
            {
                Objects.List.Add(new WorldObject(o));
            }
        }
    }

    public class QuadTreeNode<T>
    {
        Rect _rect;
        QuadTreeNode<T>[] _children;

        T _data;

        public Rect Rect
        {
            get { return _rect; }
        }

        public QuadTreeNode<T>[] Children
        {
            get { return _children; }
        }

        public QuadTreeNode<T> TopLeft
        {
            get { return _children[0]; }
            private set { _children[0] = value; }
        }
        public QuadTreeNode<T> TopRight
        {
            get { return _children[1]; }
            private set { _children[1] = value; }
        }
        public QuadTreeNode<T> BottomLeft
        {
            get { return _children[2]; }
            private set { _children[2] = value; }
        }
        public QuadTreeNode<T> BottomRight
        {
            get { return _children[3]; }
            private set { _children[3] = value; }
        }

        public T Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public QuadTreeNode(Rect rect)
        {
            _rect = rect;
        }

        public QuadTreeNode(Rect rect, T data)
        {
            _rect = rect;
            _data = data;
        }

        public void Split()
        {
            if (_children != null) { return; }

            _children = new QuadTreeNode<T>[4];

            TopLeft = new QuadTreeNode<T>(new Rect(_rect.TopLeft, _rect.Center));
            TopRight = new QuadTreeNode<T>(new Rect(new Vector2(_rect.Center.x, _rect.TopLeft.y), new Vector2(_rect.BottomRight.x, _rect.Center.y)));
            BottomLeft = new QuadTreeNode<T>(new Rect(new Vector2(_rect.TopLeft.x, _rect.Center.y), new Vector2(_rect.Center.x, _rect.BottomRight.y)));
            BottomRight = new QuadTreeNode<T>(new Rect(_rect.Center, _rect.BottomRight));
        }

        public bool Contains(Vector2 point)
        {
            return _rect.Contains(point);
        }
    }

    public class WorldObjectCollection
    {
        ObservableCollection<WorldObject> _list;
        public ObservableCollection<WorldObject> List
        {
            get { return _list; }
            set
            {
                _list = value;
            }
        }

        QuadTreeNode<List<WorldObject>> _root;
        float minSize;
        int maxCount;

        public WorldObjectCollection()
        {
            _list = new ObservableCollection<WorldObject>();
            _root = new QuadTreeNode<List<WorldObject>>(new Rect(new Vector2(), new Vector2()));
        }

        List<QuadTreeNode<List<WorldObject>>> FindNodes(Vector2 pos, QuadTreeNode<List<WorldObject>> root)
        {
            var nodes = new List<QuadTreeNode<List<WorldObject>>>();
            var queue = new Queue<QuadTreeNode<List<WorldObject>>>();
            QuadTreeNode<List<WorldObject>> node;

            queue.Enqueue(root);
            while (queue.Any())
            {
                node = queue.Dequeue();
                if (node.Contains(pos))
                {
                    if (node.Children != null)
                    {
                        for (int i = 0; i < 4; ++i)
                        {
                            queue.Enqueue(node.Children[i]);
                        }
                    }
                    else
                    {
                        nodes.Add(node);
                    }
                }
            }

            return nodes;
        }

        void PutObject(WorldObject obj, QuadTreeNode<List<WorldObject>> node)
        {
            if (node.Data.Count >= maxCount && node.Rect.Height > minSize)
            {
                node.Split();
                for (int i = 0; i < 4; ++i)
                {
                    node.Children[i].Data = new List<WorldObject>();
                    for (int j = 0; j < node.Data.Count; ++j)
                    {
                        if (node.Children[i].Contains(node.Data[j].Position))
                        {
                            node.Children[i].Data.Add(node.Data[j]);
                        }
                    }
                }
                node.Data.Clear();
                node.Data = null;

                for (int i = 0; i < 4; ++i)
                {
                    if (node.Children[i].Contains(obj.Position))
                    {
                        PutObject(obj, node.Children[i]);
                    }
                }
            }
            else
            {
                node.Data.Add(obj);
                //obj.Node = node;
            }
        }

        public void PutObject(ObjectMeta obj, Vector2 pos)
        {
            var wobj = new WorldObject(obj, pos);

            //int l = 0;
            //int r = _list.Count - 1;
            //int m;
            //WorldObject m_obj;
            //int cmp;

            //while (l <= r)
            //{
            //    m = (l + r) / 2;

            //    m_obj = _list[m];

            //    cmp = wobj.CompareTo(m_obj);
            //    if (cmp == 0)
            //    {
            //        _list.Insert(m + 1, new WorldObject(obj, pos));
            //        return;
            //    }
            //    else if (cmp < 0)
            //    {
            //        r = m - 1;
            //    }
            //    else
            //    {
            //        l = m + 1;
            //    }
            //}

            //m = (l + r / 2) + 1;
            //if (m >= _list.Count)
            //{
            //    _list.Add(new WorldObject(obj, pos));
            //}
            //else
            //{
            //    _list.Insert(m, new WorldObject(obj, pos));
            //}

            //var nodes = FindNodes(pos, _root);
            //for (int i = 0; i < nodes.Count; ++i)
            //{
            //    PutObject(wobj, nodes[i]);
            //}

            _list.Add(wobj);
        }

        public bool RemoveObject(ObjectMeta obj, Vector2 pos)
        {
            //int index = FindObject(obj, pos);
            //if (index < 0) { return false; }

            //_list.RemoveAt(index);

            //var nodes = FindNodes(pos, _root);

            return true;
        }

        public bool RemoveObject(WorldObject obj)
        {
            return _list.Remove(obj);
        }

        public void RemoveAll(ObjectMeta obj)
        {
            for (int i = 0; i < _list.Count; ++i)
            {
                if (_list[i].Object == obj)
                {
                    _list.RemoveAt(i);
                    --i;
                }
            }
        }

        public void FindNearest(Object obj, Vector2 pos)
        {
            var nodes = FindNodes(pos, _root);
        }
    }

    public class WorldObject : IComparable<WorldObject>
    {
        public ObjectMeta Object { get; set; }
        public Vector2 Position { get; set; }
        //public QuadTreeNode<List<WorldObject>> Node { get; set; }

        public WorldObject(ObjectMeta obj, Vector2 pos)
        {
            Object = obj;
            Position = pos;
        }

        public WorldObject(WorldObject obj) : this(obj.Object, new Vector2(obj.Position)) { }

        public int CompareTo(WorldObject obj)
        {
            if (WorldObject.Equals(this, obj)) { return 0; }

            if (Position.y < obj.Position.y || 
                (Position.y == obj.Position.y && Position.x < obj.Position.x) ||
                (Vector2.Equals(Position, obj.Position) && Object.ID < obj.Object.ID))
            {
                return -1;
            }

            return 1;
        }
    }
}
