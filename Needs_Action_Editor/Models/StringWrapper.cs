﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Models
{
    public class StringWrapper : ModelBase
    {
        string value;
        public string Text
        {
            get { return value; }
            set
            {
                this.value = value;
                RaisePropertyChanged("Text");
            }
        }

        public StringWrapper()
        {
            this.value = "";
        }

        public StringWrapper(string value)
        {
            this.value = value;
        }
    }
}
