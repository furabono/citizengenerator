﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Needs_Action_Editor.Models
{
    public class NeedSourceWrapper : ModelBase, Controls.IHierarchicalItem
    {
        static ImageSource icon;

        static NeedSourceWrapper()
        {
            var assembly = Assembly.GetExecutingAssembly();

            BitmapImage src = new BitmapImage();
            src.BeginInit();
            src.StreamSource = assembly.GetManifestResourceStream("Needs_Action_Editor.Resources.Icons.Document_16x.png");
            src.EndInit();

            icon = src;
        }

        public ImageSource Icon
        {
            get { return icon; }
        }

        Classes.NeedSource target;
        public Classes.NeedSource Target
        {
            get { return target; }
            set
            {
                target = value;
                RaisePropertyChanged("Target");
                RaisePropertyChanged("Enable");
                RaisePropertyChanged("Name");
            }
        }

        public bool Enable
        {
            get { return target.Enable; }
            set
            {
                target.Enable = value;
                RaisePropertyChanged("Enable");
            }
        }

        public string Name
        {
            get { return target.Name; }
            set
            {
                target.Name = value;
                RaisePropertyChanged("Name");
            }
        }

        public NeedSourceWrapper(Classes.NeedSource target)
        {
            this.target = target;
        }

        public Controls.IParentItem Parent
        {
            get; set;
        }
    }
}
