﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Models
{
    public class Rect
    {
        public Vector2 TopLeft
        {
            get; private set;
        }

        public Vector2 BottomRight
        {
            get; private set;
        }

        public Vector2 Center
        {
            get; private set;
        }

        public float Width
        {
            get; private set;
        }

        public float Height
        {
            get; private set;
        }

        public Rect(Vector2 topLeft, Vector2 bottomRight)
        {
            TopLeft = topLeft;
            BottomRight = BottomRight;

            Center = new Vector2((topLeft.x + bottomRight.x) / 2, (topLeft.y + bottomRight.y) / 2);
            Width = bottomRight.x - topLeft.x;
            Height = topLeft.y - bottomRight.y;
        }

        public bool Contains(Vector2 point)
        {
            return (point.x >= TopLeft.x && point.x <= BottomRight.x && point.y >= BottomRight.y && point.y <= TopLeft.y);
        }
    }
}
