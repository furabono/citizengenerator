﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Models
{
    public class Object : IAttribute
    {
        #region static
        public static ObservableCollection<Object> Objects
        {
            get; set;
        }

        static Object()
        {
            Objects = new ObservableCollection<Object>();
        }
        #endregion

        public int ID
        {
            get; private set;
        }

        public string Name
        {
            get; private set;
        }

        public Attribute Parent
        {
            get; private set;
        }

        public BitArray Attributes
        {
            get; private set;
        }

        public Object(int id, string name, Attribute parent)
        {
            ID = id;
            Name = name;
            Parent = parent;
        }

        public void UpdateAttributes()
        {
            if (Attributes.Length != Attribute.Attributes.Count)
            {
                Attributes = new BitArray(Attribute.Attributes.Count, false);
            }
            Attributes.SetAll(false);

            var p = Parent;
            while (p != null)
            {
                Attributes.Set(p.ID, true);
                p = p.Parent;
            }
        }
    }
}
