﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Reflection;

namespace Needs_Action_Editor.Models
{
    public class Directory : ModelBase, Controls.IParentItem
    {
        static ImageSource icon;

        static Directory()
        {
            var assembly = Assembly.GetExecutingAssembly();

            BitmapImage src = new BitmapImage();
            src.BeginInit();
            src.StreamSource = assembly.GetManifestResourceStream("Needs_Action_Editor.Resources.Icons.Folder_16x.png");
            src.EndInit();

            icon = src;
        }

        public ImageSource Icon
        {
            get { return icon; }
        }

        string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;

                RaisePropertyChanged("Name");
            }
        }

        ObservableCollection<Controls.IHierarchicalItem> children;
        public ObservableCollection<Controls.IHierarchicalItem> Children
        {
            get { return children; }
            set
            {
                children = value;

                RaisePropertyChanged("Children");
            }
        }

        public string Path
        {
            get
            {
                return ((Directory)_parent)?.Path + name + @"\";
            }
        }

        public Directory(string name)
        {
            this.name = name;
            this.children = new ObservableCollection<Controls.IHierarchicalItem>();
        }

        bool _enable;
        public bool Enable
        {
            get { return _enable; }
            set
            {
                _enable = value;
                foreach (var child in children)
                {
                    if (child is Directory) { (child as Directory).Enable = _enable; }
                    else if (child is Models.NeedSourceWrapper) { (child as NeedSourceWrapper).Enable = _enable; }
                }
                RaisePropertyChanged("Enable");
            }
        }

        #region IHierarchicalItem
        Directory _parent;
        public Controls.IParentItem Parent
        {
            get { return _parent; }
            set { _parent = value as Directory; }
        }

        public void AddChild(Controls.IHierarchicalItem item)
        {
            if (item != null && item is ModelBase)
            {
                children.Add(item);
                item.Parent = this;
            }
        }

        public bool RemoveChild(Controls.IHierarchicalItem item)
        {
            if (item != null && item is ModelBase)
            {
                return children.Remove(item);
            }
            return false;
        }
        #endregion
    }
}
