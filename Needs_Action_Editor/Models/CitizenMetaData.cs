﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Needs_Action_Editor.Models
{
    public partial class CitizenMetaData : ModelBase
    {
        ObservableCollection<ParameterMeta> parameters;
        public ObservableCollection<ParameterMeta> Parameters
        {
            get { return parameters; }
            set
            {
                parameters = value;
                RaisePropertyChanged("Parameters");
            }
        }

        public Dictionary<string, ParameterMeta> ParameterNameTable
        {
            get; set;
        }

        public Dictionary<ParameterMeta, int> ParameterIndexTable
        {
            get; set;
        }

        string _MetabolismScript;
        public string MetabolismScript
        {
            get { return _MetabolismScript; }
            set
            {
                if (_MetabolismScript != value)
                {
                    _MetabolismScript = value;
                    MetabolismScriptChanged = true;
                }
            }
        }

        public bool MetabolismScriptChanged
        {
            get; set;
        }

        public Action<Classes.Citizen> Metabolism
        {
            get; set;
        }

        public Vector2 StartPosition
        {
            get; set;
        }

        public Classes.Inventory Items
        {
            get; set;
        }

        public CitizenMetaData()
        {
            parameters = new ObservableCollection<ParameterMeta>();
            ParameterNameTable = new Dictionary<string, ParameterMeta>();
            ParameterIndexTable = new Dictionary<ParameterMeta, int>();
            MetabolismScript = "";
            StartPosition = new Vector2();
            Items = new Classes.Inventory();
        }

        public void UpdateParameters(ObservableCollection<ParameterMeta> parameters)
        {
            this.parameters = parameters;
            ParameterNameTable = parameters.ToDictionary((meta) => { return meta.Name; });
            ParameterIndexTable.Clear();
            for (int i = 0; i < parameters.Count; ++i)
            {
                ParameterIndexTable.Add(parameters[i], i);
            }
        }

        //public CitizenMetaData Clone()
        //{
        //    var clone = new CitizenMetaData();
        //    for (int i = 0; i < parameters.Count; ++i)
        //    {
        //        clone.parameters.Add(parameters[i]);
        //    }

        //    return clone;
        //}
    }
}
