﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Needs_Action_Editor
{
    /// <summary>
    /// XAML 파일에서 이 사용자 지정 컨트롤을 사용하려면 1a 또는 1b단계를 수행한 다음 2단계를 수행하십시오.
    ///
    /// 1a단계) 현재 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor"
    ///
    ///
    /// 1b단계) 다른 프로젝트에 있는 XAML 파일에서 이 사용자 지정 컨트롤 사용.
    /// 이 XmlNamespace 특성을 사용할 마크업 파일의 루트 요소에 이 특성을 
    /// 추가합니다.
    ///
    ///     xmlns:MyNamespace="clr-namespace:Needs_Action_Editor;assembly=Needs_Action_Editor"
    ///
    /// 또한 XAML 파일이 있는 프로젝트의 프로젝트 참조를 이 프로젝트에 추가하고
    /// 다시 빌드하여 컴파일 오류를 방지해야 합니다.
    ///
    ///     솔루션 탐색기에서 대상 프로젝트를 마우스 오른쪽 단추로 클릭하고
    ///     [참조 추가]->[프로젝트]를 차례로 클릭한 다음 이 프로젝트를 찾아서 선택합니다.
    ///
    ///
    /// 2단계)
    /// 계속 진행하여 XAML 파일에서 컨트롤을 사용합니다.
    ///
    ///     <MyNamespace:VSNode/>
    ///
    /// </summary>
    public abstract partial class VSNode : Control
    {
        static VSNode()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(VSNode), new FrameworkPropertyMetadata(typeof(VSNode)));
        }

        public event Action ControlDragged;

        protected ControlDragComponent cdc;
        List<VSNodePort> ports;
        Border border;

        public VSCanvas Canvas
        {
            get; set;
        }

        public Point Position
        {
            get { return TranslatePoint(new Point(ActualWidth / 2, ActualHeight / 2), Canvas); }
        }

        public string Script
        {
            get;
        }

        public VSNode() : base() { }

        public abstract VSNode Clone();

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            cdc = new ControlDragComponent(this);
            cdc.ControlDragged += OnControlDragged;

            ports = new List<VSNodePort>();
            MouseLeftButtonUp += VSNode_MouseLeftButtonUp;

            Focusable = true;
            border = GetTemplateChild("border") as Border;
            KeyDown += VSNode_KeyDown;
            GotFocus += VSNode_GotFocus;
            LostFocus += VSNode_LostFocus;

            InitContextMenu();
        }

        public void MoveTo(Point point)
        {
            var currentPosition = Position;

            var transform = RenderTransform as TranslateTransform;
            if (transform == null)
            {
                transform = new TranslateTransform();
                RenderTransform = transform;
            }

            transform.X = -currentPosition.X + point.X;
            transform.Y = -currentPosition.Y + point.Y;
        }

        void OnControlDragged()
        {
            ControlDragged();
        }

        protected void RegisterPort(VSNodePort port)
        {
            ports.Add(port);
            port.ParentNode = this;
        }

        protected void RegisterPort(string portName)
        {
            var port = GetTemplateChild(portName) as VSNodePort;
            port.ParentNode = this;
            ports.Add(port);
        }

        private void VSNode_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            for (int i = 0; i < ports.Count; ++i)
            {
                if (ports[i].EllipseMouseOver)
                {
                    Canvas.MouseClickOnPort(ports[i]);
                    e.Handled = true;
                    return;
                }
            }
        }

        // ToDo
        private void VSNode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                //Canvas.RemoveNode(this);
            }
        }

        private void VSNode_GotFocus(object sender, RoutedEventArgs e)
        {
            border.BorderBrush = Brushes.Yellow;
        }

        private void VSNode_LostFocus(object sender, RoutedEventArgs e)
        {
            border.BorderBrush = Brushes.Black;
        }
    }
}
