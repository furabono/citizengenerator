﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

using VariableCollection = System.Collections.ObjectModel.ObservableCollection<Needs_Action_Editor.Models.Variable>;

namespace Needs_Action_Editor
{
    /// <summary>
    /// ActionEditor.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ActionEditor : UserControl
    {
        public static readonly DependencyProperty GlobalVariablesProperty
            = DependencyProperty.Register("GlobalVariables", 
                                          typeof(VariableCollection), 
                                          typeof(ActionEditor),
                                          new PropertyMetadata(new VariableCollection()));

        public static readonly DependencyProperty ParametersProperty
            = DependencyProperty.Register("Parameters",
                                          typeof(VariableCollection),
                                          typeof(ActionEditor),
                                          new PropertyMetadata(new VariableCollection()));

        static readonly List<string> _TypeList
            = new List<string>() { "int", "float", "bool", "string" };

        double[] rowHeight;
        Dictionary<int, GridSplitter> gridSplitters;

        public VariableCollection GlobalVariables
        {
            get { return (VariableCollection)GetValue(GlobalVariablesProperty); }
            set { SetValue(GlobalVariablesProperty, value); }
        }

        public VariableCollection Parameters
        {
            get { return (VariableCollection)GetValue(ParametersProperty); }
            set { SetValue(ParametersProperty, value); }
        }

        public List<string> TypeList
        {
            get { return _TypeList; }
        }

        public ActionEditor()
        {
            InitializeComponent();

            rowHeight = new double[grid.RowDefinitions.Count];
            gridSplitters = new Dictionary<int, GridSplitter>();
            for (int i = 0; i < grid.Children.Count; ++i)
            {
                var gridSplitter = grid.Children[i] as GridSplitter;
                if (gridSplitter != null)
                {
                    gridSplitters[Grid.GetRow(gridSplitter)] = gridSplitter;
                }
            }

            //_Prepare_ScriptEditor.SingleLine = false;
            //_Prepare_ScriptEditor.SingleLineChangeable = false;
            _Plan_ScriptEditor.Mode = ScriptEditorMode.FunctionOnly;
            _Work_ScriptEditor.Mode = ScriptEditorMode.FunctionOnly;

            xImageButton.Click += XImageButton_Click;
            xImageImportButton.Click += XImageImportButton_Click;            

            _openFileDialog = new Ookii.Dialogs.Wpf.VistaOpenFileDialog();
            _openFileDialog.InitialDirectory = Models.Simulator.Instance.WorkingDirectory;

            //xImage = (Image)GetTemplateChild("xImage");

            xAddParameterButton.Click += XAddParameterButton_Click;

            Parameters = new ObservableCollection<Models.Variable>();
        }

        private void XAddParameterButton_Click(object sender, RoutedEventArgs e)
        {
            Parameters.Add(new Models.Variable("param", "float"));
        }

        Image _imageControl;
        Dialogs.SingleControlDialog _imageViewerDialog;
        private void XImageButton_Click(object sender, RoutedEventArgs e)
        {
            if (_image != null)
            {
                {
                    _imageControl = new Image();
                    _imageViewerDialog = new Dialogs.SingleControlDialog(_imageControl);
                    _imageViewerDialog.SizeToContent = SizeToContent.Manual;
                    _imageViewerDialog.Width = 500;
                    _imageViewerDialog.Height = 500;
                }

                _imageControl.Source = _image;
                _imageViewerDialog.ShowDialog();
            }
        }

        //Image xImage;
        Ookii.Dialogs.Wpf.VistaOpenFileDialog _openFileDialog;
        BitmapImage _image;
        public BitmapImage Image
        {
            get { return _image; }
            private set
            {
                _image = value;
                xImage.Source = value;
                //xImageButton.Content = value;
            }
        }
        private void XImageImportButton_Click(object sender, RoutedEventArgs e)
        {
            if (_openFileDialog.ShowDialog() ?? false)
            {
                var img = new BitmapImage();

                using (var stream = System.IO.File.OpenRead(_openFileDialog.FileName))
                {
                    img.BeginInit();
                    img.StreamSource = stream;
                    img.CacheOption = BitmapCacheOption.OnLoad;
                    img.EndInit();
                }

                Image = img;
            }
        }

        private void Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            if (!IsLoaded) { return; }

            var expander = (Expander)sender;
            var border = expander.Parent as Border;
            int rowIndex = Grid.GetRow(border);
            var row = grid.RowDefinitions[rowIndex];

            rowHeight[rowIndex] = row.Height.Value;
            row.Height = GridLength.Auto;
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            if (!IsLoaded) { return; }

            var expander = (Expander)sender;
            var se = expander.Content as ScriptEditor;
            var border = expander.Parent as Border;
            int rowIndex = Grid.GetRow(border);
            var row = grid.RowDefinitions[rowIndex];

            if (se.Mode == ScriptEditorMode.Function || se.Mode == ScriptEditorMode.FunctionOnly)
            {
                row.Height = new GridLength(rowHeight[rowIndex]);
            }
        }

        private void ScriptEditor_SingleLineChanged(object sender, RoutedPropertyChangedEventArgs<ScriptEditorMode> e)
        {
            var scriptEditor = (ScriptEditor)sender;
            var expander = scriptEditor.Parent as Expander;
            var border = expander.Parent as Border;
            int rowIndex = Grid.GetRow(border);
            var row = grid.RowDefinitions[rowIndex];

            if (scriptEditor.Mode == ScriptEditorMode.Expression || scriptEditor.Mode == ScriptEditorMode.ExpressionOnly)
            {
                row.Height = GridLength.Auto;
                gridSplitters[rowIndex + 1].IsEnabled = false;
            }
            else
            {
                scriptEditor.VerticalAlignment = VerticalAlignment.Stretch;
                row.Height = new GridLength(200);
                gridSplitters[rowIndex + 1].IsEnabled = true;
            }
        }

        void Prepare_Expander_Expended(object sender, RoutedEventArgs e)
        {
            if (!IsLoaded) { return; }

            var expander = (Expander)sender;
            var dp = expander.Content as DockPanel;
            var se = dp.Children[1] as ScriptEditor;
            var border = expander.Parent as Border;
            int rowIndex = Grid.GetRow(border);
            var row = grid.RowDefinitions[rowIndex];

            if (se.Mode == ScriptEditorMode.Function || se.Mode == ScriptEditorMode.FunctionOnly)
            {
                row.Height = new GridLength(rowHeight[rowIndex]);
            }
        }

        private void Prepare_ScriptEditor_SingleLineChanged(object sender, RoutedPropertyChangedEventArgs<ScriptEditorMode> e)
        {
            var scriptEditor = (ScriptEditor)sender;
            var dp = scriptEditor.Parent as DockPanel;
            var expander = dp.Parent as Expander;
            var border = expander.Parent as Border;
            int rowIndex = Grid.GetRow(border);
            var row = grid.RowDefinitions[rowIndex];

            if (scriptEditor.Mode == ScriptEditorMode.Expression || scriptEditor.Mode == ScriptEditorMode.ExpressionOnly)
            {
                row.Height = GridLength.Auto;
                gridSplitters[rowIndex + 1].IsEnabled = false;
            }
            else
            {
                scriptEditor.VerticalAlignment = VerticalAlignment.Stretch;
                row.Height = new GridLength(200);
                gridSplitters[rowIndex + 1].IsEnabled = true;
            }
        }

        public void SetSource(Classes.ActionSource source, BitmapImage image = null)
        {
            _Name_TextBox.Text = source.Name;

            Image = image;

            _Efficiency_ScriptEditor.Text = "";
            _Efficiency_ScriptEditor.Mode = (source.Efficiency.IsExpression) ? (ScriptEditorMode.Expression) : (ScriptEditorMode.Function);
            _Efficiency_ScriptEditor.Text = source.Efficiency.Code;

            GlobalVariables = new VariableCollection(source.GlobalVariables);
            Parameters = new VariableCollection(source.Parameters);

            //_Prepare_ScriptEditor.Text = "";
            //_Prepare_ScriptEditor.SingleLine = source.Prepare.IsExpression;
            //_Prepare_ScriptEditor.Text = source.Prepare.Code;

            _Plan_ScriptEditor.Text = "";
            //_Plan_ScriptEditor.Mode = (source.Plan.IsExpression) ? (ScriptEditorMode.Expression) : (ScriptEditorMode.Function);
            _Plan_ScriptEditor.Text = source.Plan.Code;

            _Work_ScriptEditor.Text = "";
           // _Work_ScriptEditor.Mode = (source.Work.IsExpression) ? (ScriptEditorMode.Expression) : (ScriptEditorMode.Function);
            _Work_ScriptEditor.Text = source.Work.Code;
        }

        public Classes.ActionSource GetSource()
        {
            var efficiency = new Classes.CodeSnippet(_Efficiency_ScriptEditor.Text, _Efficiency_ScriptEditor.IsExpression);
            var globalVariables = GlobalVariables.ToArray();
            var parameters = Parameters.ToArray();
            var prepare = new Classes.CodeSnippet(_Prepare_ScriptEditor.Text, _Prepare_ScriptEditor.IsExpression);
            var plan = new Classes.CodeSnippet(_Plan_ScriptEditor.Text, _Plan_ScriptEditor.IsExpression);
            var work = new Classes.CodeSnippet(_Work_ScriptEditor.Text, _Work_ScriptEditor.IsExpression);

            return new Classes.ActionSource(_Name_TextBox.Text, efficiency, prepare, plan, work, globalVariables, parameters);
        }
    }
}
