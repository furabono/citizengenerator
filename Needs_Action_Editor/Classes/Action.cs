﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Classes
{
    public class Action
    {
        ActionInfo info;
        Citizen citizen;
        Scope scope;
        int pc;

        public string Name
        {
            get { return info.Name; }
        }

        public Action(ActionInfo info, Citizen citizen)
        {
            this.info = info;
            this.citizen = citizen;
            scope = new Scope();
            pc = 0;
        }

        public void Start(params object[] arguments)
        {
            citizen.PushAction(this);

            //scope.SetVariable("__args", arguments);

            int i;
            for (i = 0; i < arguments.Length; ++i)
            {
                scope.SetVariable(info.Parameters[i].Name, info.Parameters[i].Type, arguments[i]);
            }
            for (; i < info.Parameters.Length; ++i)
            {
                scope.SetVariable(info.Parameters[i]);
            }
        }

        public void Process()
        {
            pc = info.Process(citizen, scope, pc);

            if (pc == -1)
            {
                citizen.PopAction();
            }
        }
    }
}
