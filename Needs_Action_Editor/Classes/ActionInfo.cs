﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Classes
{
    public class ActionInfo
    {
        #region Instance_Management
        static Dictionary<string, ActionInfo> instances;

        static ActionInfo()
        {
            instances = new Dictionary<string, ActionInfo>();
        }

        public static ActionInfo Find(string name)
        {
            ActionInfo info;
            if (instances.TryGetValue(name, out info))
            {
                return info;
            }
            return null;
        }

        public static void Register(params ActionInfo[] actionInfos)
        {
            for (int i = 0; i < actionInfos.Length; ++i)
            {
                Register(actionInfos[i]);
            }
        }

        public static void Register(ActionInfo actionInfo)
        {
            if (instances.ContainsKey(actionInfo.Name))
            {
                // already exist same name
            }

            instances.Add(actionInfo.name, actionInfo);
        }

        public static void UnRegister(string name)
        {
            instances.Remove(name);
        }

        public static void UnRegister(ActionInfo actionInfo)
        {
            UnRegister(actionInfo.Name);
        }

        public static void UnRegisterAll()
        {
            instances.Clear();
        }
        #endregion

        public delegate float EfficiencyDelegate(Citizen citizen);
        public delegate int ProcessDelegate(Citizen citizen, Scope scope, int programCounter);

        string name;
        EfficiencyDelegate efficiency;
        ProcessDelegate process;
        Models.Variable[] parameters;

        public string Name
        {
            get { return name; }
        }

        public EfficiencyDelegate Efficiency
        {
            get { return efficiency; }
            set { efficiency = value; }
        }

        public ProcessDelegate Process
        {
            get { return process; }
            set { process = value; }
        }

        public Models.Variable[] Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        public ActionInfo(string name)
        {
            this.name = name;
        }

        public ActionInfo(string name, EfficiencyDelegate efficiency, ProcessDelegate process, Models.Variable[] parameters)
        {
            this.name = name;
            this.efficiency = efficiency;
            this.process = process;
            this.parameters = parameters;
        }

        public Action CreateAction(Citizen citizen)
        {
            return new Action(this, citizen);
        }
    }
}
