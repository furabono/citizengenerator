﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Classes
{
    public class Need
    {
        NeedInfo info;
        Citizen citizen;

        public string Name
        {
            get { return info.Name; }
        }

        public Need(NeedInfo info, Citizen citizen)
        {
            this.info = info;
            this.citizen = citizen;
        }

        public void Start()
        {
            float maxEfficiency = info.ActionInfos[0].Efficiency(citizen);
            ActionInfo maxAction = info.ActionInfos[0];
            float efficiency;
            for (int i = 1; i < info.ActionInfos.Length; ++i)
            {
                efficiency = info.ActionInfos[i].Efficiency(citizen);
                if (efficiency > maxEfficiency)
                {
                    maxEfficiency = efficiency;
                    maxAction = info.ActionInfos[i];
                }
            }

            citizen.ClearAction();
            maxAction.CreateAction(citizen).Start();
        }
    }
}
