﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using UnityEngine;
using System.Linq.Expressions;

namespace Needs_Action_Editor.Classes
{
    public class Context
    {
        Citizen citizen;
        Dictionary<string, float> staticVariables;
        Dictionary<string, float> dynamicVariables;

        Dictionary<string, object> genericVariables;
        bool returned;
        float returnValue;

        public Dictionary<string, float> StaticVariables
        {
            get { return staticVariables; }
        }

        public Dictionary<string, float> DynamicVariables
        {
            get { return dynamicVariables; }
        }

        public bool Returned
        {
            get { return returned; }
        }

        public float ReturnValue
        {
            get { return returnValue; }
        }

        public Context(Citizen citizen)
        {
            this.citizen = citizen;
            staticVariables = new Dictionary<string, float>();
            dynamicVariables = new Dictionary<string, float>();

            genericVariables = new Dictionary<string, object>();
            returned = false;
            returnValue = float.NaN;
        }

        public float GetStaticVariable(string name)
        {
            float value;
            if (staticVariables.TryGetValue(name, out value))
            {
                return value;
            }

            return float.NaN;
        }

        public float GetDynamicVariable(string name)
        {
            float value;
            if (dynamicVariables.TryGetValue(name, out value))
            {
                return value;
            }

            return float.NaN;
        }

        public void SetStaticVariable(string name, float value)
        {
            staticVariables[name] = value;
        }

        public void SetDynamicVariable(string name, float value)
        {
            dynamicVariables[name] = value;
        }

        public void ClearDynamicVariables()
        {
            dynamicVariables.Clear();

            genericVariables.Clear();
        }


        public object GetGenericVariable(string name)
        {
            object value = null;
            if (genericVariables.TryGetValue(name, out value))
            {
                return value;
            }

            return null;
        }

        public void SetGenericVariable(string name, object value)
        {
            genericVariables[name] = value;
        }

        public void Return(float value)
        {
            returned = true;
            returnValue = value;
        }

        public void ResetReturn()
        {
            returned = false;
            returnValue = float.NaN;
        }
    }

    public interface IEstimator
    {
        float Estimate(Context context);
    }

    public class EConstant : IEstimator
    {
        static Dictionary<float, EConstant> instances;
        float value;

        private EConstant(float value)
        {
            this.value = value;
            //instances.Add(value, this);
        }

        public static EConstant Instance(float value)
        {
            if (instances == null)
            {
                instances = new Dictionary<float, EConstant>();
            }

            EConstant instance;
            instances.TryGetValue(value, out instance);

            if (instance == null)
            {
                instance = new EConstant(value);
                instances.Add(value, instance);
            }

            return instance;
        }

        public float Estimate(Context context)
        {
            return value;
        }
    }

    public class ESqrt : IEstimator
    {
        IEstimator value;

        public ESqrt(IEstimator value)
        {
            this.value = value;
        }

        public float Estimate(Context context)
        {
            return Mathf.Sqrt(value.Estimate(context));
        }
    }

    public class ENegative : IEstimator
    {
        IEstimator value;

        public ENegative(IEstimator value)
        {
            this.value = value;
        }

        public float Estimate(Context context)
        {
            return -value.Estimate(context);
        }
    }

    public class EAdd : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EAdd(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimate(Context context)
        {
            return left.Estimate(context) + right.Estimate(context);
        }
    }

    public class ESubtract : IEstimator
    {
        IEstimator left;
        IEstimator right;


        public ESubtract(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimate(Context context)
        {
            return left.Estimate(context) - right.Estimate(context);
        }
    }

    public class EMultiply : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EMultiply(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimate(Context context)
        {
            return left.Estimate(context) * right.Estimate(context);
        }
    }

    public class EDivide : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EDivide(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimate(Context context)
        {
            return left.Estimate(context) / right.Estimate(context);
        }
    }

    public class EMod : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EMod(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimate(Context context)
        {
            return left.Estimate(context) % right.Estimate(context);
        }
    }

    public class EPower : IEstimator
    {
        IEstimator @base;
        IEstimator exponent;

        public EPower(IEstimator left, IEstimator right)
        {
            this.@base = left;
            this.exponent = right;
        }

        public float Estimate(Context context)
        {
            return Mathf.Pow(@base.Estimate(context), exponent.Estimate(context));
        }
    }

    public class EEqual : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EEqual(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimate(Context context)
        {
            return (left.Estimate(context) == right.Estimate(context)) ? (1f) : (0f);
        }
    }

    public class ENotEqual : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public ENotEqual(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimate(Context context)
        {
            return (left.Estimate(context) != right.Estimate(context)) ? (1f) : (0f);
        }
    }

    public class EBigger : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EBigger(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimate(Context context)
        {
            return (left.Estimate(context) > right.Estimate(context)) ? (1f) : (0f);
        }
    }

    public class ESmaller : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public ESmaller(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimate(Context context)
        {
            return (left.Estimate(context) < right.Estimate(context)) ? (1f) : (0f);
        }
    }

    public class EBiggerOrEqual : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EBiggerOrEqual(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimate(Context context)
        {
            return (left.Estimate(context) >= right.Estimate(context)) ? (1f) : (0f);
        }
    }

    public class ESmallerOrEqual : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public ESmallerOrEqual(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimate(Context context)
        {
            return (left.Estimate(context) <= right.Estimate(context)) ? (1f) : (0f);
        }
    }

    public class EAnd : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EAnd(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimate(Context context)
        {
            return (left.Estimate(context) * right.Estimate(context) != 0f) ? (1f) : (0f);
        }
    }

    public class EOr : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EOr(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimate(Context context)
        {
            return (left.Estimate(context) + right.Estimate(context) != 0f) ? (1f) : (0f);
        }
    }

    public class ENot : IEstimator
    {
        IEstimator value;

        public ENot(IEstimator value)
        {
            this.value = value;
        }

        public float Estimate(Context context)
        {
            return (value.Estimate(context) == 0f) ? (1f) : (0f);
        }
    }


    // Statements...
    public class CodeBlock : IEstimator
    {
        IEstimator[] estimators;

        public CodeBlock(IEstimator[] estimators)
        {
            this.estimators = estimators;
        }

        public float Estimate(Context context)
        {
            for (int i = 0; i < estimators.Length; ++i)
            {
                estimators[i].Estimate(context);

                if (context.Returned) { break; }
            }

            return float.NaN;
        }
    }

    public class EIf : IEstimator
    {
        Tuple<IEstimator, CodeBlock>[] conditionalBlocks;

        public EIf(Tuple<IEstimator, CodeBlock>[] conditionalBlocks)
        {
            this.conditionalBlocks = conditionalBlocks;
        }

        public float Estimate(Context context)
        {
            for (int i = 0; i < conditionalBlocks.Length; ++i)
            {
                if (conditionalBlocks[i].Item1.Estimate(context) != 0f)
                {
                    conditionalBlocks[i].Item1.Estimate(context);
                    break;
                }
            }

            return float.NaN;
        }
    }

    public class EWhile : IEstimator
    {
        IEstimator condition;
        CodeBlock codeBlock;

        public EWhile(IEstimator condition, CodeBlock codeBlock)
        {
            this.condition = condition;
            this.codeBlock = codeBlock;
        }

        public float Estimate(Context context)
        {
            while (condition.Estimate(context) != 0f)
            {
                codeBlock.Estimate(context);
            }

            return float.NaN;
        }
    }


    // Variables...

    public class EDeclareStaticVariable : IEstimator
    {
        string name;
        IEstimator initialValue;

        public EDeclareStaticVariable(string name, IEstimator initialValue)
        {
            this.name = name;
            this.initialValue = initialValue;
        }
        
        public float Estimate(Context context)
        {
            if (float.IsNaN(context.GetStaticVariable(name)))
            {
                context.SetStaticVariable(name, initialValue.Estimate(context));
            }

            return float.NaN;
        }
    }

    public interface IEVariable : IEstimator
    {
        float Assign(Context context, float value);
    }

    public class EStaticVariable : IEVariable
    {
        string name;

        public EStaticVariable(string name)
        {
            this.name = name;
        }

        public float Estimate(Context context)
        {
            // UnDeclaredVariable Error
            return context.GetStaticVariable(name);
        }

        public float Assign(Context context, float value)
        {
            context.SetStaticVariable(name, value);
            return value;
        }
    }

    public class EDynamicVariable : IEVariable
    {
        string name;

        public EDynamicVariable(string name)
        {
            this.name = name;
        }

        public float Estimate(Context context)
        {
            // UnDeclaredVariable Error
            return context.GetDynamicVariable(name);
        }

        public float Assign(Context context, float value)
        {
            context.SetDynamicVariable(name, value);
            return value;
        }
    }

    public class EAssign : IEstimator
    {
        IEVariable variable;
        IEstimator value;

        public EAssign(IEVariable variable, IEstimator value)
        {
            this.variable = variable;
            this.value = value;
        }

        public float Estimate(Context context)
        {
            return variable.Assign(context, value.Estimate(context));
        }
    }

    public class EAddAssign : IEstimator
    {
        IEVariable variable;
        IEstimator value;

        public EAddAssign(IEVariable variable, IEstimator value)
        {
            this.variable = variable;
            this.value = value;
        }

        public float Estimate(Context context)
        {
            return variable.Assign(context, variable.Estimate(context) + value.Estimate(context));
        }
    }

    public class ESubtractAssign : IEstimator
    {
        IEVariable variable;
        IEstimator value;

        public ESubtractAssign(IEVariable variable, IEstimator value)
        {
            this.variable = variable;
            this.value = value;
        }

        public float Estimate(Context context)
        {
            return variable.Assign(context, variable.Estimate(context) - value.Estimate(context));
        }
    }

    public class EMultiplyAssign : IEstimator
    {
        IEVariable variable;
        IEstimator value;

        public EMultiplyAssign(IEVariable variable, IEstimator value)
        {
            this.variable = variable;
            this.value = value;
        }

        public float Estimate(Context context)
        {
            return variable.Assign(context, variable.Estimate(context) * value.Estimate(context));
        }
    }

    public class EDivideAssign : IEstimator
    {
        IEVariable variable;
        IEstimator value;

        public EDivideAssign(IEVariable variable, IEstimator value)
        {
            this.variable = variable;
            this.value = value;
        }

        public float Estimate(Context context)
        {
            return variable.Assign(context, variable.Estimate(context) / value.Estimate(context));
        }
    }

    public class EModAssign : IEstimator
    {
        IEVariable variable;
        IEstimator value;

        public EModAssign(IEVariable variable, IEstimator value)
        {
            this.variable = variable;
            this.value = value;
        }

        public float Estimate(Context context)
        {
            return variable.Assign(context, variable.Estimate(context) % value.Estimate(context));
        }
    }

    public class EPrefixIncrease : IEstimator
    {
        IEVariable variable;

        public EPrefixIncrease(IEVariable variable)
        {
            this.variable = variable;
        }

        public float Estimate(Context context)
        {
            return variable.Assign(context, variable.Estimate(context) + 1);
        }
    }

    public class EPrefixDecrease : IEstimator
    {
        IEVariable variable;

        public EPrefixDecrease(IEVariable variable)
        {
            this.variable = variable;
        }

        public float Estimate(Context context)
        {
            return variable.Assign(context, variable.Estimate(context) - 1);
        }
    }

    public class EPostfixIncrease : IEstimator
    {
        IEVariable variable;

        public EPostfixIncrease(IEVariable variable)
        {
            this.variable = variable;
        }

        public float Estimate(Context context)
        {
            float temp = variable.Estimate(context);
            variable.Assign(context, temp + 1);
            return temp;
        }
    }

    public class EPostfixDecrease : IEstimator
    {
        IEVariable variable;

        public EPostfixDecrease(IEVariable variable)
        {
            this.variable = variable;
        }

        public float Estimate(Context context)
        {
            float temp = variable.Estimate(context);
            variable.Assign(context, temp - 1);
            return temp;
        }
    }


    // Functions...

    public class EFunction : IEstimator
    {
        CodeBlock codeBlock;

        public EFunction(CodeBlock codeBlock)
        {
            this.codeBlock = codeBlock;
        }

        public float Estimate(Context context)
        {
            codeBlock.Estimate(context);

            // if not returned => Syntax Error

            float returnValue = context.ReturnValue;
            context.ResetReturn();

            return returnValue;
        }
    }


    // Strings...

    public class EString : IEstimator
    {
        public EString()
        {
            Parser parser;
            
        }
    }

    public class EStartAction : IEstimator
    {
        
    }
}
