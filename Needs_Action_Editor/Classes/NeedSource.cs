﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Classes
{
    public class NeedSource
    {
        string name;
        CodeSnippet activation;
        CodeSnippet priority;
        string[] actionInfos;

        public bool Modified
        {
            get; set;
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public bool Enable
        {
            get; set;
        }

        public string Extend
        {
            get;
            set;
        }

        public CodeSnippet Activation
        {
            get
            {
                return activation;
            }

            set
            {
                activation = value;
            }
        }

        public CodeSnippet Priority
        {
            get
            {
                return priority;
            }

            set
            {
                priority = value;
            }
        }

        public string[] ActionInfos
        {
            get
            {
                return actionInfos;
            }

            set
            {
                actionInfos = value;
            }
        }

        public NeedSource(string name, string extend, CodeSnippet activation, CodeSnippet priority, string[] actionInfos)
        {
            this.name = name;
            this.Extend = extend;
            this.activation = activation;
            this.priority = priority;
            this.actionInfos = actionInfos;
            Modified = false;
        }
    }
}
