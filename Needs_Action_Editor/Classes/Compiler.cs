﻿#define OPTIMIZE_LV1

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Text.RegularExpressions;
using Microsoft.CodeAnalysis.Scripting;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis;
using System.Linq.Expressions;

namespace Needs_Action_Editor.Classes
{
    public class Compiler
    {
        static readonly string ActionProcessTemplate;
        static readonly string ActionTemplate;

        static readonly string NeedTemplate;

        static readonly string MetabolismTemplate;

        static readonly string WorldTemplate;

        static Compiler()
        {
            var assembly = Assembly.GetExecutingAssembly();

            {
                var stream = assembly.GetManifestResourceStream("Needs_Action_Editor.Resources.ActionProcessTemplate1.txt");
                var reader = new StreamReader(stream);
                ActionProcessTemplate = reader.ReadToEnd();
            }

            {
                var stream = assembly.GetManifestResourceStream("Needs_Action_Editor.Resources.ActionTemplate.txt");
                var reader = new StreamReader(stream);
                ActionTemplate = reader.ReadToEnd();
            }

            {
                var stream = assembly.GetManifestResourceStream("Needs_Action_Editor.Resources.NeedTemplate.txt");
                var reader = new StreamReader(stream);
                NeedTemplate = reader.ReadToEnd();
            }

            {
                var stream = assembly.GetManifestResourceStream("Needs_Action_Editor.Resources.MetabolismTemplate1.txt");
                var reader = new StreamReader(stream);
                MetabolismTemplate = reader.ReadToEnd();
            }

            {
                var stream = assembly.GetManifestResourceStream("Needs_Action_Editor.Resources.WorldTemplate.txt");
                var reader = new StreamReader(stream);
                WorldTemplate = reader.ReadToEnd();
            }
        }

        ScriptOptions _options;

        Models.CitizenMetaData _metadata;

        public Compiler(Models.CitizenMetaData metadata)
        {
            this._metadata = metadata;

            _options = ScriptOptions.Default;
            _options = _options.AddReferences(Assembly.GetEntryAssembly(), Assembly.GetAssembly(typeof(UnityEngine.Mathf)), Assembly.GetAssembly(typeof(System.Collections.ObjectModel.ObservableCollection<float>)));
            _options = _options.AddImports("Needs_Action_Editor.UnityFeatures");
            _options = _options.WithEmitDebugInformation(false);
        }

        string ProcessCitizenParameter(string code)
        {
            var citizenParameters = _metadata.Parameters;
            for (int i = 0; i < citizenParameters.Count; ++i)
            {
                //code = code.Replace("@" + citizenParameters[i].Name, "__citizen[\"" + citizenParameters[i].Name + "\"]");
                code = code.Replace("@" + citizenParameters[i].Name, "__citizen.Parameters[" + i.ToString() + "]");
            }

            code = code.Replace("@Position", "__citizen.Position");
            code = code.Replace("@MoveTo", "__citizen.MoveTo");

            code = code.Replace("World.Tick", "__citizen.World.Tick");
            code = code.Replace("World.Time", "__citizen.World.Time");

            code = Regex.Replace(code, @"World.\s*([_a-zA-Z]+[_a-zA-Z0-9]*)", (match) =>
            {
                if (match.Value == "World.Tick" || match.Value == "World.Time") { return match.Value; }
                return string.Format("__citizen.World.VariableTable[\"{0}\"].Cast<{1}>().Value", match.Groups[1], Models.Simulator.Instance.World.VariableTable[match.Groups[1].Value].Type);
            });

            code = code.Replace("Need.Activation", "__citizen.NeedActivation");

            return code;
        }

        string ProcessGlobalVariable(string code, Models.Variable[] globalVariables)
        {
            if (string.IsNullOrEmpty(code)) { return code; }

            string processed = "";

            int close = 0;
            int open;
            while((open = NextQuote(code, close)) != -1)
            {
                processed += ReplaceGlobalVariable(code.Substring(close, open - close), globalVariables);

                close = NextQuote(code, open + 1) + 1;

                if (close < open) { throw new Exception("Quotation Error"); }
                processed += code.Substring(open, close - open);
            }

            //processed += code.Substring(close);
            processed += ReplaceGlobalVariable(code.Substring(close), globalVariables);

            return processed;
        }

        string ReplaceGlobalVariable(string code, Models.Variable[] globalVariables)
        {
            for (int i = 0; i < globalVariables.Length; ++i)
            {
                /*code = code.Replace(globalVariables[i].Name,
                                    string.Format("__scope.GetVariable<{0}>(\"{1}\").Value",
                                                  globalVariables[i].Type,
                                                  globalVariables[i].Name));*/
                var ex = string.Format(@"(^|[^A-Za-z0-9_]+)({0})($|[^A-Za-z0-9_]+)", globalVariables[i].Name);
                code = Regex.Replace(code, ex, (match) =>
                {
                    return match.Value.Replace(globalVariables[i].Name,
                                               string.Format("__scope.GetVariable(\"{1}\").Cast<{0}>().Value",
                                                             globalVariables[i].Type,
                                                             globalVariables[i].Name));
                });
            }

            return code;
        }

        int NextQuote(string str, int begin)
        {
            if (begin < 0 || begin >= str.Length) { throw new ArgumentOutOfRangeException("begin"); }

            for (int i = begin; i < str.Length; ++i)
            {
                if (str[i] == '"') { return i; }
            }

            return -1;
        }

        public void Compile(ActionSource actionSource, ActionInfo target)
        {
            string className = "A" + actionSource.Name;

            string code = ActionTemplate;

            try
            {
                string efficiencyCode = PreProcessActionEfficiency(actionSource.Efficiency);
                string processCode = PreProcessActionProcess(actionSource.Prepare, actionSource.Plan, actionSource.Work, actionSource.GlobalVariables, actionSource.Parameters);

                code = code.Replace("#__EFFICIENCY_BODY__", efficiencyCode);
                code = code.Replace("#__PROCESS_BODY__", processCode);

                CSharpScript.RunAsync(code, _options, target).Wait();
            }
            catch (Exception e)
            {
                throw new CompilerException("Action : " + actionSource.Name, e);
            }
        }

        string PreProcessActionEfficiency(CodeSnippet effiencyCodeSnippet)
        {
            string code = effiencyCodeSnippet.Code;

            #if OPTIMIZE_LV1
            // @name => citizen.parameter["name"]
            code = ProcessCitizenParameter(code);
            #endif

            if (effiencyCodeSnippet.IsExpression)
            {
                code = "return " + code + ";";
            }
            return code;
        }

        string PreProcessActionProcess(CodeSnippet prepareCodeSnippet, 
                                       CodeSnippet planCodeSnippet, 
                                       CodeSnippet workCodeSnippet, 
                                       Models.Variable[] globalVariables,
                                       Models.Variable[] parameters)
        {
            string process = ActionProcessTemplate;

            #region Prepare
            {
                //string code = prepareCodeSnippet.Code;

                //code = ProcessCitizenParameter(code);
                //code = ProcessGlobalVariable(code, globalVariables);
                //code = ProcessGlobalVariable(code, parameters);

                process = process.Replace("#__PREPARE_BODY__", "");
            }
            #endregion

            #region Plan
            {
                string code = planCodeSnippet.Code;

                code = ProcessCitizenParameter(code);
                code = ProcessGlobalVariable(code, globalVariables);
                code = ProcessGlobalVariable(code, parameters);

                code = Regex.Replace(code, @"Work\s*\(\)\s*;", "return 2;");
                code = Regex.Replace(code, @"StartAction\s*\(", "__citizen.StartAction(");
                code = Regex.Replace(code, @"__citizen.StartAction\(.*;", (match)=> { return "{" + match.Value + " return 1; }"; });

                process = process.Replace("#__PLAN__BODY__", code);
                process = process.Replace("#__PLAN_LABELS__", "");
            }
            #endregion

            #region Work
            {
                string code = workCodeSnippet.Code;

                code = ProcessCitizenParameter(code);
                code = ProcessGlobalVariable(code, globalVariables);
                code = ProcessGlobalVariable(code, parameters);

                code = Regex.Replace(code, @"Finish\s*\(\s*\)\s*;", "return -1;");
                code = Regex.Replace(code, @"Plan\s*\(\s*\)\s*;", "return 1;");

                process = process.Replace("#__WORK_BODY__", code);
            }
            #endregion

            return process;

            // Work(); -> return;
            //scriptSource = Regex.Replace(scriptSource, @"^Work\s*\(\)\s*;$", "return;");

            // StartAction("name", args...); -> StartAction(__citizen, "name", args...);
            //scriptSource = Regex.Replace(scriptSource, @"^StartAction\s*($", "StartActions(__citizen, ");

            //return scriptSource;
        }

        public void Compile(NeedSource source, NeedInfo target)
        {
            string className = "N" + source.Name;
            string code = NeedTemplate;

            try
            {
                string activationCode = PreProcessNeedActivation(source.Activation);
                string priorityCode = PreProcessNeedPriority(source.Priority);

                code = code.Replace("#__ACTIVATION_BODY__", activationCode);
                code = code.Replace("#__PRIORITY_BODY__", priorityCode);

                CSharpScript.RunAsync(code, _options, target).Wait();
            }
            catch (Exception e)
            {
                throw new CompilerException("Need : " + source.Name, e);
            }
        }

        string PreProcessNeedActivation(CodeSnippet activationSnippet)
        {
            string code = activationSnippet.Code;

            code = ProcessCitizenParameter(code);

            if (activationSnippet.IsExpression)
            {
                code = "return " + code + ";";
            }
            return code;
        }

        string PreProcessNeedPriority(CodeSnippet prioritySnippet)
        {
            string code = prioritySnippet.Code;

            code = ProcessCitizenParameter(code);

            if (prioritySnippet.IsExpression)
            {
                code = "return " + code + ";";
            }
            return code;
        }

        public void CompileMetabolism()
        {
            try
            {
                var metabolismSource = PreProcessMetabolism(_metadata.MetabolismScript);
                var code = MetabolismTemplate.Replace("#__METABOLISM_BODY__", metabolismSource);

                CSharpScript.RunAsync(code, _options, _metadata).Wait();
            }
            catch (Exception e)
            {
                throw new CompilerException("Metabolism", e);
            }
        }

        string PreProcessMetabolism(string code)
        {
            code = ProcessCitizenParameter(code);
            return code;
        }

        public void CompileWorld(Models.World world)
        {
            string code = world.Script ?? "";
            code = Regex.Replace(code, @"World.\s*([_a-zA-Z]+[_a-zA-Z0-9]*)", (match) =>
            {
                var variable = match.Groups[1].Value;
                if (variable == "Time" || variable == "Tick") { return string.Format("__world.{0}", variable); }
                return string.Format("__world.VariableTable[\"{0}\"].Cast<{1}>().Value", match.Groups[1], world.VariableTable[match.Groups[1].Value].Type);
            });
            code = WorldTemplate.Replace("#__UPDATE_BODY__", code);

            try
            {
                CSharpScript.RunAsync(code, _options, world).Wait();
            }
            catch (Exception e)
            {
                throw new CompilerException("World", e);
            }
        }
    }

    public class CompilerException : Exception
    {
        public CompilerException() : base() { }
        public CompilerException(string message) : base(message) { }
        public CompilerException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class RuntimeException : Exception
    {
        public RuntimeException() : base() { }
        public RuntimeException(string message) : base(message) { }
        public RuntimeException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class Scope
    {
        Dictionary<string, Models.Variable> variables;

        public Scope()
        {
            variables = new Dictionary<string, Models.Variable>();
        }

        public Models.Variable GetVariable(string name)
        {
            return variables[name];
        }

        public void SetVariable<T>(string name, string type, T value)
        {
            variables[name] = Models.Variable.Create(name, type, value);
        }

        public void SetVariable(string name, string type, object value)
        {
            switch (type)
            {
                case "int":
                    {
                        if (!(value is int))
                        {
                            throw new RuntimeException(string.Format("Type Error : passing {0} into <int>", value.GetType()));
                        }
                        SetVariable(name, type, (int)value);
                        break;
                    }
                case "float":
                    {
                        if (!(value is float))
                        {
                            throw new RuntimeException(string.Format("Type Error : passing {0} into <float>", value.GetType()));
                        }
                        SetVariable(name, type, (float)value);
                        break;
                    }
                case "bool":
                    {
                        if (!(value is bool))
                        {
                            throw new RuntimeException(string.Format("Type Error : passing {0} into <bool>", value.GetType()));
                        }
                        SetVariable(name, type, (bool)value);
                        break;
                    }
                case "string":
                    if (!(value is string))
                    {
                        throw new RuntimeException(string.Format("Type Error : passing {0} into <string>", value.GetType()));
                    }
                    SetVariable(name, type, (string)value);
                    break;
            }
        }

        public void SetVariable(Models.Variable variable)
        {
            variables[variable.Name] = new Models.Variable(variable);
        }
    }
}
