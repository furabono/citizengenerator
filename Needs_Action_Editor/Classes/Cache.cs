﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Classes
{
    public class Cache<TKey, TValue>
    {
        LinkedList<TKey> _cacheQueue;
        Dictionary<TKey, TValue> _cache;

        int _maxCount;
        public int MaxCount
        {
            get { return _maxCount; }
        }

        public Cache(int maxCount)
        {
            _cacheQueue = new LinkedList<TKey>();
            _cache = new Dictionary<TKey, TValue>();

            _maxCount = maxCount;
        }

        public void Add(TKey key, TValue value)
        {
            if (value == null) { return; }

            if (_cache.ContainsKey(key))
            {
                _cacheQueue.Remove(key);
                _cache.Remove(key);
            }
            else if (_cacheQueue.Count == _maxCount)
            {
                var old = _cacheQueue.First.Value;
                _cacheQueue.RemoveFirst();
                _cache.Remove(old);
            }

            _cacheQueue.AddLast(key);
            _cache.Add(key, value);
        }

        public bool Remove(TKey key)
        {
            if (_cache.ContainsKey(key))
            {
                _cacheQueue.Remove(key);
                _cache.Remove(key);

                return true;
            }

            return false;
        }

        public bool ContainsKey(TKey key)
        {
            return _cache.ContainsKey(key);
        }

        public TValue Get(TKey key)
        {
            return _cache[key];
        }
    }
}
