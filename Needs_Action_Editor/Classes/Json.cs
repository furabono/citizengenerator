﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Needs_Action_Editor.Models;
using System.IO;
using System.Collections.ObjectModel;

namespace Needs_Action_Editor.Classes
{
    public class Json
    {
        public static readonly string NeedSourcePath = @"NeedSources\";
        public static readonly string ActionSourcePath = @"ActionSources\";

        static void WriteJsonFile(string directory, string file, JObject obj)
        {
            System.IO.Directory.CreateDirectory(directory);
            using (StreamWriter sw = new StreamWriter(File.Open(directory + file, FileMode.Create)))
            using (JsonTextWriter writer = new JsonTextWriter(sw))
            {
                obj.WriteTo(writer);
            }
        }

        static void WriteJsonFile(string file, JObject obj)
        {
            using (StreamWriter sw = new StreamWriter(File.Open(file, FileMode.Create)))
            using (JsonTextWriter writer = new JsonTextWriter(sw))
            {
                obj.WriteTo(writer);
            }
        }

        static JObject ReadJsonFile(string file)
        {
            JObject jobject;

            using (StreamReader sr = File.OpenText(file))
            using (JsonTextReader reader = new JsonTextReader(sr))
            {
                jobject = JObject.Load(reader);
            }

            return jobject;
        }

        static JObject VariableToJObject(Variable var)
        {
            return new JObject(new JProperty("Name", var.Name), new JProperty("Type", var.Type), new JProperty("InitValue", var.InitValue.Value));
        }

        static Variable JObjectToVariable(JObject obj)
        {
            string name = obj["Name"].Value<string>();
            string type = obj["Type"].Value<string>();
            IValueHolder vh = null;

            switch (type)
            {
                case "int":
                    vh = new ValueHolder<int>(type, obj["InitValue"].Value<int>());
                    break;
                case "float":
                    vh = new ValueHolder<float>(type, obj["InitValue"].Value<float>());
                    break;
                case "bool":
                    vh = new ValueHolder<bool>(type, obj["InitValue"].Value<bool>());
                    break;
                case "string":
                    vh = new ValueHolder<string>(type, obj["InitValue"].Value<string>());
                    break;
            }

            return new Variable(name, type, vh);
        }

        public static void WriteNeedSource(NeedSource needSource, string workingDirectory, string subDirectory = null)
        {
            JObject obj = new JObject();

            obj.Add(new JProperty("Name", needSource.Name));
            obj.Add(new JProperty("Extend", needSource.Extend));
            obj.Add(new JProperty("Activation", new JObject(new JProperty("IsExpression", needSource.Activation.IsExpression),
                                                            new JProperty("Code", needSource.Activation.Code))));
            obj.Add(new JProperty("Priority", new JObject(new JProperty("IsExpression", needSource.Priority.IsExpression),
                                                          new JProperty("Code", needSource.Priority.Code))));
            obj.Add(new JProperty("ActionInfos", new JArray(needSource.ActionInfos)));

            WriteJsonFile(workingDirectory + subDirectory, needSource.Name + ".json", obj);
        }

        public static NeedSource ReadNeedSource(string file)
        {
            JObject obj = ReadJsonFile(file);

            string name = obj["Name"].Value<string>();
            string extend = obj["Extend"]?.Value<string>();
            CodeSnippet activation = new CodeSnippet(obj["Activation"]["Code"].Value<string>(),
                                                     obj["Activation"]["IsExpression"].Value<bool>());
            CodeSnippet priority = new CodeSnippet(obj["Priority"]["Code"].Value<string>(),
                                                   obj["Priority"]["IsExpression"].Value<bool>());
            string[] actionInfos = (from action in obj["ActionInfos"]
                                    select action.Value<string>()).ToArray();

            return new NeedSource(name, extend, activation, priority, actionInfos);
        }

        public static void WriteActionSource(ActionSource actionSource, string workingDirectory, string subDirectory = null)
        {
            JObject obj = new JObject();

            obj.Add(new JProperty("Name", actionSource.Name));
            obj.Add(new JProperty("Efficiency", new JObject(new JProperty("IsExpression", actionSource.Efficiency.IsExpression),
                                                            new JProperty("Code", actionSource.Efficiency.Code))));
            obj.Add(new JProperty("Parameters", new JArray(from p in actionSource.Parameters
                                                           select VariableToJObject(p))));
            obj.Add(new JProperty("GlobalVariables", new JArray(from p in actionSource.GlobalVariables
                                                                select VariableToJObject(p))));
            obj.Add(new JProperty("Prepare", actionSource.Prepare.Code));
            obj.Add(new JProperty("Plan", actionSource.Plan.Code));
            obj.Add(new JProperty("Work", actionSource.Work.Code));
            obj.Add(new JProperty("Revision", 1));

            WriteJsonFile(workingDirectory + subDirectory, actionSource.Name + ".json", obj);
        }

        public static ActionSource ReadActionSource(string file)
        {
            JObject obj = ReadJsonFile(file);

            int revision = obj["Revision"]?.Value<int>() ?? 0;

            switch(revision)
            {
                case 0:
                    return ReadActionSource0(obj);
                case 1:
                    return ReadActionSource1(obj);
            }

            return null;
        }

        public static ActionSource ReadActionSource1(JObject obj)
        {
            var name = obj["Name"].Value<string>();
            var efficiency = new CodeSnippet(obj["Efficiency"]["Code"].Value<string>(),
                                             obj["Efficiency"]["IsExpression"].Value<bool>());
            var parameters = (from p in obj["Parameters"]
                              select JObjectToVariable((JObject)p)).ToArray();
            var globalVariables = (from p in obj["GlobalVariables"]
                                   select JObjectToVariable((JObject)p)).ToArray();
            var prepare = new CodeSnippet(obj["Prepare"].Value<string>(), false);
            var plan = new CodeSnippet(obj["Plan"].Value<string>(), false);
            var work = new CodeSnippet(obj["Work"].Value<string>(), false);

            return new ActionSource(name, efficiency, prepare, plan, work, globalVariables, parameters);
        }

        public static ActionSource ReadActionSource0(JObject obj)
        {
            var name = obj["Name"].Value<string>();
            var efficiency = new CodeSnippet(obj["Efficiency"]["Code"].Value<string>(),
                                             obj["Efficiency"]["IsExpression"].Value<bool>());
            var parameters = (from p in obj["Parameters"]
                              select Variable.Parse(p["Name"].Value<string>(),
                                                    p["Type"].Value<string>(),
                                                    p["InitialValue"].Value<string>())).ToArray();
            var globalVariables = (from p in obj["GlobalVariables"]
                                   select Variable.Parse(p["Name"].Value<string>(),
                                                         p["Type"].Value<string>(),
                                                         p["InitialValue"].Value<string>())).ToArray();
            var prepare = new CodeSnippet(obj["Prepare"].Value<string>(), false);
            var plan = new CodeSnippet(obj["Plan"].Value<string>(), false);
            var work = new CodeSnippet(obj["Work"].Value<string>(), false);

            return new ActionSource(name, efficiency, prepare, plan, work, globalVariables, parameters);
        }

        public static void WriteCitizenMetaData(CitizenMetaData citizenMetaData, string workingDirectory)
        {
            JObject obj = new JObject();

            obj.Add(new JProperty("StartPosition", new JArray(citizenMetaData.StartPosition.x, citizenMetaData.StartPosition.y)));

            obj.Add(new JProperty("Parameters", new JArray(from meta in citizenMetaData.Parameters
                                                           select new JArray(meta.Name, meta.InitialValue))));
            obj.Add(new JProperty("Metabolism", citizenMetaData.MetabolismScript));

            obj.Add(new JProperty("Revision", 2));

            WriteJsonFile(workingDirectory, "Metabolism.json", obj);
        }

        public static CitizenMetaData ReadCitizenMetaData(string directory)
        {
            JObject obj = ReadJsonFile(directory + "Metabolism.json");

            switch (obj["Revision"]?.Value<int>() ?? 0)
            {
                case 0:
                    return ReadCitizenMetaData0(obj);
                case 1:
                    return ReadCitizenMetaData1(obj);
                case 2:
                    return ReadCitizenMetaData2(obj);
            }

            return null;
        }

        static CitizenMetaData ReadCitizenMetaData0(JObject obj)
        {
            var metadata = new CitizenMetaData();
            var parameters = new ObservableCollection<CitizenMetaData.ParameterMeta>(
                                        from param in obj["Parameters"]
                                        select new CitizenMetaData.ParameterMeta(param.Value<string>()));
            metadata.UpdateParameters(parameters);
            metadata.MetabolismScript = obj["Metabolism"].Value<string>();

            return metadata;
        }

        static CitizenMetaData ReadCitizenMetaData1(JObject obj)
        {
            var metadata = new CitizenMetaData();
            var parameters = new ObservableCollection<CitizenMetaData.ParameterMeta>(
                                        from param in obj["Parameters"]
                                        select new CitizenMetaData.ParameterMeta(param[0].Value<string>(), param[1].Value<float>()));
            metadata.UpdateParameters(parameters);
            metadata.MetabolismScript = obj["Metabolism"].Value<string>();

            return metadata;
        }

        static CitizenMetaData ReadCitizenMetaData2(JObject obj)
        {
            var metadata = new CitizenMetaData();
            metadata.StartPosition = new Vector2(obj["StartPosition"][0].Value<float>(), obj["StartPosition"][1].Value<float>());
            var parameters = new ObservableCollection<CitizenMetaData.ParameterMeta>(
                                        from param in obj["Parameters"]
                                        select new CitizenMetaData.ParameterMeta(param[0].Value<string>(), param[1].Value<float>()));
            metadata.UpdateParameters(parameters);
            metadata.MetabolismScript = obj["Metabolism"].Value<string>();

            return metadata;
        }

        public static void WriteWorld(World world, string workingDirectorJy)
        {
            JObject obj = new JObject();

            obj.Add(new JProperty("Variables", new JArray(from v in world.Variables select VariableToJObject(v))));
            obj.Add(new JProperty("Script", world.Script));
            obj.Add(new JProperty(
                "Objects",
                new JArray(
                    from o in world.InitObjects.List
                    select new JObject(
                        new JProperty("Object", o.Object.ID),
                        new JProperty("X", o.Position.x),
                        new JProperty("Y", o.Position.y)))));
            obj.Add(new JProperty("Revision", 3));

            WriteJsonFile(workingDirectorJy, "World.json", obj);
        }

        public static World ReadWorld(string directory, ObservableCollection<ObjectMeta> objects)
        {
            JObject obj = ReadJsonFile(directory + "World.json");

            int revision = obj["Revision"]?.Value<int>() ?? 0;

            switch (revision)
            {
                case 0:
                case 1:
                    return ReadWorld1(obj);
                case 2:
                    return ReadWorld2(obj, objects);
                case 3:
                    return ReadWorld3(obj, objects);
            }

            return null;
        }

        static World ReadWorld1(JObject obj)
        {
            ObservableCollection<Variable> variables = new ObservableCollection<Variable>();
            foreach (var v in obj["Variables"])
            {
                string name = v["Name"].Value<string>();
                string type = v["Type"].Value<string>();
                IValueHolder vh = null;

                switch (type)
                {
                    case "int":
                        vh = new ValueHolder<int>(type, v["Value"].Value<int>());
                        break;
                    case "float":
                        vh = new ValueHolder<float>(type, v["Value"].Value<float>());
                        break;
                    case "bool":
                        vh = new ValueHolder<bool>(type, v["Value"].Value<bool>());
                        break;
                    case "string":
                        vh = new ValueHolder<string>(type, v["Value"].Value<string>());
                        break;
                }

                variables.Add(new Variable(name, type, vh));
            }

            string script = obj["Script"]?.Value<string>();
            var wobjects = new ObservableCollection<WorldObject>();

            return new World(variables, script, wobjects);
        }

        static World ReadWorld2(JObject obj, ObservableCollection<ObjectMeta> objects)
        {
            ObservableCollection<Variable> variables = new ObservableCollection<Variable>();
            foreach (var v in obj["Variables"])
            {
                string name = v["Name"].Value<string>();
                string type = v["Type"].Value<string>();
                IValueHolder vh = null;

                switch(type)
                {
                    case "int":
                        vh = new ValueHolder<int>(type, v["InitValue"].Value<int>());
                        break;
                    case "float":
                        vh = new ValueHolder<float>(type, v["InitValue"].Value<float>());
                        break;
                    case "bool":
                        vh = new ValueHolder<bool>(type, v["InitValue"].Value<bool>());
                        break;
                    case "string":
                        vh = new ValueHolder<string>(type, v["InitValue"].Value<string>());
                        break;
                }

                variables.Add(new Variable(name, type, vh));
            }

            string script = obj["Script"].Value<string>();

            var wobjects = new ObservableCollection<WorldObject>();
            if (obj["Objects"] != null)
            {
                int objID;
                float x;
                float y;
                foreach (var o in obj["Objects"])
                {
                    objID = o["Object"].Value<int>();
                    x = o["X"].Value<float>();
                    y = o["Y"].Value<float>();
                    wobjects.Add(new WorldObject(objects[objID], new Vector2(x, y)));
                }
            }

            return new World(variables, script, wobjects);
        }

        static World ReadWorld3(JObject obj, ObservableCollection<ObjectMeta> objects)
        {
            var variables = new ObservableCollection<Variable>(
                from v in obj["Variables"] select JObjectToVariable((JObject)v)
                );

            string script = obj["Script"].Value<string>();

            var wobjects = new ObservableCollection<WorldObject>();
            if (obj["Objects"] != null)
            {
                int objID;
                float x;
                float y;
                foreach (var o in obj["Objects"])
                {
                    objID = o["Object"].Value<int>();
                    x = o["X"].Value<float>();
                    y = o["Y"].Value<float>();
                    wobjects.Add(new WorldObject(objects[objID], new Vector2(x, y)));
                }
            }

            return new World(variables, script, wobjects);
        }

        public static void WriteObjectMetas(ObservableCollection<Property> properties, ObservableCollection<AttributeMeta> attributes, ObservableCollection<ObjectMeta> objects, string workingDirectory)
        {
            JObject obj = new JObject();

            obj.Add("Properties", new JArray(from p in properties select p.Name));
            obj.Add(
                "Attributes",
                new JArray(
                    from a in attributes
                    select new JObject(
                        new JProperty("Name", a.Name),
                        new JProperty("Parent", a.Parent?.ID),
                        new JProperty("OwnProperties", new JArray(from p in a.OwnProperties select p.ID))
                        )
                    )
                );
            obj.Add(
                "Objects",
                new JArray(
                    from o in objects
                    select new JObject(
                        new JProperty("Name", o.Name),
                        new JProperty("Parent", o.Parent.ID),
                        new JProperty("Width", o.Width),
                        new JProperty("Height", o.Height)
                    )));
            obj.Add("Revision", 1);

            WriteJsonFile(workingDirectory, "ObjectMeta.json", obj);
        }

        public static Tuple<ObservableCollection<Property>, ObservableCollection<AttributeMeta>, ObservableCollection<ObjectMeta>> ReadObjectMetas(string directory)
        {
            var obj = ReadJsonFile(directory + "ObjectMeta.json");

            int revision = obj["Revision"]?.Value<int>() ?? 0;
            switch (revision)
            {
                case 0:
                    return ReadObjectMetas0(obj);
                case 1:
                    return ReadObjectMetas1(obj);
            }

            return null;
        }

        public static Tuple<ObservableCollection<Property>, ObservableCollection<AttributeMeta>, ObservableCollection<ObjectMeta>> ReadObjectMetas0(JObject obj)
        {
            var properties = new ObservableCollection<Property>(
                obj["Properties"].Select((token, i)=> new Property(i, token.Value<string>())));

            var attrdict = new Dictionary<int, AttributeMeta>();
            var stack = new Stack<Tuple<int, JToken>>();
            int id;
            string name;
            int parent;
            ObservableCollection<Property> ownproperties;

            JToken attr;
            var attrarr = (JArray)obj["Attributes"];

            attr = attrarr[0];
            ownproperties = new ObservableCollection<Property>(
                                from p in attr["OwnProperties"] select properties[p.Value<int>()]);
            attrdict.Add(0, new AttributeMeta(0, attr["Name"].Value<string>(), null, ownproperties));

            for (int i = 0; i < attrarr.Count; ++i)
            {
                if (!attrdict.ContainsKey(i))
                {
                    parent = i;
                    attr = attrarr[i];
                    do
                    {
                        stack.Push(Tuple.Create(parent, attr));
                        parent = attr["Parent"].Value<int>();
                        attr = attrarr[parent];
                    } while (!attrdict.ContainsKey(parent));
                    while (stack.Any())
                    {
                        id = stack.Peek().Item1;
                        attr = stack.Peek().Item2;
                        stack.Pop();

                        name = attr["Name"].Value<string>();
                        parent = attr["Parent"].Value<int>();
                        ownproperties = new ObservableCollection<Property>(
                            from p in attr["OwnProperties"] select properties[p.Value<int>()]);

                        attrdict.Add(id, new AttributeMeta(id, name, attrdict[parent], ownproperties));
                    }
                }
            }
            ownproperties = null;
            attr = null;
            attrarr = null;

            var attributes = new ObservableCollection<AttributeMeta>(
                from a in attrdict
                orderby a.Key ascending
                select a.Value);

            var objects = new ObservableCollection<ObjectMeta>(
                obj["Objects"].Select((o, i)=>new ObjectMeta(i, o["Name"].Value<string>(), attrdict[o["Parent"].Value<int>()], 1, 1)));

            return Tuple.Create(properties, attributes, objects);
        }

        public static Tuple<ObservableCollection<Property>, ObservableCollection<AttributeMeta>, ObservableCollection<ObjectMeta>> ReadObjectMetas1(JObject obj)
        {
            var properties = new ObservableCollection<Property>(
                obj["Properties"].Select((token, i) => new Property(i, token.Value<string>())));

            var attrdict = new Dictionary<int, AttributeMeta>();
            var stack = new Stack<Tuple<int, JToken>>();
            int id;
            string name;
            int parent;
            ObservableCollection<Property> ownproperties;

            JToken attr;
            var attrarr = (JArray)obj["Attributes"];

            attr = attrarr[0];
            ownproperties = new ObservableCollection<Property>(
                                from p in attr["OwnProperties"] select properties[p.Value<int>()]);
            attrdict.Add(0, new AttributeMeta(0, attr["Name"].Value<string>(), null, ownproperties));

            for (int i = 0; i < attrarr.Count; ++i)
            {
                if (!attrdict.ContainsKey(i))
                {
                    parent = i;
                    attr = attrarr[i];
                    do
                    {
                        stack.Push(Tuple.Create(parent, attr));
                        parent = attr["Parent"].Value<int>();
                        attr = attrarr[parent];
                    } while (!attrdict.ContainsKey(parent));
                    while (stack.Any())
                    {
                        id = stack.Peek().Item1;
                        attr = stack.Peek().Item2;
                        stack.Pop();

                        name = attr["Name"].Value<string>();
                        parent = attr["Parent"].Value<int>();
                        ownproperties = new ObservableCollection<Property>(
                            from p in attr["OwnProperties"] select properties[p.Value<int>()]);

                        attrdict.Add(id, new AttributeMeta(id, name, attrdict[parent], ownproperties));
                    }
                }
            }
            ownproperties = null;
            attr = null;
            attrarr = null;

            var attributes = new ObservableCollection<AttributeMeta>(
                from a in attrdict
                orderby a.Key ascending
                select a.Value);

            var objects = new ObservableCollection<ObjectMeta>(
                obj["Objects"].Select((o, i) => new ObjectMeta(i, o["Name"].Value<string>(), attrdict[o["Parent"].Value<int>()], o["Width"].Value<float>(), o["Height"].Value<float>())));

            return Tuple.Create(properties, attributes, objects);
        }

        public static void WriteCitizens(ObservableCollection<Citizen> citizens, CitizenMetaData citizenMetaData, string file)
        {
            JObject obj = new JObject();

            obj.Add(new JProperty("Parameters", new JArray(from meta in citizenMetaData.Parameters
                                                           select meta.Name)));
            //obj.Add(new JProperty("Citizens", new JArray(from citizen in citizens
            //                                             select new JObject(from param in citizen.Parameters
            //                                                                select new JProperty(meta.Name, param)))));

            obj.Add(new JProperty("Citizens", new JArray(from citizen in citizens
                                                         select new JArray(citizen.Parameters.Select((value, index)=> new JProperty(citizenMetaData.Parameters[index].Name, value))))));

            WriteJsonFile(file, obj);
        }

        //public static ObservableCollection<Citizen> ReadCitizens(string file, CitizenMetaData citizenMetaData)
        //{
        //    JObject obj = ReadJsonFile(file);

        //    var parameters = (from param in obj["Parameters"] select param.Value<string>()).ToArray();

        //    var missings = citizenMetaData.ParameterNameTable.Keys.Except(parameters).ToArray();
        //    var includes = citizenMetaData.ParameterNameTable.Keys.Except(missings).ToArray();

        //    var j_citizens = obj["Citizens"] as JArray;
        //    var citizens = new ObservableCollection<Citizen>();
        //    for (int i = 0; i < j_citizens.Count; ++i)
        //    {
        //        var param = new Dictionary<CitizenMetaData.ParameterMeta, float>();

        //        for (int j = 0; j < includes.Length; ++j)
        //        {
        //            param.Add(citizenMetaData.ParameterNameTable[includes[j]], j_citizens[i][includes[j]].Value<float>());
        //        }
        //        for (int j = 0; j < missings.Length; ++j)
        //        {
        //            param.Add(citizenMetaData.ParameterNameTable[missings[j]], 0);
        //        }

        //        citizens.Add(new Citizen(i, null, param));
        //    }

        //    return citizens;
        //}
    }
}
