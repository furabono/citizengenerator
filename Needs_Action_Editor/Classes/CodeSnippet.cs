﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Classes
{
    public class CodeSnippet
    {
        string code;
        bool isExpression;

        public string Code
        {
            get { return code; }
        }

        public bool IsExpression
        {
            get { return isExpression; }
        }

        public CodeSnippet(string code, bool isExpression)
        {
            this.code = code ?? "";
            this.isExpression = isExpression;
        }
    }
}
