﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Classes
{
    public class ActionSource
    {
        #region Fields
        string name;
        CodeSnippet efficiency;
        CodeSnippet prepare;
        CodeSnippet plan;
        CodeSnippet work;
        Models.Variable[] globalVariables;
        Models.Variable[] parameters;
        #endregion

        #region Properties
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public CodeSnippet Efficiency
        {
            get
            {
                return efficiency;
            }

            set
            {
                efficiency = value;
            }
        }

        public CodeSnippet Prepare
        {
            get
            {
                return prepare;
            }

            set
            {
                prepare = value;
            }
        }

        public CodeSnippet Plan
        {
            get
            {
                return plan;
            }

            set
            {
                plan = value;
            }
        }

        public CodeSnippet Work
        {
            get
            {
                return work;
            }

            set
            {
                work = value;
            }
        }

        public Models.Variable[] GlobalVariables
        {
            get
            {
                return globalVariables;
            }

            set
            {
                globalVariables = value;
            }
        }

        public Models.Variable[] Parameters
        {
            get
            {
                return parameters;
            }

            set
            {
                parameters = value;
            }
        }
        #endregion

        public ActionSource(string name, 
                            CodeSnippet efficiency, 
                            CodeSnippet prepare, 
                            CodeSnippet plan, 
                            CodeSnippet work,
                            Models.Variable[] globalVariables,
                            Models.Variable[] parameters)
        {
            this.name = name;
            this.efficiency = efficiency;
            this.prepare = prepare;
            this.plan = plan;
            this.work = work;
            this.globalVariables = globalVariables;
            this.parameters = parameters;
        }
    }

    public class VariableInfo
    {
        public string Name
        {
            get; set;
        }

        public string Type
        {
            get; set;
        }

        public string InitialValue
        {
            get; set;
        }

        public VariableInfo() : this("var", "object", "") { }

        public VariableInfo(string name) : this(name, "object", "") { }

        public VariableInfo(string name, string type) : this(name, type, "") { }

        public VariableInfo(string name, string type, string init)
        {
            Name = name;
            Type = type;
            InitialValue = init;
        }
    }
}
