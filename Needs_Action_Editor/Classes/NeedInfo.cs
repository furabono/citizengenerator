﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Classes
{
    public class NeedInfo
    {
        public NeedSource Source
        {
            get; set;
        }

        public void Update(Compiler compiler)
        {
            compiler.Compile(Source, this);
            this.name = Source.Name;
            this.actionInfos = (from action in Source.ActionInfos
                                select ActionInfo.Find(action)).ToArray();
        }

        string name;
        //string[] actionNames;

        ActionInfo[] actionInfos;

        public delegate bool ActivationDelegate(Citizen citizen);
        public delegate float PriorityDelegate(Citizen citizen);

        public ActivationDelegate Activation
        {
            get;
            set;
        }

        public PriorityDelegate Priority
        {
            get;
            set;
        }

        public string Name
        {
            get { return name; }
        }

        public NeedInfo Extend
        {
            get;
            set;
        }

        public ActionInfo[] ActionInfos
        {
            get { return actionInfos; }
        }

        public NeedInfo()
        {

        }

        public NeedInfo(string name, NeedInfo extend, ActivationDelegate activation, PriorityDelegate priority, ActionInfo[] actionInfos)
        {
            this.name = name;
            this.Extend = extend;
            Activation = activation;
            Priority = priority;
            this.actionInfos = actionInfos;
            
            //this.actionNames = actionNames;
        }

        public void Start(Citizen citizen)
        {
            float maxEfficiency = ActionInfos[0].Efficiency(citizen);
            ActionInfo maxAction = ActionInfos[0];
            float efficiency;
            for (int i = 1; i < ActionInfos.Length; ++i)
            {
                efficiency = ActionInfos[i].Efficiency(citizen);
                if (efficiency > maxEfficiency)
                {
                    maxEfficiency = efficiency;
                    maxAction = ActionInfos[i];
                }
            }

            citizen.ClearAction();
            maxAction.CreateAction(citizen).Start();
        }

        //public Need CreateNeed(Citizen citizen)
        //{

        //}
    }
}
