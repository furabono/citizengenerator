﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needs_Action_Editor.Classes
{
    public class Citizen : Models.ModelBase
    {
        public Models.CitizenMetaData Metadata
        {
            get; set;
        }

        int id;
        List<NeedInfo> needs;
        //Dictionary<Models.CitizenMetaData.ParameterMeta, float> parameters;
        ObservableCollection<float> _parameters;

        NeedInfo priorNeed;
        Stack<Action> actionStack;

        /*public float this[string parameterName]
        {
            get { return parameters[Models.Simulator.Instance.CitizenMetaData.ParameterNameTable[parameterName]]; }
            set { parameters[Models.Simulator.Instance.CitizenMetaData.ParameterNameTable[parameterName]] = value; }
        }*/

        Models.Vector2 _position;

        public Models.Vector2 Position
        {
            get { return _position; }
        }

        public int ID
        {
            get { return id; }
        }

        public List<NeedInfo> Needs
        {
            get { return needs; }
            set { needs = value; }
        }

        public NeedInfo PriorNeed
        {
            get { return priorNeed; }
            set
            {
                priorNeed = value;
                RaisePropertyChanged("PriorNeed");
            }
        }

        public List<NeedInfo> ActiveNeeds
        {
            get; private set;
        }

        public Action Acting
        {
            get
            {
                if (actionStack.Count > 0)
                {
                    return actionStack.Peek();
                }
                return null;
            }
        }

        public Models.World World
        {
            get { return Models.Simulator.Instance.World; }
        }

        /*public Dictionary<Models.CitizenMetaData.ParameterMeta, float> Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }*/

        public ObservableCollection<float> Parameters
        {
            get { return _parameters; }
        }

        public Inventory Items
        {
            get; private set;
        }

        /*public Citizen(int id, List<NeedInfo> needInfos, Dictionary<Models.CitizenMetaData.ParameterMeta, float> parameters)
        {
            this.id = id;
            needs = needInfos;
            //this.parameters = parameters;

            actionStack = new Stack<Action>();
            ActiveNeeds = new List<NeedInfo>();
        }*/

        public Citizen(int id, List<NeedInfo> needInfos, ObservableCollection<float> parameters, Inventory inventory, Models.Vector2 startPosition)
        {
            this.id = id;
            needs = needInfos;
            _parameters = parameters;

            actionStack = new Stack<Action>();
            ActiveNeeds = new List<NeedInfo>();

            _position = new Models.Vector2(startPosition);

            Items = new Inventory(inventory);
        }

        public void StartAction(string actionName, params object[] arguments)
        {
            ActionInfo.Find(actionName).CreateAction(this).Start(arguments);
        }

        public void PushAction(Action action)
        {
            actionStack.Push(action);
            RaisePropertyChanged("Acting");
        }

        public void PopAction()
        {
            actionStack.Pop();
            RaisePropertyChanged("Acting");
        }

        public void ClearAction()
        {
            actionStack.Clear();
            RaisePropertyChanged("Acting");
        }

        public bool NeedActivation
        {
            get { return (priorNeed != null) ? (priorNeed.Activation(this)) : (false); }
        }

        public void MoveTo(float x, float y)
        {
            _position.x = x;
            _position.y = y;
        }

        public void Update()
        {
            Metadata.Metabolism(this);

            if (actionStack.Count > 0)
            {
                actionStack.Peek().Process();
            }

            {
                NeedInfo maxNeed = null;
                float maxPriority = float.MinValue;
                float priority;
                for (int i = 0; i < needs.Count; ++i)
                {
                    priority = needs[i].Priority(this);
                    if (needs[i].Activation(this) && priority > maxPriority)
                    {
                        maxNeed = needs[i];
                        maxPriority = priority;
                    }
                }

                if (!Equals(priorNeed, maxNeed) || !actionStack.Any())
                {
                    PriorNeed = maxNeed;
                    PriorNeed?.Start(this);
                }
            }
        }
    }

    public class Inventory
    {
        public Dictionary<Models.ObjectMeta, int> InventoryTable
        {
            get; set;
        }

        public ObservableCollection<InventoryEntry> Items
        {
            get; set;
        }

        public Inventory()
        {
            InventoryTable = new Dictionary<Models.ObjectMeta, int>();
            Items = new ObservableCollection<InventoryEntry>();
        }

        public Inventory(Inventory inventory)
        {
            InventoryTable = new Dictionary<Models.ObjectMeta, int>(inventory.InventoryTable);
            Items = new ObservableCollection<InventoryEntry>(from ie in inventory.Items select new InventoryEntry(ie));
        }

        public void AddItem(Models.ObjectMeta obj, int count = 1)
        {
            if (InventoryTable.ContainsKey(obj))
            {
                Items[InventoryTable[obj]].Count += count;
            }
            else
            {
                var ie = new InventoryEntry(obj, count);
                Items.Add(ie);
                InventoryTable.Add(obj, Items.Count - 1);
            }
        }

        public void RemoveItem(Models.ObjectMeta obj, int count = 1)
        {
            if (InventoryTable.ContainsKey(obj))
            {
                var index = InventoryTable[obj];
                if (Items[index].Count >= count && (Items[index].Count -= count) == 0)
                {
                    Items.RemoveAt(index);
                    InventoryTable.Remove(obj);

                    for (int i = index; i < Items.Count; ++i)
                    {
                        InventoryTable[Items[i].Object] = i;
                    }
                }
            }
        }

        public void RemoveItem(Models.AttributeMeta attr, int count = 1)
        {
            int cnt = 0;
            var objs = GetObjects(attr, out cnt);
            if (cnt >= count)
            {
                foreach (var obj in objs)
                {
                    if (obj.Count >= count)
                    {
                        if ((obj.Count -= count) == 0)
                        {
                            Items.RemoveAt(InventoryTable[obj.Object]);
                            InventoryTable.Remove(obj.Object);
                        }
                        break;
                    }
                    else
                    {
                        count -= obj.Count;
                        Items.RemoveAt(InventoryTable[obj.Object]);
                        InventoryTable.Remove(obj.Object);
                    }
                }

                for (int i = InventoryTable[objs[0].Object]; i < Items.Count; ++i)
                {
                    InventoryTable[Items[i].Object] = i;
                }
            }
        }

        public void RemoveAll(Models.ObjectMeta obj)
        {
            if (InventoryTable.ContainsKey(obj))
            {
                var index = InventoryTable[obj];
                Items.RemoveAt(index);
                InventoryTable.Remove(obj);

                for (int i = index; i < Items.Count; ++i)
                {
                    InventoryTable[Items[i].Object] = i;
                }
            }
        }

        public bool HasItem(Models.ObjectMeta obj)
        {
            return InventoryTable.ContainsKey(obj);
        }

        public bool HasItem(Models.AttributeMeta attr)
        {
            foreach (var obj in InventoryTable.Keys)
            {
                if (obj.Attributes[attr.ID])
                {
                    return true;
                }
            }

            return false;
        }

        public int ItemCount(Models.ObjectMeta obj)
        {
            if (InventoryTable.ContainsKey(obj))
            {
                return Items[InventoryTable[obj]].Count;
            }

            return 0;
        }

        public int ItemsCount(Models.AttributeMeta attr)
        {
            int count = 0;
            foreach (var obj in Items)
            {
                count += (obj.Object.Attributes[attr.ID]) ? (obj.Count) : (0);
            }

            return count;
        }

        public List<InventoryEntry> GetObjects(Models.AttributeMeta attr)
        {
            var list = new List<InventoryEntry>();
            foreach (var obj in Items)
            {
                if (obj.Object.Attributes[attr.ID])
                {
                    list.Add(obj);
                }
            }

            return list;
        }

        public List<InventoryEntry> GetObjects(Models.AttributeMeta attr, out int count)
        {
            var list = new List<InventoryEntry>();
            int cnt = 0;
            foreach (var obj in Items)
            {
                if (obj.Object.Attributes[attr.ID])
                {
                    list.Add(obj);
                    cnt += obj.Count;
                }
            }

            count = cnt;
            return list;
        }
    }

    public class InventoryEntry : Models.ModelBase
    {
        public Models.ObjectMeta Object
        {
            get; set;
        }

        public int _count;
        public int Count
        {
            get { return _count; }
            set
            {
                _count = value;
                RaisePropertyChanged("Count");
            }
        }

        public InventoryEntry(Models.ObjectMeta obj, int count)
        {
            Object = obj;
            Count = count;
        }

        public InventoryEntry(Models.ObjectMeta obj) : this(obj, 1) { }

        public InventoryEntry(InventoryEntry obj) : this(obj.Object, obj._count) { }
    }
}
