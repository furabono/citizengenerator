﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GameCore.Utility;

namespace GameCore.Class
{
    public class Civil : IEnumerable<Citizen>
    {
        List<Brain> brains;
        List<Citizen> citizens;

        public Citizen this[int index]
        {
            get { return citizens[index]; }
        }

        public int Count
        {
            get { return citizens.Count; }
        }

        public Civil(Brain[] brains)
        {
            this.brains = brains.ToList();
            this.citizens = new List<Citizen>(brains.Length);
            
            for (int i = 0; i < brains.Length; ++i)
            {
                citizens.Add(new Citizen(brains[i]));
            }
        }

        IEnumerator<Citizen> IEnumerable<Citizen>.GetEnumerator()
        {
            for (int i = 0; i < citizens.Count; ++i)
            {
                yield return citizens[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < citizens.Count; ++i)
            {
                yield return citizens[i];
            }
        }

        public void ReadMind(Policy policy)
        {
            MindReader.Read(brains, policy);
        }

        public Task ReadMindAsync(Policy policy)
        {
            return MindReader.ReadAsync(brains, policy);
        }
    }
}
