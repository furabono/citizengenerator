﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Class
{
    public class ParameterInfoCollection : IEnumerable<ParameterInfo>
    {
        List<ParameterInfo> parameterInfoList;
        Dictionary<string, int> nameIndexMap;

        public ParameterInfo this[int index]
        {
            get { return parameterInfoList[index]; }
            //set { parameterInfoList[index] = value; }
        }

        public ParameterInfo this[string name]
        {
            get { return parameterInfoList[nameIndexMap[name]]; }
        }

        public int Count
        {
            get { return parameterInfoList.Count; }
        }

        public ParameterInfoCollection(params ParameterInfo[] parameterInfos)
        {
            nameIndexMap = new Dictionary<string, int>();
            for (int i = 0; i < parameterInfos.Length; ++i)
            {
                nameIndexMap.Add(parameterInfos[i].Name, i);
            }

            parameterInfoList = parameterInfos.ToList();
        }

        IEnumerator<ParameterInfo> IEnumerable<ParameterInfo>.GetEnumerator()
        {
            return parameterInfoList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return parameterInfoList.GetEnumerator();
        }

        public int GetIndex(string name)
        {
            return nameIndexMap[name];
        }

        public int GetIndex(ParameterInfo obj)
        {
            return nameIndexMap[obj.Name];
        }

        public bool Contains(ParameterInfo obj)
        {
            return parameterInfoList.Contains(obj);
        }

        public bool Contains(string name)
        {
            return nameIndexMap.ContainsKey(name);
        }
    }
}
