﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace GameCore.Class
{
    public abstract class ParameterInfo
    {
        public ParameterInfo SetName(string name)
        {
            if (Type == ParameterType.Qualitative)
            {
                var param = (QualitativeParameterInfo)this;
                return param.SetName(name);
            }
            else
            {
                var param = (QuantitativeParameterInfo)this;
                return param.SetName(name);
            }
        }

        public abstract bool IsValueValid(int value);

        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }
            if (!(obj is ParameterInfo)) { return false; }

            return Equals((ParameterInfo)obj);
        }

        public abstract bool Equals(ParameterInfo pi);
        public abstract override int GetHashCode();

        public abstract ParameterType Type
        {
            get;
        }

        public string Name
        {
            get { return name; }
            //set { value = name; }
        }

        protected string name;
    }

    public enum ParameterType
    {
        Qualitative, Quantitative
    }

    public class QualitativeParameterInfo : ParameterInfo
    {
        public QualitativeParameterInfo(string name, params string[] categories)
        {
            this.name = name;
            this.categories = categories;
        }

        public new QualitativeParameterInfo SetName(string name)
        {
            return new QualitativeParameterInfo(name, this.categories);
        }

        public override bool IsValueValid(int value)
        {
            return (value >= 0 && value < categories.Length);
        }

        public override bool Equals(ParameterInfo pi)
        {
            if (pi == null) { return false; }
            if (this.Type != pi.Type) { return false; }

            return this.Equals((QualitativeParameterInfo)pi);
        }

        public bool Equals(QualitativeParameterInfo qpi)
        {
            if (qpi == null) { return false; }

            return (this.name == qpi.name) && this.categories.SequenceEqual(qpi.categories);
        }

        public override int GetHashCode()
        {
            return name.GetHashCode() ^ categories.GetHashCode();
        }

        public override ParameterType Type
        {
            get { return ParameterType.Qualitative; }
        }

        public string[] Categories
        {
            get { return categories.ToArray(); }
        }

        string[] categories;
    }

    public class QuantitativeParameterInfo : ParameterInfo
    {
        public QuantitativeParameterInfo(string name, int min, int max)
        {
            this.name = name;
            this.min = min;
            this.max = max;
        }

        public new QuantitativeParameterInfo SetName(string name)
        {
            return new QuantitativeParameterInfo(name, min, max);
        }

        public QuantitativeParameterInfo SetMin(int min)
        {
            return new QuantitativeParameterInfo(this.name, min, this.max);
        }

        public QuantitativeParameterInfo SetMax(int max)
        {
            return new QuantitativeParameterInfo(this.name, this.min, max);
        }

        public override bool IsValueValid(int value)
        {
            return (value >= min && value <= max);
        }

        public override bool Equals(ParameterInfo pi)
        {
            if (pi == null) { return false; }
            if (this.Type != pi.Type) { return false; }

            return Equals((QuantitativeParameterInfo)pi);
        }

        public bool Equals(QuantitativeParameterInfo qpi)
        {
            if (qpi == null) { return false; }

            return (this.name == qpi.name) && (this.min == qpi.min) && (this.max == qpi.max);
        }

        public override int GetHashCode()
        {
            return name.GetHashCode() ^ min ^ max;
        }

        public override ParameterType Type
        {
            get { return ParameterType.Quantitative; }
        }

        public int Min
        {
            get { return min; }
            //set { min = value; }
        }

        public int Max
        {
            get { return max; }
            //set { max = value; }
        }

        int min;
        int max;
    }
}
