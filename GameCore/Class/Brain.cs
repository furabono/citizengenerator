﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// ToDo : Optimize...

namespace GameCore.Class
{
    public class Brain
    {
        ParameterInfoCollection parameterInfoCollection;
        int[] parameters;
        Dictionary<Policy, float> evaluations;

        public ParameterInfoCollection ParameterInfoCollection
        {
            get { return parameterInfoCollection; }
        }

        public dynamic this[int parameterIndex]
        {
            get { return parameters[parameterIndex]; }
        }

        public dynamic this[string parameterName]
        {
            get { return parameters[parameterInfoCollection.GetIndex(parameterName)]; }
        }

        public dynamic this[ParameterInfo parameterInfo]
        {
            get { return parameters[parameterInfoCollection.GetIndex(parameterInfo)]; }
        }

        public Brain(ParameterInfoCollection parameterInfoCollection)
        {
            if (parameterInfoCollection == null)
            {
                throw new ArgumentNullException("parameterInfoCollection");
            }

            this.parameterInfoCollection = parameterInfoCollection;
            this.parameters = new int[parameterInfoCollection.Count];
            this.evaluations = new Dictionary<Policy, float>();
        }

        public Brain(ParameterInfoCollection parameterInfoCollection, int[] parameters)
        {
            if (parameterInfoCollection == null)
            {
                throw new ArgumentNullException("parameterInfoCollection");
            }
            if (parameters == null)
            {
                throw new ArgumentNullException("parameters");
            }
            if (parameterInfoCollection.Count != parameters.Length)
            {
                throw new ArgumentException("length of parameters must be same as parameterInfoCollection");
            }

            this.parameterInfoCollection = parameterInfoCollection;
            this.parameters = parameters.ToArray();
            this.evaluations = new Dictionary<Policy, float>();
        }

        public int GetParameterRaw(int index)
        {
            return parameters[index];
        }

        public int GetParameterRaw(string name)
        {
            return parameters[parameterInfoCollection.GetIndex(name)];
        }

        public int GetParameterRaw(ParameterInfo parameterInfo)
        {
            return parameters[parameterInfoCollection.GetIndex(parameterInfo)];
        }

        public void SetParameterRaw(int index, int value)
        {
            if (!parameterInfoCollection[index].IsValueValid(value))
            {
                throw new ArgumentException("value is not valid for parameter " + parameterInfoCollection[index].Name, "value");
            }
            parameters[index] = value;
        }

        public void SetParameterRaw(string name, int value)
        {
            if (!parameterInfoCollection[name].IsValueValid(value))
            {
                throw new ArgumentException("value is not valid for parameter " + name, "value");
            }

            parameters[parameterInfoCollection.GetIndex(name)] = value;
        }

        public void SetParameterRaw(ParameterInfo parameterInfo, int value)
        {
            if (!parameterInfo.IsValueValid(value))
            {
                throw new ArgumentException("value is not valid for parameter " + parameterInfo.Name, "value");
            }

            parameters[parameterInfoCollection.GetIndex(parameterInfo)] = value;
        }

        public dynamic GetParameterDisplay(int index)
        {
            if (parameterInfoCollection[index].Type == ParameterType.Quantitative)
            {
                return parameters[index];
            }
            else
            {
                var qpi = (QualitativeParameterInfo)parameterInfoCollection[index];
                return qpi.Categories[parameters[index]];
            }
        }

        public dynamic GetParameterDisplay(string name)
        {
            return GetParameterDisplay(parameterInfoCollection.GetIndex(name));
        }

        public dynamic GetParameterDisplay(ParameterInfo parameterInfo)
        {
            return GetParameterDisplay(parameterInfoCollection.GetIndex(parameterInfo));
        }

        public float GetEvaluation(Policy policy)
        {
            return evaluations[policy];
        }

        public void SetEvaluation(Policy policy, float value)
        {
            evaluations[policy] = value;
        }
    }
}
