﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameCore.Utility;

// ToDo

namespace GameCore.Class
{
    public class Policy
    {
        string name;
        KeyValuePair<string, float>[] variables;
        string script;
        string executable;

        public string Name
        {
            get { return name; }
        }

        public KeyValuePair<string, float>[] Variables
        {
            get { return variables; }
        }

        public string Script
        {
            get { return script; }
        }

        public string Executable
        {
            get { return executable; }
        }

        public Policy(string name, KeyValuePair<string, float>[] variables, string script)
        {
            this.name = name;
            this.variables = variables;
            this.script = script;
        }
    }

    public class _Policy
    {
        string name;
        KeyValuePair<string, float>[] variables;
        Script script;

        public string Name
        {
            get { return name; }
        }

        public KeyValuePair<string, float>[] Variables
        {
            get { return variables; }
        }

        public Script Script
        {
            get { return script; }
        }

        // arguments null exception
        public _Policy(string name, KeyValuePair<string, float>[] variables, Script script)
        {
            if (script == null) { throw new ArgumentNullException("script"); }

            this.name = name;
            this.variables = variables;
            this.script = script;
        }
    }
}
