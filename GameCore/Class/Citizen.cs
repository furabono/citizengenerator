﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Class
{
    public class Citizen
    {
        Brain brain;

        public ParameterInfoCollection ParameterInfoCollection
        {
            get { return brain.ParameterInfoCollection; }
        }

        public dynamic this[int parameterIndex]
        {
            get { return brain.GetParameterDisplay(parameterIndex); }
        }

        public dynamic this[string parameterName]
        {
            get { return brain.GetParameterDisplay(parameterName); }
        }

        public dynamic this[ParameterInfo parameterInfo]
        {
            get { return brain.GetParameterDisplay(parameterInfo); }
        }

        public float this[Policy policy]
        {
            get { return brain.GetEvaluation(policy); }
        }

        public Citizen(Brain brain)
        {
            if (brain == null)
            {
                throw new ArgumentNullException("brain");
            }

            this.brain = brain;
        }
    }
}