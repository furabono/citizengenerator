﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Engine = Microsoft.Scripting.Hosting.ScriptEngine;
using Scope = Microsoft.Scripting.Hosting.ScriptScope;

namespace GameCore.Utility
{
    public class ScriptEngine
    {
        Engine engine;
        Scope scope;

        public ScriptEngine()
        {
            engine = IronPython.Hosting.Python.CreateEngine();
            scope = engine.CreateScope();
        }

        public void SetVariable<T>(string name, T value)
        {
            scope.SetVariable(name, value);
        }

        public T GetVariable<T>(string name)
        {
            return scope.GetVariable<T>(name);
        }

        public void Run(string script)
        {
            var source = engine.CreateScriptSourceFromString(script);
            source.Execute(scope);
        }

        //public T Run<T>(Script<T> script);
    }
}
