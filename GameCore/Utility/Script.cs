﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Utility
{
    public class Script
    {
        ScriptType type;
        string scriptText;

        public ScriptType Type
        {
            get { return type; }
        }

        public string ScriptText
        {
            get { return scriptText; }
        }

        public Script(ScriptType type, string scriptText)
        {
            this.type = type;
            this.scriptText = scriptText;
        }
    }

    public enum ScriptType
    {
        Expression, Function
    }
}
