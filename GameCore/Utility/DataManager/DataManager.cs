﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GameCore.Utility
{
    public partial class DataManager
    {
        static JObject ReadJsonFile(string file)
        {
            JObject jobject;

            using (StreamReader sr = File.OpenText(file))
            using (JsonTextReader reader = new JsonTextReader(sr))
            {
                jobject = JObject.Load(reader);
            }

            return jobject;
        }
    }
}
