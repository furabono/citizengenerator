﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameCore.Class;
using Newtonsoft.Json.Linq;

namespace GameCore.Utility
{
    public partial class DataManager
    {
        public static Policy JsonToPolicy(string file)
        {
            return JsonToPolicy(ReadJsonFile(file));
        }

        public static Task<Policy> JsonToPolicyAsync(string file)
        {
            return Task.Run(() => JsonToPolicy(file));
        }

        public static Policy JsonToPolicy(JObject jobject)
        {
            if (jobject == null)
            {
                throw new ArgumentNullException("jobject");
            }

            var rev = jobject["Revision"].Value<int>();
            Policy policy = null;
            switch (rev)
            {
                case 1:
                    policy = JsonToPolicyRev1(jobject);
                    break;
            }

            return policy;
        }

        private static Policy JsonToPolicyRev1(JObject jobject)
        {
            string name = jobject["Name"].Value<string>();
            var variables = (from v in jobject["Variable"]
                             select new KeyValuePair<string, float>(v["Name"].Value<string>(), v["Value"].Value<float>())).ToArray();
            string script = jobject["Result"].Value<string>();

            return new Policy(name, variables, script);
        }

        private static Policy JsonToPolicyRev2(JObject jobject)
        {
            string name = jobject["Name"].Value<string>();
            var variables = (from v in jobject["Variable"]
                             select new KeyValuePair<string, float>(v["Name"].Value<string>(), v["Value"].Value<float>())).ToArray();
            string script = jobject["Script"].Value<string>();
            string executable = jobject["Executable"].Value<string>();

            return new Policy(name, variables, script);
        }
    }
}