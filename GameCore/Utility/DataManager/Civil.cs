﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameCore.Class;
using Newtonsoft.Json.Linq;

namespace GameCore.Utility
{
    public partial class DataManager
    {
        public static Civil JsonToCivil(string file)
        {
            return JsonToCivil(ReadJsonFile(file));
        }

        public static Task<Civil> JsonToCivilAsync(string file)
        {
            return Task.Run(() => JsonToCivil(file));
        }

        public static Civil JsonToCivil(JObject jobject)
        {
            if (jobject == null)
            {
                throw new ArgumentNullException("jobject");
            }

            ParameterInfoCollection parameterInfoCollection;
            {
                JArray parameters = (JArray)jobject["Parameter"];

                var parameterInfoArray = new ParameterInfo[parameters.Count];
                JToken p;

                for (int i = 0; i < parameters.Count; ++i)
                {
                    p = parameters[i];
                    
                    if (p["Type"].Value<string>() == "Quantitative")
                    {
                        parameterInfoArray[i] = new QuantitativeParameterInfo(p["Name"].Value<string>(),
                                                                                p["Min"].Value<int>(),
                                                                                p["Max"].Value<int>());
                    }
                    else
                    {
                        parameterInfoArray[i] = new QualitativeParameterInfo(p["Name"].Value<string>(),
                                                                                (from c in p["Category"] select c.Value<string>()).ToArray());
                    }
                }

                parameterInfoCollection = new ParameterInfoCollection(parameterInfoArray);
            }

            Brain[] brains;
            {
                JArray citizen = (JArray)jobject["Citizen"];
                brains = new Brain[citizen.Count];

                for (int i = 0; i < citizen.Count; ++i)
                {
                    brains[i] = new Brain(parameterInfoCollection,
                                            (from p in citizen[i] select p.Value<int>()).ToArray());
                }
            }

            return new Civil(brains);
        }

        public static Task<Civil> JsonToCivilAsync(JObject jobject)
        {
            return Task.Run(() => JsonToCivil(jobject));
        }
    }
}
