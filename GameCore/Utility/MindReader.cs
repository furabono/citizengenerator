﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameCore.Class;

namespace GameCore.Utility
{
    public class MindReader
    {
        public static void Read(List<Brain> brains, Policy policy)
        {
            if (brains == null)
            {
                throw new ArgumentNullException("brains");
            }
            if (policy == null)
            {
                throw new ArgumentNullException("policy");
            }

            var psb = new PythonScriptBuilder();
            psb.Import("clr");
            psb.ImportFrom("System.Threading.Tasks", "Parallel");
            psb.ImportFrom("System", "Action");
            //psb.ImportFrom("GameCore.Class", "Brain");

            psb.WriteLine(@"clr.AddReference('GameCore')")
                .WriteLine("from GameCore.Class import Brain");

            psb.BeginFunction("func", "param")
                .WriteLine("value = " + policy.Script)
                .WriteLine("param.SetEvaluation(policy, value)")
                .EndFunction()
                .WriteLine()
                .WriteLine("Parallel.ForEach(brains, Action[Brain](func))");

            var engine = new ScriptEngine();
            engine.SetVariable("policy", policy);
            engine.SetVariable("brains", brains);

            engine.SetVariable("var", (from v in policy.Variables select v.Value).ToArray());
            for (int i = 0; i < policy.Variables.Length; ++i)
            {
                var v = policy.Variables[i];
                engine.SetVariable(v.Key, v.Value);
            }

            engine.Run(psb.Script);
        }

        public static Task ReadAsync(List<Brain> brains, Policy policy)
        {
            return Task.Run(() => Read(brains, policy));
        }
    }
}
