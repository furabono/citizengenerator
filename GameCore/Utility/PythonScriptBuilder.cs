﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Utility
{
    public class PythonScriptBuilder
    {
        StringBuilder imports;
        StringBuilder body;
        string indent;
        Stack<BuildingBlock> buildingBlockStack;

        public string Script
        {
            get
            {
                return imports.ToString() + "\n" + body.ToString();
            }
        }

        public PythonScriptBuilder()
        {
            imports = new StringBuilder();
            body = new StringBuilder();
            indent = "";
            buildingBlockStack = new Stack<BuildingBlock>();
            buildingBlockStack.Push(BuildingBlock.None);
        }

        // check argument null?
        public void Import(string import)
        {
            imports.AppendFormat("import {0}\n", import);
        }

        public void Import(string import, string _as)
        {
            imports.AppendFormat("import {0} as {1}\n", import, _as);
        }

        // check argument null?
        public void ImportFrom(string from, string import)
        {
            imports.AppendFormat("from {0} import {1}\n", from, import);
        }

        public void ImportFrom(string from, string import, string _as)
        {
            imports.AppendFormat("from {0} import {1} as {2}\n", from, import, _as);
        }

        public PythonScriptBuilder Write(string text)
        {
            if (string.IsNullOrEmpty(text)) { return this; }

            text = text.Replace("\r", "");
            string[] lines = text.Split('\n');

            foreach (string line in lines)
            {
                WriteLine(line);
            }

            return this;
        }

        // check line is null?
        public PythonScriptBuilder WriteLine(string line = "")
        {
            if (line.Contains("\n"))
            {
                throw new ArgumentException("line must consist of only one line of string (must have no 'new line character'", "line");
            }

            body.AppendLine(indent + line + "\n");
            return this;
        }

        public PythonScriptBuilder Indent()
        {
            indent = indent + "\t";
            return this;
        }

        public PythonScriptBuilder Outdent()
        {
            if (indent.Length > 0) { indent = indent.Remove(0, 1); }
            return this;
        }

        // check name, body null?
        public PythonScriptBuilder Function(string name, string[] arguments, string body)
        {
            if (arguments == null) { arguments = new string[] { }; }

            this.body.AppendFormat("{0}def {1}({2}):\n", indent, name, string.Join(", ", arguments));

            Indent();
            Write(body);
            Outdent();

            return this;
        }

        // check name, body null?
        public PythonScriptBuilder Function(string name, string[] arguments, Script bodyScript)
        {
            if (bodyScript == null) { throw new ArgumentNullException("bodyScript"); }
            if (bodyScript.Type != ScriptType.Function) { throw new ArgumentException("bodyScript must be Function Type", "bodyScript"); }

            return Function(name, arguments, bodyScript.ScriptText);
        }

        // check name null?
        public PythonScriptBuilder BeginFunction(string name, params string[] arguments)
        {
            this.body.AppendFormat("{0}def {1}({2}):\n", indent, name, string.Join(", ", arguments));
            Indent();
            buildingBlockStack.Push(BuildingBlock.Function);

            return this;
        }

        public PythonScriptBuilder EndFunction()
        {
            if (buildingBlockStack.Peek() != BuildingBlock.Function) { throw new PythonScriptContextException(BuildingBlock.Function); }

            Outdent();
            buildingBlockStack.Pop();

            return this;
        }

        // check condition, body null?
        public PythonScriptBuilder If(string condition, string body)
        {
            this.body.AppendFormat("{0}if ( {1} ):\n", indent, condition);

            Indent();
            Write(body);
            Outdent();

            return this;
        }

        // check condition null?
        public PythonScriptBuilder BeginIf(string condition)
        {
            this.body.AppendFormat("{0}if ( {1} ):\n", indent, condition);

            Indent();
            buildingBlockStack.Push(BuildingBlock.If);

            return this;
        }

        public PythonScriptBuilder EndIf()
        {
            if (buildingBlockStack.Peek() != BuildingBlock.If) { throw new PythonScriptContextException(BuildingBlock.If); }

            Outdent();
            buildingBlockStack.Pop();

            return this;
        }

        // check variable, range..., body null?
        public PythonScriptBuilder For(string variable, int rangeMinInclusive, int rangeMaxExclusive, string body)
        {
            BeginFor(variable, rangeMinInclusive, rangeMaxExclusive);
            Write(body);
            EndFor();

            return this;
        }

        // check argument null?
        public PythonScriptBuilder BeginFor(string variable, int rangeMinInclusive, int rangeMaxExclusive)
        {
            BeginFor(variable, string.Format("range({0},{1})", rangeMinInclusive, rangeMaxExclusive));

            return this;
        }

        // check argument null?
        public PythonScriptBuilder For(string variable, string container, string body)
        {
            BeginFor(variable, container);
            Write(body);
            EndFor();

            return this;
        }

        // check argument null?
        public PythonScriptBuilder BeginFor(string variable, string container)
        {
            this.body.AppendFormat("{0}for {1} in {2}:\n", indent, variable, container);

            Indent();
            buildingBlockStack.Push(BuildingBlock.For);

            return this;
        }

        public PythonScriptBuilder EndFor()
        {
            if (buildingBlockStack.Peek() != BuildingBlock.For) { throw new PythonScriptContextException(BuildingBlock.For); }

            Outdent();
            buildingBlockStack.Pop();

            return this;
        }
    }

    enum BuildingBlock
    {
        None, Function, If, For
    }

    class PythonScriptContextException : Exception
    {
        public PythonScriptContextException(BuildingBlock triedToEnd)
            : base(string.Format("Tried to End{0} without Begin", triedToEnd.ToString()))
        { }
    }
}
