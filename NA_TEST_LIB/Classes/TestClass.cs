﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NA_TEST_LIB.Classes
{
    public class TestClass
    {
        public int TestInteger { get; set; }

        public int Add(int x, int y) { return x + y; }

        public void Loop()
        {
            for (int i = 0; i < 100000; ++i)
            {
                Add(12345, 67890);
            }
        }
    }
}
