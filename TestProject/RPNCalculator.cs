﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject
{
    public class RPNCalculator
    {
        Stack<float> stack;

        public RPNCalculator()
        {
            stack = new Stack<float>();
        }

        public float Calculate(List<Expression.Token> expression)
        {
            stack.Clear();

            for (int i = 0; i < expression.Count; ++i)
            {
                if (expression[i].Type == Expression.TokenType.Value)
                {
                    stack.Push(expression[i].Value);
                }
                else
                {
                    Operate(expression[i].Operator);
                }
            }

            return stack.Pop();
        }

        public enum Operator
        {
            NOT,
            AND,
            OR,
            EQUAL,
            BIGGER,
            SMALLER,
            BIGGER_OR_EQUAL,
            SMALLER_OR_EQUAL,
            ADD,
            SUBTRACT,
            MULTIPLY,
            DIVIDE,
            MOD,
            ABS,
            POWER,
            SQRT,
            LOG,
            LOG_2,
            LOG_10,
            LOG_E,
            FACTORIAL
        }

        void Operate(Operator op)
        {
            switch (op)
            {
                case Operator.NOT:
                    {
                        Not();
                        break;
                    }
                case Operator.AND:
                    {
                        And();
                        break;
                    }
                case Operator.OR:
                    {
                        Or();
                        break;
                    }
                case Operator.EQUAL:
                    {
                        Equal();
                        break;
                    }
                case Operator.BIGGER:
                    {
                        Bigger();
                        break;
                    }
                case Operator.SMALLER:
                    {
                        Smaller();
                        break;
                    }
                case Operator.BIGGER_OR_EQUAL:
                    {
                        BiggerOrEqual();
                        break;
                    }
                case Operator.SMALLER_OR_EQUAL:
                    {
                        SmallerOrEqual();
                        break;
                    }
                case Operator.ADD:
                    {
                        Add();
                        break;
                    }
                case Operator.SUBTRACT:
                    {
                        Subtract();
                        break;
                    }
                case Operator.MULTIPLY:
                    {
                        Multiply();
                        break;
                    }
                case Operator.DIVIDE:
                    {
                        Divide();
                        break;
                    }
            }
        }

        bool FloatToBool(float value)
        {
            return value > 0f;
        }

        float BoolToFloat(bool value)
        {
            return (value ? 1f : 0f);
        }

        void Not()
        {
            stack.Push(BoolToFloat(!FloatToBool(stack.Pop())));
        }
        void And()
        {
            stack.Push(BoolToFloat(FloatToBool(stack.Pop()) && FloatToBool(stack.Pop())));
        }
        void Or()
        {
            stack.Push(BoolToFloat(FloatToBool(stack.Pop()) || FloatToBool(stack.Pop())));
        }
        void Equal()
        {
            stack.Push(BoolToFloat(stack.Pop() == stack.Pop()));
        }
        void Bigger()
        {
            stack.Push(BoolToFloat(stack.Pop() > stack.Pop()));
        }
        void Smaller()
        {
            stack.Push(BoolToFloat(stack.Pop() < stack.Pop()));
        }
        void BiggerOrEqual()
        {
            stack.Push(BoolToFloat(stack.Pop() >= stack.Pop()));
        }
        void SmallerOrEqual()
        {
            stack.Push(BoolToFloat(stack.Pop() <= stack.Pop()));
        }
        void Add()
        {
            stack.Push(stack.Pop() + stack.Pop());
        }
        void Subtract()
        {
            stack.Push(-stack.Pop() + stack.Pop());
        }
        void Multiply()
        {
            stack.Push(stack.Pop() * stack.Pop());
        }
        void Divide()
        {
            float right = stack.Pop();
            stack.Push(stack.Pop() / right);
        }
        void Mod()
        {
            float right = stack.Pop();
            stack.Push(stack.Pop() % right);
        }
        void Abs()
        {
            stack.Push(Math.Abs(stack.Pop()));
        }

        public class Expression
        {
            List<Token> tokens;

            public enum TokenType
            {
                Value, Operation
            }

            public abstract class Token
            {
                public abstract TokenType Type
                {
                    get;
                }
                public virtual float Value
                {
                    get { throw new Exception(); }
                }
                public virtual Operator Operator
                {
                    get { throw new Exception(); }
                }
            }

            public class TValue : Token
            {
                float value;

                public override TokenType Type
                {
                    get { return TokenType.Value; }
                }

                public override float Value
                {
                    get { return value; }
                }

                public TValue(float value)
                {
                    this.value = value;
                }
            }

            public class TOperator : Token
            {
                Operator @operator;

                public override TokenType Type
                {
                    get { return TokenType.Operation; }
                }

                public override Operator Operator
                {
                    get { return @operator; }
                }

                public TOperator(Operator @operator)
                {
                    this.@operator = @operator;
                }
            }
        }
    }
}
