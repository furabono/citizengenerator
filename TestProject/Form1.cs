﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameCore.Utility;
using GameCore.Class;
using System.Linq.Expressions;
using System.Dynamic;
using Microsoft.CodeAnalysis.Scripting;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CSharp;
using System.CodeDom.Compiler;

namespace TestProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            button1.Click += Button1_Click;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //var sw = System.Diagnostics.Stopwatch.StartNew();
            //var civil = DataManager.JsonToCivil("test2.json");
            //richTextBox1.AppendText(sw.Elapsed.TotalSeconds.ToString() + "\n");

            //sw.Restart();
            //var policy = DataManager.JsonToPolicy("testpolicy1.json");
            //richTextBox1.AppendText(sw.Elapsed.TotalSeconds.ToString() + "\n");

            //civil.ReadMind(policy);
            //test();
            //test1();
            //test2();
            //test3();
            //test4();
            //test5();
            //test6();
            //test7();
            test8();
        }

        public class MyClass
        {
            public Func<int, int, int> func;
        }

        void test6()
        {
            var mc = new MyClass();

            CSharpScript.RunAsync("func = delegate(int a, int b) { int c = a + b; return c; }", null, mc).Wait();
            richTextBox1.AppendText(mc.func(1, 2).ToString());

            mc.func = null;
            GC.Collect();
        }

        void test5(params object[] args)
        {
            var p1 = Expression.Parameter(typeof(int), "a");
            var p2 = Expression.Parameter(typeof(int), "b");
            string exp = @"{ int c = a + b; return c; }";
            var del = System.Linq.Dynamic.DynamicExpression.ParseLambda(new[] { p1, p2 }, typeof(int), exp).Compile();
            var d = (Func<int, int, int>)del;
            richTextBox1.AppendText(d(1, 2).ToString());
        }

        void test3()
        {
            Dictionary<int, int> @int = new Dictionary<int, int>();
            Dictionary<int, object> @object = new Dictionary<int, object>();
            Dictionary<int, ValueType> @value = new Dictionary<int, ValueType>();
            int tmp;

            var sw = new System.Diagnostics.Stopwatch();
            sw.Restart();

            for (int i = 0; i < 100000; ++i)
            {
                @int[i] = i;
            }

            sw.Stop();
            richTextBox1.AppendText(sw.Elapsed.TotalMilliseconds.ToString());
            richTextBox1.AppendText("\n");

            sw.Restart();

            for (int i = 0; i < 100000; ++i)
            {
                tmp = @int[i];
            }

            sw.Stop();
            richTextBox1.AppendText(sw.Elapsed.TotalMilliseconds.ToString());
            richTextBox1.AppendText("\n");

            sw.Restart();

            for (int i = 0; i < 100000; ++i)
            {
                @object[i] = i;
            }

            sw.Stop();
            richTextBox1.AppendText(sw.Elapsed.TotalMilliseconds.ToString());
            richTextBox1.AppendText("\n");

            sw.Restart();

            for (int i = 0; i < 100000; ++i)
            {
                tmp = (int)@object[i];
            }

            sw.Stop();
            richTextBox1.AppendText(sw.Elapsed.TotalMilliseconds.ToString());
            richTextBox1.AppendText("\n");

            sw.Restart();

            for (int i = 0; i < 100000; ++i)
            {
                value[i] = i;
            }

            sw.Stop();
            richTextBox1.AppendText(sw.Elapsed.TotalMilliseconds.ToString());
            richTextBox1.AppendText("\n");

            sw.Restart();

            for (int i = 0; i < 100000; ++i)
            {
                tmp = (int)value[i];
            }

            sw.Stop();
            richTextBox1.AppendText(sw.Elapsed.TotalMilliseconds.ToString());
            richTextBox1.AppendText("\n");
        }

        void test4()
        {
            var sw = new System.Diagnostics.Stopwatch();

            // refarg
            {
                Instruction_refarg pc = test4_method1;
                sw.Restart();

                for (int i = 0; i < 100000; ++i)
                {
                    pc(ref pc);
                }

                sw.Stop();
                richTextBox1.AppendText(sw.Elapsed.TotalMilliseconds.ToString());
                richTextBox1.AppendText("\n");
            }

            // return
            //{
            //    Instruction_return pc = test4_method4;

            //    sw.Restart();

            //    for (int i = 0; i < 100000; ++i)
            //    {
            //        while (pc != null)
            //        {
            //            pc = pc(ref pc);
            //        }
            //    }

            //    sw.Stop();
            //    richTextBox1.AppendText(sw.Elapsed.TotalMilliseconds.ToString());
            //    richTextBox1.AppendText("\n");
            //}

            // native
            {
                sw.Restart();

                Action<int> pc = test4_method7;
                for (int i = 0; i < 100000; ++i)
                {
                    pc(0);
                }

                sw.Stop();
                richTextBox1.AppendText(sw.Elapsed.TotalMilliseconds.ToString());
                richTextBox1.AppendText("\n");
            }
        }

        delegate void Instruction_refarg(ref Instruction_refarg programCounter);

        void test4_method1(ref Instruction_refarg programCounter)
        {
            int a = 1;
            if (a > 2)
            {
                // Do Something
            }
            else
            {
                test4_method2(ref programCounter);
            }
        }

        void test4_method2(ref Instruction_refarg programCounter)
        {
            int b = 3;
            if (b > 4)
            {
                // Do Something
            }
            else
            {
                test4_method3(ref programCounter);
            }
        }

        void test4_method3(ref Instruction_refarg programCounter)
        {
            int c = 5;
            if (c > 6)
            {
                // Do Something
            }
            else
            {
                programCounter = test4_method1;
            }
        }

        delegate Instruction_return Instruction_return(ref Instruction_return pc);

        Instruction_return test4_method4(ref Instruction_return pc)
        {
            int a = 1;
            if (a > 2)
            {
                return null;
            }
            else
            {
                return test4_method5;
            }
        }

        Instruction_return test4_method5(ref Instruction_return pc)
        {
            int b = 3;
            if (b > 4)
            {
                return null;
            }
            else
            {
                return test4_method6;
            }
        }

        Instruction_return test4_method6(ref Instruction_return pc)
        {
            int c = 5;
            if (c > 6)
            {
                return null;
            }
            else
            {
                pc = test4_method4;
                return null;
            }
        }

        void test4_method7(int pc = 0)
        {
            int a = 1;
            if (pc == 1 || a > 2)
            {
                // Do Something
            }
            else
            {
                int b = 3;
                if (pc == 2 || b > 4)
                {
                    // Do Something
                }
                else
                {
                    int c = 5;
                    if (pc == 3 || c > 6)
                    {
                        // Do Something
                    }
                    else
                    {
                        // Do Something
                    }
                }
            }
        }


        void test1()
        {
            Parser parser = new Parser();
            IEstimator e = parser.ParseExpression(richTextBox1.Text);
            Context context = new Context();
            richTextBox1.ResetText();
            richTextBox1.AppendText(e.Estimation(context).ToString());
        }

        void test2()
        {
            Parser parser = new Parser();
            CodeBlock b = parser.ParseCodeBlock(richTextBox1.Text);
            Context context = new Context();
            b.Estimation(context);
            richTextBox1.AppendText("\n");
            foreach (var elem in context.StaticVariables)
            {
                richTextBox1.AppendText("static " + elem.Key.ToString() + " : " + elem.Value.ToString());
            }
            foreach (var elem in context.DynamicVariables)
            {
                richTextBox1.AppendText(elem.Key.ToString() + " : " + elem.Value.ToString());
            }
        }

        void test()
        {
            richTextBox1.AppendText(native().ToString());
            richTextBox1.AppendText("\n");
            richTextBox1.AppendText(estimator().ToString());
            richTextBox1.AppendText("\n");
            richTextBox1.AppendText(rpn().ToString());
            richTextBox1.AppendText("\n");
            richTextBox1.AppendText(python("(1+2)*3/4.0").ToString());
            richTextBox1.AppendText("\n");
            richTextBox1.AppendText(dynamic().ToString());
            richTextBox1.AppendText("\n");
        }

        double native()
        {
            var sw = new System.Diagnostics.Stopwatch();
            sw.Restart();

            float t;
            for (int i = 0; i < 100000; ++i)
            {
                t = (1f + 2f) * 3f / 4f;
            }

            sw.Stop();
            return sw.Elapsed.TotalMilliseconds;
        }

        double estimator()
        {
            var c_1 = EConstant.Instance(1f);
            var c_2 = EConstant.Instance(2f);
            var c_3 = EConstant.Instance(3f);
            var c_4 = EConstant.Instance(4f);

            var add = new EAdd(c_1, c_2);
            var multiply = new EMultiply(add, c_3);
            var divide = new EDivide(multiply, c_4);

            Context context = new Context();

            var sw = new System.Diagnostics.Stopwatch();
            sw.Restart();

            float t;
            for (int i = 0; i < 100000; ++i)
            {
                t = divide.Estimation(context);
            }
            //Parallel.For(0, 100000, (i) => { float m = divide.Estimation; });

            sw.Stop();
            return sw.Elapsed.TotalMilliseconds;
        }

        double rpn()
        {
            RPNCalculator c = new RPNCalculator();

            List<RPNCalculator.Expression.Token> ex = new List<RPNCalculator.Expression.Token>()
            {
                new RPNCalculator.Expression.TValue(1),
                new RPNCalculator.Expression.TValue(2),
                new RPNCalculator.Expression.TOperator(RPNCalculator.Operator.ADD),
                new RPNCalculator.Expression.TValue(3),
                new RPNCalculator.Expression.TOperator(RPNCalculator.Operator.MULTIPLY),
                new RPNCalculator.Expression.TValue(4),
                new RPNCalculator.Expression.TOperator(RPNCalculator.Operator.DIVIDE)
            };

            var sw = new System.Diagnostics.Stopwatch();
            sw.Restart();

            for (int i = 0; i < 100000; ++i)
            {
                c.Calculate(ex);
            }

            sw.Stop();
            return sw.Elapsed.TotalMilliseconds;
        }

        double python(string expression)
        {
            var engine = IronPython.Hosting.Python.CreateEngine();
            var scope = engine.CreateScope();

            //var source = "def func():\n  for i in range(0,100000):\n    " + expression;
            var source = "def func():\n  " + expression;
            var script = engine.CreateScriptSourceFromString(source);
            var compiled = script.Compile();
            compiled.Execute(scope);
            Action func = scope.GetVariable("func");

            var sw = new System.Diagnostics.Stopwatch();
            sw.Restart();
            script.Execute(scope);
            for (int i = 0; i < 100000; ++i)
            {
                func();
            }
            sw.Stop();
            return sw.Elapsed.TotalMilliseconds;
        }

        double dynamic()
        {
            string code = @"
                using System;
                //using IronPython;
                namespace TestNamespace
                {
                    public class TestClass
                    {
                        public static float TestFunction()
                        {
                            return (1+2)*3/4f;
                        }
                    }
                }";

            Microsoft.CSharp.CSharpCodeProvider provider = new Microsoft.CSharp.CSharpCodeProvider();
            System.CodeDom.Compiler.CompilerResults results = provider.CompileAssemblyFromSource(new System.CodeDom.Compiler.CompilerParameters(), code);

            Type functionType = results.CompiledAssembly.GetType("TestNamespace.TestClass");
            var function = functionType.GetMethod("TestFunction");

            Func<float> dele = (Func<float>)function.CreateDelegate(typeof(Func<float>));

            var sw = new System.Diagnostics.Stopwatch();
            sw.Restart();
            for (int i = 0; i < 100000; ++i)
            {
                dele();
            }
            sw.Stop();
            return sw.Elapsed.TotalMilliseconds;

            
        }

        void test7()
        {
            //var assembly = System.Reflection.Assembly.LoadFile(@"C:\Users\eric9\Documents\Bitbucket\CitizenGenerator\NA_TEST_LIB\bin\Release\NA_TEST_LIB.dll");
            var assembly = System.Reflection.Assembly.LoadFile(@"L:\Bitbucket\LeaderMakerTools\NA_TEST_LIB\bin\Release\NA_TEST_LIB.dll");
            richTextBox1.AppendText(ManagedDLL().ToString());
            richTextBox1.AppendText("\n");
            richTextBox1.AppendText(DynamicDLL(assembly).ToString());
            richTextBox1.AppendText("\n");
            richTextBox1.AppendText(DynamicDLLWithLambda(assembly).ToString());
            richTextBox1.AppendText("\n");
        }

        double ManagedDLL()
        {
            NA_TEST_LIB.Classes.TestClass testInstance = new NA_TEST_LIB.Classes.TestClass();

            var sw = new System.Diagnostics.Stopwatch();
            sw.Restart();
            for (int i = 0; i < 100000; ++i)
            {
                testInstance.Add(12345, 67890);
            }
            //testInstance.Loop();
            sw.Stop();

            return sw.Elapsed.TotalMilliseconds;
        }

        double DynamicDLL(System.Reflection.Assembly assembly)
        {
            //var assembly = System.Reflection.Assembly.LoadFile(@"C:\Users\eric9\Documents\Bitbucket\CitizenGenerator\NA_TEST_LIB\bin\Release\NA_TEST_LIB.dll");
            var testClass = assembly.GetType("NA_TEST_LIB.Classes.TestClass");
            var testInstance = Activator.CreateInstance(testClass);
            var addMethod = testClass.GetMethod("Add");
            var addDelegate = (Func<int, int, int>)addMethod.CreateDelegate(typeof(Func<int, int, int>), testInstance);
            var loopMethod = testClass.GetMethod("Loop");
            var loopDelegate = (Action)loopMethod.CreateDelegate(typeof(Action), testInstance);

            var sw = new System.Diagnostics.Stopwatch();
            sw.Restart();
            for (int i = 0; i < 100000; ++i)
            {
                addDelegate(12345, 67890);
            }
            //loopDelegate();
            sw.Stop();

            return sw.Elapsed.TotalMilliseconds;
        }

        double DynamicDLLWithLambda(System.Reflection.Assembly assembly)
        {
            var testClass = assembly.GetType("NA_TEST_LIB.Classes.TestClass");
            var testInstance = Activator.CreateInstance(testClass);
            var addMethod = testClass.GetMethod("Add");
            var loopMethod = testClass.GetMethod("Loop");

            var instanceExpression = Expression.Parameter(typeof(object));
            var castExpression = Expression.TypeAs(instanceExpression, testClass);
            var xExpression = Expression.Parameter(typeof(int));
            var yExpression = Expression.Parameter(typeof(int));
            //var loopCallExpression = Expression.Call(castExpression, loopMethod);
            //var loopLambda = Expression.Lambda<Action<object>>(loopCallExpression, instanceExpression);
            //var compiledLoopLambda = loopLambda.Compile();
            var addCallExpression = Expression.Call(castExpression, addMethod, xExpression, yExpression);
            var addLambda = Expression.Lambda<Func<object, int, int, int>>(addCallExpression, instanceExpression, xExpression, yExpression);
            var compiledAddLambda = addLambda.Compile();

            var sw = new System.Diagnostics.Stopwatch();
            sw.Restart();
            for (int i = 0; i < 100000; ++i)
            {
                compiledAddLambda(testInstance, 12345, 67890);
            }
            sw.Stop();

            return sw.Elapsed.TotalMilliseconds;
        }

        void test8()
        {
            string src = "class Test{}";
            var options = new CompilerParameters();
            options.GenerateExecutable = false;
            options.GenerateInMemory = false;
            options.OutputAssembly = "test.dll";
            var compiler = new CSharpCodeProvider();
            var result = compiler.CompileAssemblyFromSource(options, src);

            //Type testType = result.CompiledAssembly.GetType("Test");

            bool contains = AppDomain.CurrentDomain.GetAssemblies().Contains(result.CompiledAssembly);
            richTextBox1.AppendText(contains.ToString());
        }
    }
}