﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using GameCore;

public class Citizen : MonoBehaviour {

    PathManager pathManager;

    [SerializeField]
    int numArea;

    [SerializeField]
    float speed;

    HiddenMarkovMachine hiddenMarkovMachine;
    InputState input1;
    InputState input2;
    InputState input3;
    public int output;
    List<Objective> actions;
    Objective currentAction;

    NavMeshAgent agent;
    Area area;

    [SerializeField]
    Dictionary<string, int> parameters;

    public class Friend
    {
        protected static NavMeshAgent Agent(Citizen citizen) { return citizen.agent; }
        protected static Area Area(Citizen citizen) { return citizen.area; }
        protected static void Area(Citizen citizen, Area area) { citizen.area = area; }
        protected static Objective Objective(Citizen citizen) { return citizen.currentAction; }
        protected static float Psychology(Citizen citizen, string name) { return 0; }
        protected static void Psychology(Citizen citizen, string name, float value) {  }
        protected static float Ability(Citizen citizen, string name) { return 0; }
        protected static void Ability(Citizen citizen, string name, float value) { }
        protected static float State(Citizen citizen, string name) { return 0; }
        protected static void State(Citizen citizen, string name, float value) { }
    }

    public Objective[] Actions
    {
        get { return actions.ToArray(); }
    }

    public Objective CurrentAction
    {
        get { return currentAction; }
    }

	void Start () {
        pathManager = PathManager.Instance;
        agent = GetComponent<NavMeshAgent>();

        //InitHMM();
        //input2.Value = Random.Range(-100f, 100f);
        //input3.Value = Random.Range(-100f, 100f);

        area = pathManager.GetArea(Random.Range(0, numArea));
        agent.Warp(area.GetRandomCoord());
        //transform.position = area.GetRandomCoord();

        parameters = new Dictionary<string, int>
        {
            {"Satiety", Random.Range(0, 100) },
            {"Food", Random.Range(0, 100) },
            {"Strength", Random.Range(0, 100) },
            {"Concentration", Random.Range(0, 100) },
            {"Endurance", Random.Range(0, 100) }
        };

        actions = new List<Objective>
        {
            new OEat(this),
            new OSleep(this),
            new AHunt(this)
        };
        currentAction = null;
	}

    void InitHMM()
    {
        input1 = new InputState();
        input2 = new InputState();
        input3 = new InputState();

        State[] inputs = new State[3] { input1, input2, input3 };

        State[] hidden1 = new State[7];
        {
            State[] valueParents = new State[0];
            float[] weights = new float[0];
            State[] workParents = new State[1] { input1 };

            for (int i = 0; i < 6; ++i)
            {
                hidden1[i] = new HiddenState(
                    valueParents, 
                    weights, 
                    0f,
                    workParents, 
                    new float[1] { i - 0.50f }, 
                    new float[1] { i + 0.49f });
            }

            hidden1[6] = new HiddenState(
                valueParents,
                weights,
                0f,
                workParents,
                new float[1] { -2.2f },
                new float[1] { -0.51f });
        }

        State[] hidden2 = new State[3];
        {
            State[] valueParents = new State[2] { input2, input3 };
            State[] workParents = new State[0];
            
            for (int i = 0; i < 3; ++i)
            {
                hidden2[i] = new HiddenState(
                    valueParents, 
                    new float[2] { Random.Range(-10f, 10f), Random.Range(-10f, 10f) }, 
                    Random.Range(-10f, 10f), 
                    workParents, 
                    null, 
                    null);
            }
        }

        State[] hidden3 = new State[3];
        {
            State[] workParents = new State[0];

            for (int i = 0; i < 3; ++i)
            {
                hidden3[i] = new HiddenState(
                    hidden2, 
                    new float[3] { Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f) },
                    Random.Range(-10f, 10f),
                    workParents, 
                    null, 
                    null);
            }
        }

        /*State[] outputs = new State[7];
        {        
            for (int i = 0; i < 6; ++i)
            {
                outputs[i] = new HiddenState(hidden3
                    , new float[3] { Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f) }
                    , Random.Range(-10f, 10f)
                    , new State[1] { hidden1[i] }
                    , new float[1] { 1 }
                    , new float[1] { -1 });
            }

            outputs[6] = new HiddenState(hidden3
                , new float[3] { Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f) }
                , 100000f
                , new State[1] { hidden1[6] }
                , new float[1] { 1 }
                , new float[1] { -1 });
        }*/
        State[] outputs = new State[6];
        {
            State[] workParents = new State[0];
            for (int i = 0; i < 6; ++i)
            {
                outputs[i] = new HiddenState(hidden3
                    , new float[3] { Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f) }
                    , Random.Range(-10f, 10f)
                    , workParents
                    , null
                    , null);
            }
        }

        State[][] layers = new State[4][] { inputs, hidden2, hidden3, outputs };
        hiddenMarkovMachine = new HiddenMarkovMachine(layers);
    }

    void Update()
    {
        //input1.Value = (area != null) ? ((float)area.Index) : (-1f);
        //input2.Value = input2.Value + Random.Range(-1f, 1f);
        //input3.Value = input2.Value + Random.Range(-1f, 1f);
        //hiddenMarkovMachine.Tick();

        {
            int maxIndex = -1;
            float maxValue = float.MinValue;

            for (int i = 0; i < actions.Count; ++i)
            {
                if (actions[i].Priority == 0)
                {
                    maxIndex = i;
                    break;
                }
                if (actions[i].Priority > maxValue)
                {
                    maxValue = actions[i].Priority;
                    maxIndex = i;
                }
            }
            if (maxIndex < 0) { return; }
            output = maxIndex;
        }

        if (!ReferenceEquals(currentAction, actions[output]))
        {
            if (currentAction == null)
            {
                actions[output].Start();
                currentAction = actions[output];
            }
            else if (actions[output].Priority == 0)
            {
                currentAction.Terminate();
                actions[output].Start();
                currentAction = actions[output];
            }
        }

        if (currentAction.TerminateCondition)
        {
            currentAction.Terminate();
            currentAction = null;
        }
        else
        {
            currentAction.Update();
        }

        --parameters["Satiety"];
    }

    void onClick()
    {

    }
}

public class HiddenMarkovMachine
{
    State[][] layers;
    State[] output;

    public State[] Output
    {
        get { return output; }
    }

    public HiddenMarkovMachine(State[][] layers)
    {
        this.layers = layers;
        output = layers[layers.Length - 1];
    }

    public void Tick()
    {
        State[] layer = null;
        for (int i = 0; i < layers.Length;  ++i)
        {
            layer = layers[i];
            for (int j = 0; j < layer.Length; ++j)
            {
                layer[j].Update();
            }
        }
    }
}

public interface State
{
    float Value
    {
        get;
    }

    bool Work
    {
        get;
    }

    void Update();
}

public class InputState : State
{
    float value;
    bool work;

    public float Value
    {
        get { return value; }
        set { this.value = value; }
    }

    public bool Work
    {
        get { return work; }
        set { work = value; }
    }

    public InputState()
    {
        //value = Random.Range(0f, 100f);
        value = 0;
        work = true;
    }

    public void Update()
    {
        //value += Random.Range(-5f, 5f);
        //if (value < 0f) { value = 0f; }
        //else if (value > 100f) { value = 100f; }
    }
}

public class HiddenState : State
{
    float value;
    bool work;
    State[] valueParents;
    float[] weights;
    float bias;
    State[] workParents;
    float[] mins;
    float[] maxs;

    public float Value
    {
        get { return value; }
    }

    public bool Work
    {
        get { return work; }
    }

    public HiddenState(State[] valueParents, float[] weights, float bias, State[] workParents, float[] mins, float[] maxs)
    {
        this.valueParents = valueParents;
        this.weights = weights;
        this.workParents = workParents;
        this.mins = mins;
        this.maxs = maxs;

        this.value = 0f;
        this.work = true;
    }

    public void Update()
    {
        //value = null;
        //work = null;

        work = true;
        for (int i = 0; i < workParents.Length; ++i)
        {
            if (workParents[i].Work && (workParents[i].Value < mins[i] || workParents[i].Value > maxs[i]))
            {
                work = false;
                break;
            }
        }

        if (!work) { return; }

        value = bias;
        for (int i = 0; i < valueParents.Length; ++i)
        {
            value += (valueParents[i].Work) ? (valueParents[i].Value * weights[i]) : (0f);
        }
    }
}

public abstract class Objective : Citizen.Friend
{
    protected Citizen target;
    protected int priority;
    protected int priorityUpdatedTick;
    protected bool terminateCondition;

    public int Priority
    {
        get
        {
            if (World.Tick > priorityUpdatedTick)
            {
                UpdatePriority();
            }
            return priority;
        }
    }
    public bool TerminateCondition
    {
        get { return terminateCondition; }
    }

    public abstract string Name
    {
        get;
    }

    public Objective(Citizen target)
    {
        this.target = target;
        this.priority = 5;
        this.priorityUpdatedTick = 0;
        this.terminateCondition = false;
    }

    public abstract void Start();
    public abstract void Update();
    public abstract void Terminate();
    protected abstract void UpdatePriority();
}

public class OEat :Objective
{
    public override string Name
    {
        get { return "EAT"; }
    }

    public OEat(Citizen target) : base(target) { }

    public override void Start()
    {
        if (Parameter(target, "Food") < 50 - Parameter(target, "Satiety"))
        {
            // 채집 or 사냥
        }
    }

    public override void Update()
    {
        int satiety = Parameter(target, "Satiety") + 2;
        Parameter(target, "Satiety", satiety);
        Parameter(target, "Food", Parameter(target, "Food") - 1);

        terminateCondition = satiety >= 50;
    }

    public override void Terminate()
    {
        // Do Nothing...
    }

    protected override void UpdatePriority()
    {
        int satiety = Parameter(target, "Satiety");

        if (Parameter(target, "Food") < 50 - Parameter(target, "Satiety"))
        {
            priority = 9;
        }
        else if (satiety < 10)
        {
            priority = 0;
        }
        else if (satiety < 20)
        {
            priority = 9;
        }
        else if (satiety < 30)
        {
            priority = 7;
        }
        else
        {
            priority = 5;
        }
    }
}

public class OSleep :Objective
{
    AMoveArea moveArea;
    AMoveRandomPosInArea movePos;
    System.Action update;

    public override string Name
    {
        get { return "SLEEP"; }
    }

    public OSleep(Citizen target) : base(target)
    {
        moveArea = new AMoveArea(target, PathManager.Instance.GetArea(0));
        movePos = new AMoveRandomPosInArea(target);
    }

    void MoveArea()
    {
        if (!moveArea.BeingPerformed && Priority > 0)
        {
            moveArea.Update();
            return;
        }

        moveArea.Terminate();
        movePos.Start();
        update = MovePos;
    }

    void MovePos()
    {
        if (!movePos.BeingPerformed && Priority > 0)
        {
            movePos.Update();
            return;
        }

        movePos.Terminate();
        update = Sleep;
    }

    void Sleep()
    {
        int strength = Parameter(target, "Strength") + 1;
        Parameter(target, "Strength", strength);

        int concentration = Parameter(target, "Concentration") + 1;
        Parameter(target, "Concentration", concentration);

        int endurance = Parameter(target, "Endurance") + 1;
        Parameter(target, "Endurance", endurance);

        int satiety = Parameter(target, "Satiety") - 2;
        Parameter(target, "Satiety", satiety);

        terminateCondition = (World.Time > 480) || (endurance >= 90 && (strength >= 90 || concentration >= 90));
    }

    public override void Start()
    {
        terminateCondition = false;

        // 거처 있는경우 거처로 이동
        // 임시 : Room1으로 이동
        if (Priority > 0)
        {
            moveArea.Start();
            update = MoveArea;
        }
        else
        {
            update = Sleep;
        }
    }

    public override void Update()
    {
        update();
    }

    public override void Terminate()
    {
        moveArea.Terminate();
        movePos.Terminate();
        update = null;
    }

    protected override void UpdatePriority()
    {
        int count = 0;
        int strength = Parameter(target, "Strength");
        int concentration = Parameter(target, "Concentration");
        int endurance = Parameter(target, "Endurance");

        if (strength < 10 || concentration < 10)
        {
            count += 2;
        }
        else if (strength < 30 || concentration < 10)
        {
            count += 1;
        }

        if (endurance < 10)
        {
            count += 2;
        }
        else if (endurance < 30)
        {
            count += 1;
        }

        if (count >= 2)
        {
            priority = 0;
        }
        else if (World.Time > 0 && World.Time < 480)
        {
            priority = 8;
        }
    }
}

public class AHunt :Objective
{
    AMoveArea moveArea;
    AMoveRandomPosInArea movePos;
    System.Action update;

    public override string Name
    {
        get { return "HUNT"; }
    }

    public AHunt(Citizen target) :base(target)
    {
        moveArea = new AMoveArea(target, PathManager.Instance.GetArea(1));
        movePos = new AMoveRandomPosInArea(target);
    }

    void MoveArea()
    {
        if (!moveArea.BeingPerformed)
        {
            moveArea.Update();
            return;
        }

        moveArea.Terminate();
        update = Hunt;
    }

    void Hunt()
    {
        if (movePos.BeingPerformed)
        {
            movePos.Terminate();
            movePos.Start();
        }
        else
        {
            movePos.Update();

            int food = Parameter(target, "Food") + 2;
            Parameter(target, "Food", food);

            int strength = Parameter(target, "Strength") - 1;
            Parameter(target, "Strength", strength);

            int concentration = Parameter(target, "Concentration") - 1;
            Parameter(target, "Concentration", concentration);

            int endurance = Parameter(target, "Endurance") - 1;
            Parameter(target, "Endurance", endurance);

            int satiety = Parameter(target, "Satiety") - 1;
            Parameter(target, "Satiety", satiety);

            terminateCondition = food >= 60;
        }
    }

    public override void Start()
    {
        // 임시 : Room2로 이동
        moveArea.Start();
        update = MoveArea;
    }

    public override void Update()
    {
        update();
    }

    public override void Terminate()
    {
        moveArea.Terminate();
        movePos.Terminate();
        update = null;
    }

    protected override void UpdatePriority()
    {
        int food = Parameter(target, "Food");

        if (food < 50 - Parameter(target, "Satiety"))
        {
            priority = 0;
        }
        else if (food < 10)
        {
            priority = 9;
        }
        else if (food < 20)
        {
            priority = 7;
        }
        else
        {
            priority = 5;
        }
    }
}

public abstract class Action :Citizen.Friend
{
    protected Citizen target;
    protected float efficiency;
    protected bool beingPerformed;
    protected System.Action do_;

    public float Efficiency
    {
        get { return efficiency; }
    }

    public bool BeingPerformed
    {
        get { return beingPerformed; }
    }

    public Action(Citizen target)
    {
        this.target = target;
        beingPerformed = true;
    }

    public void Do()
    {
        do_();
    }
    protected virtual void Start()
    {
        beingPerformed = false;
    }
    protected abstract void Update();
    public virtual void Terminate()
    {
        beingPerformed = false;
    }
}

public class AMoveArea :Action
{
    Area area;
    Path path;
    System.Action update;
    NavMeshAgent agent;

    public AMoveArea(Citizen target)
        :base(target)
    {
        agent = Agent(target);
        path = null;
    }

    public AMoveArea(Citizen target, Area area)
        :this(target)
    {
        this.area = area;
        do_ = Start;
    }

    protected override void Start()
    {
        base.Start();

        do_ = Update;

        if (object.ReferenceEquals(area, Area(target)))
        {
            Terminate();
        }
        else if (Area(target) == null)
        {
            update = MoveToArea0;
        }
        else
        {
            update = TracePath0;
        }
    }

    protected override void Update()
    {
        update();
    }

    public override void Terminate()
    {
        base.Terminate();

        do_ = Start;

        if (agent.pathPending || agent.remainingDistance > float.Epsilon)
        {
            agent.isStopped = true;
        }
        update = null;
        path = null;
    }

    void SetArea(Area area)
    {
        this.area = area;
    }

    void MoveToArea0()
    {
        Vector3 randomGate = area.Gates[Random.Range(0, area.Gates.Length)];
        agent.destination = randomGate;
        update = MoveToArea1;
    }

    void MoveToArea1()
    {
        if (!agent.pathPending && agent.remainingDistance <= float.Epsilon)
        {
            Terminate();
        }
    }

    void TracePath0()
    {
        path = PathManager.Instance.GetPath(Area(target).Index, area.Index);
        agent.destination = path.NavMeshPath.corners[0];
        update = TracePath1;
    }

    void TracePath1()
    {
        if (!agent.pathPending && agent.remainingDistance <= float.Epsilon)
        {
            Area(target, null);
            agent.SetPath(path.NavMeshPath);
            update = TracePath2;
        }
    }

    void TracePath2()
    {
        if (!agent.pathPending && agent.remainingDistance <= float.Epsilon)
        {
            Area(target, area);
            Terminate();
        }
    }
}

public class AMoveRandomPosInArea : Action
{
    Area area;
    NavMeshAgent agent;

    public AMoveRandomPosInArea(Citizen target)
        : base(target)
    {
        agent = Agent(target);
        do_ = Start;
    }

    protected override void Start()
    {
        base.Start();

        area = Area(target);

        do_ = Update;
        
        if (area == null)
        {
            Terminate();
        }
        else
        {
            agent.destination = area.GetRandomCoord();
        }
    }

    protected override void Update()
    {
        if (!agent.pathPending && agent.remainingDistance <= float.Epsilon)
        {
            Terminate();
        }
    }

    public override void Terminate()
    {
        base.Terminate();

        do_ = Start;

        if (agent.pathPending || agent.remainingDistance > float.Epsilon)
        {
            agent.isStopped = true;
        }
    }
}

public interface IEstimator
{
    float Estimation
    {
        get;
    }
}

public class EConstant :IEstimator
{
    static Dictionary<float, EConstant> instances;
    float value;

    public float Estimation
    {
        get { return value; }
    }

    private EConstant(float value)
    {
        this.value = value;
        instances.Add(value, this);
    }

    public static EConstant Instance(float value)
    {
        if (instances == null)
        {
            instances = new Dictionary<float, EConstant>();
        }

        EConstant instance;
        instances.TryGetValue(value, out instance);

        if (instance == null)
        {
            instance = new EConstant(value);
            instances.Add(value, instance);
        }

        return instance;
    }
}

public class EPsychology :Citizen.Friend, IEstimator
{
    static Dictionary<string, EPsychology> instances;
    string parameterName;
    Citizen target;

    public float Estimation
    {
        get { return Psychology(target, parameterName); }
    }

    private EPsychology(string parameterName)
    {
        this.parameterName = parameterName;
        instances.Add(parameterName, this);
    }

    public EPsychology Instance(string parameterName)
    {
        if (instances == null)
        {
            instances = new Dictionary<string, EPsychology>();
        }

        EPsychology instance;
        instances.TryGetValue(parameterName, out instance);

        if (instance == null)
        {
            instance = new EPsychology(parameterName);
            instances.Add(parameterName, instance);
        }

        return instance;
    }

    public void SetTarget(Citizen target)
    {
        // target != null
        this.target = target;
    }
}

public class EAbility : Citizen.Friend, IEstimator
{
    static Dictionary<string, EAbility> instances;
    string parameterName;
    Citizen target;

    public float Estimation
    {
        get { return Ability(target, parameterName); }
    }

    private EAbility(string parameterName)
    {
        this.parameterName = parameterName;
        instances.Add(parameterName, this);
    }

    public EAbility Instance(string parameterName)
    {
        if (instances == null)
        {
            instances = new Dictionary<string, EAbility>();
        }

        EAbility instance;
        instances.TryGetValue(parameterName, out instance);

        if (instance == null)
        {
            instance = new EAbility(parameterName);
            instances.Add(parameterName, instance);
        }

        return instance;
    }

    public void SetTarget(Citizen target)
    {
        // target != null
        this.target = target;
    }
}

public class EState : Citizen.Friend, IEstimator
{
    static Dictionary<string, EState> instances;
    string parameterName;
    Citizen target;

    public float Estimation
    {
        get { return State(target, parameterName); }
    }

    private EState(string parameterName)
    {
        this.parameterName = parameterName;
        instances.Add(parameterName, this);
    }

    public EState Instance(string parameterName)
    {
        if (instances == null)
        {
            instances = new Dictionary<string, EState>();
        }

        EState instance;
        instances.TryGetValue(parameterName, out instance);

        if (instance == null)
        {
            instance = new EState(parameterName);
            instances.Add(parameterName, instance);
        }

        return instance;
    }

    public void SetTarget(Citizen target)
    {
        // target != null
        this.target = target;
    }
}

public class EAdd
{

}

namespace TTest
{
    public class RPNCalculator
    {
        Stack<float> stack;

        public class Expression
        {
            
        }

        public enum Operator
        {
            NOT,
            AND,
            OR,
            EQUAL,
            BIGGER,
            SMALLER,
            BIGGER_OR_EQUAL,
            SMALLER_OR_EQUAL,
            ADD,
            SUBTRACT,
            MULTIPLY,
            DIVIDE,
            MOD,
            ABS,
            POWER,
            SQRT,
            LOG,
            LOG_2,
            LOG_10,
            LOG_E,
            FACTORIAL
        }

        void Operate(Operator op)
        {
            switch (op)
            {
                case Operator.NOT:
                    {
                        Not();
                        break;
                    }
                case Operator.AND:
                    {
                        And();
                        break;
                    }
                case Operator.OR:
                    {
                        Or();
                        break;
                    }
                case Operator.EQUAL:
                    {
                        Equal();
                        break;
                    }
                case Operator.BIGGER:
                    {
                        Bigger();
                        break;
                    }
                case Operator.SMALLER:
                    {
                        Smaller();
                        break;
                    }
                case Operator.BIGGER_OR_EQUAL:
                    {
                        BiggerOrEqual();
                        break;
                    }
                case Operator.SMALLER_OR_EQUAL:
                    {
                        SmallerOrEqual();
                        break;
                    }
                case Operator.ADD:
                    {
                        Add();
                        break;
                    }
                case Operator.SUBTRACT:
                    {
                        Subtract();
                        break;
                    }
                case Operator.MULTIPLY:
                    {
                        Multiply();
                        break;
                    }
                case Operator.DIVIDE:
                    {
                        Divide();
                        break;
                    }
            }
        }

        bool FloatToBool(float value)
        {
            return value > 0f;
        }

        float BoolToFloat(bool value)
        {
            return (value ? 1f : 0f);
        }

        void Not()
        {
            stack.Push(BoolToFloat(!FloatToBool(stack.Pop())));
        }
        void And()
        {
            stack.Push(BoolToFloat(FloatToBool(stack.Pop()) && FloatToBool(stack.Pop())));
        }
        void Or()
        {
            stack.Push(BoolToFloat(FloatToBool(stack.Pop()) || FloatToBool(stack.Pop())));
        }
        void Equal()
        {
            stack.Push(BoolToFloat(stack.Pop() == stack.Pop()));
        }
        void Bigger()
        {
            stack.Push(BoolToFloat(stack.Pop() > stack.Pop()));
        }
        void Smaller()
        {
            stack.Push(BoolToFloat(stack.Pop() < stack.Pop()));
        }
        void BiggerOrEqual()
        {
            stack.Push(BoolToFloat(stack.Pop() >= stack.Pop()));
        }
        void SmallerOrEqual()
        {
            stack.Push(BoolToFloat(stack.Pop() <= stack.Pop()));
        }
        void Add()
        {
            stack.Push(stack.Pop() + stack.Pop());
        }
        void Subtract()
        {
            stack.Push(-stack.Pop() + stack.Pop());
        }
        void Multiply()
        {
            stack.Push(stack.Pop() * stack.Pop());
        }
        void Divide()
        {
            float right = stack.Pop();
            stack.Push(stack.Pop() / right);
        }
        void Mod()
        {
            float right = stack.Pop();
            stack.Push(stack.Pop() % right);
        }
        void Abs()
        {
            stack.Push(Mathf.Abs(stack.Pop()));
        }
    }
}

namespace Utility
{
    public abstract class SingleTon<T> where T:new()
    {
        private static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new T();
                }

                return instance;
            }
        }
    }
}