﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace TestProject
{
    public class Context
    {
        Dictionary<string, float> staticVariables;
        Dictionary<string, float> dynamicVariables;

        public Dictionary<string, float> StaticVariables
        {
            get { return staticVariables; }
        }

        public Dictionary<string, float> DynamicVariables
        {
            get { return dynamicVariables; }
        }

        public Context()
        {
            staticVariables = new Dictionary<string, float>();
            dynamicVariables = new Dictionary<string, float>();
        }

        public float GetStaticVariable(string name)
        {
            float value;
            if (staticVariables.TryGetValue(name, out value))
            {
                return value;
            }

            return float.NaN;
        }

        public float GetDynamicVariable(string name)
        {
            float value;
            if (dynamicVariables.TryGetValue(name, out value))
            {
                return value;
            }

            return float.NaN;
        }

        public void SetStaticVariable(string name, float value)
        {
            staticVariables[name] = value;
        }

        public void SetDynamicVariable(string name, float value)
        {
            dynamicVariables[name] = value;
        }

        void ClearDynamicVariables()
        {
            dynamicVariables.Clear();
        }
    }

    public interface IEstimator
    {
        float Estimation(Context context);
    }

    public class EConstant : IEstimator
    {
        static Dictionary<float, EConstant> instances;
        float value;

        private EConstant(float value)
        {
            this.value = value;
            //instances.Add(value, this);
        }

        public static EConstant Instance(float value)
        {
            if (instances == null)
            {
                instances = new Dictionary<float, EConstant>();
            }

            EConstant instance;
            instances.TryGetValue(value, out instance);

            if (instance == null)
            {
                instance = new EConstant(value);
                instances.Add(value, instance);
            }

            return instance;
        }

        public float Estimation(Context context)
        {
            return value;
        }
    }

    public class ESqrt : IEstimator
    {
        IEstimator value;

        public ESqrt(IEstimator value)
        {
            this.value = value;
        }

        public float Estimation(Context context)
        {
            return Mathf.Sqrt(value.Estimation(context));
        }
    }

    public class ENegative : IEstimator
    {
        IEstimator value;

        public ENegative(IEstimator value)
        {
            this.value = value;
        }

        public float Estimation(Context context)
        {
            return -value.Estimation(context);
        }
    }

    public class EAdd : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EAdd(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimation(Context context)
        {
            return left.Estimation(context) + right.Estimation(context);
        }
    }

    public class ESubtract : IEstimator
    {
        IEstimator left;
        IEstimator right;


        public ESubtract(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimation(Context context)
        {
            return left.Estimation(context) - right.Estimation(context);
        }
    }

    public class EMultiply : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EMultiply(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimation(Context context)
        {
            return left.Estimation(context) * right.Estimation(context);
        }
    }

    public class EDivide : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EDivide(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimation(Context context)
        {
            return left.Estimation(context) / right.Estimation(context);
        }
    }

    public class EMod : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EMod(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimation(Context context)
        {
            return left.Estimation(context) % right.Estimation(context);
        }
    }

    public class EPower : IEstimator
    {
        IEstimator @base;
        IEstimator exponent;

        public EPower(IEstimator left, IEstimator right)
        {
            this.@base = left;
            this.exponent = right;
        }

        public float Estimation(Context context)
        {
            return Mathf.Pow(@base.Estimation(context), exponent.Estimation(context));
        }
    }

    public class EEqual : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EEqual(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimation(Context context)
        {
            return (left.Estimation(context) == right.Estimation(context)) ? (1f) : (0f);
        }
    }

    public class ENotEqual : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public ENotEqual(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimation(Context context)
        {
            return (left.Estimation(context) != right.Estimation(context)) ? (1f) : (0f);
        }
    }

    public class EBigger : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EBigger(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimation(Context context)
        {
            return (left.Estimation(context) > right.Estimation(context)) ? (1f) : (0f);
        }
    }

    public class ESmaller : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public ESmaller(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimation(Context context)
        {
            return (left.Estimation(context) < right.Estimation(context)) ? (1f) : (0f);
        }
    }

    public class EBiggerOrEqual : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EBiggerOrEqual(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimation(Context context)
        {
            return (left.Estimation(context) >= right.Estimation(context)) ? (1f) : (0f);
        }
    }

    public class ESmallerOrEqual : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public ESmallerOrEqual(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimation(Context context)
        {
            return (left.Estimation(context) <= right.Estimation(context)) ? (1f) : (0f);
        }
    }

    public class EAnd : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EAnd(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimation(Context context)
        {
            return (left.Estimation(context) * right.Estimation(context) != 0f) ? (1f) : (0f);
        }
    }

    public class EOr : IEstimator
    {
        IEstimator left;
        IEstimator right;

        public EOr(IEstimator left, IEstimator right)
        {
            this.left = left;
            this.right = right;
        }

        public float Estimation(Context context)
        {
            return (left.Estimation(context) + right.Estimation(context) != 0f) ? (1f) : (0f);
        }
    }

    public class ENot : IEstimator
    {
        IEstimator value;

        public ENot(IEstimator value)
        {
            this.value = value;
        }

        public float Estimation(Context context)
        {
            return (value.Estimation(context) == 0f) ? (1f) : (0f);
        }
    }


    // Statements...
    public class CodeBlock : IEstimator
    {
        IEstimator[] estimators;

        public CodeBlock(IEstimator[] estimators)
        {
            this.estimators = estimators;
        }

        public float Estimation(Context context)
        {
            for (int i = 0; i < estimators.Length; ++i)
            {
                estimators[i].Estimation(context);
            }

            return float.NaN;
        }
    }

    public class EIf : IEstimator
    {
        Tuple<IEstimator, CodeBlock>[] conditionalBlocks;

        public EIf(Tuple<IEstimator, CodeBlock>[] conditionalBlocks)
        {
            this.conditionalBlocks = conditionalBlocks;
        }

        public float Estimation(Context context)
        {
            for (int i = 0; i < conditionalBlocks.Length; ++i)
            {
                if (conditionalBlocks[i].Item1.Estimation(context) != 0f)
                {
                    conditionalBlocks[i].Item1.Estimation(context);
                    break;
                }
            }

            return float.NaN;
        }
    }

    public class EWhile : IEstimator
    {
        IEstimator condition;
        CodeBlock codeBlock;

        public EWhile(IEstimator condition, CodeBlock codeBlock)
        {
            this.condition = condition;
            this.codeBlock = codeBlock;
        }

        public float Estimation(Context context)
        {
            while (condition.Estimation(context) != 0f)
            {
                codeBlock.Estimation(context);
            }

            return float.NaN;
        }
    }


    // Variables...

    public class EDeclareStaticVariable : IEstimator
    {
        string name;
        IEstimator initialValue;

        public EDeclareStaticVariable(string name, IEstimator initialValue)
        {
            this.name = name;
            this.initialValue = initialValue;
        }
        
        public float Estimation(Context context)
        {
            if (float.IsNaN(context.GetStaticVariable(name)))
            {
                context.SetStaticVariable(name, initialValue.Estimation(context));
            }

            return float.NaN;
        }
    }

    public interface IEVariable : IEstimator
    {
        float Assign(Context context, float value);
    }

    public class EStaticVariable : IEVariable
    {
        string name;

        public EStaticVariable(string name)
        {
            this.name = name;
        }

        public float Estimation(Context context)
        {
            // UnDeclaredVariable Error
            return context.GetStaticVariable(name);
        }

        public float Assign(Context context, float value)
        {
            context.SetStaticVariable(name, value);
            return value;
        }
    }

    public class EDynamicVariable : IEVariable
    {
        string name;

        public EDynamicVariable(string name)
        {
            this.name = name;
        }

        public float Estimation(Context context)
        {
            // UnDeclaredVariable Error
            return context.GetDynamicVariable(name);
        }

        public float Assign(Context context, float value)
        {
            context.SetDynamicVariable(name, value);
            return value;
        }
    }

    public class EAssign : IEstimator
    {
        IEVariable variable;
        IEstimator value;

        public EAssign(IEVariable variable, IEstimator value)
        {
            this.variable = variable;
            this.value = value;
        }

        public float Estimation(Context context)
        {
            return variable.Assign(context, value.Estimation(context));
        }
    }

    public class EAddAssign : IEstimator
    {
        IEVariable variable;
        IEstimator value;

        public EAddAssign(IEVariable variable, IEstimator value)
        {
            this.variable = variable;
            this.value = value;
        }

        public float Estimation(Context context)
        {
            return variable.Assign(context, variable.Estimation(context) + value.Estimation(context));
        }
    }

    public class ESubtractAssign : IEstimator
    {
        IEVariable variable;
        IEstimator value;

        public ESubtractAssign(IEVariable variable, IEstimator value)
        {
            this.variable = variable;
            this.value = value;
        }

        public float Estimation(Context context)
        {
            return variable.Assign(context, variable.Estimation(context) - value.Estimation(context));
        }
    }

    public class EMultiplyAssign : IEstimator
    {
        IEVariable variable;
        IEstimator value;

        public EMultiplyAssign(IEVariable variable, IEstimator value)
        {
            this.variable = variable;
            this.value = value;
        }

        public float Estimation(Context context)
        {
            return variable.Assign(context, variable.Estimation(context) * value.Estimation(context));
        }
    }

    public class EDivideAssign : IEstimator
    {
        IEVariable variable;
        IEstimator value;

        public EDivideAssign(IEVariable variable, IEstimator value)
        {
            this.variable = variable;
            this.value = value;
        }

        public float Estimation(Context context)
        {
            return variable.Assign(context, variable.Estimation(context) / value.Estimation(context));
        }
    }

    public class EModAssign : IEstimator
    {
        IEVariable variable;
        IEstimator value;

        public EModAssign(IEVariable variable, IEstimator value)
        {
            this.variable = variable;
            this.value = value;
        }

        public float Estimation(Context context)
        {
            return variable.Assign(context, variable.Estimation(context) % value.Estimation(context));
        }
    }

    public class EPrefixIncrease : IEstimator
    {
        IEVariable variable;

        public EPrefixIncrease(IEVariable variable)
        {
            this.variable = variable;
        }

        public float Estimation(Context context)
        {
            return variable.Assign(context, variable.Estimation(context) + 1);
        }
    }

    public class EPrefixDecrease : IEstimator
    {
        IEVariable variable;

        public EPrefixDecrease(IEVariable variable)
        {
            this.variable = variable;
        }

        public float Estimation(Context context)
        {
            return variable.Assign(context, variable.Estimation(context) - 1);
        }
    }

    public class EPostfixIncrease : IEstimator
    {
        IEVariable variable;

        public EPostfixIncrease(IEVariable variable)
        {
            this.variable = variable;
        }

        public float Estimation(Context context)
        {
            float temp = variable.Estimation(context);
            variable.Assign(context, temp + 1);
            return temp;
        }
    }

    public class EPostfixDecrease : IEstimator
    {
        IEVariable variable;

        public EPostfixDecrease(IEVariable variable)
        {
            this.variable = variable;
        }

        public float Estimation(Context context)
        {
            float temp = variable.Estimation(context);
            variable.Assign(context, temp - 1);
            return temp;
        }
    }
}
