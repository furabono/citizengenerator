﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace TestProject
{
    class Parser
    {
        [Flags]
        enum TokenType
        {
            NONE = 1 << 0,
            RVALUE = 1 << 1,
            LVALUE = 1 << 2,
            PREFIX_ONE_HAND_OPEERATOR = 1 << 3,
            POSTFIX_ONE_HAND_OPEERATOR = 1 << 4,
            TWO_HAND_OPERATOR = 1 << 5
        }

        HashSet<string> statements;

        Dictionary<string, int> functionNumArgument;
        Dictionary<string, int> operatorPriority;
        Dictionary<string, Type> operatorBindings;
        Dictionary<string, TokenType> operatorTypes;
        const int operatorMaxLength = 2;

        HashSet<string> names;

        public Parser()
        {
            statements = new HashSet<string>()
            {
                "static", "if", "while"
            };

            functionNumArgument = new Dictionary<string, int>()
            {
                { "sqrt", 1 }
            };

            operatorPriority = new Dictionary<string, int>()
            {
                { "=", 1 }, { "+=", 1 }, { "-=", 1 }, { "*=", 1 }, { "/=", 1 }, { "%=", 1 },
                { "||", 2 },
                { "&&", 3 },
                { "==", 4 }, { "!=", 4 },
                { "<", 5 }, { ">", 5 }, { "<=", 5 }, { ">=", 5 },
                { "+", 6 }, { "-", 6 },
                { "*", 7 }, { "/", 7 }, { "%", 7 },
                { "**", 8 },
                { "!", 9 }, { "negative", 9 }, { "prefix_increase", 9 }, { "prefix_decrease", 9 },
                { "postfix_increase", 10 }, { "postfix_decrease", 10 },
                { "(", -1 }
            };

            operatorBindings = new Dictionary<string, Type>()
            {
                { "=", typeof(EAssign) },
                { "+=", typeof(EAddAssign) },
                { "-=", typeof(ESubtractAssign) },
                { "*=", typeof(EMultiplyAssign) },
                { "/=", typeof(EDivideAssign) },
                { "%=", typeof(EModAssign) },
                { "||", typeof(EOr) },
                { "&&", typeof(EAnd) },
                { "==", typeof(EEqual) },
                { "!=", typeof(ENotEqual) },
                { "<", typeof(ESmaller) },
                { ">", typeof(EBigger) },
                { "<=", typeof(ESmallerOrEqual) },
                { ">=", typeof(EBiggerOrEqual) },
                { "+", typeof(EAdd) },
                { "-", typeof(ESubtract) },
                { "*", typeof(EMultiply) },
                { "/", typeof(EDivide) },
                { "%", typeof(EMod) },
                { "**", typeof(EPower) },
                { "!", typeof(ENot) },
                { "negative", typeof(ENegative) },
                { "prefix_increase", typeof(EPrefixIncrease) },
                { "prefix_decrease", typeof(EPrefixDecrease) },
                { "postfix_increase", typeof(EPostfixIncrease) },
                { "postfix_decrease", typeof(EPostfixDecrease) },
            };

            operatorTypes = new Dictionary<string, TokenType>()
            {
                { "=", TokenType.TWO_HAND_OPERATOR },
                { "+=", TokenType.TWO_HAND_OPERATOR },
                { "-=", TokenType.TWO_HAND_OPERATOR },
                { "*=", TokenType.TWO_HAND_OPERATOR },
                { "/=", TokenType.TWO_HAND_OPERATOR },
                { "%=", TokenType.TWO_HAND_OPERATOR },
                { "||", TokenType.TWO_HAND_OPERATOR },
                { "&&", TokenType.TWO_HAND_OPERATOR },
                { "==", TokenType.TWO_HAND_OPERATOR },
                { "!=", TokenType.TWO_HAND_OPERATOR },
                { "<", TokenType.TWO_HAND_OPERATOR },
                { ">", TokenType.TWO_HAND_OPERATOR },
                { "<=", TokenType.TWO_HAND_OPERATOR },
                { ">=", TokenType.TWO_HAND_OPERATOR },
                { "+", TokenType.TWO_HAND_OPERATOR },
                { "-", TokenType.TWO_HAND_OPERATOR | TokenType.PREFIX_ONE_HAND_OPEERATOR },
                { "*", TokenType.TWO_HAND_OPERATOR },
                { "/", TokenType.TWO_HAND_OPERATOR },
                { "%", TokenType.TWO_HAND_OPERATOR },
                { "**", TokenType.TWO_HAND_OPERATOR },
                { "!", TokenType.PREFIX_ONE_HAND_OPEERATOR },
                { "++", TokenType.PREFIX_ONE_HAND_OPEERATOR | TokenType.POSTFIX_ONE_HAND_OPEERATOR },
                { "--", TokenType.PREFIX_ONE_HAND_OPEERATOR | TokenType.POSTFIX_ONE_HAND_OPEERATOR }
                //{ "negative", TokenType.PREFIX_ONE_HAND_OPEERATOR }

            };

            names = new HashSet<string>();
        }

        public CodeBlock ParseCodeBlock(string source)
        {
            List<IEstimator> estimators = new List<IEstimator>();

            char ch;
            for (int i = 0; i < source.Length; ++i)
            {
                ch = source[i];

                if (!IsValidCharacter(ch)) { continue; }

                // statements (are begin with alphabet)
                // variable name
                if (IsValidNameCharacter(ch, true))
                {
                    int tokenEnd = FindNameEnd(source, i);
                    string token = source.Substring(i, tokenEnd - i + 1);

                    // statement
                    if (statements.Contains(token))
                    {
                        Tuple<IEstimator, int> parsed = ParseStatement(source, token, tokenEnd);
                        estimators.Add(parsed.Item1);
                        i = parsed.Item2;
                    }
                    // call function
                    else if (functionNumArgument.ContainsKey(token))
                    {

                    }
                    // expression (begin with variable)
                    else
                    {
                        int semicolonPosition = FindSemicolon(source, tokenEnd + 1);
                        
                        if (semicolonPosition == -1)
                        {
                            // Syntax Error : expression must be end in semicolon;
                        }

                        int expressionBegin = i;
                        int expressionLength = semicolonPosition - expressionBegin;

                        estimators.Add(ParseExpression(source.Substring(expressionBegin, expressionLength)));
                        i = semicolonPosition;
                    }
                }
            }

            return new CodeBlock(estimators.ToArray());
        }

        // <Return>
        // IEstimator Item1 : IEstimator of the statement
        // int        Item2 : statement end point
        Tuple<IEstimator, int> ParseStatement(string source, string token, int tokenEnd)
        {
            switch(token)
            {
                // static <variable name> = <initial value (expression)>
                case "static":
                    {
                        // <variable name>'s begin position is right after WhiteSpace's end position
                        int variableNameBegin = FindWhiteSpaceEnd(source, tokenEnd + 1) + 1;

                        if (variableNameBegin == -1)
                        {
                            // Syntax Error : Unknown Token (Ex. static@# ...?)
                        }

                        int variableNameEnd = FindNameEnd(source, variableNameBegin);

                        if (variableNameEnd == -1)
                        {
                            // Syntax Error : Name Error (Name begin with invalid character)
                        }

                        string variableName = source.Substring(variableNameBegin, variableNameEnd - variableNameBegin + 1);

                        if (names.Contains(variableName) || statements.Contains(variableName) || functionNumArgument.ContainsKey(variableName))
                        {
                            // Syntax Error : Invalid Name (Name already defined)
                        }

                        // static variable must be declared with initialization

                        // `=`s position is right after WhiteSpace's end position
                        int assignOperatorPosition = FindWhiteSpaceEnd(source, variableNameEnd + 1);

                        if (assignOperatorPosition == -1) { assignOperatorPosition = variableNameEnd + 1; }

                        if (source[assignOperatorPosition] != '=')
                        {
                            // Syntax Error : Invalid Token (There must be `=`)
                        }

                        // static variable decalration must be end in semicolon
                        int semicolonPosition = FindSemicolon(source, assignOperatorPosition + 1);

                        if (semicolonPosition == -1)
                        {
                            // Syntax Error : expression must be end in semicolon;
                        }

                        // parse expression between `=` and `;`
                        int expressionBegin = assignOperatorPosition + 1;
                        int expressionLength = semicolonPosition - expressionBegin;
                        IEstimator initialValue = ParseExpression(source.Substring(expressionBegin, expressionLength));

                        names.Add(variableName);

                        return new Tuple<IEstimator, int>(new EDeclareStaticVariable(variableName, initialValue), semicolonPosition);
                    }

                case "if":
                    {
                        break;
                    }

                case "while":
                    {
                        break;
                    }
            }

            return null;
        }

        // <Return>
        // end position of the token (sequence of character satisfying given [conditions])
        // if token's begin does not satisfy any [conditions] (token not found)
        // then return value will be -1 (smaller than [tokenBegin])
        int FindTokenEnd(string source, int tokenBegin, params Func<char, bool>[] conditions)
        {
            int i, j;

            // Check the begin point
            for (i = 0; i < conditions.Length; ++i)
            {
                if (conditions[i](source[tokenBegin])) { break; }
            }

            // token not found
            if (i == conditions.Length)
            {
                return -1;
            }

            // conditions 중 하나라도 만족시키면 통과
            // 하나도 만족 시키지 못하는 지점에서 끝
            for (i = tokenBegin + 1; i < source.Length; ++i)
            {
                for (j = 0; j < conditions.Length; ++j)
                {
                    if (conditions[j](source[i])) { break; }
                }

                if (j == conditions.Length) { return i - 1; }
            }

            // source 의 끝까지 token 인 경우
            return i - 1;
        }

        // Specialization of `FindTokenEnd` for WhiteSpace
        int FindWhiteSpaceEnd(string source, int whiteSpaceBegin)
        {
            if (IsValidCharacter(source[whiteSpaceBegin]))
            {
                return -1;
            }

            int i;
            for (i = whiteSpaceBegin + 1; i < source.Length;  ++i)
            {
                if (IsValidCharacter(source[i]))
                {
                    return i - 1;
                }
            }

            return i - 1;
        }

        // Specialization of `FindTokenEnd` for Name
        int FindNameEnd(string source, int nameBegin)
        {
            if (!IsValidNameCharacter(source[nameBegin], true))
            {
                return -1;
            }

            int i;
            for (i = nameBegin + 1; i < source.Length; ++i)
            {
                if (!IsValidNameCharacter(source[i]))
                {
                    return i - 1;
                }
            }

            return i - 1;
        }

        // Find first comming Semicolon from [begin]
        // if not found then return -1
        int FindSemicolon(string source, int begin)
        {
            for (int i = 0; i < source.Length; ++i)
            {
                if (source[i] == ';')
                {
                    return i;
                }
            }

            return -1;
        }

        int FindClosingParenthesis(string source, int openingIndex)
        {
            int numOpenedParentheses = 1;

            for (int i = openingIndex + 1; i < source.Length; ++i)
            {
                if (source[i] == ')')
                {
                    if (--numOpenedParentheses == 0)
                    {
                        return i;
                    }
                }
                else if (source[i] == '(')
                {
                    ++numOpenedParentheses;
                }
            }

            // Syntax Error : Unclosed Parenthesis
            throw new Exception();
        }

        int FindClosingBrace(string source, int openingIndex)
        {
            int numOpenedBraces = 1;

            for (int i = openingIndex + 1; i < source.Length; ++i)
            {
                if (source[i] == '}')
                {
                    if (--numOpenedBraces == 0)
                    {
                        return i;
                    }
                }
                else if (source[i] == '{')
                {
                    ++numOpenedBraces;
                }
            }

            // Syntax Error : Unclosed Parenthesis
            throw new Exception();
        }

        public IEstimator ParseExpression(string expression)
        {
            string[] tokens = SplitToken(expression);

            var rpn = new Queue<string>();
            var stack = new Stack<string>();

            string token;
            for (int i = 0; i < tokens.Length; ++i)
            {
                token = tokens[i];

                if (operatorPriority.ContainsKey(token))
                {
                    if (token != "(")
                    {
                        int priority = operatorPriority[token];
                        while (stack.Count > 0 && operatorPriority[stack.Peek()] >= priority)
                        {
                            rpn.Enqueue(stack.Pop());
                        }
                    }

                    stack.Push(token);
                }
                else if (functionNumArgument.ContainsKey(token))
                {
                    stack.Push(token);
                    if (functionNumArgument[token] == -1)
                    {
                        rpn.Enqueue("(");
                    }
                }
                else if (token == ")")
                {
                    while (stack.Peek() != "(")
                    {
                        rpn.Enqueue(stack.Pop());
                    }
                    stack.Pop();
                }
                else if (token == ",")
                {
                    // pass
                }
                else
                {
                    rpn.Enqueue(token);
                }
            }

            while (stack.Count > 0)
            {
                rpn.Enqueue(stack.Pop());
            }

            Stack<IEstimator> calc = new Stack<IEstimator>();
            while (rpn.Count > 0)
            {
                token = rpn.Dequeue();

                if (operatorPriority.ContainsKey(token))
                {
                    Operate(calc, token);
                }
                else if (functionNumArgument.ContainsKey(token))
                {
                    Function(calc, token);
                }
                else
                {
                    float value;
                    if (float.TryParse(token, out value))
                    {
                        calc.Push(EConstant.Instance(value));
                    }
                    else
                    {
                        calc.Push(new EDynamicVariable(token));
                    }
                }
            }

            if (calc.Count != 1) { /*SYNTAX ERROR*/ }

            return calc.Peek();
        }

        string[] SplitToken(string expression)
        {

            List<string> atomics = new List<string>();
            List<TokenType> atomicTypes = new List<TokenType>() { TokenType.NONE };
            List<string> operators = new List<string>();
            List<int> operatorPositions = new List<int>();

            char ch;
            for (int i = 0; i < expression.Length; ++i)
            {
                ch = expression[i];

                if (!IsValidCharacter(ch)) { continue; }

                if (IsValidNameCharacter(ch, true))
                {
                    int j;
                    for (j = i + 1; j < expression.Length; ++j)
                    {
                        ch = expression[j];
                        if (!IsValidNameCharacter(ch)) { break; }
                    }

                    atomics.Add(expression.Substring(i, j - i));
                    atomicTypes.Add(TokenType.LVALUE);   // Function 의 경우 다음에 무조건 ( ) 가 나오므로 상관없다
                    i = j - 1;
                }
                else if (IsDigit(ch))
                {
                    int j;
                    for (j = i + 1; j < expression.Length; ++j)
                    {
                        ch = expression[j];
                        if (!(IsDigit(ch) || ch == '.')) { break; }
                    }

                    atomics.Add(expression.Substring(i, j - i));
                    atomicTypes.Add(TokenType.RVALUE);
                    i = j - 1;
                }
                else if (ch == '(')
                {
                    atomics.Add("(");
                    atomicTypes.Add(TokenType.NONE);
                }
                else if (ch == ')')
                {
                    atomics.Add(")");
                    atomicTypes.Add(TokenType.RVALUE);
                }
                else if (ch == ',')
                {
                    atomics.Add(",");
                    atomicTypes.Add(TokenType.NONE);
                }
                else
                {
                    int j;
                    for (j = i + 1; j < expression.Length; ++j)
                    {
                        ch = expression[j];
                        if (IsValidNameCharacter(ch)
                            || (ch == '(')
                            || (ch == ')')
                            || (ch == ',')
                            || !IsValidCharacter(ch))
                        { break; }
                    }

                    operators.Add(expression.Substring(i, j - i));
                    operatorPositions.Add(atomics.Count);
                    i = j - 1;
                }
            }
            atomicTypes.Add(TokenType.NONE);

            List<string> tokens = new List<string>();
            List<TokenType> tokenTypes = new List<TokenType>() { TokenType.NONE };
            int opCounter = 0;
            for (int i = 0; i < atomics.Count; ++i)
            {
                while (opCounter < operatorPositions.Count && operatorPositions[opCounter] == i)
                {
                    var split = SplitOperator(tokenTypes[tokenTypes.Count - 1], atomicTypes[i + 1], operators[opCounter]);
                    if (split == null) { /*SYNTAX ERROR*/ }
                    else
                    {
                        var ops = split.Item1;
                        var opTypes = split.Item2;
                        tokens.AddRange(ops);
                        tokenTypes.AddRange(opTypes);
                    }
                    ++opCounter;
                }

                tokens.Add(atomics[i]);
                tokenTypes.Add(atomicTypes[i + 1]);
            }

            if (operators.Count - opCounter > 1) { /*SYNTAX ERROR*/ }
            if (operators.Count - opCounter > 0)
            {
                var split = SplitOperator(atomicTypes[atomics.Count], TokenType.NONE, operators[opCounter]);
                if (split == null) { /*SYNTAX ERROR*/ }
                else
                {
                    var ops = split.Item1;
                    var opTypes = split.Item2;
                    tokens.AddRange(ops);
                    //tokenTypes.AddRange(opTypes);
                }
            }

            return tokens.ToArray();
        }

        Tuple<string[], TokenType[]> SplitOperator(TokenType tokenAhead, TokenType tokenBehind, string operators)
        {
            if (operators.Length == 0)
            {
                switch (tokenBehind)
                {
                    case TokenType.NONE:
                        {
                            if (tokenAhead != TokenType.POSTFIX_ONE_HAND_OPEERATOR) { return null; }
                            break;
                        }
                    case TokenType.RVALUE:
                        {
                            if (tokenAhead != TokenType.TWO_HAND_OPERATOR && tokenAhead != TokenType.PREFIX_ONE_HAND_OPEERATOR) { return null; }
                            break;
                        }
                    case TokenType.LVALUE:
                        {
                            if (tokenAhead != TokenType.TWO_HAND_OPERATOR && tokenAhead != TokenType.PREFIX_ONE_HAND_OPEERATOR) { return null; }
                            break;
                        }
                    default:
                        {
                            return null;
                        }
                }

                return Tuple.Create(new string[0], new TokenType[0]);
            }

            for (int i = 0; i < Math.Min(operatorMaxLength, operators.Length); ++i)
            {
                string op = operators.Substring(0, i + 1);

                if (!operatorPriority.ContainsKey(op)) { return null; }
                TokenType opType = operatorTypes[op];

                if ((opType & TokenType.PREFIX_ONE_HAND_OPEERATOR) != 0)
                {
                    if (tokenAhead != TokenType.NONE && tokenAhead != TokenType.TWO_HAND_OPERATOR && tokenAhead != TokenType.PREFIX_ONE_HAND_OPEERATOR) { continue; }
                    if (operators.Length != i + 1) { continue; }

                    Tuple<string[], TokenType[]> split = SplitOperator(TokenType.PREFIX_ONE_HAND_OPEERATOR, tokenBehind, operators.Substring(i + 1));
                    if (split != null)
                    {
                        if (op == "-") { op = "negative"; }
                        var ops = new List<string>() { op };
                        var opTypes = new List<TokenType> { TokenType.PREFIX_ONE_HAND_OPEERATOR };
                        ops.AddRange(split.Item1);
                        opTypes.AddRange(split.Item2);
                        return Tuple.Create(ops.ToArray(), opTypes.ToArray());
                    }
                }
                if ((opType & TokenType.POSTFIX_ONE_HAND_OPEERATOR) != 0)
                {
                    if (tokenAhead != TokenType.LVALUE) { continue; }

                    Tuple<string[], TokenType[]> split = SplitOperator(TokenType.POSTFIX_ONE_HAND_OPEERATOR, tokenBehind, operators.Substring(i + 1));
                    if (split != null)
                    {
                        var ops = new List<string>() { op };
                        var opTypes = new List<TokenType> { TokenType.POSTFIX_ONE_HAND_OPEERATOR };
                        ops.AddRange(split.Item1);
                        opTypes.AddRange(split.Item2);
                        return Tuple.Create(ops.ToArray(), opTypes.ToArray());
                    }
                }
                if ((opType & TokenType.TWO_HAND_OPERATOR) != 0)
                {
                    if (tokenAhead == TokenType.NONE || tokenAhead == TokenType.TWO_HAND_OPERATOR) { continue; }

                    Tuple<string[], TokenType[]> split = SplitOperator(TokenType.TWO_HAND_OPERATOR, tokenBehind, operators.Substring(i + 1));
                    if (split != null)
                    {
                        var ops = new List<string>() { op };
                        var opTypes = new List<TokenType> { TokenType.TWO_HAND_OPERATOR };
                        ops.AddRange(split.Item1);
                        opTypes.AddRange(split.Item2);
                        return Tuple.Create(ops.ToArray(), opTypes.ToArray());
                    }
                }
                else
                {
                    return null;
                }
            }

            return null;
        }

        void Operate(Stack<IEstimator> rpn, string @operator)
        {
            Type type = operatorBindings[@operator];
            int numArguments = type.GetConstructors()[0].GetParameters().Length;

            IEstimator[] args = new IEstimator[numArguments];
            for (int i = 0; i < numArguments; ++i)
            {
                args[numArguments - i - 1] = rpn.Pop();
            }

            rpn.Push((IEstimator)Activator.CreateInstance(type, args));
            return;
        }

        void Function(Stack<IEstimator> rpn, string functionName)
        {
            switch (functionName)
            {
                case "sqrt":
                    {
                        rpn.Push(new ESqrt(rpn.Pop()));
                        break;
                    }
            }
        }

        bool IsValidCharacter(char ch)
        {
            return !(ch == ' ' || ch == '\n' || ch == '\t' || ch == '\r');
        }

        bool IsValidNameCharacter(char ch, bool isInitialCharacter = false)
        {
            return (ch >= 'A' && ch <= 'Z')
                || (ch >= 'a' && ch <= 'z')
                || (!isInitialCharacter && (ch >= '0' && ch <= '9'))
                || (ch == '_');
        }

        bool IsAlphabet(char ch)
        {
            return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z');
        }

        bool IsDigit(char ch)
        {
            return (ch >= '0' && ch <= '9');
        }
    }
}
