﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using UnityEngine;

namespace BaseLib.Utilities
{
    public class Utility
    {
        public static Texture2D LoadTexture(string file)
        {
            Texture2D tex = new Texture2D(0, 0);
            byte[] data = File.ReadAllBytes(file);

            ImageConversion.LoadImage(tex, data);

            return tex;
        }

        public static Sprite LoadSprite(string file)
        {
            Texture2D tex = LoadTexture(file);

            return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0));
        }
    }
}
