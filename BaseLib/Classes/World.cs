﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

using Random = UnityEngine.Random;

namespace BaseLib.Classes
{
    public class World : WorldBase
    {
        #region Singleton
        public static World Instance { get; private set; }

        void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
            }
        }

        void OnDestroy()
        {
            Instance = null;
        }
        #endregion

        void Start()
        {
            Init();
        }

        public override void ManualUpdate()
        {
            // __WORLD_UPDATE__
        }

        public override void NextTick()
        {
            ++Tick;
            Time = (++Time) % 3600;
        }

        public int Tick;
        public int Time;

        // __WORLD_VARIABLES__

        public void Init()
        {
            Tick = 0;
            Time = 0;

            // __INITIALIZE_VARIABLES__
        }
    }
}
