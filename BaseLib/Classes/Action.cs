﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLib.Classes
{
    public abstract class Action
    {
        protected Citizen _citizen;
        protected Need _need;
        
        public abstract ActionMeta Meta { get; }
        public Citizen Citizen { get { return _citizen; } }
        public Need Need { get { return _need; } }
        public Action Parent { get; protected set; }

        //protected abstract void Init();
        public abstract Action Do();
        public abstract void Release();
    }
}
