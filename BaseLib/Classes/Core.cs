﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLib.Classes
{
    public class Core
    {
        Action _action;

        public Need Need { get; private set; }
        public bool RescheduleFlag { get; private set; }

        public void Schedule(Need need)
        {
            Need = need;
            //_action = null;
            RescheduleFlag = false;
        }

        public void Update()
        {
            
        }
    }
}
