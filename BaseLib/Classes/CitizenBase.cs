﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace BaseLib.Classes
{
    public abstract class CitizenBase : MonoBehaviour
    {
        //public abstract string ActionName { get; }

        public abstract void InitCharacteristics(Dictionary<string, int> nameToIndex, int[] values);
        public abstract void ManualUpdate();
    }
}
