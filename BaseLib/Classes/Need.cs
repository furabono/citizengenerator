﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLib.Classes
{
    public abstract class Need
    {
        protected Citizen _citizen;
        protected ActionMeta[] _actions;
        public Action _action;

        public Citizen Citizen { get { return _citizen; } }
        public bool Activation { get; private set; }
        public float Priority { get; private set; }
        public bool IsIdle { get { return _action == null; } }

        public abstract bool CalcActivation();
        public abstract float CalcPriority();

        public bool UpdateActivation()
        {
            return Activation = CalcActivation();
        }

        public float UpdatePriority()
        {
            return Priority = CalcPriority();
        }

        public void Start()
        {
            ActionMeta maxAction = null;
            float maxEfficiency = float.MinValue;
            float efficiency;
            for (int i = 0; i < _actions.Length; ++i)
            {
                if ((efficiency = _actions[i].GetEfficiency(_citizen)) > maxEfficiency)
                {
                    maxAction = _actions[i];
                    maxEfficiency = efficiency;
                }
            }

            _action = maxAction.Start(this);
        }

        public void Update()
        {
            if (_action == null)
            {
                Start();
            }
            else
            {
                _action = _action.Do();
            }
        }
    }
}
