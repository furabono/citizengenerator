﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace BaseLib.Classes
{
    public abstract class ActionMetaBase
    {
        protected string _name;
        protected Sprite _image;

        public string Name { get { return _name; } }
        public Sprite Image { get { return _image; } }

        public void Init(string streamingAssetsPath)
        {
            var image = streamingAssetsPath + "/Resources/ActionImages/" + _name + ".png";
            if (System.IO.File.Exists(image))
            {
                var tex = Utilities.Utility.LoadTexture(image);
                float pixelsPerUnit = Mathf.Max(tex.width, tex.height) / 1.143f;
                _image = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f), pixelsPerUnit);
            }
        }

        public abstract void ManualUpdate();
    }
}
