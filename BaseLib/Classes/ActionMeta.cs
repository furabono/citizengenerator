﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLib.Classes
{
    public abstract class ActionMeta : ActionMetaBase
    {
        Dictionary<Citizen, float> _efficiencyCache;

        public ActionMeta()
        {
            _efficiencyCache = new Dictionary<Citizen, float>();
        }

        public float GetEfficiency(Citizen citizen)
        {
            float efficiency;
            if (_efficiencyCache.TryGetValue(citizen, out efficiency))
            {
                return efficiency;
            }

            return (_efficiencyCache[citizen] = CalcEfficiency(citizen));
        }

        public void Update()
        {
            _efficiencyCache.Clear();
        }

        public override void ManualUpdate()
        {
            Update();
        }

        protected abstract float CalcEfficiency(Citizen citizen);
        public abstract Action Start(Need need);
    }
}
