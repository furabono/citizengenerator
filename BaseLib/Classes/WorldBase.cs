﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLib.Classes
{
    public abstract class WorldBase : UnityEngine.MonoBehaviour
    {
        public abstract void ManualUpdate();
        public abstract void NextTick();
    }
}
