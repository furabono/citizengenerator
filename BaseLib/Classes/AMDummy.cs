﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Mathf = UnityEngine.Mathf;
using Random = UnityEngine.Random;

namespace BaseLib.Classes
{
    public class AM__NAME__ : ActionMeta
    {
        static AM__NAME__ instance;
        public static AM__NAME__ Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AM__NAME__();
                }
                return instance;
            }
        }

        public AM__NAME__()
        {
            _name = "__NAME__";
        }

        protected override float CalcEfficiency(Citizen citizen)
        {
            // __EFFICIENCY__
        }

        public override Action Start(Need need)
        {
            return A__NAME__.Start(need);
        }
    }
}
