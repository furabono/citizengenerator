﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace PolicyTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            policyNewButton.Click += PolicyNewButton_Click;
            policyLoadButton.Click += PolicyLoadButton_Click;
            policySaveButton.Click += PolicySaveButton_Click;

            loadButton.Click += LoadButton_Click;
            addButton.Click += AddButton_Click;
            executeButton.Click += ExecuteButton_Click;

            checkedListBox.ItemCheck += CheckedListBox_ItemCheck;

            Title title = new Title();
            title.Name = "Sample";
            title.Docking = Docking.Bottom;
            chartViewer.Chart.Titles.Add(title);

            progressBar.Visible = false;

            pfmf = new GUI.PolicyFileManageForm();

            policyName = "Untitled";

            //Shared.Debug.Debug.UseDebug = true;
            //Shared.Debug.Debug.ShowDebugWindow();
        }

        private void PolicyNewButton_Click(object sender, EventArgs e)
        {
            //policy = null;
            policyName = "Untitled";
            variableFlowLayoutPanel.Controls.Clear();
            variableFlowLayoutPanel.Controls.Add(addButton);
            m1TextBox.Text = "";
            m2TextBox.Text = "";
            m3TextBox.Text = "";
            resultTextBox.Text = "";
        }

        private void PolicyLoadButton_Click(object sender, EventArgs e)
        {
            if (pfmf.ShowDialog(GUI.PolicyFileManageForm.Mode.Load) == DialogResult.OK)
            {
                var policy = Shared.IO.JsonReader.ReadPolicyData(pfmf.FullPath);

                policyName = policy.Name;

                variableFlowLayoutPanel.Controls.Clear();
                foreach (var v in policy.Variables)
                {
                    variableFlowLayoutPanel.Controls.Add(new GUI.PolicyVariable(v.Key, v.Value));
                }
                variableFlowLayoutPanel.Controls.Add(addButton);

                m1TextBox.Text = policy.Memory1;
                m2TextBox.Text = policy.Memory2;
                m3TextBox.Text = policy.Memory3;
                resultTextBox.Text = policy.Result;
            }
        }

        private void PolicySaveButton_Click(object sender, EventArgs e)
        {
            if (pfmf.ShowDialog(GUI.PolicyFileManageForm.Mode.Save, policyName) == DialogResult.OK)
            {
                policyName = pfmf.FileName;
                var variables = (from Control control in variableFlowLayoutPanel.Controls
                                 where control is GUI.PolicyVariable
                                 let pv = (GUI.PolicyVariable)control
                                 select new KeyValuePair<string, float>(pv.GetName(), pv.GetValue())).ToArray();
                var policy = new Shared.Class.Policy(policyName, variables, m1TextBox.Text, m2TextBox.Text, m3TextBox.Text, resultTextBox.Text);
                Shared.IO.JsonWriter.WritePolicyData(pfmf.FullPath, policy);
            }
        }

        private async void LoadButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = Application.StartupPath;
            ofd.Filter = "Json 파일 (*.json)|*.json";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                SetButtonsEnable(false, true);

                civil = await Shared.IO.JsonReader.ReadCivilDataAsync(ofd.FileName);

                dataGridView.Visible = false;

                dataGridView.DataSource = null;
                display = MakeDataTable(civil);
                display.Columns.Add("RESULT", typeof(float));
                display.Columns.Add("M1", typeof(float));
                display.Columns.Add("M2", typeof(float));
                display.Columns.Add("M3", typeof(float));
                dataGridView.DataSource = display;
                dataGridView.Columns["RESULT"].DisplayIndex = 0;
                dataGridView.Columns["M1"].DisplayIndex = 1;
                dataGridView.Columns["M2"].DisplayIndex = 2;
                dataGridView.Columns["M3"].DisplayIndex = 3;

                dataGridView.Visible = true;

                checkedListBox.Items.Clear();
                for (int i = 0; i < dataGridView.Columns.Count; ++i)
                {
                    if (dataGridView.Columns[i].Name != "RESULT")
                    {
                        checkedListBox.Items.Add(dataGridView.Columns[i].Name, true);
                    }
                }

                ChartCitizen();

                SetButtonsEnable(true, false);
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            variableFlowLayoutPanel.Controls.Remove((Button)sender);
            string name = string.Format("Var{0}", variableFlowLayoutPanel.Controls.Count);
            GUI.PolicyVariable pv = new GUI.PolicyVariable(name);
            pv.RemoveButtonClick += RemoveButton_Click;
            variableFlowLayoutPanel.Controls.Add(pv);
            variableFlowLayoutPanel.Controls.Add((Button)sender);
            pv.EditMode();
        }

        private async void ExecuteButton_Click(object sender, EventArgs e)
        {
            if (civil == null) { return; }
            if (resultTextBox.Text.Length == 0) { return; }

            SetButtonsEnable(false, true);

            KeyValuePair<string, float>[] variables = (from Control c in variableFlowLayoutPanel.Controls
                                                       where c is GUI.PolicyVariable
                                                       let pv = (GUI.PolicyVariable)c
                                                       select new KeyValuePair<string, float>(pv.GetName(), pv.GetValue())).ToArray();
            var policy = new Shared.Class.Policy("", variables, m1TextBox.Text, m2TextBox.Text, m3TextBox.Text, resultTextBox.Text);

            try
            {
                var result = await Utility.Simulator.SimulateParallelAsync(civil, policy);

                dataGridView.Visible = false;
                dataGridView.DataSource = null;
                FillDataTable(display, civil, result);
                Shared.Debug.Debug.PrintLn(DateTime.Now.ToLongTimeString());

                Shared.Debug.Debug.PrintLn(DateTime.Now.ToLongTimeString());
                dataGridView.DataSource = display;
                dataGridView.Columns["RESULT"].DisplayIndex = 0;
                dataGridView.Columns["M1"].DisplayIndex = 1;
                dataGridView.Columns["M2"].DisplayIndex = 2;
                dataGridView.Columns["M3"].DisplayIndex = 3;
                Shared.Debug.Debug.PrintLn(DateTime.Now.ToLongTimeString());
                dataGridView.Visible = true;

                DrawChart(result);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                SetButtonsEnable(true, false);
            }
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            variableFlowLayoutPanel.Controls.Remove(((Button)sender).Parent);
        }

        private void CheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            CheckedListBox clb = (CheckedListBox)sender;

            dataGridView.Columns[(string)clb.Items[e.Index]].Visible = (e.NewValue == CheckState.Checked);
        }

        private void SetButtonsEnable(bool enable, bool progress)
        {
            //executeButton.Enabled = enable;
            //loadButton.Enabled = enable;
            //policyLoadButton.Enabled = enable;
            //policySaveButton.Enabled = enable;
            tableLayoutPanel.Enabled = enable;
            progressBar.Visible = progress;
        }

        private void FillDataTable(DataTable display, Shared.Citizen.Civil civil, Utility.SimulationResult simulationResult)
        {
            //display.Rows.Clear();

            DataColumn M1 = display.Columns["M1"];
            DataColumn M2 = display.Columns["M2"];
            DataColumn M3 = display.Columns["M3"];
            DataColumn result = display.Columns["RESULT"];

            var rows = display.Select();

            if (simulationResult.M1 != null)
            {
                for (int i = 0; i < display.Rows.Count; ++i)
                {
                    rows[i][M1] = simulationResult.M1[i];
                }
            }
            if (simulationResult.M2 != null)
            {
                for (int i = 0; i < display.Rows.Count; ++i)
                {
                    rows[i][M2] = simulationResult.M2[i];
                }
            }
            if (simulationResult.M3 != null)
            {
                for (int i = 0; i < display.Rows.Count; ++i)
                {
                    rows[i][M3] = simulationResult.M3[i];
                }
            }

            for (int i = 0; i < display.Rows.Count; ++i)
            {
                rows[i][result] = simulationResult.Result[i];
            }
        }

        private void DrawChart(Utility.SimulationResult simulationResult)
        {
            chartViewer.Chart.Titles[0].Text = string.Format("Sample {0} of {1}", Math.Min(civil.Count, 1000), civil.Count);

            {
                string page = "RESULT-QUANTITATIVES";

                float min = simulationResult.Result.Min();
                float max = simulationResult.Result.Max();
                int unit = (int)Math.Pow(10, (int)Math.Log10(max - min));
                max = ((int)(max / unit) + 1) * unit;
                min = ((int)(min / unit) - 1) * unit;

                var sw = System.Diagnostics.Stopwatch.StartNew();
                var serieses = (from p in civil.Parameter.AsParallel()
                                                 where p.Type == Shared.Citizen.ParameterType.Quantitative
                                                 let qp = (Shared.Citizen.QuantitativeParameterInfo)p
                                                 select new GUI.SeriesData(MakeSeries(civil, p.Name, simulationResult.Result, "RESULT"), qp.Min, qp.Max, min, max)).ToList();
                
                Shared.Debug.Debug.PrintLn(sw.Elapsed.TotalSeconds);

                serieses.Sort((l, r) => { return l.series.Name.CompareTo(r.series.Name); });

                sw.Restart();
                bool[] check = new bool[serieses.Count];
                if (chartViewer.SeriesCount > 0)
                {
                    for (int i = 0; i < check.Length; ++i)
                    {
                        check[i] = chartViewer.GetSeriesEnabled(i, page);
                    }
                }
                else
                {
                    for (int i = 0; i < check.Length; ++i)
                    {
                        check[i] = true;
                    }
                }

                chartViewer.RemoveAllSeriesData();

                for (int i = 0; i < serieses.Count; ++i)
                {
                    chartViewer.AddSeriesData(page, serieses[i], check[i]);
                }

                Shared.Debug.Debug.PrintLn(sw.Elapsed.TotalSeconds);
            }

            {
                string page = "RESULT-QUALITATIVES";

                var serieses = (from p in civil.Parameter.AsParallel()
                                where p.Type == Shared.Citizen.ParameterType.Qualitative
                                let qp = (Shared.Citizen.QualitativeParameterInfo)p
                                select new GUI.SeriesData(MakeSeries(civil, p.Name, simulationResult.Result, "RESULT"))).ToList();

                for (int i = 0; i < serieses.Count; ++i)
                {
                    chartViewer.AddSeriesData(page, serieses[i]);
                }
            }
        }

        private Series MakeSeries(Shared.Citizen.Civil civil, string xAxisName, float[] yAxis, string yAxisName)
        {
            var series = MakeSeries(civil, civil.Parameter.GetIndex(xAxisName), yAxis);
            series.Name = yAxisName + "-" + xAxisName;
            return series;
        }

        private Series MakeSeries(Shared.Citizen.Civil civil, string xAxisName, float[] yAxis)
        {
            return MakeSeries(civil, civil.Parameter.GetIndex(xAxisName), yAxis);
        }

        private Series MakeSeries(Shared.Citizen.Civil civil, int xAxisIndex, float[] yAxis)
        {
            Series series = new Series();

            var citizen = civil.ToArray();

            int[] indicies = new int[civil.Count];
            for (int i = 0; i < indicies.Length; ++i)
            {
                indicies[i] = i;
            }
            ShuffleArray(indicies);

            if (civil.Parameter[xAxisIndex].Type == Shared.Citizen.ParameterType.Quantitative)
            {
                series.ChartType = SeriesChartType.FastPoint;

                //HashSet<KeyValuePair<object, object>> set = new HashSet<KeyValuePair<object, object>>();
                //HashSet<DataPoint> set = new HashSet<DataPoint>();

                //foreach (DataRow row in table.Rows)
                //{
                //series.Points.AddXY(row[xAxisColumn], row[yAxisColumn]);
                //set.Add(new KeyValuePair<object, object>(row[xAxisColumn], row[yAxisColumn]));
                //set.Add(new DataPoint((double)row[xAxisColumn], (double)row[yAxisColumn]));
                //}

                //foreach (var dp in set)
                //{
                //series.Points.AddXY(dp.Key, dp.Value);
                //}

                for (int i = 0; i < Math.Min(civil.Count, 1000); ++i)
                {
                    series.Points.AddXY(citizen[indicies[i]].GetRaw(xAxisIndex), yAxis[indicies[i]]);
                }
            }
            else
            {
                series.ChartType = SeriesChartType.Column;

                var param = (Shared.Citizen.QualitativeParameterInfo)civil.Parameter[xAxisIndex];
                float[] avg = new float[param.Categories.Length];
                int sample = Math.Min(civil.Count, 1000);

                for (int i = 0; i < sample; ++i)
                {
                    avg[citizen[indicies[i]].GetRaw(xAxisIndex)] += yAxis[indicies[i]];
                }
                
                for (int i = 0; i < param.Categories.Length; ++i)
                {
                    avg[i] /= sample;
                    //series.Points.AddXY(param.Categories[i], avg[i]);
                    var dp = new DataPoint(i + 1, avg[i]);
                    dp.Label = param.Categories[i];
                    series.Points.Add(dp);
                }
            }

            return series;
        }

        private void ChartCitizen()
        {
            distChartViewer.RemoveAllSeriesData();

            var citizen = civil.ToArray();

            for (int i = 0; i < civil.Parameter.Count; ++i)
            {
                Series series = new Series(civil.Parameter[i].Name);
                series.Color = Color.Red;

                if (civil.Parameter[i].Type == Shared.Citizen.ParameterType.Quantitative)
                {
                    series.ChartType = SeriesChartType.Line;

                    var parameter = (Shared.Citizen.QuantitativeParameterInfo)civil.Parameter[i];
                    int min = parameter.Min;
                    int max = parameter.Max;

                    int[] cnt = new int[max - min + 1];

                    for (int j = 0; j < citizen.Length; ++j)
                    { 
                        ++cnt[citizen[j].GetRaw(i) - min];
                    }

                    for (int j = 0; j <= max - min; ++j)
                    {
                        series.Points.Add(new DataPoint(min + j, cnt[j]));
                    }

                    distChartViewer.AddSeriesData("DISTRIBUTIONS", new GUI.SeriesData(series, min, max, 0, null), false);
                }
                else
                {
                    series.ChartType = SeriesChartType.Pie;
                    series.IsValueShownAsLabel = true;

                    var parameter = (Shared.Citizen.QualitativeParameterInfo)civil.Parameter[i];

                    int count;

                    for (int j = 0; j < parameter.Categories.Length; ++j)
                    {
                        count = (from c in citizen.AsParallel()
                                 where c.GetRaw(i) == j
                                 select c).Count();

                        DataPoint dp = new DataPoint();
                        if (count == 0) { dp.LabelForeColor = Color.Transparent; }
                        dp.SetValueXY(string.Format("{0} ({1})", parameter.Categories[j], count), count);
                        series.Points.Add(dp);
                    }

                    distChartViewer.AddSeriesData("DISTRIBUTIONS", new GUI.SeriesData(series), false);
                }
            }

            distChartViewer.SetSeriesEnabled(0, true);
        }

        DataTable MakeDataTable(Shared.Citizen.Civil civil)
        {
            DataTable dt = new DataTable();
            foreach (var pi in civil.Parameter)
            {
                dt.Columns.Add(pi.Name, ((pi.Type == Shared.Citizen.ParameterType.Quantitative) ? (typeof(int)) : (typeof(string))));
            }

            DataRow row;
            foreach (var c in civil)
            {
                row = dt.NewRow();
                for (int i = 0; i < civil.Parameter.Count; ++i)
                {
                    row[i] = c[i];
                }
                dt.Rows.Add(row);
            }

            return dt;
        }

        static void ShuffleArray<T>(params T[] array)
        {
            Random random = new Random();
            //MersenneTwister random = MersenneTwister.Default;
            int exchangeIndex;
            T temp;

            for (int i = 0; i < array.Length - 1; ++i)
            {
                exchangeIndex = random.Next(i, array.Length);
                temp = array[i];
                array[i] = array[exchangeIndex];
                array[exchangeIndex] = temp;
            }

            //return array;
        }

        Shared.Citizen.Civil civil;
        DataTable display;
        GUI.PolicyFileManageForm pfmf;
        //Shared.Class.Policy policy;
        string policyName;
    }
}
