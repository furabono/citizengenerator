﻿namespace PolicyTester
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.variableGroupBox = new System.Windows.Forms.GroupBox();
            this.variableFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.addButton = new System.Windows.Forms.Button();
            this.scriptGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.m1TextBox = new System.Windows.Forms.TextBox();
            this.m1Label = new System.Windows.Forms.Label();
            this.m2Label = new System.Windows.Forms.Label();
            this.m3Label = new System.Windows.Forms.Label();
            this.resultLabel = new System.Windows.Forms.Label();
            this.m2TextBox = new System.Windows.Forms.TextBox();
            this.m3TextBox = new System.Windows.Forms.TextBox();
            this.resultTextBox = new System.Windows.Forms.TextBox();
            this.buttonPanel = new System.Windows.Forms.Panel();
            this.loadButton = new System.Windows.Forms.Button();
            this.executeButton = new System.Windows.Forms.Button();
            this.policyMenuFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.policySaveButton = new System.Windows.Forms.Button();
            this.policyLoadButton = new System.Windows.Forms.Button();
            this.policyNewButton = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.rawTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView = new PolicyTester.GUI.DoubleBufferedDataGridView();
            this.checkedListBox = new System.Windows.Forms.CheckedListBox();
            this.chartTabPage = new System.Windows.Forms.TabPage();
            this.chartViewer = new PolicyTester.GUI.ChartViewer();
            this.distChartPage = new System.Windows.Forms.TabPage();
            this.distChartViewer = new PolicyTester.GUI.ChartViewer();
            this.tableLayoutPanel.SuspendLayout();
            this.variableGroupBox.SuspendLayout();
            this.variableFlowLayoutPanel.SuspendLayout();
            this.scriptGroupBox.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.buttonPanel.SuspendLayout();
            this.policyMenuFlowLayoutPanel.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.rawTabPage.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.chartTabPage.SuspendLayout();
            this.distChartPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.progressBar, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.variableGroupBox, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.scriptGroupBox, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.buttonPanel, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.policyMenuFlowLayoutPanel, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.tabControl, 0, 4);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 6;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(865, 531);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // progressBar
            // 
            this.progressBar.Dock = System.Windows.Forms.DockStyle.Right;
            this.progressBar.Location = new System.Drawing.Point(812, 513);
            this.progressBar.MarqueeAnimationSpeed = 50;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(50, 15);
            this.progressBar.Step = 1;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 2;
            // 
            // variableGroupBox
            // 
            this.variableGroupBox.Controls.Add(this.variableFlowLayoutPanel);
            this.variableGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.variableGroupBox.Location = new System.Drawing.Point(3, 3);
            this.variableGroupBox.Name = "variableGroupBox";
            this.variableGroupBox.Size = new System.Drawing.Size(859, 91);
            this.variableGroupBox.TabIndex = 0;
            this.variableGroupBox.TabStop = false;
            this.variableGroupBox.Text = "Variables";
            // 
            // variableFlowLayoutPanel
            // 
            this.variableFlowLayoutPanel.AutoScroll = true;
            this.variableFlowLayoutPanel.Controls.Add(this.addButton);
            this.variableFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.variableFlowLayoutPanel.Location = new System.Drawing.Point(3, 17);
            this.variableFlowLayoutPanel.Name = "variableFlowLayoutPanel";
            this.variableFlowLayoutPanel.Size = new System.Drawing.Size(853, 71);
            this.variableFlowLayoutPanel.TabIndex = 0;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(3, 3);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(133, 52);
            this.addButton.TabIndex = 0;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            // 
            // scriptGroupBox
            // 
            this.scriptGroupBox.Controls.Add(this.tableLayoutPanel3);
            this.scriptGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scriptGroupBox.Location = new System.Drawing.Point(3, 130);
            this.scriptGroupBox.Name = "scriptGroupBox";
            this.scriptGroupBox.Size = new System.Drawing.Size(859, 119);
            this.scriptGroupBox.TabIndex = 1;
            this.scriptGroupBox.TabStop = false;
            this.scriptGroupBox.Text = "Script";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.m1TextBox, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.m1Label, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.m2Label, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.m3Label, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.resultLabel, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.m2TextBox, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.m3TextBox, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.resultTextBox, 1, 3);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 17);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(853, 99);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // m1TextBox
            // 
            this.m1TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m1TextBox.Location = new System.Drawing.Point(63, 3);
            this.m1TextBox.Name = "m1TextBox";
            this.m1TextBox.Size = new System.Drawing.Size(787, 21);
            this.m1TextBox.TabIndex = 1;
            // 
            // m1Label
            // 
            this.m1Label.AutoSize = true;
            this.m1Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m1Label.Location = new System.Drawing.Point(3, 3);
            this.m1Label.Margin = new System.Windows.Forms.Padding(3);
            this.m1Label.Name = "m1Label";
            this.m1Label.Size = new System.Drawing.Size(54, 19);
            this.m1Label.TabIndex = 0;
            this.m1Label.Text = "M1";
            this.m1Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // m2Label
            // 
            this.m2Label.AutoSize = true;
            this.m2Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m2Label.Location = new System.Drawing.Point(3, 25);
            this.m2Label.Name = "m2Label";
            this.m2Label.Size = new System.Drawing.Size(54, 25);
            this.m2Label.TabIndex = 2;
            this.m2Label.Text = "M2";
            this.m2Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // m3Label
            // 
            this.m3Label.AutoSize = true;
            this.m3Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m3Label.Location = new System.Drawing.Point(3, 50);
            this.m3Label.Name = "m3Label";
            this.m3Label.Size = new System.Drawing.Size(54, 25);
            this.m3Label.TabIndex = 3;
            this.m3Label.Text = "M3";
            this.m3Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultLabel.Location = new System.Drawing.Point(3, 75);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(54, 25);
            this.resultLabel.TabIndex = 4;
            this.resultLabel.Text = "Result";
            this.resultLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // m2TextBox
            // 
            this.m2TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m2TextBox.Location = new System.Drawing.Point(63, 28);
            this.m2TextBox.Name = "m2TextBox";
            this.m2TextBox.Size = new System.Drawing.Size(787, 21);
            this.m2TextBox.TabIndex = 5;
            // 
            // m3TextBox
            // 
            this.m3TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m3TextBox.Location = new System.Drawing.Point(63, 53);
            this.m3TextBox.Name = "m3TextBox";
            this.m3TextBox.Size = new System.Drawing.Size(787, 21);
            this.m3TextBox.TabIndex = 6;
            // 
            // resultTextBox
            // 
            this.resultTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultTextBox.Location = new System.Drawing.Point(63, 78);
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.Size = new System.Drawing.Size(787, 21);
            this.resultTextBox.TabIndex = 7;
            // 
            // buttonPanel
            // 
            this.buttonPanel.Controls.Add(this.loadButton);
            this.buttonPanel.Controls.Add(this.executeButton);
            this.buttonPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPanel.Location = new System.Drawing.Point(3, 255);
            this.buttonPanel.Name = "buttonPanel";
            this.buttonPanel.Size = new System.Drawing.Size(859, 24);
            this.buttonPanel.TabIndex = 3;
            // 
            // loadButton
            // 
            this.loadButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.loadButton.Location = new System.Drawing.Point(0, 0);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(75, 24);
            this.loadButton.TabIndex = 1;
            this.loadButton.Text = "Load Json";
            this.loadButton.UseVisualStyleBackColor = true;
            // 
            // executeButton
            // 
            this.executeButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.executeButton.Location = new System.Drawing.Point(784, 0);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(75, 24);
            this.executeButton.TabIndex = 0;
            this.executeButton.Text = "Execute";
            this.executeButton.UseVisualStyleBackColor = true;
            // 
            // policyMenuFlowLayoutPanel
            // 
            this.policyMenuFlowLayoutPanel.Controls.Add(this.policySaveButton);
            this.policyMenuFlowLayoutPanel.Controls.Add(this.policyLoadButton);
            this.policyMenuFlowLayoutPanel.Controls.Add(this.policyNewButton);
            this.policyMenuFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.policyMenuFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.policyMenuFlowLayoutPanel.Location = new System.Drawing.Point(3, 100);
            this.policyMenuFlowLayoutPanel.Name = "policyMenuFlowLayoutPanel";
            this.policyMenuFlowLayoutPanel.Size = new System.Drawing.Size(859, 24);
            this.policyMenuFlowLayoutPanel.TabIndex = 4;
            // 
            // policySaveButton
            // 
            this.policySaveButton.Location = new System.Drawing.Point(779, 0);
            this.policySaveButton.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.policySaveButton.Name = "policySaveButton";
            this.policySaveButton.Size = new System.Drawing.Size(75, 23);
            this.policySaveButton.TabIndex = 0;
            this.policySaveButton.Text = "Save";
            this.policySaveButton.UseVisualStyleBackColor = true;
            // 
            // policyLoadButton
            // 
            this.policyLoadButton.Location = new System.Drawing.Point(699, 0);
            this.policyLoadButton.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.policyLoadButton.Name = "policyLoadButton";
            this.policyLoadButton.Size = new System.Drawing.Size(75, 23);
            this.policyLoadButton.TabIndex = 1;
            this.policyLoadButton.Text = "Load";
            this.policyLoadButton.UseVisualStyleBackColor = true;
            // 
            // policyNewButton
            // 
            this.policyNewButton.Location = new System.Drawing.Point(574, 0);
            this.policyNewButton.Margin = new System.Windows.Forms.Padding(0, 0, 50, 0);
            this.policyNewButton.Name = "policyNewButton";
            this.policyNewButton.Size = new System.Drawing.Size(75, 23);
            this.policyNewButton.TabIndex = 2;
            this.policyNewButton.Text = "New";
            this.policyNewButton.UseVisualStyleBackColor = true;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.rawTabPage);
            this.tabControl.Controls.Add(this.chartTabPage);
            this.tabControl.Controls.Add(this.distChartPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(3, 285);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(859, 222);
            this.tabControl.TabIndex = 5;
            // 
            // rawTabPage
            // 
            this.rawTabPage.Controls.Add(this.tableLayoutPanel1);
            this.rawTabPage.Location = new System.Drawing.Point(4, 22);
            this.rawTabPage.Name = "rawTabPage";
            this.rawTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.rawTabPage.Size = new System.Drawing.Size(851, 196);
            this.rawTabPage.TabIndex = 0;
            this.rawTabPage.Text = "Raw";
            this.rawTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.dataGridView, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkedListBox, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(845, 190);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(3, 3);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowTemplate.Height = 23;
            this.dataGridView.Size = new System.Drawing.Size(639, 184);
            this.dataGridView.TabIndex = 0;
            // 
            // checkedListBox
            // 
            this.checkedListBox.CheckOnClick = true;
            this.checkedListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBox.FormattingEnabled = true;
            this.checkedListBox.Location = new System.Drawing.Point(648, 3);
            this.checkedListBox.Name = "checkedListBox";
            this.checkedListBox.Size = new System.Drawing.Size(194, 184);
            this.checkedListBox.TabIndex = 1;
            // 
            // chartTabPage
            // 
            this.chartTabPage.Controls.Add(this.chartViewer);
            this.chartTabPage.Location = new System.Drawing.Point(4, 22);
            this.chartTabPage.Name = "chartTabPage";
            this.chartTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.chartTabPage.Size = new System.Drawing.Size(851, 196);
            this.chartTabPage.TabIndex = 1;
            this.chartTabPage.Text = "Chart";
            this.chartTabPage.UseVisualStyleBackColor = true;
            // 
            // chartViewer
            // 
            this.chartViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartViewer.Location = new System.Drawing.Point(3, 3);
            this.chartViewer.MultiSeriesVisible = true;
            this.chartViewer.Name = "chartViewer";
            this.chartViewer.Size = new System.Drawing.Size(845, 190);
            this.chartViewer.TabIndex = 0;
            // 
            // distChartPage
            // 
            this.distChartPage.Controls.Add(this.distChartViewer);
            this.distChartPage.Location = new System.Drawing.Point(4, 22);
            this.distChartPage.Name = "distChartPage";
            this.distChartPage.Size = new System.Drawing.Size(851, 196);
            this.distChartPage.TabIndex = 2;
            this.distChartPage.Text = "Distribution";
            this.distChartPage.UseVisualStyleBackColor = true;
            // 
            // distChartViewer
            // 
            this.distChartViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.distChartViewer.Location = new System.Drawing.Point(0, 0);
            this.distChartViewer.MultiSeriesVisible = false;
            this.distChartViewer.Name = "distChartViewer";
            this.distChartViewer.Size = new System.Drawing.Size(851, 196);
            this.distChartViewer.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 531);
            this.Controls.Add(this.tableLayoutPanel);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.tableLayoutPanel.ResumeLayout(false);
            this.variableGroupBox.ResumeLayout(false);
            this.variableFlowLayoutPanel.ResumeLayout(false);
            this.scriptGroupBox.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.buttonPanel.ResumeLayout(false);
            this.policyMenuFlowLayoutPanel.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.rawTabPage.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.chartTabPage.ResumeLayout(false);
            this.distChartPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.GroupBox variableGroupBox;
        private System.Windows.Forms.GroupBox scriptGroupBox;
        private System.Windows.Forms.FlowLayoutPanel variableFlowLayoutPanel;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Panel buttonPanel;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.Button executeButton;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.FlowLayoutPanel policyMenuFlowLayoutPanel;
        private System.Windows.Forms.Button policySaveButton;
        private System.Windows.Forms.Button policyLoadButton;
        private System.Windows.Forms.Button policyNewButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private GUI.DoubleBufferedDataGridView dataGridView;
        private System.Windows.Forms.CheckedListBox checkedListBox;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage rawTabPage;
        private System.Windows.Forms.TabPage chartTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox m1TextBox;
        private System.Windows.Forms.Label m1Label;
        private System.Windows.Forms.Label m2Label;
        private System.Windows.Forms.Label m3Label;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.TextBox m2TextBox;
        private System.Windows.Forms.TextBox m3TextBox;
        private System.Windows.Forms.TextBox resultTextBox;
        private GUI.ChartViewer chartViewer;
        private System.Windows.Forms.TabPage distChartPage;
        private GUI.ChartViewer distChartViewer;
    }
}

