﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace PolicyTester.Utility
{
    class Simulator
    {
        //public static float[] Simulate(Shared.Citizen.Civil civil, KeyValuePair<string, float>[] variables, string script)
        //{
        //    var engine = IronPython.Hosting.Python.CreateEngine();
        //    var scope = engine.CreateScope();

        //    scope.SetVariable("citizen", civil.Select());

        //    IronPython.Runtime.List var = new IronPython.Runtime.List();
        //    foreach (KeyValuePair<string, float> v in variables)
        //    {
        //        script = script.Replace(v.Key, v.Key.Replace(' ', '_'));
        //        scope.SetVariable(v.Key.Replace(' ', '_'), v.Value);
        //        var.append(v.Value);
        //    }
        //    scope.SetVariable("var", var);

        //    string pre_script = "";
        //    pre_script += "from System import Single" + Environment.NewLine;
        //    pre_script += "from System import Array" + Environment.NewLine;
        //    pre_script += Environment.NewLine;
        //    pre_script += "def Compute(i):" + Environment.NewLine;
        //    pre_script += "\tparam = citizen[i]" + Environment.NewLine;
        //    for (int i = 0; i < civil.Columns.Count; ++i)
        //    {
        //        script = script.Replace(civil.Columns[i].ColumnName, civil.Columns[i].ColumnName.Replace(' ', '_'));
        //        pre_script += string.Format("\t{0} = param[{1}]", civil.Columns[i].ColumnName.Replace(' ', '_'), i) + Environment.NewLine;
        //    }
        //    foreach (string line in script.Split(new string[] { Environment.NewLine }, StringSplitOptions.None))
        //    {
        //        pre_script += "\t" + line + Environment.NewLine;
        //    }
        //    pre_script += "\treturn result" + Environment.NewLine;
        //    pre_script += Environment.NewLine;
        //    pre_script += "results = Array.CreateInstance(Single, citizen.Length)" + Environment.NewLine;
        //    pre_script += "for i in range(0, citizen.Length):" + Environment.NewLine;
        //    pre_script += "\tresults[i] = Compute(i)" + Environment.NewLine;

        //    try
        //    {
        //        var scriptSource = engine.CreateScriptSourceFromString(pre_script);
        //        scriptSource.Execute(scope);
        //    }
        //    catch (Microsoft.Scripting.SyntaxErrorException)
        //    {

        //    }

        //    return scope.GetVariable<float[]>("results");
        //}

        //public static Task<float[]> SimulateAsync(DataTable citizen, KeyValuePair<string, float>[] variables, string script)
        //{
        //    return Task.Run(() => { return Simulate(citizen, variables, script); });
        //}

        public static SimulationResult SimulateParallel(Shared.Citizen.Civil civil, Shared.Class.Policy policy)
        {
            SimulationData sd = new SimulationData(civil, policy);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("from math import *");
            sb.AppendLine("from System import Single");
            sb.AppendLine("from System import Array");
            sb.AppendLine("from System import Action");
            sb.AppendLine("from System.Threading.Tasks import *");
            sb.AppendLine();

            if (sd.Memory1.Length > 0)
            {
                sb.AppendLine("M1 = Array.CreateInstance(Single, civil.Length)");
            }
            if (sd.Memory2.Length > 0)
            {
                sb.AppendLine("M2 = Array.CreateInstance(Single, civil.Length)");
            }
            if (sd.Memory3.Length > 0)
            {
                sb.AppendLine("M3 = Array.CreateInstance(Single, civil.Length)");
            }
            sb.AppendLine("result = Array.CreateInstance(Single, civil.Length)");
            sb.AppendLine();

            sb.AppendLine("def Compute(i):");
            sb.AppendLine("\t" + "param = civil[i]");
            for (int i = 0; i < sd.ParamName.Length; ++i)
            {
                sb.AppendFormat("\t{0} = param[{1}]", sd.ParamName[i], i);
                sb.AppendLine();
            }
            if (sd.Memory1.Length > 0)
            {
                sb.AppendLine("\t" + "M1[i] = " + sd.Memory1);
            }
            if (sd.Memory2.Length > 0)
            {
                sb.AppendLine("\t" + "M2[i] = " + sd.Memory2);
            }
            if (sd.Memory3.Length > 0)
            {
                sb.AppendLine("\t" + "M3[i] = " + sd.Memory3);
            }
            sb.AppendLine("\t" + "result[i] = " + sd.Result);
            sb.AppendLine();
            sb.AppendLine("Parallel.For(0, civil.Length, Action[int](Compute))");

            try
            {
                var scriptSource = sd.Engine.CreateScriptSourceFromString(sb.ToString());
                scriptSource.Execute(sd.Scope);
            }
            catch (Microsoft.Scripting.SyntaxErrorException e)
            {
                throw new Exception(e.Message);
            }

            {
                float[] M1 = null;
                float[] M2 = null;
                float[] M3 = null;

                if (sd.Memory1.Length > 0)
                {
                    M1 = sd.Scope.GetVariable<float[]>("M1");
                }
                if (sd.Memory2.Length > 0)
                {
                    M2 = sd.Scope.GetVariable<float[]>("M2");
                }
                if (sd.Memory3.Length > 0)
                {
                    M3 = sd.Scope.GetVariable<float[]>("M3");
                }
                var result = sd.Scope.GetVariable<float[]>("result");

                return new SimulationResult(M1, M2, M3, result);
            }
        }

        public static Task<SimulationResult> SimulateParallelAsync(Shared.Citizen.Civil civil, Shared.Class.Policy policy)
        {
            return Task.Run(() => { return SimulateParallel(civil, policy); });
        }
    }

    class SimulationData
    {
        public SimulationData(Shared.Citizen.Civil civil, Shared.Class.Policy policy)
        {
            {
                paramName = new string[civil.Parameter.Count];
                for (int i = 0; i < paramName.Length; ++i)
                {
                    paramName[i] = civil.Parameter[i].Name.Replace(' ', '_');
                }

                var = new KeyValuePair<string, float>[policy.Variables.Length];
                for (int i = 0; i < var.Length; ++i)
                {
                    var[i] = new KeyValuePair<string, float>(policy.Variables[i].Key.Replace(' ', '_'), policy.Variables[i].Value);
                }
            }

            {
                engine = IronPython.Hosting.Python.CreateEngine();
                scope = engine.CreateScope();

                scope.SetVariable("civil", civil.ToArray());

                IronPython.Runtime.List var = new IronPython.Runtime.List();
                foreach (KeyValuePair<string, float> v in this.var)
                {
                    scope.SetVariable(v.Key, v.Value);
                    var.append(v.Value);
                }
                scope.SetVariable("var", var);
            }

            {
                memory1 = policy.Memory1;
                memory2 = policy.Memory2;
                memory3 = policy.Memory3;
                result = policy.Result;

                for (int i = 0; i < paramName.Length; ++i)
                {
                    memory1 = memory1.Replace(civil.Parameter[i].Name, paramName[i]);
                    memory2 = memory2.Replace(civil.Parameter[i].Name, paramName[i]);
                    memory3 = memory3.Replace(civil.Parameter[i].Name, paramName[i]);
                    result = result.Replace(civil.Parameter[i].Name, paramName[i]);
                }

                for (int i = 0; i < var.Length; ++i)
                {
                    memory1 = memory1.Replace(policy.Variables[i].Key, var[i].Key);
                    memory2 = memory2.Replace(policy.Variables[i].Key, var[i].Key);
                    memory3 = memory3.Replace(policy.Variables[i].Key, var[i].Key);
                    result = result.Replace(policy.Variables[i].Key, var[i].Key);
                }
            }
        }

        public string[] ParamName
        {
            get { return paramName; }
        }

        public KeyValuePair<string, float>[] Var
        {
            get { return var; }
        }

        public Microsoft.Scripting.Hosting.ScriptEngine Engine
        {
            get { return engine; }
        }

        public Microsoft.Scripting.Hosting.ScriptScope Scope
        {
            get { return scope; }
        }

        public string Memory1
        {
            get { return memory1; }
        }

        public string Memory2
        {
            get { return memory2; }
        }

        public string Memory3
        {
            get { return memory3; }
        }

        public string Result
        {
            get { return result; }
        }

        string[] paramName;
        KeyValuePair<string, float>[] var;

        Microsoft.Scripting.Hosting.ScriptEngine engine;
        Microsoft.Scripting.Hosting.ScriptScope scope;

        string memory1;
        string memory2;
        string memory3;
        string result;
    }

    public class SimulationResult
    {
        public SimulationResult(float[] memory1, float[] memory2, float[] memory3, float[] result)
        {
            this.memory1 = memory1;
            this.memory2 = memory2;
            this.memory3 = memory3;
            this.result = result;
        }

        public float[] M1
        {
            get { return memory1; }
        }

        public float[] M2
        {
            get { return memory2; }
        }

        public float[] M3
        {
            get { return memory3; }
        }

        public float[] Result
        {
            get { return result; }
        }

        float[] memory1;
        float[] memory2;
        float[] memory3;
        float[] result;
    }
}
