﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace PolicyTester.GUI
{
    public partial class ChartViewer : UserControl
    {
        public ChartViewer()
        {
            InitializeComponent();

            checkedListBox.ItemCheck += CheckedListBox_ItemCheck;
            comboBox.SelectedIndexChanged += ComboBox_SelectedIndexChanged;

            multiSeriesVisible = true;
            page = new Dictionary<string, List<Tuple<SeriesData, bool>>>();
        }

        private void CheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            CheckedListBox clb = (CheckedListBox)sender;

            if (!multiSeriesVisible && e.NewValue == CheckState.Checked)
            {
                foreach (int i in clb.CheckedIndices)
                {
                    if (i != e.Index)
                    {
                        clb.SetItemChecked(i, false);
                    }
                }
            }

            var sd = (GUI.SeriesData)clb.Items[e.Index];
            sd.series.Enabled = (e.NewValue == CheckState.Checked);
            page[(string)comboBox.SelectedItem][e.Index] = Tuple.Create(sd, sd.series.Enabled);

            SetupAxis();
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var page = this.page[(string)comboBox.SelectedItem];

            checkedListBox.Items.Clear();
            chart.Series.Clear();

            for (int i = 0; i < page.Count; ++i)
            {
                var t = page[i];
                checkedListBox.Items.Add(t.Item1, false);
                t.Item1.series.Enabled = false;
                chart.Series.Add(t.Item1.series);

                checkedListBox.SetItemChecked(checkedListBox.Items.Count - 1, t.Item2);
            }
        }

        public void AddSeriesData(string page, SeriesData seriesData, bool isChecked = true)
        {
            if (!this.page.ContainsKey(page))
            {
                this.page.Add(page, new List<Tuple<SeriesData, bool>>());
                comboBox.Items.Add(page);
                //chart.ChartAreas.Add(page);

                if (comboBox.SelectedItem == null) { comboBox.SelectedIndex = 0; }
            }

            this.page[page].Add(Tuple.Create(seriesData, isChecked));

            if ((string)comboBox.SelectedItem == page)
            {
                checkedListBox.Items.Add(seriesData, false);
                seriesData.series.Enabled = false;
                chart.Series.Add(seriesData.series);

                checkedListBox.SetItemChecked(checkedListBox.Items.Count - 1, isChecked);
            }
        }

        public void RemoveSeriesData(SeriesData seriesData)
        {
            foreach (var p in page)
            {
                foreach (var t in p.Value)
                {
                    if (t.Item1.Equals(seriesData))
                    {
                        p.Value.Remove(t);

                        if ((string)comboBox.SelectedItem == p.Key)
                        {
                            checkedListBox.Items.Remove(seriesData);
                            chart.Series.Remove(seriesData.series);
                        }

                        if (seriesData.series.Enabled) { SetupAxis(); }

                        if (p.Value.Count == 0)
                        {
                            comboBox.Items.Remove(p.Key);
                            //chart.ChartAreas.Remove(chart.ChartAreas[p.Key]);
                            page.Remove(p.Key);
                        }

                        return;
                    }
                }
            }
        }

        public void RemoveAllSeriesData()
        {
            checkedListBox.Items.Clear();
            chart.Series.Clear();
            //chart.ChartAreas.Clear();
            comboBox.Items.Clear();
            page.Clear();
        }

        public SeriesData GetSeriesData(int index, string page = null)
        {
            if (page == null)
            {
                page = (string)comboBox.SelectedItem;
            }

            return this.page[page][index].Item1;
        }

        public bool GetSeriesEnabled(int index, string page = null)
        {
            if (page == null)
            {
                page = (string)comboBox.SelectedItem;
            }

            return this.page[page][index].Item2;
        }

        public void SetSeriesEnabled(int index, bool value, string page = null)
        {
            if (page == null || page == (string)comboBox.SelectedItem)
            {
                checkedListBox.SetItemChecked(index, value);
            }
            else
            {
                this.page[page][index] = Tuple.Create(this.page[page][index].Item1, value);
            }
        }

        private void SetupAxis()
        {
            double minX = double.MaxValue;
            double maxX = double.MinValue;
            double minY = double.MaxValue;
            double maxY = double.MinValue;

            foreach (GUI.SeriesData series in checkedListBox.Items)
            {
                if (series.series.Enabled)
                {
                    minX = Math.Min(minX, series.minX);
                    maxX = Math.Max(maxX, series.maxX);
                    minY = Math.Min(minY, series.minY);
                    maxY = Math.Max(maxY, series.maxY);
                }
            }

            if (minX <= maxX && minY <= maxY)
            {
                ChartArea chartArea = chart.ChartAreas[0];
                chartArea.AxisY.Maximum = maxY;
                chartArea.AxisY.Minimum = minY;
                chartArea.AxisY.Interval = (int)Math.Pow(10, (int)Math.Log10(maxY - minY));
                chartArea.AxisX.Minimum = minX;
                chartArea.AxisX.Maximum = maxX;
            }
        }

        public bool MultiSeriesVisible
        {
            get { return multiSeriesVisible; }
            set
            {
                if (!value)
                {
                    for (int i = 0; i < checkedListBox.CheckedIndices.Count - 1; ++i)
                    {
                        checkedListBox.SetItemChecked(checkedListBox.CheckedIndices[i], false);
                    }
                }

                multiSeriesVisible = value;
            }
        }

        public int SeriesCount
        {
            get { return checkedListBox.Items.Count; }
        }

        public Chart Chart
        {
            get { return chart; }
        }

        bool multiSeriesVisible;
        Dictionary<string, List<Tuple<SeriesData, bool>>> page;
    }

    public class SeriesData
    {
        public SeriesData(Series series, double? minX, double? maxX, double? minY, double? maxY)
        {
            this.series = series;

            if (!minX.HasValue)
            {
                double value = series.Points.FindMinByValue("X").XValue;
                int unit = (int)Math.Pow(10, (int)Math.Log10(Math.Abs(value)));
                minX = ((int)(value / unit) - 1) * unit;
            }
            if (!maxX.HasValue)
            {
                double value = series.Points.FindMaxByValue("X").XValue;
                int unit = (int)Math.Pow(10, (int)Math.Log10(Math.Abs(value)));
                maxX = ((int)(value / unit) + 1) * unit;
            }
            if (!minY.HasValue)
            {
                double value = series.Points.FindMaxByValue("Y").YValues.Min();
                int unit = (int)Math.Pow(10, (int)Math.Log10(Math.Abs(value)));
                minY = ((int)(value / unit) - 1) * unit;
            }
            if (!maxY.HasValue)
            {
                double value = series.Points.FindMaxByValue("Y").YValues.Max();
                int unit = (int)Math.Pow(10, (int)Math.Log10(Math.Abs(value)));
                maxY = ((int)(value / unit) + 1) * unit;
            }

            this.minX = minX.Value;
            this.maxX = maxX.Value;
            this.minY = minY.Value;
            this.maxY = maxY.Value;

            //this.minX = (minX.HasValue) ? (minX.Value) : (series.Points.FindMinByValue("X").XValue);
            //this.maxX = (maxX.HasValue) ? (maxX.Value) : (series.Points.FindMaxByValue("X").XValue);
            //this.minY = (minY.HasValue) ? (minY.Value) : (series.Points.FindMinByValue("Y").YValues.Min());
            //this.maxY = (maxY.HasValue) ? (maxY.Value) : (series.Points.FindMaxByValue("Y").YValues.Max());
        }

        public SeriesData(Series series, Shared.Citizen.QuantitativeParameterInfo parameterX, Shared.Citizen.QuantitativeParameterInfo parameterY)
            : this(series, parameterX.Min, parameterX.Max, parameterY.Min, parameterY.Max)
        { }

        private SeriesData(Series series, int minX, int maxX)
        {
            this.series = series;
            this.minX = minX;
            this.maxX = maxX;
            this.minY = 0;

            double maxY = series.Points.FindMaxByValue("Y").YValues[0];
            int unit = (int)Math.Pow(10, (int)Math.Log10(maxY));
            this.maxY = ((int)(maxY / unit) + 1) * unit;
            //this.auto = false;
        }

        public SeriesData(Series series) :this(series, null, null, null, null)
        { }

        public override string ToString()
        {
            return series.Name;
        }

        public Series series;
        public double maxX;
        public double minX;
        public double maxY;
        public double minY;
        //public bool auto;
    }
}
