﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shared.Citizen;

namespace CitizenGenerator
{
    //[Serializable]
    public class Scheme
    {
        public SchemeCategory root;

        public Scheme()
        {
            root = new SchemeCategory("ROOT");
            //root.Children = new List<SchemeElement>();
        }

        public Scheme(SchemeCategory root, int innovation)
        {
            this.root = root;
        }

        public static Scheme CreateFromJson(String filename)
        {
            JObject obj;

            using (StreamReader sr = File.OpenText("Schemes\\" + filename))
            using (JsonTextReader reader = new JsonTextReader(sr))
            {
                obj = JObject.Load(reader);
            }

            return new Scheme((SchemeCategory)JsonObjectToSchemeElement((JObject)obj["Root"]), obj["Innovation"].Value<int>());
        }

        private static void SetParent(SchemeCategory schemeCategory)
        {
            if (schemeCategory.Children != null)
            {
                foreach (SchemeElement se in schemeCategory.Children)
                {
                    se.parent = schemeCategory;
                    if (se.Type == SchemeElementType.Category)
                    {
                        SetParent(se as SchemeCategory);
                    }
                }
            }
        }

        public void WriteToJson(String filename)
        {
            JObject obj = new JObject();
            obj.Add("Root", SchemeElementToJsonObject(root));

            using (StreamWriter sw = new StreamWriter(@"Schemes\" + filename))
            using (JsonTextWriter writer = new JsonTextWriter(sw))
            {
                obj.WriteTo(writer);
            }
        }

        private JObject SchemeElementToJsonObject(SchemeElement schemeElement)
        {
            JObject obj = new JObject();

            obj.Add("Type", schemeElement.Type.ToString());
            obj.Add("Name", schemeElement.Name);

            switch (schemeElement.Type)
            {
                case SchemeElementType.Category:
                    {
                        var sc = (SchemeCategory)schemeElement;
                        JArray children = new JArray(from child in sc.Children
                                                     select SchemeElementToJsonObject(child));
                        obj.Add("Children", children);
                    }
                    break;
                case SchemeElementType.Qualitative:
                    {
                        var sqp = (SchemeQualitativeParameter)schemeElement;
                        JArray categories = new JArray(from category in sqp.Categories
                                                       select new JValue(category));
                        obj.Add("Categories", categories);
                    }
                    break;
                case SchemeElementType.Quantitative:
                    {
                        var sqp = (SchemeQuantitativeParameter)schemeElement;
                        obj.Add("Min", sqp.Min);
                        obj.Add("Max", sqp.Max);
                    }
                    break;
            }

            return obj;
        }

        private static SchemeElement JsonObjectToSchemeElement(JObject obj)
        {
            SchemeElement schemeElement;

            switch (obj["Type"].Value<string>())
            {
                case "Category":
                    {
                        SchemeCategory sc = new SchemeCategory(obj["Name"].Value<string>());
                        foreach (JObject child in obj.GetValue("Children"))
                        {
                            sc.AddChild(JsonObjectToSchemeElement(child));
                        }
                        schemeElement = sc;
                    }
                    break;
                case "Qualitative":
                    {
                        List<string> categories = new List<string>();
                        foreach (JToken category in obj["Categories"])
                        {
                            categories.Add(category.Value<string>());
                        }

                        schemeElement = new SchemeQualitativeParameter(obj["Name"].Value<string>(), categories.ToArray());
                    }
                    break;
                case "Quantitative":
                default:
                    {
                        schemeElement = new SchemeQuantitativeParameter(obj["Name"].Value<string>(), obj["Min"].Value<int>(), obj["Max"].Value<int>());
                    }
                    break;
            }

            return schemeElement;
        }
    }

    public enum SchemeElementType
    {
        Category, Quantitative, Qualitative
    }

    public abstract class SchemeElement
    {
        //String name;
        //int id;

        public SchemeCategory parent;

        public abstract SchemeElementType Type
        {
            get;
        }

        public abstract string Name
        {
            get;
            set;
        }

        public void RemoveFromParent()
        {
            if (parent != null) 
            {
                parent.RemoveChild(this);
            }
        }
    }

    //[Serializable]
    public class SchemeCategory : SchemeElement
    {
        List<SchemeElement> children;
        string name;

        public override SchemeElementType Type
        {
            get
            {
                return SchemeElementType.Category;
            }
        }

        public override string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public List<SchemeElement> Children
        {
            get
            {
                return children;
            }
        }

        public SchemeCategory(string name)
        {
            this.name = name;
            children = new List<SchemeElement>();
        }

        public void AddChild(SchemeElement child)
        {
            if(children == null)
            {
                children = new List<SchemeElement>();
            }
            if (child.parent != null)
            {
                child.RemoveFromParent();
            }
            children.Add(child);
            child.parent = this;
        }

        public void RemoveChild(SchemeElement child)
        {
            if(children != null)
            {
                if(children.Remove(child))
                {
                    child.parent = null;
                }
            }
        }
    }

    public class SchemeQuantitativeParameter : SchemeElement
    {
        public override SchemeElementType Type
        {
            get
            {
                return SchemeElementType.Quantitative;
            }
        }

        public override string Name
        {
            get
            {
                return quantitativeParameter.Name;
            }

            set
            {
                quantitativeParameter = quantitativeParameter.SetName(value);
            }
        }

        public int Min
        {
            get
            {
                return quantitativeParameter.Min;
            }

            set
            {
                quantitativeParameter = quantitativeParameter.SetMin(value);
            }
        }

        public int Max
        {
            get
            {
                return quantitativeParameter.Max;
            }

            set
            {
                quantitativeParameter = quantitativeParameter.SetMax(value);
            }
        }

        public SchemeQuantitativeParameter(string name, int min, int max)
        {
            quantitativeParameter = new QuantitativeParameterInfo(name, min, max);
        }

        public SchemeQuantitativeParameter(QuantitativeParameterInfo quantitativeParameter)
        {
            this.quantitativeParameter = quantitativeParameter;
        }

        QuantitativeParameterInfo quantitativeParameter;
    }

    public class SchemeQualitativeParameter : SchemeElement
    {
        public override SchemeElementType Type
        {
            get
            {
                return SchemeElementType.Qualitative;
            }
        }

        public override string Name
        {
            get
            {
                return qualitativeParameter.Name;
            }

            set
            {
                qualitativeParameter = qualitativeParameter.SetName(value);
            }
        }

        public string[] Categories
        {
            get
            {
                return qualitativeParameter.Categories;
            }
        }

        public SchemeQualitativeParameter(string name, params string[] categories)
        {
            qualitativeParameter = new QualitativeParameterInfo(name, categories);
        }

        public SchemeQualitativeParameter(QualitativeParameterInfo qualitativeParameter)
        {
            this.qualitativeParameter = qualitativeParameter;
        }

        public void SetCategory(params string[] categories)
        {
            qualitativeParameter = new QualitativeParameterInfo(Name, categories);
        }

        QualitativeParameterInfo qualitativeParameter;
    }
}
