﻿namespace CitizenGenerator
{
    partial class AddParameterDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.minLabel = new System.Windows.Forms.Label();
            this.maxLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.minNUD = new System.Windows.Forms.NumericUpDown();
            this.maxNUD = new System.Windows.Forms.NumericUpDown();
            this.quantitativePanel = new System.Windows.Forms.Panel();
            this.qualitativePanel = new System.Windows.Forms.Panel();
            this.removeButton = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.addButton = new System.Windows.Forms.Button();
            this.comboBox = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.minNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxNUD)).BeginInit();
            this.quantitativePanel.SuspendLayout();
            this.qualitativePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(13, 15);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(39, 12);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "Name";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(58, 12);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(199, 21);
            this.nameTextBox.TabIndex = 0;
            // 
            // minLabel
            // 
            this.minLabel.AutoSize = true;
            this.minLabel.Location = new System.Drawing.Point(15, 13);
            this.minLabel.Name = "minLabel";
            this.minLabel.Size = new System.Drawing.Size(26, 12);
            this.minLabel.TabIndex = 2;
            this.minLabel.Text = "Min";
            // 
            // maxLabel
            // 
            this.maxLabel.AutoSize = true;
            this.maxLabel.Location = new System.Drawing.Point(139, 13);
            this.maxLabel.Name = "maxLabel";
            this.maxLabel.Size = new System.Drawing.Size(30, 12);
            this.maxLabel.TabIndex = 4;
            this.maxLabel.Text = "Max";
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(110, 276);
            this.okButton.Margin = new System.Windows.Forms.Padding(110, 3, 3, 3);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 7;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // minNUD
            // 
            this.minNUD.Location = new System.Drawing.Point(47, 11);
            this.minNUD.Name = "minNUD";
            this.minNUD.Size = new System.Drawing.Size(85, 21);
            this.minNUD.TabIndex = 2;
            // 
            // maxNUD
            // 
            this.maxNUD.Location = new System.Drawing.Point(175, 11);
            this.maxNUD.Name = "maxNUD";
            this.maxNUD.Size = new System.Drawing.Size(85, 21);
            this.maxNUD.TabIndex = 3;
            // 
            // quantitativePanel
            // 
            this.quantitativePanel.Controls.Add(this.maxLabel);
            this.quantitativePanel.Controls.Add(this.maxNUD);
            this.quantitativePanel.Controls.Add(this.minLabel);
            this.quantitativePanel.Controls.Add(this.minNUD);
            this.quantitativePanel.Location = new System.Drawing.Point(3, 230);
            this.quantitativePanel.Name = "quantitativePanel";
            this.quantitativePanel.Size = new System.Drawing.Size(284, 40);
            this.quantitativePanel.TabIndex = 5;
            // 
            // qualitativePanel
            // 
            this.qualitativePanel.Controls.Add(this.removeButton);
            this.qualitativePanel.Controls.Add(this.dataGridView);
            this.qualitativePanel.Controls.Add(this.addButton);
            this.qualitativePanel.Location = new System.Drawing.Point(3, 3);
            this.qualitativePanel.Name = "qualitativePanel";
            this.qualitativePanel.Size = new System.Drawing.Size(284, 221);
            this.qualitativePanel.TabIndex = 6;
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(85, 3);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 5;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(4, 32);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowTemplate.Height = 23;
            this.dataGridView.Size = new System.Drawing.Size(277, 186);
            this.dataGridView.TabIndex = 6;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(4, 3);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 4;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            // 
            // comboBox
            // 
            this.comboBox.FormattingEnabled = true;
            this.comboBox.Items.AddRange(new object[] {
            "Quantitative",
            "Qualitative"});
            this.comboBox.Location = new System.Drawing.Point(13, 40);
            this.comboBox.Name = "comboBox";
            this.comboBox.Size = new System.Drawing.Size(121, 20);
            this.comboBox.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.qualitativePanel);
            this.flowLayoutPanel1.Controls.Add(this.quantitativePanel);
            this.flowLayoutPanel1.Controls.Add(this.okButton);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(10, 67);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(291, 303);
            this.flowLayoutPanel1.TabIndex = 8;
            // 
            // AddParameterDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 372);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.comboBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.nameLabel);
            this.Name = "AddParameterDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddParameterDialog";
            ((System.ComponentModel.ISupportInitialize)(this.minNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxNUD)).EndInit();
            this.quantitativePanel.ResumeLayout(false);
            this.quantitativePanel.PerformLayout();
            this.qualitativePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label minLabel;
        private System.Windows.Forms.Label maxLabel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.NumericUpDown minNUD;
        private System.Windows.Forms.NumericUpDown maxNUD;
        private System.Windows.Forms.Panel quantitativePanel;
        private System.Windows.Forms.Panel qualitativePanel;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.ComboBox comboBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.DataGridView dataGridView;
    }
}