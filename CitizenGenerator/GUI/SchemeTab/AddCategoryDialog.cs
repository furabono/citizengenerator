﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CitizenGenerator
{
    public partial class AddCategoryDialog : Form
    { 
        public AddCategoryDialog()
        {
            InitializeComponent();

            this.Shown += AddCategoryDialog_Shown;
            this.KeyDown += AddCategoryDialog_KeyDown;
            this.KeyPreview = true;
        }
        
        public new DialogResult ShowDialog()
        {
            return base.ShowDialog();
        }

        public DialogResult ShowDialog(SchemeElement schemeCategory)
        {
            this.nameTextBox.Text = schemeCategory.Name;
            return base.ShowDialog();
        }

        private void AddCategoryDialog_Shown(object sender, EventArgs e)
        {
            this.nameTextBox.Focus();
        }

        private void AddCategoryDialog_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }

        public string text;

        private void button1_Click(object sender, EventArgs e)
        {
            if(this.nameTextBox.TextLength > 0)
            {
                text = this.nameTextBox.Text;
                this.nameTextBox.Text = "";
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
