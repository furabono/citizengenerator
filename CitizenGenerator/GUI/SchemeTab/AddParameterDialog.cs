﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CitizenGenerator
{
    public partial class AddParameterDialog : Form
    {
        public AddParameterDialog()
        {
            InitializeComponent();

            this.Shown += AddParameterDialog_Shown;
            this.KeyDown += AddParameterDialog_KeyDown;
            this.KeyPreview = true;

            this.comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBox.SelectedIndexChanged += ComboBox_SelectedIndexChanged;

            this.addButton.Click += AddButton_Click;
            this.removeButton.Click += RemoveButton_Click;
            this.dataGridView.Columns.Add("Categories", "Categories");
            this.dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.RowHeadersVisible = false;

            this.minNUD.Minimum = int.MinValue;
            this.minNUD.Maximum = int.MaxValue;
            this.maxNUD.Minimum = int.MinValue;
            this.maxNUD.Maximum = int.MaxValue;

            comboBox.SelectedIndex = 0;

            categories = new List<string>();
        }

        public new DialogResult ShowDialog()
        {
            comboBox.Enabled = true;

            return base.ShowDialog();
        }

        public DialogResult ShowDialog(SchemeElement schemeParameter)
        {
            if(schemeParameter.Type == SchemeElementType.Quantitative)
            {
                comboBox.SelectedIndex = 0;

                var sqp = (SchemeQuantitativeParameter)schemeParameter;
                nameTextBox.Text = schemeParameter.Name;
                minNUD.Value = sqp.Min;
                maxNUD.Value = sqp.Max;
            }
            else
            {
                comboBox.SelectedIndex = 1;

                var sqp = (SchemeQualitativeParameter)schemeParameter;
                nameTextBox.Text = schemeParameter.Name;
                dataGridView.Rows.Clear();
                foreach(string category in sqp.Categories)
                {
                    dataGridView.Rows.Add(category);
                }
            }

            comboBox.Enabled = false;

            return base.ShowDialog();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            dataGridView.Rows.Add("name");
            dataGridView.ClearSelection();
            dataGridView.CurrentCell = dataGridView[0, dataGridView.RowCount - 1];
            dataGridView.BeginEdit(true);
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            foreach(DataGridViewCell cell in dataGridView.SelectedCells)
            {
                dataGridView.Rows.RemoveAt(cell.RowIndex);
            }
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            mode = comboBox.SelectedItem.ToString();
            if(mode == "Quantitative")
            {
                quantitativePanel.Visible = true;
                qualitativePanel.Visible = false;
            }
            else
            {
                quantitativePanel.Visible = false;
                qualitativePanel.Visible = true;
            }
        }

        private void AddParameterDialog_Shown(object sender, EventArgs e)
        {
            this.nameTextBox.Focus();
        }

        private void AddParameterDialog_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                okButton_Click(sender, e);
            }
        }

        public string mode;
        public string name;
        public int min;
        public int max;
        public List<string> categories;

        private void okButton_Click(object sender, EventArgs e)
        {
            if(mode == "Quantitative")
            {
                if (nameTextBox.Text.Length > 0 && minNUD.Text.Length > 0 && maxNUD.Text.Length > 0)
                {
                    name = nameTextBox.Text;
                    min = (int)minNUD.Value;
                    max = (int)maxNUD.Value;

                    nameTextBox.Text = "";
                    this.DialogResult = DialogResult.OK;
                }
            }
            else
            {
                if(nameTextBox.Text.Length > 0 && dataGridView.RowCount > 0)
                {
                    name = nameTextBox.Text;
                    categories.Clear();
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        categories.Add(row.Cells[0].Value.ToString());
                    }
                    nameTextBox.Text = "";
                    dataGridView.Rows.Clear();
                    this.DialogResult = DialogResult.OK;
                }
            }
        }
    }
}
