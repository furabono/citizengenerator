﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace CitizenGenerator
{
    public partial class Form1 : Form
    {
        SchemeTreeView treeView;
        AddCategoryDialog acd;
        AddParameterDialog apd;

        MenuItem addCategoryMenuItem;
        MenuItem addParameterMenuItem;
        MenuItem removeMenuItem;

        Label nameLabel;
        TextBox nameTextBox;
        Label minLabel;
        NumericUpDown minNUD;
        Label maxLabel;
        NumericUpDown maxNUD;
        ListBox categoryListBox;
        Button editButton;

        SchemeNode selectedNode;

        public TabPage InitSchemeTab()
        {
            TabPage schemePage = new TabPage("Scheme");

            SplitContainer splitContainer = new SplitContainer();
            splitContainer.Dock = DockStyle.Fill;
            splitContainer.SplitterDistance = 600;
            schemePage.Controls.Add(splitContainer);

            treeView = new SchemeTreeView();
            treeView.Dock = DockStyle.Fill;
            treeView.HideSelection = false;
            treeView.SelectedNodeChanged += TreeView_SelectedNodeChanged;
            treeView.MouseDown += TreeView_MouseDown;
            treeView.NodeMouseClick += TreeView_NodeMouseClick;
            treeView.KeyDown += TreeView_KeyDown; ;
            treeView.AllowDrop = true;
            treeView.ItemDrag += TreeView_ItemDrag;
            treeView.DragEnter += TreeView_DragEnter;
            treeView.DragOver += TreeView_DragOver;
            treeView.DragDrop += TreeView_DragDrop;
            treeView.AfterSelect += TreeView_AfterSelect;
            splitContainer.Panel1.Controls.Add(treeView);

            ContextMenu treeViewContextMenu = new ContextMenu();
            treeView.ContextMenu = treeViewContextMenu;

            addCategoryMenuItem = new MenuItem("Add Category");
            addCategoryMenuItem.Click += AddCategoryMenuItem_Click;
            treeViewContextMenu.MenuItems.Add(addCategoryMenuItem);

            addParameterMenuItem = new MenuItem("Add Parameter");
            addParameterMenuItem.Click += AddParameterMenuItem_Click;
            treeViewContextMenu.MenuItems.Add(addParameterMenuItem);

            removeMenuItem = new MenuItem("Remove");
            removeMenuItem.Click += RemoveMenuItem_Click;
            treeViewContextMenu.MenuItems.Add(removeMenuItem);

            FlowLayoutPanel menuPanel = new FlowLayoutPanel();
            menuPanel.Dock = DockStyle.Fill;
            menuPanel.FlowDirection = FlowDirection.TopDown;
            menuPanel.Padding = new Padding(10, 20, 10, 20);
            splitContainer.Panel2.Controls.Add(menuPanel);

            nameLabel = new Label();
            nameLabel.Text = "Name";
            nameLabel.AutoSize = true;
            //nameLabel.Margin = new Padding(30);
            menuPanel.Controls.Add(nameLabel);

            nameTextBox = new TextBox();
            nameTextBox.Margin = new Padding(0, 10, 0, 20);
            nameTextBox.ReadOnly = true;
            nameTextBox.KeyDown += Menu_KeyDown;
            menuPanel.Controls.Add(nameTextBox);

            minLabel = new Label();
            minLabel.Text = "Min Value";
            minLabel.AutoSize = true;
            menuPanel.Controls.Add(minLabel);

            minNUD = new NumericUpDown();
            minNUD.Margin = new Padding(0, 10, 0, 20);
            minNUD.ReadOnly = true;
            minNUD.Minimum = int.MinValue;
            minNUD.Maximum = int.MaxValue;
            minNUD.KeyDown += Menu_KeyDown;
            menuPanel.Controls.Add(minNUD);

            maxLabel = new Label();
            maxLabel.Text = "Max Value";
            maxLabel.AutoSize = true;
            menuPanel.Controls.Add(maxLabel);

            maxNUD = new NumericUpDown();
            maxNUD.Margin = new Padding(0, 10, 0, 20);
            maxNUD.ReadOnly = true;
            maxNUD.Minimum = int.MinValue;
            maxNUD.Maximum = int.MaxValue;
            maxNUD.KeyDown += Menu_KeyDown;
            menuPanel.Controls.Add(maxNUD);

            categoryListBox = new ListBox();
            menuPanel.Controls.Add(categoryListBox);

            editButton = new Button();
            editButton.Text = "Edit";
            editButton.Dock = DockStyle.Right;
            editButton.Click += EditButton_Click;
            menuPanel.Controls.Add(editButton);

            Deselect();

            return schemePage;
        }

        private void TreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("AfterSelect");
            if(!e.Node.Equals(selectedNode))
            {
                (sender as SchemeTreeView).SelectedNodeChanged(e.Node);
            }
        }

        private void TreeView_SelectedNodeChanged(TreeNode obj)
        {
            selectedNode = obj as SchemeNode;
            if (obj == null)
            {
                Deselect();
            }
            else
            {
                switch(selectedNode.SchemeElement.Type)
                {
                    case SchemeElementType.Category:
                        SelectCategory();
                        break;
                    case SchemeElementType.Quantitative:
                        SelectQuantitativeParameter();
                        break;
                    case SchemeElementType.Qualitative:
                        SelectQualitativeParameter();
                        break;
                }
            }
        }

        private void TreeView_MouseDown(object sender, MouseEventArgs e)
        {
            SchemeTreeView treeView = sender as SchemeTreeView;
            if (treeView.GetNodeAt(treeView.PointToClient(e.Location)) == null)
            {
                treeView.SelectedNode = null;
            }

            string selectedNodeStr = (selectedNode == null) ? ("null") : (selectedNode.Text);
            string treeNodeStr = (treeView.SelectedNode == null) ? ("null") : (treeView.SelectedNode.Text);
            System.Diagnostics.Debug.WriteLine(selectedNodeStr + " / " + treeNodeStr);
        }

        private void TreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            SchemeTreeView treeView = sender as SchemeTreeView;
            if (e.Node.Bounds.Contains(e.Location))
            {
                treeView.SelectedNode = e.Node;
            }
        }

        private void TreeView_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Delete && selectedNode != null)
            {
                selectedNode.Remove();
                treeView.SelectedNode = null;
            }
        }

        private void TreeView_DragOver(object sender, DragEventArgs e)
        {
            SchemeTreeView treeView = sender as SchemeTreeView;
            Point targetPoint = treeView.PointToClient(new Point(e.X, e.Y));
            TreeNode targetNode = treeView.GetNodeAt(targetPoint);

            treeView.SelectedNode = targetNode;
        }

        private void TreeView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void TreeView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            SchemeNodeDragDropWrapper snddw = new SchemeNodeDragDropWrapper((SchemeNode)e.Item);
            treeView.DoDragDrop(snddw, DragDropEffects.Move | DragDropEffects.Copy);
        }

        private void TreeView_DragDrop(object sender, DragEventArgs e)
        {
            SchemeTreeView treeView = sender as SchemeTreeView;
            Point targetPoint = treeView.PointToClient(new Point(e.X, e.Y));
            TreeNode targetNode = treeView.GetNodeAt(targetPoint);
            SchemeNodeDragDropWrapper dragNodeWrapper = e.Data.GetData(typeof(SchemeNodeDragDropWrapper)) as SchemeNodeDragDropWrapper;
            SchemeNode dragNode = dragNodeWrapper.SchemeNode;

            if (targetNode == null)
            {
                dragNode.Remove();
                scheme.root.AddChild(dragNode.SchemeElement);
                treeView.Nodes.Add(dragNode);
            }
            else if (targetNode is CategoryNode)
            {
                bool isRecursive = false;
                TreeNode parentNode = targetNode;
                while(parentNode != null && dragNode.Level <= parentNode.Level)
                {
                    if(dragNode.Equals(parentNode))
                    {
                        isRecursive = true;
                    }
                    parentNode = parentNode.Parent;
                }
                if(!isRecursive)
                {
                    dragNode.Remove();
                    (targetNode as CategoryNode).AddChild(dragNode);
                }
            }
        }

        private void AddCategoryMenuItem_Click(object sender, EventArgs e)
        {
            if(acd == null)
            {
                acd = new AddCategoryDialog();
            }

            treeView.Enabled = false;
            if (acd.ShowDialog() == DialogResult.OK)
            {
                SchemeCategory schemeCategory = new SchemeCategory(acd.text);
                CategoryNode categoryeNode = new CategoryNode(schemeCategory);
                if(selectedNode == null)
                {
                    scheme.root.AddChild(schemeCategory);
                    treeView.Nodes.Add(categoryeNode);
                }
                else if(selectedNode is CategoryNode)
                {
                    //System.Diagnostics.Debug.WriteLine(selectedNode.Text);
                    (selectedNode as CategoryNode).AddChild(categoryeNode);
                }
                treeView.SelectedNode = categoryeNode;
            }
            treeView.Enabled = true;
            treeView.Focus();
        }

        private void AddParameterMenuItem_Click(object sender, EventArgs e)
        {
            if(apd == null)
            {
                apd = new AddParameterDialog();
            }
            treeView.Enabled = false;
            if(apd.ShowDialog() == DialogResult.OK)
            {
                SchemeNode parameterNode;
                if(apd.mode == "Quantitative")
                {
                    SchemeQuantitativeParameter schemeParameter = new SchemeQuantitativeParameter(apd.name, apd.min, apd.max);
                    parameterNode = new QuantitativeParameterNode(schemeParameter);
                }
                else
                {
                    SchemeQualitativeParameter schemeParameter = new SchemeQualitativeParameter(apd.name, apd.categories.ToArray());
                    parameterNode = new QualitativeParameterNode(schemeParameter);
                }

                if(selectedNode == null)
                {
                    scheme.root.AddChild(parameterNode.SchemeElement);
                    treeView.Nodes.Add(parameterNode);
                }
                else
                {
                    (selectedNode as CategoryNode).AddChild(parameterNode);
                }

                treeView.SelectedNode = parameterNode;
            }
            treeView.Enabled = true;
            treeView.Focus();
        }

        private void RemoveMenuItem_Click(object sender, EventArgs e)
        {
            selectedNode.Remove();
        }

        private void Menu_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                //EditButton_Click(sender, e);
            }
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            SchemeElement se = selectedNode.SchemeElement;
            switch(se.Type)
            {
                case SchemeElementType.Category:
                    {
                        if (acd == null) { acd = new AddCategoryDialog(); }
                        if(acd.ShowDialog(se) == DialogResult.OK)
                        {
                            selectedNode.SchemeName = acd.text;
                            SelectCategory();
                        }
                    }
                    break;

                case SchemeElementType.Quantitative:
                    {
                        if (apd == null) { apd = new AddParameterDialog(); }
                        if(apd.ShowDialog(se) == DialogResult.OK)
                        {
                            QuantitativeParameterNode qpn = (QuantitativeParameterNode)selectedNode;
                            qpn.SchemeName = apd.name;
                            qpn.SchemeMin = apd.min;
                            qpn.SchemeMax = apd.max;
                            SelectQuantitativeParameter();
                        }
                    }
                    break;

                case SchemeElementType.Qualitative:
                    {
                        if (apd == null) { apd = new AddParameterDialog(); }
                        if (apd.ShowDialog(se) == DialogResult.OK)
                        {
                            QualitativeParameterNode qpn = (QualitativeParameterNode)selectedNode;
                            qpn.SchemeName = apd.name;
                            qpn.SetCategories(apd.categories.ToArray());
                            SelectQualitativeParameter();
                        }
                    }
                    break;
            }
        }

        public void Deselect()
        {
            nameLabel.Visible = false;
            nameTextBox.Visible = false;
            minLabel.Visible = false;
            minNUD.Visible = false;
            maxLabel.Visible = false;
            maxNUD.Visible = false;
            categoryListBox.Visible = false;
            editButton.Visible = false;

            addCategoryMenuItem.Visible = true;
            addParameterMenuItem.Visible = true;
            removeMenuItem.Visible = false;
        }

        public void SelectCategory()
        {
            nameLabel.Visible = true;
            nameTextBox.Visible = true;
            minLabel.Visible = false;
            minNUD.Visible = false;
            maxLabel.Visible = false;
            maxNUD.Visible = false;
            categoryListBox.Visible = false;
            editButton.Visible = true;

            addCategoryMenuItem.Visible = true;
            addParameterMenuItem.Visible = true;
            removeMenuItem.Visible = true;

            nameTextBox.Text = selectedNode.SchemeName;
        }

        public void SelectQuantitativeParameter()
        {
            nameLabel.Visible = true;
            nameTextBox.Visible = true;
            minLabel.Visible = true;
            minNUD.Visible = true;
            maxLabel.Visible = true;
            maxNUD.Visible = true;
            categoryListBox.Visible = false;
            editButton.Visible = true;

            addCategoryMenuItem.Visible = false;
            addParameterMenuItem.Visible = false;
            removeMenuItem.Visible = true;

            QuantitativeParameterNode parameterNode = selectedNode as QuantitativeParameterNode;

            nameTextBox.Text = parameterNode.SchemeName;
            minNUD.Text = parameterNode.SchemeMin.ToString();
            maxNUD.Text = parameterNode.SchemeMax.ToString();
        }

        public void SelectQualitativeParameter()
        {
            nameLabel.Visible = true;
            nameTextBox.Visible = true;
            minLabel.Visible = false;
            minNUD.Visible = false;
            maxLabel.Visible = false;
            maxNUD.Visible = false;
            categoryListBox.Visible = true;
            editButton.Visible = true;

            addCategoryMenuItem.Visible = false;
            addParameterMenuItem.Visible = false;
            removeMenuItem.Visible = true;

            QualitativeParameterNode parameterNode = selectedNode as QualitativeParameterNode;

            nameTextBox.Text = parameterNode.SchemeName;
            categoryListBox.Items.Clear();

            var sqp = (SchemeQualitativeParameter)parameterNode.SchemeElement;
            foreach(string category in sqp.Categories)
            {
                categoryListBox.Items.Add(category);
            }
        }
    }
}
