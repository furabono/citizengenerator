﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CitizenGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            this.FormClosed += Form1_FormClosed;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitGUI();

            InitSchemeData();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(scheme != null)
            {
                //scheme.WriteToJson("scheme.json");
            }
        }

        private void InitGUI()
        {
            this.Size = new Size(800, 600);
            this.MinimumSize = this.Size;
            this.MaximumSize = this.Size;

            TabControl tabControl = new TabControl();
            tabControl.Dock = DockStyle.Fill;
            this.Controls.Add(tabControl);

            TabPage schemePage = InitSchemeTab();
            tabControl.TabPages.Add(schemePage);

            TabPage generatePage = InitGenerateTab();
            tabControl.TabPages.Add(generatePage);
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private Scheme scheme;

        private void InitSchemeData()
        {
            FileInfo fileInfo = new FileInfo("Schemes\\scheme.json");
            if (fileInfo.Exists)
            {
                scheme = Scheme.CreateFromJson("scheme.json");
                if (scheme.root.Children != null)
                {
                    foreach (SchemeElement se in scheme.root.Children)
                    {
                        switch(se.Type)
                        {
                            case SchemeElementType.Category:
                                CategoryNode cn = new CategoryNode(se as SchemeCategory);
                                treeView.Nodes.Add(cn);
                                InitSchemeTreeView(cn);
                                break;
                            case SchemeElementType.Quantitative:
                                QuantitativeParameterNode quantative = new QuantitativeParameterNode(se as SchemeQuantitativeParameter);
                                treeView.Nodes.Add(quantative);
                                break;
                            case SchemeElementType.Qualitative:
                                QualitativeParameterNode qualitative = new QualitativeParameterNode(se as SchemeQualitativeParameter);
                                treeView.Nodes.Add(qualitative);
                                break;
                        }
                    }
                }
                treeView.ExpandAll();
            }
            else
            {
                Directory.CreateDirectory("Schemes");
                scheme = new Scheme();
            }
        }

        private void InitSchemeTreeView(CategoryNode categoryNode)
        {
            SchemeCategory sc = categoryNode.SchemeElement as SchemeCategory;
            if(sc.Children != null)
            {
                foreach (SchemeElement se in sc.Children)
                {
                    switch (se.Type)
                    {
                        case SchemeElementType.Category:
                            CategoryNode cn = new CategoryNode(se as SchemeCategory);
                            categoryNode.Nodes.Add(cn);
                            InitSchemeTreeView(cn);
                            break;
                        case SchemeElementType.Quantitative:
                            QuantitativeParameterNode quantative = new QuantitativeParameterNode(se as SchemeQuantitativeParameter);
                            categoryNode.Nodes.Add(quantative);
                            break;
                        case SchemeElementType.Qualitative:
                            QualitativeParameterNode qualitative = new QualitativeParameterNode(se as SchemeQualitativeParameter);
                            categoryNode.Nodes.Add(qualitative);
                            break;
                    }
                }
            }
        }
    }
}
