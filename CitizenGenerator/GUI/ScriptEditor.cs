﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CitizenGenerator.GUI
{
    public partial class ScriptEditor : UserControl
    {
        public ScriptEditor()
        {
            InitializeComponent();

            expressionRadioButton.CheckedChanged += RadioButton_CheckedChanged;
        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            var rb = (RadioButton)sender;
            if (!rb.Checked) { return; }

            richTextBox.ResetText();
            if (object.ReferenceEquals(rb, expressionRadioButton)) { richTextBox.Multiline = false; }
            else { richTextBox.Multiline = true; }
        }

        public void Init(GenerateTab.ScriptType scriptType, string scriptText)
        {
            if (scriptType == GenerateTab.ScriptType.Expression) { expressionRadioButton.Checked = true; }
            else { functionRadioButton.Checked = true; }

            richTextBox.Text = scriptText;
        }

        public GenerateTab.Script<T> GetScript<T>()
        {
            GenerateTab.ScriptType st = (richTextBox.Multiline) ? (GenerateTab.ScriptType.Function) : (GenerateTab.ScriptType.Expression);
            return new GenerateTab.Script<T>(st, richTextBox.Text);
        }
    }
}
