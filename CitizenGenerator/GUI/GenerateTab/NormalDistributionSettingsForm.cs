﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CitizenGenerator.GUI.GenerateTab;

namespace CitizenGenerator
{
    public partial class NormalDistributionSettingsForm : Form
    {
        NormalDistributionSettingsForm qsd;

        public NormalDistributionSettingsForm()
        {
            InitializeComponent();

            paramListBox.SelectedIndexChanged += ParamListBox_SelectedIndexChanged;

            dataGridView.RowHeadersVisible = false;
            dataGridView.CellEndEdit += DataGridView_CellEndEdit;
            //dataGridView.CellValidating += DataGridView_CellValidating;
            dataGridView.CellValueChanged += DataGridView_CellValueChanged;
            dataGridView.CellContentClick += DataGridView_CellContentClick;

            addButton.Click += AddButton_Click;
            removeButton.Click += RemoveButton_Click;
            editButton.Click += EditButton_Click;

            okButton.Click += OkButton_Click;
        }

        private void DataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView.Columns[e.ColumnIndex].Name == "Random")
            {
                dataGridView.EndEdit();
            }
        }

        private void DataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            string columnName = dataGridView.Columns[e.ColumnIndex].Name;
            if (columnName == "Random")
            {
                Parameter p = (Parameter)paramListBox.SelectedItem;
                if ((bool)dataGridView[e.ColumnIndex, e.RowIndex].Value)
                {
                    dataGridView[2, e.RowIndex].ReadOnly = true;
                    dataGridView[2, e.RowIndex].Value = "Random";
                    p.Proportions[e.RowIndex] = -1;
                }
                else
                {
                    dataGridView[2, e.RowIndex].ReadOnly = false;
                    dataGridView[2, e.RowIndex].Value = "0";
                    p.Proportions[e.RowIndex] = 0;
                    dataGridView.CurrentCell = dataGridView[2, e.RowIndex];
                    dataGridView.BeginEdit(true);
                }
            }
            else if(columnName == "Proportion")
            {
                updateTotalProprotion();
            }
        }

        private void DataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (dataGridView.Columns[e.ColumnIndex].Name == "Proportion")
            {
                int proportion;
                if(int.TryParse(e.FormattedValue.ToString(), out proportion))
                {
                    if (proportion >= 0 && proportion <= 100)
                    {
                        Parameter p = (Parameter)paramListBox.SelectedItem;
                        p.Proportions[e.RowIndex] = proportion;
                        return;
                    }
                }

                e.Cancel = true;
            }
        }

        private void DataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView.Columns[e.ColumnIndex].Name == "Proportion")
            {
                Parameter p = (Parameter)paramListBox.SelectedItem;
                int proportion;
                if (int.TryParse(dataGridView[e.ColumnIndex, e.RowIndex].Value.ToString(), out proportion))
                {
                    if (proportion >= 0 && proportion <= 100)
                    {
                        p.Proportions[e.RowIndex] = proportion;
                        return;
                    }
                }

                dataGridView[e.ColumnIndex, e.RowIndex].Value = p.Proportions[e.RowIndex].ToString();
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (condition == null && !ValidateProportions()) { return; }

            this.DialogResult = DialogResult.OK;
        }

        DistInfoDialog did;

        private void AddButton_Click(object sender, EventArgs e)
        {
            if(did == null)
            {
                did = new DistInfoDialog();
            }

            Parameter p = paramListBox.SelectedItem as Parameter;

            DistInfo newDistInfo = new DistInfo();
            newDistInfo.min = p.Min;
            newDistInfo.max = p.Max;

            DialogResult dialogResult;

            if (condition == null) { dialogResult = did.ShowDialog(qualitativeParameters, newDistInfo); }
            else { dialogResult = did.ShowDialog(condition, newDistInfo); }

            if(dialogResult == DialogResult.OK)
            {
                if (ValidateDistInfo(p, newDistInfo))
                {
                    p.DistInfos.Add(newDistInfo);
                    dataGridView.Rows.Add(DistInfoToString(newDistInfo));
                    dataGridView.ClearSelection();
                    dataGridView.Rows[dataGridView.RowCount - 1].Selected = true;
                }
            }
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            int index = dataGridView.SelectedRows[0].Index;
            dataGridView.Rows.RemoveAt(index);

            Parameter p = paramListBox.SelectedItem as Parameter;
            p.DistInfos.RemoveAt(index);
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            Parameter p = paramListBox.SelectedItem as Parameter;

            if(p.Type == ParameterType.Quantitative)
            {
                int index = dataGridView.SelectedRows[0].Index;
                DistInfo newDistInfo = new DistInfo(p.DistInfos[index]);

                if (did == null)
                {
                    did = new DistInfoDialog();
                }

                DialogResult dialogResult;

                if (condition == null) { dialogResult = did.ShowDialog(qualitativeParameters, newDistInfo); }
                else { dialogResult = did.ShowDialog(condition, newDistInfo); }

                if (dialogResult == DialogResult.OK)
                {
                    if (ValidateDistInfo(p, newDistInfo, index))
                    {
                        p.DistInfos[index] = newDistInfo;
                        dataGridView[0, index].Value = DistInfoToString(newDistInfo);
                    }
                }
            }
            else
            {
                Target condition = new Target();
                condition.parameter = (QualitativeParameter)p;
                condition.value = dataGridView.SelectedRows[0].Index;

                if (qsd == null) { qsd = new NormalDistributionSettingsForm(); }

                if(qsd.ShowDialog(parameters, condition) == DialogResult.OK)
                {

                }
            }
        }

        private void ParamListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Parameter p = (Parameter)((sender as ListBox).SelectedItem);
            dataGridView.Rows.Clear();
            dataGridView.Columns.Clear();
            
            if(p.Type == ParameterType.Quantitative)
            {
                addButton.Enabled = true;
                removeButton.Enabled = true;
                //editButton.Enabled = true;
                totalLabel.Text = "";

                dataGridView.Columns.Add("Disttibution", "Distribution");
                dataGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridView.Columns[0].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                dataGridView.Columns[0].ReadOnly = true;
                dataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

                foreach(DistInfo di in p.DistInfos)
                {
                    dataGridView.Rows.Add(DistInfoToString(di));
                }
            }
            else
            {
                addButton.Enabled = false;
                removeButton.Enabled = false;
                //editButton.Enabled = false;
                //totalLabel.Visible = true;

                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
                checkBoxColumn.Name = "Random";
                checkBoxColumn.HeaderText = "Rand";
                checkBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                dataGridView.Columns.Add(checkBoxColumn);
                dataGridView.Columns.Add("Category", "Category");
                dataGridView.Columns[1].ReadOnly = true;
                //dataGridView.Columns[0].ReadOnly = true;
                dataGridView.Columns.Add("Proportion", "Proportion(%)");
                //dataGridView.Columns[1].ReadOnly = true;

                for (int i = 0; i < p.Categories.Length; ++i)
                {
                    if(p.Proportions[i] == -1)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(dataGridView);
                        row.Cells[0].Value = true;
                        row.Cells[1].Value = p.Categories[i];
                        row.Cells[2].Value = "Random";
                        row.Cells[2].ReadOnly = true;
                        //row.Cells[0].Value = p.Categories[i];
                        //row.Cells[1].Value = "Random";
                        dataGridView.Rows.Add(row);
                        //dataGridView.Rows.Add(true, p.Categories[i], "Random");
                    }
                    else
                    {
                        dataGridView.Rows.Add(false, p.Categories[i], p.Proportions[i]);
                        //dataGridView.Rows.Add(p.Categories[i], p.Proportions[i]);
                    }
                }

                updateTotalProprotion();
            }
        }

        public DialogResult ShowDialog(List<Parameter> parameters)
        {
            if (qualitativeParameters == null) { qualitativeParameters = new List<QualitativeParameter>(); }

            this.parameters = null;
            this.parameters = (from p in parameters
                               select p.Clone()).ToArray();

            paramListBox.Items.Clear();
            paramListBox.Items.AddRange(this.parameters);

            qualitativeParameters.Clear();
            foreach (Parameter p in this.parameters)
            {
                if(p.Type == ParameterType.Qualitative)
                {
                    qualitativeParameters.Add((QualitativeParameter)p);
                }
            }

            paramListBox.SelectedIndex = 0;

            DialogResult dialogResult = base.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                for (int i = 0; i<parameters.Count; ++i)
                {
                    parameters[i] = this.parameters[i];
                }
            }

            this.parameters = null;

            return dialogResult;
        }

        public DialogResult ShowDialog(Parameter[] parameters, Target condition)
        {
            this.parameters = null;
            this.parameters = (from p in parameters
                               where p.Type == ParameterType.Quantitative
                               select p.Clone()).ToArray();

            foreach (Parameter p in this.parameters)
            {
                var t = (from di in p.DistInfos
                         where CompareCondition(condition, di.condition)
                         select di).ToArray();
                p.DistInfos.Clear();
                p.DistInfos.AddRange(t);
            }

            paramListBox.Items.Clear();
            paramListBox.Items.AddRange(this.parameters);
            paramListBox.SelectedIndex = 0;

            this.condition = condition;

            DialogResult dialogResult = base.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                var qparameters = (from p in parameters
                                   where p.Type == ParameterType.Quantitative
                                   select p).ToArray();
                for (int i = 0; i < qparameters.Length; ++i)
                {
                    this.parameters[i].DistInfos.AddRange(from di in qparameters[i].DistInfos
                                                          where !CompareCondition(condition, di.condition)
                                                          select di);
                    qparameters[i].DistInfos.Clear();
                    qparameters[i].DistInfos.AddRange(this.parameters[i].DistInfos);
                }
            }

            this.parameters = null;
            this.condition = null;

            return dialogResult;
        } 

        string DistInfoToString(DistInfo distInfo)
        {
            string str = "";

            if(distInfo.condition == null)
            {
                str += "None" + Environment.NewLine;
            }
            else
            {
                str += distInfo.condition.parameter.Name;
                str += " = ";
                str += distInfo.condition.CategoryName;
                str += Environment.NewLine;
            }

            //str += string.Format("Mean : {0} Var : {1}", distInfo.mean, distInfo.variance);
            str += distInfo.ToString();

            return str;
        }

        bool ValidateDistInfo(Parameter parameter, DistInfo distInfo, int selfIndex = -1)
        {
            if (distInfo.min > parameter.Max || distInfo.max < parameter.Min) { return false; }

            for (int i = 0; i < parameter.DistInfos.Count; ++i)
            {
                if (i == selfIndex) { continue; }

                DistInfo di = parameter.DistInfos[i];
                if (di.condition == null && distInfo.condition != null) { continue; }
                if (di.condition != null && !di.condition.Equals(distInfo.condition)) { continue; }
                if (di.mean != distInfo.mean) { continue; }

                return false;
            }

            return true;
        }

        bool ValidateProportions()
        {
            foreach (Parameter p in paramListBox.Items)
            {
                if (p.Type == ParameterType.Quantitative)
                {
                    if (p.DistInfos.Count == 0) { return false; }

                    bool baseDistExist = false;
                    int proportionSum = 0;
                    foreach (DistInfo di in p.DistInfos)
                    {
                        if (di.condition == null) { baseDistExist = true; }
                        else { proportionSum += Math.Max(di.condition.parameter.Proportions[di.condition.value], 0); }
                    }

                    if (proportionSum < 100 && !baseDistExist) { return false; }
                }
                else
                {
                    bool randomExist = false;
                    int proportionSum = 0;
                    foreach (int pro in p.Proportions)
                    {
                        if (pro == -1) { randomExist = true; }
                        else { proportionSum += pro; }
                    }

                    if (proportionSum > 100) { return false; }
                    if (proportionSum < 100 && !randomExist) { return false; }
                }
            }

            return true;
        }

        bool CompareCondition(Target f, Target l)
        {
            if (f == null && l == null) { return true; }
            if (f == null && l != null) { return false; }
            if (f != null && l == null) { return false; }

            return f.parameter.ID == l.parameter.ID && f.value == l.value;
        }

        void updateTotalProprotion()
        {
            int total = 0;
            string value;
            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                value = row.Cells["Proportion"].Value.ToString();
                if (value != "Random")
                {
                    total += int.Parse(value);
                }
            }
            totalLabel.Text = string.Format("Total : {0}%", total);
        }

        Parameter[] parameters;
        List<QualitativeParameter> qualitativeParameters;
        Target condition;
    }
}
