﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Data;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.Random;
using Shared.Citizen;

//using Citizen = Shared.Citizen.Citizen;

namespace CitizenGenerator.GUI.GenerateTab
{
    class Generator
    {
        public static Civil Generate(GenerationSettings settings, int population)
        {
            var pic = new ParameterInfoCollection((from p in settings.Parameters select p.ParameterInfo).ToArray());
            var citizens = new Citizen[population];

            Citizen[] selected;

            foreach (Parameter p in settings.OrderedParameters)
            {
                if (p.Type == ParameterType.Quantitative)
                {
                    var qp = (QuantitativeParameter)p;
                    foreach (QuantitativeGenerationInfo qgi in qp.GenerationInfos)
                    {
                        selected = Select(citizens, qgi.Target);
                        ShuffleArray(selected);
                        
                        switch (qgi.Type)
                        {
                            case QuantitativeGenerationInfoType.Random:
                                { GenerateQuantitativeRandom(selected, qp.ParameterInfo, (RandomQuantitativeGenerationInfo)qgi); }
                                break;

                            case QuantitativeGenerationInfoType.Script:
                                { GenerateQuantitativeScript(selected, qp.ParameterInfo, (ScriptQuantitativeGenerationInfo)qgi); }
                                break;

                            case QuantitativeGenerationInfoType.ProbabilityDistribution:
                                { GenerateQuantitativeProbabilityDistribution(selected, qp.ParameterInfo, (ProbabilityDistributionQuantitativeGenerationInfo)qgi); }
                                break;
                        }
                    }
                }
                else
                {
                    var qp = (QualitativeParameter)p;

                    var random = MersenneTwister.Default;
                    var toGenerate = new int[qp.Proportions.Length];
                    for (int i = 0; i < toGenerate.Length; ++i)
                    {
                        toGenerate[i] = (int)((qp.Proportions[i] ?? random.Next(0, 101 - qp.ProportionSum)) * population / 100.0);
                    }

                    ShuffleArray(citizens);
                    if (qp.Scripts == null) { GenerateQualitative(citizens, qp.ParameterInfo, toGenerate); }
                    else { GenerateQualitative(citizens, qp.ParameterInfo, toGenerate, qp.Scripts); }
                }
            }

            return new Civil(pic, citizens);
        }

        static Citizen[] Select(Citizen[] citizens, Target target)
        {
            var sb = new PythonScriptBuilder();
            sb.Import("System", "Func");
            sb.Import("Shared.Citizen", "Citizen");
            sb.WriteLine("selected = citizens.Where(lambda citizen: " + target.Script.ScriptText + ").ToArray()");

            var se = new ScriptEngine();
            se.SetVariable("citizens", citizens);
            se.Run(sb.Script);

            return se.GetVariable<Citizen[]>("selected");
        }

        static void ShuffleArray<T>(T[] array)
        {
            //Random random = new Random();
            MersenneTwister random = MersenneTwister.Default;
            int exchangeIndex;
            T temp;

            for (int i = 0; i < array.Length - 1; ++i)
            {
                exchangeIndex = random.Next(i, array.Length);
                temp = array[i];
                array[i] = array[exchangeIndex];
                array[exchangeIndex] = temp;
            }
        }

        // ParameterInfo -> QuantativeParameterInfo 반환하도록?
        static void GenerateQuantitativeRandom(Citizen[] citizens, QuantitativeParameterInfo quantitativeParameterInfo, RandomQuantitativeGenerationInfo randomQuantitativeGenerationInfo)
        {
            int min = quantitativeParameterInfo.Min;
            int max = quantitativeParameterInfo.Max;
            MersenneTwister random = MersenneTwister.Default;

            foreach (Citizen c in citizens)
            {
                c.SetRaw(quantitativeParameterInfo.Name, random.Next(min, max + 1));
            }
        }

        static void GenerateQuantitativeScript(Citizen[] citizens, QuantitativeParameterInfo quantitativeParameterInfo, ScriptQuantitativeGenerationInfo scriptQuantitativeGenerationInfo)
        {
            var sb = new PythonScriptBuilder();
            sb.Function("Generate", "citizen")
                .WriteLine("citizen.SetRaw(" + quantitativeParameterInfo.Name + ", " + scriptQuantitativeGenerationInfo.Script.ScriptText + ")")
                .EndFunction()
              .WriteLine()
              .For("i", 0, citizens.Length, "Generate(citizens[i])");

            var se = new ScriptEngine();
            se.SetVariable("citizens", citizens);
            se.Run(sb.Script);
        }

        static void GenerateQuantitativeProbabilityDistribution(Citizen[] citizens, QuantitativeParameterInfo quantitativeParameterInfo, ProbabilityDistributionQuantitativeGenerationInfo probabilityDistributionQuantitativeGenerationInfo)
        {
            var distributionArray = (from di in probabilityDistributionQuantitativeGenerationInfo.DistributionInfos
                                     let d = new Distribution(di)
                                     orderby d.Mean
                                     select d).ToArray();

            double coefficient;
            int[] mins;
            int[] maxs;
            {
                Tuple<double, int[], int[]> tuple = GetCoefficient(distributionArray, citizens.Length);
                coefficient = tuple.Item1;
                mins = tuple.Item2;
                maxs = tuple.Item3;
            }

            int toGenerate;
            int generated = 0;
            for (int i = 0; i < distributionArray.Length; ++i)
            {
                for (int j = mins[i]; j <= maxs[i]; ++j)
                {
                    toGenerate = (int)((distributionArray[i].CumulativeDistribution(j + 0.5) 
                        - distributionArray[i].CumulativeDistribution(j - 0.5)) 
                        * coefficient);

                    for (; toGenerate > 0; --toGenerate)
                    {
                        citizens[generated++].SetRaw(quantitativeParameterInfo.Name, j);
                    }
                }
            }

            //Random random = new Random();
            MersenneTwister random = MersenneTwister.Default;
            int randomIndex;
            for (; generated < citizens.Length; ++generated)
            {
                randomIndex = random.Next(0, distributionArray.Length);
                citizens[generated].SetRaw(quantitativeParameterInfo.Name, random.Next(mins[randomIndex], maxs[randomIndex] + 1));
            }
        }

        static Tuple<double, int[], int[]> GetCoefficient(Distribution[] distributionArray, int population)
        {
            double[] nodalPoints = GetNodalPoints(distributionArray);

            int numDist = distributionArray.Length;
            var mins = new int[numDist];
            var maxs = new int[numDist];

            mins[0] = distributionArray[0].Min;
            maxs[numDist - 1] = distributionArray[numDist - 1].Max;

            for (int i = 0; i < nodalPoints.Length; ++i)
            {
                mins[i + 1] = Math.Max((int)nodalPoints[i] + 1, distributionArray[i].Min);
                maxs[i] = Math.Min((int)nodalPoints[i], distributionArray[i].Max);
            }

            double coefficient;
            bool finished = false;

            do
            {
                finished = true;

                coefficient = 0;
                for (int i = 0; i < numDist; ++i)
                {
                    coefficient += distributionArray[i].CumulativeDistribution(maxs[i] + 0.5) - distributionArray[i].CumulativeDistribution(mins[i] - 0.5);
                }
                coefficient = population / coefficient;

                for (int i = 0; i < numDist; ++i)
                {
                    if (mins[i] == distributionArray[i].Mean)
                    {
                        continue;
                    }
                    if ((distributionArray[i].CumulativeDistribution(mins[i] + 0.5) - distributionArray[i].CumulativeDistribution(mins[i] - 0.5)) * coefficient < 1)
                    {
                        ++mins[i];
                        finished = false;
                    }
                }
                for (int i = 0; i < numDist; ++i)
                {
                    if (maxs[i] == distributionArray[i].Mean)
                    {
                        continue;
                    }
                    if ((distributionArray[i].CumulativeDistribution(maxs[i] + 0.5) - distributionArray[i].CumulativeDistribution(maxs[i] - 0.5)) * coefficient < 1)
                    {
                        --maxs[i];
                        finished = false;
                    }
                }
            } while (!finished);

            return Tuple.Create(coefficient, mins, maxs);
        }

        // check ordered -> Exception
        static double[] GetNodalPoints(Distribution[] distributionArray)
        {
            // check ordered
            for (int i = 0; i < distributionArray.Length - 1; ++i)
            {
                if (distributionArray[i].ContinuousDistribution.Mean >= distributionArray[i + 1].ContinuousDistribution.Mean) { return null; }
            }

            double[] nodalPoints = new double[distributionArray.Length - 1];
            for (int i = 0; i < distributionArray.Length - 1; ++i)
            {
                double l = distributionArray[i].Mean;
                double r = distributionArray[i + 1].Mean;
                double m;
                double lDensity, rDensity;
                while (r - l > 1)
                {
                    m = (l + r) / 2;
                    lDensity = distributionArray[i].Density(m);
                    rDensity = distributionArray[i + 1].Density(m);

                    if (lDensity > rDensity)
                    {
                        l = m;
                    }
                    else if (lDensity < rDensity)
                    {
                        r = m;
                    }
                    else
                    {
                        l = m;
                        r = m;
                    }
                }

                nodalPoints[i] = (l + r) / 2;
            }

            return nodalPoints;
        }

        static void GenerateQualitative(Citizen[] citizens, QualitativeParameterInfo quantitativeParameterInfo, int[] toGenerate)
        {
            int generated = 0;
            for (int i = 0; i < toGenerate.Length; ++i)
            {
                for (int j = 0; j < toGenerate[i]; ++j)
                {
                    citizens[generated++].SetRaw(quantitativeParameterInfo.Name, i);
                }
            }
        }

        static void GenerateQualitative(Citizen[] citizens, QualitativeParameterInfo quantitativeParameterInfo, int[] toGenerate, Script<float>[] scripts)
        {
            var psb = new PythonScriptBuilder();

            var func = psb.Function("Generate", "citizen");
            func.WriteLine("fitness = []");
            for (int i = 0; i < scripts.Length; ++i)
            {
                func.WriteLine(string.Format("fitness.Append({0})\n", scripts[i].ScriptText));
            }
            func.WriteLine("max = -9999999")
                .WriteLine("maxIndex = -1")
                .For("i", 0, scripts.Length)
                    .If("toGenerate[i] > 0 && fitness[i] > max")
                        .WriteLine("max = fitness[i]")
                        .WriteLine("maxIndex = i")
                    .EndIf()
                .EndFor()
                .WriteLine(string.Format("citizen.SetRaw({0}, maxIndex)", quantitativeParameterInfo.Name))
                .EndFunction();

            psb.WriteLine()
                .For("i", 0, citizens.Length, "Generate(citizens[i])");

            var se = new ScriptEngine();
            se.SetVariable("citizens", citizens);
            se.SetVariable("toGenerate", toGenerate);

            se.Run(psb.Script);
        }
    }

    class Distribution
    {
        IContinuousDistribution continuousDistribution;
        int min;
        int max;

        public IContinuousDistribution ContinuousDistribution
        {
            get { return continuousDistribution; }
        }

        public int Min
        {
            get { return min; }
        }

        public int Max
        {
            get { return max; }
        }

        public double Mean
        {
            get { return this.continuousDistribution.Mean; }
        }

        public Distribution(DistributionInfo distributionInfo)
        {
            this.min = distributionInfo.Min;
            this.max = distributionInfo.Max;

            switch (distributionInfo.Type)
            {
                case DistributionType.Normal:
                    {
                        var di = (NormalDistributionInfo)distributionInfo;
                        this.continuousDistribution = Normal.WithMeanVariance(di.Mean, di.Variance);
                        break;
                    }
            }
        }

        public double CumulativeDistribution(double x)
        {
            return this.continuousDistribution.CumulativeDistribution(x);
        }

        public double Density(double x)
        {
            return this.continuousDistribution.Density(x);
        }
    }
}
