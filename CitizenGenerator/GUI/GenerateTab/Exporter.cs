﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.IO;

namespace CitizenGenerator.GUI.GenerateTab
{
    class Exporter
    {
        public static void ExportJson(string filepath, DataSet dataSet)
        {
            StreamWriter sw = new StreamWriter(filepath);
            JObject root = new JObject();

            JArray parameter = new JArray();

            DataTable quantitativeDT = dataSet.Tables["QuantitativeParameterDataTable"];
            DataTable qualitativeDT = dataSet.Tables["QualitativeParameterDataTable"];
            DataTable citizenDT = dataSet.Tables["CitizenDataTable"];

            Dictionary<string, Dictionary<string, int>> qp = new Dictionary<string, Dictionary<string, int>>();

            foreach (DataColumn col in citizenDT.Columns)
            {
                if (col.DataType == typeof(int))
                {
                    DataRow row = quantitativeDT.Select(string.Format("Name = '{0}'", col.ColumnName))[0];
                    parameter.Add(new JObject(
                        new JProperty("Type", "Quantitative"),
                        new JProperty("Name", row["Name"]),
                        new JProperty("Min", row["Min"]),
                        new JProperty("Max", row["Max"])
                        ));
                }
                else
                {
                    DataRow row = qualitativeDT.Select(string.Format("Name = '{0}'", col.ColumnName))[0];

                    string[] categories = row[1].ToString().Split(',');

                    Dictionary<string, int> dict = new Dictionary<string, int>();
                    for (int i = 0; i < categories.Length; ++i)
                    {
                        dict.Add(categories[i], i);
                    }
                    qp.Add(row[0].ToString(), dict);

                    parameter.Add(new JObject(
                        new JProperty("Type", "Qualitative"),
                        new JProperty("Name", row["Name"]),
                        new JProperty("Category", new JArray(categories))
                        ));
                }
            }

            root.Add("Parameter", parameter);

            root.Add("Citizen", new JArray(
                from row in citizenDT.Select()
                select new JArray(
                    from DataColumn col in citizenDT.Columns
                    select ((col.DataType == typeof(int)) ? (new JValue(row[col.ColumnName])) : (new JValue(qp[col.ColumnName][row[col.ColumnName].ToString()]))))
                ));

            sw.Write(root.ToString());
            sw.Close();
        }

        public static void ExportJson(DataTable quantitativeParameterDataTable, DataTable qualitativeParameterDataTable, DataTable citizenDataTable)
        {
            
        }
    }
}
