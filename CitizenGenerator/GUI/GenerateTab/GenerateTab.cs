﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;
using System.Data;

using CitizenGenerator.GUI.GenerateTab;

namespace CitizenGenerator
{
    public class DoubleBufferedDataGridView : DataGridView
    {
        public DoubleBufferedDataGridView() : base()
        {
            DoubleBuffered = true;
        }
    }

    public partial class Form1 : Form
    {
        TableLayoutPanel tableLayoutPanel;
        NumericUpDown numNUD;
        ProgressBar progressBar;
        Label timeLabel;

        Label popLabel;

        DoubleBufferedDataGridView dataGridView;

        Chart chart;
        ChartArea chartArea;
        ListBox chartListBox;
        Series enabledSeries;

        TabPage InitGenerateTab()
        {
            TabPage generateTab = new TabPage("Generate");
            generateTab.Enter += GenerateTab_Enter;

            tableLayoutPanel = new TableLayoutPanel();
            tableLayoutPanel.Dock = DockStyle.Fill;
            tableLayoutPanel.ColumnCount = 1;
            tableLayoutPanel.RowCount = 3;
            tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
            tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 7));
            tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 5));
            tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 88));
            generateTab.Controls.Add(tableLayoutPanel);

            FlowLayoutPanel menuFlowLayoutPanel = new FlowLayoutPanel();
            menuFlowLayoutPanel.Dock = DockStyle.Fill;
            menuFlowLayoutPanel.FlowDirection = FlowDirection.LeftToRight;
            //menuFlowLayoutPanel.Padding = new Padding(10, 10, 10, 0);
            tableLayoutPanel.Controls.Add(menuFlowLayoutPanel, 0, 0);
            //menuFlowLayoutPanel.BackColor = Color.Blue;

            ComboBox modeComboBox = new ComboBox();
            modeComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            modeComboBox.Items.Add("Normal Dist");
            modeComboBox.SelectedIndex = 0;
            menuFlowLayoutPanel.Controls.Add(modeComboBox);

            Label numLabel = new Label();
            numLabel.AutoSize = true;
            numLabel.Text = "Polulation ";
            numLabel.Margin = new Padding(10, 5, 0, 0);
            menuFlowLayoutPanel.Controls.Add(numLabel);

            numNUD = new NumericUpDown();
            numNUD.Minimum = 50;
            numNUD.Maximum = 100000;
            numNUD.Value = 50;
            menuFlowLayoutPanel.Controls.Add(numNUD);

            Button generateButton = new Button();
            generateButton.Text = "Generate...";
            generateButton.Dock = DockStyle.Right;
            generateButton.Click += GenerateButton_Click;
            menuFlowLayoutPanel.Controls.Add(generateButton);

            progressBar = new ProgressBar();
            progressBar.Style = ProgressBarStyle.Marquee;
            progressBar.MarqueeAnimationSpeed = 50;
            progressBar.Size = new Size(30, 20);
            progressBar.Margin = new Padding(10, 3, 0, 0);
            progressBar.Visible = false;
            menuFlowLayoutPanel.Controls.Add(progressBar);

            timeLabel = new Label();
            timeLabel.AutoSize = true;
            timeLabel.Margin = new Padding(10, 5, 0, 0);
            menuFlowLayoutPanel.Controls.Add(timeLabel);

            FlowLayoutPanel subMenuFlowLayoutPanel = new FlowLayoutPanel();
            subMenuFlowLayoutPanel.Dock = DockStyle.Fill;
            subMenuFlowLayoutPanel.FlowDirection = FlowDirection.LeftToRight;
            subMenuFlowLayoutPanel.Margin = new Padding(0);
            tableLayoutPanel.Controls.Add(subMenuFlowLayoutPanel, 0, 1);

            Button filterButton = new Button();
            filterButton.Text = "Filter...";
            filterButton.Click += FilterButton_Click;
            //tableLayoutPanel.Controls.Add(filterButton, 0, 1);
            subMenuFlowLayoutPanel.Controls.Add(filterButton);

            Button saveButton = new Button();
            saveButton.Text = "Save...";
            saveButton.Click += SaveButton_Click;
            //tableLayoutPanel.Controls.Add(saveButton, 0, 1);
            subMenuFlowLayoutPanel.Controls.Add(saveButton);

            popLabel = new Label();
            popLabel.Margin = new Padding(0, 10, 0, 0);
            popLabel.AutoSize = true;
            subMenuFlowLayoutPanel.Controls.Add(popLabel);

            TabControl tabControl = new TabControl();
            tabControl.Dock = DockStyle.Fill;
            tableLayoutPanel.Controls.Add(tabControl, 0, 2);

            TabPage dataGridViewTab = new TabPage("Raw");
            tabControl.TabPages.Add(dataGridViewTab);

            dataGridView = new DoubleBufferedDataGridView();
            dataGridView.Dock = DockStyle.Fill;
            dataGridView.ReadOnly = true;
            dataGridViewTab.Controls.Add(dataGridView);

            TabPage chartTab = new TabPage("Chart");
            tabControl.TabPages.Add(chartTab);

            TableLayoutPanel chartTableLayoutPanel = new TableLayoutPanel();
            chartTableLayoutPanel.Dock = DockStyle.Fill;
            chartTableLayoutPanel.ColumnCount = 2;
            chartTableLayoutPanel.RowCount = 1;
            chartTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 70));
            chartTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 30));
            chartTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
            //tableLayoutPanel.Controls.Add(chartTableLayoutPanel, 0, 2);
            chartTab.Controls.Add(chartTableLayoutPanel);

            chart = new Chart();
            chart.Dock = DockStyle.Fill;
            chartTableLayoutPanel.Controls.Add(chart, 0, 0);

            chartArea = new ChartArea("ChartArea");
            chartArea.AxisX.Title = "Value";
            chartArea.AxisY.Title = "Population";
            chart.ChartAreas.Add(chartArea);

            chartListBox = new ListBox();
            chartListBox.Dock = DockStyle.Fill;
            chartListBox.SelectedIndexChanged += ChartListBox_SelectedIndexChanged;
            chartTableLayoutPanel.Controls.Add(chartListBox, 1, 0);

            //Shared.Debug.Debug.UseDebug = true;
            Shared.Debug.Debug.ShowDebugWindow();

            return generateTab;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (ds == null) { return; }

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Json 파일 (*.json)|*.json";
            sfd.InitialDirectory = Application.StartupPath;

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                Exporter.ExportJson(sfd.FileName, ds);
            }
        }

        private void FilterButton_Click(object sender, EventArgs e)
        {
            if (filter == null) { return; }

            FilterDialog fd = new FilterDialog();

            if (fd.ShowDialog(ds.Tables["QuantitativeParameterDataTable"], ds.Tables["QualitativeParameterDataTable"], filter) == DialogResult.OK)
            {
                DrawChart();
            }
        }

        private void GenerateTab_Enter(object sender, EventArgs e)
        {
            InitParameter();
        }

        private async void GenerateButton_Click(object sender, EventArgs e)
        {
            if (parameters.Count == 0)
            {
                return;
            }

            if (ndsf == null)
            {
                ndsf = new NormalDistributionSettingsForm();
            }

            if (ndsf.ShowDialog(parameters) != DialogResult.OK)
            {
                return;
            }

            chart.Series.Clear();
            chart.Legends.Clear();
            chartListBox.Items.Clear();

            progressBar.Visible = true;
            tableLayoutPanel.Enabled = false;

            var sw = System.Diagnostics.Stopwatch.StartNew();
            ds = await Generator.GenerateCitizenAsync(parameters, (int)numNUD.Value);
            //ds = Generator.GenerateCitizen(parameters, (int)numNUD.Value);
            timeLabel.Text = string.Format("Generated in {0} Secs", sw.Elapsed.TotalSeconds);
            filter = new Filter("Filter");

            sw.Restart();
            DrawChart();
            timeLabel.Text += string.Format(" / Analysis in {0} Secs", sw.Elapsed.TotalSeconds);

            progressBar.Visible = false;
            tableLayoutPanel.Enabled = true;
        }

        private void ChartListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(enabledSeries != null)
            {
                enabledSeries.Enabled = false;
            }

            var tuple = serieses[chartListBox.SelectedIndex];
            enabledSeries = tuple.Item1;
            enabledSeries.Enabled = true;

            if (enabledSeries.ChartType == SeriesChartType.Line)
            {
                double maxY = enabledSeries.Points.FindMaxByValue("Y").YValues[0];
                int unit = (int)Math.Pow(10, (int)Math.Log10(maxY));
                chartArea.AxisY.Maximum = ((int)(maxY / unit) + 1) * unit;
                chartArea.AxisY.Interval = unit;
                chartArea.AxisX.Minimum = tuple.Item2;
                chartArea.AxisX.Maximum = tuple.Item3;
                chartArea.AxisX.Interval = Math.Pow(10, (int)Math.Log10(Math.Max(Math.Abs(tuple.Item2), Math.Abs(tuple.Item3))) - 1);
            }
        }

        DataSet ds;
        List<Parameter> parameters;
        List<Tuple<Series, int, int>> serieses;
        Filter filter;

        void InitParameter()
        {
            if(parameters == null)
            {
                parameters = new List<Parameter>();
            }

            List<Parameter> newParameters = new List<Parameter>();

            Queue<SchemeElement> bfs = new Queue<SchemeElement>();
            bfs.Enqueue(scheme.root);

            Parameter preexist;
            while (bfs.Count > 0)
            {
                SchemeElement sc = bfs.Dequeue();
                foreach (SchemeElement se in sc.Children)
                {
                    if(se.Type == SchemeElementType.Category)
                    {
                        if (se.Children != null) { bfs.Enqueue(se); }
                    }
                    else
                    {
                        preexist = null;
                        foreach (Parameter p in parameters)
                        {
                            if (p.schemeElement == se)
                            {
                                preexist = p;
                                break;
                            }
                        }

                        if (preexist != null)
                        {
                            parameters.Remove(preexist);
                            newParameters.Add(preexist);
                        }
                        else
                        {
                            if (se.Type == SchemeElementType.Quantitative)
                            {
                                Parameter param = new QuantitativeParameter(se);
                                //param.color = Color.Red;
                                newParameters.Add(param);
                            }
                            else
                            {
                                Parameter param = new QualitativeParameter(se);
                                //param.color = Color.Red;
                                newParameters.Add(param);
                            }
                        }
                    }
                }
            }
            bfs = null;

            //foreach (Parameter p in parameters)
            //{
            //    if(p.Type == ParameterType.Qualitative)
            //    {
            //        p.Categories.Clear();
            //    }
            //}

            foreach (Parameter p in newParameters)
            {
                p.Update();
            }

            parameters.Clear();
            parameters = null;
            parameters = newParameters;
            newParameters = null;
        }

        NormalDistributionSettingsForm ndsf;

        void DrawChart()
        {
            if (ds == null) { return; }

            var sw = System.Diagnostics.Stopwatch.StartNew();
            Shared.Debug.Debug.PrintLn("");

            DataTable cdt = SelectFromDataTable(ds.Tables["CitizenDataTable"], this.filter.ToFilterExpression());

            popLabel.Text = string.Format("Selected : {0}", cdt.Rows.Count);

            DataTable qndt = ds.Tables["QuantitativeParameterDataTable"];
            DataTable qldt = ds.Tables["QualitativeParameterDataTable"];
            Series series;
            Legend legend;
            DataRow parameterInfo;
            int max;
            int min;
            string columnName;
            int paramCount = cdt.Columns.Count;
            string[] categories;

            dataGridView.DataSource = cdt;

            if (serieses == null) { serieses = new List<Tuple<Series, int, int>>(); }
            serieses.Clear();

            chart.Series.Clear();
            chart.Legends.Clear();
            chartListBox.Items.Clear();

            for (int i = 0; i < paramCount; ++i)
            {
                columnName = cdt.Columns[i].ColumnName;

                series = new Series(columnName);
                series.Color = Color.Red;
                series.ChartArea = chartArea.Name;
                series.Legend = columnName;
                series.Enabled = false;
                chart.Series.Add(series);

                legend = new Legend(columnName);
                legend.Docking = Docking.Bottom;
                chart.Legends.Add(legend);

                if (cdt.Columns[i].DataType == typeof(int))
                {
                    series.ChartType = SeriesChartType.Line;

                    parameterInfo = qndt.Select(string.Format("Name = '{0}'", columnName))[0];
                    min = (int)parameterInfo["Min"];
                    max = (int)parameterInfo["Max"];

                    int[] cnt = new int[max - min + 1];
                    //DataColumn dataColumn = cdt.Columns[columnName];
                    int columnIndex = cdt.Columns.IndexOf(columnName);

                    foreach (DataRow row in cdt.AsEnumerable())
                    {
                        //++cnt[(int)row[dataColumn] - min];
                        ++cnt[(int)row[columnIndex] - min];
                    }

                    for (int j = 0; j <= max - min; ++j)
                    {
                        series.Points.Add(new DataPoint(min + j, cnt[j]));
                    }

                    serieses.Add(Tuple.Create(series, min, max));
                }
                else
                {
                    series.ChartType = SeriesChartType.Pie;
                    series.IsValueShownAsLabel = true;

                    parameterInfo = qldt.Select(string.Format("Name = '{0}'", columnName))[0];
                    categories = parameterInfo["Category"].ToString().Split(',');

                    //string filter;
                    DataRow[] rows = cdt.Select();
                    DataColumn dataColumn = cdt.Columns[columnName];
                    int count;

                    foreach (string category in categories)
                    {
                        //filter = string.Format("[{0}] = '{1}'", columnName, category);
                        //count = cdt.Select(filter).Length;
                        count = Generator.Select(rows, dataColumn, category).Length;

                        DataPoint dp = new DataPoint();
                        if (count == 0) { dp.LabelForeColor = Color.Transparent; }
                        dp.SetValueXY(string.Format("{0} ({1})", category, count), count);
                        series.Points.Add(dp);
                    }

                    serieses.Add(Tuple.Create(series, 0, 0));
                }

                chartListBox.Items.Add(columnName);
            }

            chartListBox.SelectedIndex = 0;

            chart.Update();
        }

        DataTable SelectFromDataTable(DataTable dataTable, string filterExpression)
        {
            DataTable newDataTable = dataTable.Clone();
            DataRow[] rows = dataTable.Select(filterExpression);
            foreach (DataRow row in rows)
            {
                newDataTable.ImportRow(row);
            }
            rows = null;

            return newDataTable;
        }
    }
}
