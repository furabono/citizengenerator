﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CitizenGenerator.GUI.GenerateTab
{
    public partial class FilterDialog : Form
    {
        DataTable quantitativeParameterDataTable;
        DataTable qualitativeParameterDataTable;

        public FilterDialog()
        {
            InitializeComponent();

            treeView.AfterSelect += TreeView_AfterSelect;
            treeView.KeyDown += TreeView_KeyDown;

            andButton.Click += AndButton_Click;
            orButton.Click += OrButton_Click;

            removeButton.Click += RemoveButton_Click;
            removeAllButton.Click += RemoveAllButton_Click;

            parameterComboBox.SelectedIndexChanged += ParameterComboBox_SelectedIndexChanged;
            addButton.Click += AddButton_Click;

            minNUD.Minimum = int.MinValue;
            minNUD.Maximum = int.MaxValue;
            maxNUD.Minimum = int.MinValue;
            maxNUD.Maximum = int.MaxValue;

            okButton.Click += OkButton_Click;
        }

        private void TreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (((FilterTreeNode)e.Node).IsLogicNode)
            {
                andButton.Enabled = true;
                orButton.Enabled = true;
                addButton.Enabled = true;

                if (e.Node == treeView.Nodes[0]) { removeButton.Enabled = false; }
                else { removeButton.Enabled = true; }
            }
            else
            {
                andButton.Enabled = false;
                orButton.Enabled = false;
                addButton.Enabled = false;
                removeButton.Enabled = true;
            }
        }

        private void TreeView_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }

        private void AndButton_Click(object sender, EventArgs e)
        {
            FilterLogicTreeNode newNode = new FilterLogicTreeNode(LogicType.AND);
            treeView.SelectedNode.Nodes.Add(newNode);
        }

        private void OrButton_Click(object sender, EventArgs e)
        {
            FilterLogicTreeNode newNode = new FilterLogicTreeNode(LogicType.OR);
            treeView.SelectedNode.Nodes.Add(newNode);
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            TreeNode parent = treeView.SelectedNode.Parent;
            treeView.SelectedNode.Remove();
            treeView.SelectedNode = parent;
        }

        private void RemoveAllButton_Click(object sender, EventArgs e)
        {
            treeView.Nodes[0].Nodes.Clear();
            treeView.SelectedNode = treeView.Nodes[0];
        }

        private void ParameterComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((ParameterComboBoxItem)parameterComboBox.SelectedItem).ParameterType == ParameterType.Quantitative)
            {
                quantitativePanel.Visible = true;
                qualitativePanel.Visible = false;

                minNUD.Value = 0;
                maxNUD.Value = 0;
            }
            else
            {
                quantitativePanel.Visible = false;
                qualitativePanel.Visible = true;

                categoryComboBox.Items.Clear();
                QualitativeParameterComboBoxItem item = (QualitativeParameterComboBoxItem)parameterComboBox.SelectedItem;
                categoryComboBox.Items.AddRange(item.Categories);
                categoryComboBox.SelectedIndex = 0;
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            FilterConditionTreeNode newNode;

            if (quantitativePanel.Visible)
            {
                QuantitativeParameterComboBoxItem item = (QuantitativeParameterComboBoxItem)parameterComboBox.SelectedItem;
                newNode = new FilterConditionTreeNode(item.Name, (int)minNUD.Value, (int)maxNUD.Value);
            }
            else
            {
                QualitativeParameterComboBoxItem item = (QualitativeParameterComboBoxItem)parameterComboBox.SelectedItem;
                newNode = new FilterConditionTreeNode(item.Name, (string)categoryComboBox.SelectedItem);
            }
            treeView.SelectedNode.Nodes.Add(newNode);
            treeView.SelectedNode.Expand();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        public DialogResult ShowDialog(DataTable quantitativeParameterDataTable, DataTable qualitativeParameterDataTable, Filter filter)
        {
            this.quantitativeParameterDataTable = quantitativeParameterDataTable;
            this.qualitativeParameterDataTable = qualitativeParameterDataTable;

            FilterTreeView.InitializeTreeViewWithFilter(filter, treeView);
            treeView.ExpandAll();

            parameterComboBox.Items.Clear();
            {
                int nameColumnIndex = quantitativeParameterDataTable.Columns.IndexOf("Name");
                int minColumnIndex = quantitativeParameterDataTable.Columns.IndexOf("Min");
                int maxColumnIndex = quantitativeParameterDataTable.Columns.IndexOf("Max");

                foreach (DataRow r in quantitativeParameterDataTable.Rows)
                {
                    parameterComboBox.Items.Add(ParameterComboBoxItem.Create((string)r[nameColumnIndex], (int)r[minColumnIndex], (int)r[maxColumnIndex]));
                }
            }

            {
                int nameColumnIndex = qualitativeParameterDataTable.Columns.IndexOf("Name");
                int categoryColumnIndex = qualitativeParameterDataTable.Columns.IndexOf("Category");

                foreach (DataRow r in qualitativeParameterDataTable.Rows)
                {
                    parameterComboBox.Items.Add(ParameterComboBoxItem.Create((string)r[nameColumnIndex], ((string)r[categoryColumnIndex]).Split(',')));
                }
            }

            parameterComboBox.SelectedIndex = 0;

            DialogResult dialogResult = base.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                filter.Root = FilterTreeView.TreeViewToFilter(treeView, filter.Name).Root;
            }

            this.quantitativeParameterDataTable = null;
            this.qualitativeParameterDataTable = null;
            parameterComboBox.Items.Clear();

            return dialogResult;
        }
    }
}
