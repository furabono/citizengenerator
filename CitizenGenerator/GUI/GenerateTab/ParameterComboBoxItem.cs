﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CitizenGenerator.GUI.GenerateTab
{
    abstract class ParameterComboBoxItem
    {
        string name;

        public string Name
        {
            get { return name; }
        }

        public abstract ParameterType ParameterType
        {
            get;
        }

        protected ParameterComboBoxItem(string name)
        {
            this.name = name;
        }

        public override string ToString()
        {
            return name;
        }

        public static ParameterComboBoxItem Create(string name, int min, int max)
        {
            return new QuantitativeParameterComboBoxItem(name, min, max);
        }

        public static ParameterComboBoxItem Create(string name, string[] categories)
        {
            return new QualitativeParameterComboBoxItem(name, categories);
        }
    }

    class QuantitativeParameterComboBoxItem : ParameterComboBoxItem
    {
        int min;
        int max;

        public int Min
        {
            get { return min; }
        }

        public int Max
        {
            get { return max; }
        }

        public override ParameterType ParameterType
        {
            get
            {
                return ParameterType.Quantitative;
            }
        }

        public QuantitativeParameterComboBoxItem(string name, int min, int max) : base(name)
        {
            this.min = min;
            this.max = max;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    class QualitativeParameterComboBoxItem : ParameterComboBoxItem
    {
        string[] categories;

        public string[] Categories
        {
            get { return categories; }
        }

        public override ParameterType ParameterType
        {
            get
            {
                return ParameterType.Qualitative;
            }
        }

        public QualitativeParameterComboBoxItem(string name, string[] categories) : base(name)
        {
            this.categories = categories;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
