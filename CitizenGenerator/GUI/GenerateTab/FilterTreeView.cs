﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CitizenGenerator.GUI.GenerateTab
{
    abstract class FilterTreeNode : TreeNode
    {
        public virtual bool IsLogicNode
        {
            get { return false; }
        }

        public abstract IFilterElement ToFilterElement();
    }

    class FilterLogicTreeNode : FilterTreeNode
    {
        LogicType logicType;

        public override bool IsLogicNode
        {
            get { return true; }
        }

        public FilterLogicTreeNode(LogicType logicType) : base()
        {
            this.logicType = logicType;

            Text = (logicType == LogicType.AND) ? ("AND") : ("OR");
        }

        public override IFilterElement ToFilterElement()
        {
            FilterLogicalElement fle = new FilterLogicalElement(logicType);
            foreach (FilterTreeNode ftn in Nodes)
            {
                fle.Children.Add(ftn.ToFilterElement());
            }

            return fle;
        }
    }

    class FilterConditionTreeNode : FilterTreeNode
    {
        IFilterElement filterElement;

        public FilterConditionTreeNode(string name, int min, int max) : base()
        {
            filterElement = new FilterQuantitativeConditionalElement(name, min, max);

            Text = filterElement.ToFilterExpression();
        }

        public FilterConditionTreeNode(string name, string category) : base()
        {
            filterElement = new FilterQualitativeConditionElement(name, category);

            Text = filterElement.ToFilterExpression();
        }

        public override IFilterElement ToFilterElement()
        {
            return filterElement;
        }
    }

    class FilterTreeView
    {
        public static void InitializeTreeViewWithFilter(Filter filter, TreeView treeView)
        {
            treeView.Nodes.Clear();
            treeView.Nodes.Add(FilterElementToFilterTreeNode(filter.Root));
        }

        public static Filter TreeViewToFilter(TreeView treeView, string name)
        {
            FilterTreeNode filterTreeNode = treeView.Nodes[0] as FilterTreeNode;
            if (filterTreeNode != null)
            {
                FilterLogicalElement root = filterTreeNode.ToFilterElement() as FilterLogicalElement;
                if (root != null)
                {
                    Filter filter = new Filter(name);
                    filter.Root = root;
                    return filter;
                }
            }

            return null;
        }

        static FilterTreeNode FilterElementToFilterTreeNode(IFilterElement filterElement)
        {
            FilterTreeNode filterTreeNode = null;
            switch(filterElement.Type)
            {
                case FilterElementType.Logical:
                    {
                        FilterLogicalElement filterLogicElement = (FilterLogicalElement)filterElement;
                        filterTreeNode = new FilterLogicTreeNode(filterLogicElement.LogicType);
                        foreach (IFilterElement child in filterLogicElement.Children)
                        {
                            filterTreeNode.Nodes.Add(FilterElementToFilterTreeNode(child));
                        }
                    }
                    break;

                case FilterElementType.QuantitativeConditional:
                    {
                        FilterQuantitativeConditionalElement filterQuantitativeConditionalElement = (FilterQuantitativeConditionalElement)filterElement;
                        filterTreeNode = new FilterConditionTreeNode(filterQuantitativeConditionalElement.Name, filterQuantitativeConditionalElement.Min, filterQuantitativeConditionalElement.Max);
                    }
                    break;

                case FilterElementType.QualitativeConditional:
                    {
                        FilterQualitativeConditionElement filterQualitativeConditionElement = (FilterQualitativeConditionElement)filterElement;
                        filterTreeNode = new FilterConditionTreeNode(filterQualitativeConditionElement.Name, filterQualitativeConditionElement.Category);
                    }
                    break;
            }

            return filterTreeNode;
        }
    }
}
