﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CitizenGenerator.GUI.GenerateTab;

namespace CitizenGenerator
{
    public partial class DistInfoDialog : Form
    {
        public DistInfoDialog()
        {
            InitializeComponent();

            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size;

            this.okButton.Click += OkButton_Click;

            meanNUD.Minimum = int.MinValue;
            meanNUD.Maximum = int.MaxValue;

            varianceNUD.Minimum = 0;
            varianceNUD.Maximum = int.MaxValue;

            minNUD.Minimum = int.MinValue;
            minNUD.Maximum = int.MaxValue;
            maxNUD.Minimum = int.MinValue;
            maxNUD.Maximum = int.MaxValue;

            conditionComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            conditionComboBox.SelectedIndexChanged += ConditionComboBox_SelectedIndexChanged;

            categoryComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void ConditionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(conditionComboBox.SelectedIndex == 0)
            {
                categoryComboBox.Items.Clear();
                categoryComboBox.Enabled = false;
            }
            else
            {
                categoryComboBox.Enabled = true;
                categoryComboBox.Items.Clear();
                categoryComboBox.Items.AddRange(qualitativeParameters[conditionComboBox.SelectedIndex - 1].Categories.ToArray());
                categoryComboBox.SelectedIndex = 0;
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if(meanNUD.Text.Length > 0 && varianceNUD.Text.Length > 0 && varianceNUD.Value > 0 && minNUD.Value <= maxNUD.Value)
            {
                distInfo.mean = (int)meanNUD.Value;
                distInfo.variance = (int)varianceNUD.Value;
                distInfo.min = (int)minNUD.Value;
                distInfo.max = (int)maxNUD.Value;
                
                if(conditionComboBox.SelectedIndex == 0)
                {
                    distInfo.condition = null;
                }
                else
                {
                    if (distInfo.condition == null) { distInfo.condition = new Target(); }
                    distInfo.condition.parameter = qualitativeParameters[conditionComboBox.SelectedIndex - 1];
                    distInfo.condition.value = categoryComboBox.SelectedIndex;
                }

                this.distInfo = null;
                this.qualitativeParameters = null;

                this.DialogResult = DialogResult.OK;
            }
        }

        public DialogResult ShowDialog(IEnumerable<QualitativeParameter> qualitativeParameters, DistInfo distInfo)
        {
            if (this.qualitativeParameters == null) { this.qualitativeParameters = new List<QualitativeParameter>(); }
            this.qualitativeParameters.Clear();
            this.qualitativeParameters.AddRange(qualitativeParameters);

            conditionComboBox.Enabled = true;
            this.conditionComboBox.Items.Clear();
            this.conditionComboBox.Items.Add("None");
            foreach (QualitativeParameter qp in this.qualitativeParameters)
            {
                this.conditionComboBox.Items.Add(qp.Name);
            }

            this.distInfo = distInfo;

            meanNUD.Value = (decimal)distInfo.mean;
            varianceNUD.Value = (decimal)distInfo.variance;

            minNUD.Value = distInfo.min;
            maxNUD.Value = distInfo.max;

            if (distInfo.condition == null)
            {
                this.conditionComboBox.SelectedIndex = 0;
            }
            else
            {
                for (int i = 0; i < this.qualitativeParameters.Count; ++i)
                {
                    if (distInfo.condition.parameter.ID == this.qualitativeParameters[i].ID)
                    {
                        this.conditionComboBox.SelectedIndex = i + 1;
                        this.categoryComboBox.SelectedIndex = distInfo.condition.value;
                        break;
                    }
                }
            }

            //this.qualitativeParameters.Clear();

            return base.ShowDialog();
        }

        public DialogResult ShowDialog(Target condition, DistInfo distInfo)
        {
            this.distInfo = distInfo;

            meanNUD.Value = (decimal)distInfo.mean;
            varianceNUD.Value = (decimal)distInfo.variance;

            minNUD.Value = distInfo.min;
            maxNUD.Value = distInfo.max;

            conditionComboBox.Items.Clear();
            conditionComboBox.Items.Add(condition.parameter.Name);
            conditionComboBox.SelectedIndex = 0;
            conditionComboBox.Enabled = false;

            categoryComboBox.Items.Clear();
            categoryComboBox.Items.Add(condition.CategoryName);
            categoryComboBox.SelectedIndex = 0;
            categoryComboBox.Enabled = false;

            DialogResult dialogResult = base.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                distInfo.condition = condition;
            }

            return dialogResult;
        }

        DistInfo distInfo;
        List<QualitativeParameter> qualitativeParameters;
    }
}
