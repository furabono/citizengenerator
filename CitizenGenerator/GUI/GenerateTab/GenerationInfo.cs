﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Citizen;

namespace CitizenGenerator.GUI.GenerateTab
{
    public enum ParameterType
    {
        Quantitative, Qualitative
    }

    public class ReferenceError : Exception
    {
        public override string Message
        {
            get { return "참조 오류"; }
        }
    }

    public class GenerationSettings
    {
        Parameter[] parameters;
        int[] order;

        public Parameter[] Parameters
        {
            get { return parameters; }
        }

        public IEnumerable<Parameter> OrderedParameters
        {
            get { return (from i in order select parameters[i]); }
        }

        public GenerationSettings(Parameter[] parameters)
        {
            this.order = Order(parameters);
            this.parameters = parameters;
        }

        public GenerationSettings(Parameter[] parameters, int[] order)
        {
            if (parameters.Length != order.Length) { }

            this.parameters = parameters.ToArray();
            this.order = order.ToArray();
        }

        private int[] Order(Parameter[] parameters)
        {
            var adj = new HashSet<Parameter>[parameters.Length];

            for (int i = 0; i < parameters.Length; ++i)
            {
                adj[i] = new HashSet<Parameter>();

                if (parameters[i].Type == ParameterType.Quantitative)
                {
                    var qp = (QuantitativeParameter)parameters[i];

                    foreach (QuantitativeGenerationInfo qgi in qp.GenerationInfos)
                    {
                        string script = qgi.Target.Script.ScriptText;
                        foreach (Parameter p in parameters)
                        {
                            if (script.Contains(p.Name)) { adj[i].Add(p); }
                        }
                    }
                }
                else
                {
                    var qp = (QualitativeParameter)parameters[i];

                    if (qp.Scripts != null)
                    {
                        foreach (Script<float> s in qp.Scripts)
                        {
                            string script = s.ScriptText;
                            foreach (Parameter p in parameters)
                            {
                                if (script.Contains(p.Name)) { adj[i].Add(p); }
                            }
                        }
                    }
                }
            }

            var order = new List<int>();
            while (order.Count < parameters.Length)
            {
                int i;
                for (i = 0; i < adj.Length; ++i)
                {
                    if (adj[i].Count == 0)
                    {
                        order.Add(i);

                        for (int j = 0; j < adj.Length; ++j)
                        {
                            adj[j].Remove(parameters[i]);
                        }

                        adj[i].Add(parameters[i]);

                        break;
                    }
                }

                if (i == adj.Length)
                {
                    order = null;
                    throw new ReferenceError();
                }
            }

            return order.ToArray();
        }
    }

    public abstract class Parameter
    {
        protected ParameterInfo parameterInfo;

        public abstract ParameterType Type
        {
            get;
        }

        public ParameterInfo ParameterInfo
        {
            get { return parameterInfo; }
        }

        public string Name
        {
            get { return parameterInfo.Name; }
        }

        public Parameter(ParameterInfo parameterInfo)
        {
            this.parameterInfo = parameterInfo;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class QuantitativeParameter : Parameter
    {
        List<QuantitativeGenerationInfo> generationInfos;

        public override ParameterType Type
        {
            get { return ParameterType.Quantitative; }
        }

        public new QuantitativeParameterInfo ParameterInfo
        {
            get { return (QuantitativeParameterInfo)parameterInfo; }
        }

        public QuantitativeGenerationInfo[] GenerationInfos
        {
            get { return generationInfos.ToArray(); }
        }

        public QuantitativeParameter(QuantitativeParameterInfo parameterInfo) : base(parameterInfo)
        {
            generationInfos = new List<QuantitativeGenerationInfo>();
        }

        public QuantitativeParameter(QuantitativeParameterInfo parameterInfo, params QuantitativeGenerationInfo[] generationInfos) : base(parameterInfo)
        {
            this.generationInfos = generationInfos.ToList();
        }

        public QuantitativeParameter AddGenerationInfo(QuantitativeGenerationInfo qgi)
        {
            if (qgi == null) { return null; }

            var tmp = generationInfos.ToList();
            tmp.Add(qgi);

            return new QuantitativeParameter(ParameterInfo, tmp.ToArray());
        }

        public QuantitativeParameter RemoveGenerationInfo(QuantitativeGenerationInfo qgi)
        {
            if (qgi == null) { return null; }

            var tmp = generationInfos.ToList();
            if (tmp.Remove(qgi)) { return null; }

            return new QuantitativeParameter(ParameterInfo, tmp.ToArray());
        }
    }

    public class QualitativeParameter : Parameter
    {
        int?[] proportions;
        int proportionSum;
        Script<float>[] scripts;

        public override ParameterType Type
        {
            get{ return ParameterType.Qualitative; }
        }

        public new QualitativeParameterInfo ParameterInfo
        {
            get { return (QualitativeParameterInfo)parameterInfo; }
        }

        public int?[] Proportions
        {
            get { return proportions; }
        }

        public int ProportionSum
        {
            get { return proportionSum; }
        }

        public Script<float>[] Scripts
        {
            get { return scripts; }
        }

        public QualitativeParameter(QualitativeParameterInfo parameterInfo) : base(parameterInfo)
        {
            this.proportions = new int?[parameterInfo.Categories.Length];
            this.proportionSum = 0;
        }

        // proprotions != null
        public QualitativeParameter(QualitativeParameterInfo parameterInfo, int?[] proportions, Script<float>[] scripts) : base(parameterInfo)
        {
            this.proportions = proportions;
            foreach (int? p in proportions)
            {
                proportionSum += p ?? 0;
            }

            this.scripts = scripts;
        }
    }

    public enum ScriptType
    {
        Expression, Function
    }

    public class Script<T>
    {
        ScriptType type;
        string scriptText;
        
        public ScriptType Type
        {
            get { return type; }
        }

        public string ScriptText
        {
            get { return scriptText; }
        }

        public Script(ScriptType type, string scriptText)
        {
            this.type = type;
            this.scriptText = scriptText;
        }
    }

    public class Target
    {
        Script<bool> script;

        public Script<bool> Script
        {
            get { return script; }
        }

        public Target(Script<bool> script)
        {
            this.script = script;
        }
    }

    public enum QuantitativeGenerationInfoType
    {
        Random, Script, ProbabilityDistribution
    }

    public abstract class QuantitativeGenerationInfo
    {
        //protected QuantitativeParameterInfo quantitativeParameterInfo;
        protected Target target;

        public abstract QuantitativeGenerationInfoType Type
        {
            get;
        }

        //public QuantitativeParameterInfo QuantitativeParameterInfo
        //{
        //    get { return quantitativeParameterInfo; }
        //}

        public Target Target
        {
            get { return target; }
        }

        public QuantitativeGenerationInfo(Target target)
        {
            this.target = target;
        }

        //public QuantitativeGenerationInfo SetCondition(Condition condition)
        //{
            
        //}
    }

    public class RandomQuantitativeGenerationInfo : QuantitativeGenerationInfo
    {
        public override QuantitativeGenerationInfoType Type
        {
            get { return QuantitativeGenerationInfoType.Random; }
        }

        public RandomQuantitativeGenerationInfo(Target target) : base(target)
        { }

        //public new RandomQuantitativeGenerationInfo SetCondition(Condition condition)
        //{
        //    if (condition == null) { return null; }
        //    if (condition == this.condition) { return null; }

        //    return new RandomQuantitativeGenerationInfo(condition);
        //}
    }

    public class ScriptQuantitativeGenerationInfo : QuantitativeGenerationInfo
    {
        Script<int> script;

        public override QuantitativeGenerationInfoType Type
        {
            get { return QuantitativeGenerationInfoType.Script; }
        }

        public Script<int> Script
        {
            get { return script; }
        }

        public ScriptQuantitativeGenerationInfo(Target target, Script<int> script) : base(target)
        {
            this.script = script;
        }

        //public new ScriptQuantitativeGenerationInfo SetCondition(Condition condition)
        //{
        //    if (condition == null) { return null; }
        //    if (condition == this.condition) { return null; }

        //    return new ScriptQuantitativeGenerationInfo(condition, this.script);
        //}

        //public ScriptQuantitativeGenerationInfo SetScript(Script<int> script)
        //{
        //    if (script == null) { return null; }
        //    if (script == this.script) { return null; }

        //    return new ScriptQuantitativeGenerationInfo(this.condition, script);
        //}
    }

    public class ProbabilityDistributionQuantitativeGenerationInfo : QuantitativeGenerationInfo
    {
        DistributionInfo[] distributionInfos;

        public override QuantitativeGenerationInfoType Type
        {
            get { return QuantitativeGenerationInfoType.ProbabilityDistribution; }
        }

        public DistributionInfo[] DistributionInfos
        {
            get { return distributionInfos; }
        }

        public ProbabilityDistributionQuantitativeGenerationInfo(Target target, DistributionInfo[] distributionInfos) : base(target)
        {
            this.distributionInfos = distributionInfos.ToArray();
        }

        // return null cases -> Exception ?
        public ProbabilityDistributionQuantitativeGenerationInfo AddDistributionInfo(DistributionInfo distributionInfo)
        {
            if (distributionInfos == null) { return null; }
            if (distributionInfos.Contains(distributionInfo)) { return null; }

            var tmp = distributionInfos.ToList();
            tmp.Add(distributionInfo);
            return new ProbabilityDistributionQuantitativeGenerationInfo(this.target, tmp.ToArray());
        }

        public ProbabilityDistributionQuantitativeGenerationInfo RemoveDistributionInfo(DistributionInfo distributionInfo)
        {
            if (distributionInfos == null) { return null; }

            var tmp = distributionInfos.ToList();
            if (tmp.Remove(distributionInfo)) { return new ProbabilityDistributionQuantitativeGenerationInfo(this.target, tmp.ToArray()); }

            return null;
        }
    }

    public enum DistributionType
    {
        Normal
    }

    public abstract class DistributionInfo
    {
        protected int min;
        protected int max;

        public abstract DistributionType Type
        {
            get;
        }

        public int Min
        {
            get { return min; }
        }

        public int Max
        {
            get { return max; }
        }

        public DistributionInfo(int min, int max)
        {
            this.min = min;
            this.max = max;
        }

        public abstract override bool Equals(object obj);
        public abstract override int GetHashCode();
    }

    public class NormalDistributionInfo : DistributionInfo
    {
        float mean;
        float variance;

        public override DistributionType Type
        {
            get { return DistributionType.Normal; }
        }

        public float Mean
        {
            get { return mean; }
        }

        public float Variance
        {
            get { return variance; }
        }

        public NormalDistributionInfo(int min, int max, float mean, float variance) : base(min, max)
        {
            this.mean = mean;
            this.variance = variance;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }

            var ndi = obj as NormalDistributionInfo;
            if (ndi == null) { return false; }

            return Equals(ndi);
        }

        public bool Equals(NormalDistributionInfo normalDistributionInfo)
        {
            return (this.min == normalDistributionInfo.min
                && this.max == normalDistributionInfo.max
                && this.mean == normalDistributionInfo.mean
                && this.variance == normalDistributionInfo.variance);
        }

        public override int GetHashCode()
        {
            return (Type.GetHashCode() ^ min ^ max ^ mean.GetHashCode() ^ variance.GetHashCode());
        }
    }
}
