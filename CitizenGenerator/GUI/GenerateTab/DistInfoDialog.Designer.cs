﻿namespace CitizenGenerator
{
    partial class DistInfoDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.meanNUD = new System.Windows.Forms.NumericUpDown();
            this.varianceNUD = new System.Windows.Forms.NumericUpDown();
            this.okButton = new System.Windows.Forms.Button();
            this.conditionLabel = new System.Windows.Forms.Label();
            this.conditionComboBox = new System.Windows.Forms.ComboBox();
            this.categoryComboBox = new System.Windows.Forms.ComboBox();
            this.minLabel = new System.Windows.Forms.Label();
            this.minNUD = new System.Windows.Forms.NumericUpDown();
            this.maxLabel = new System.Windows.Forms.Label();
            this.maxNUD = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.meanNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.varianceNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxNUD)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mean";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(154, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "Variance";
            // 
            // meanNUD
            // 
            this.meanNUD.Location = new System.Drawing.Point(55, 20);
            this.meanNUD.Name = "meanNUD";
            this.meanNUD.Size = new System.Drawing.Size(77, 21);
            this.meanNUD.TabIndex = 0;
            // 
            // varianceNUD
            // 
            this.varianceNUD.Location = new System.Drawing.Point(215, 20);
            this.varianceNUD.Name = "varianceNUD";
            this.varianceNUD.Size = new System.Drawing.Size(77, 21);
            this.varianceNUD.TabIndex = 1;
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(120, 152);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 6;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // conditionLabel
            // 
            this.conditionLabel.AutoSize = true;
            this.conditionLabel.Location = new System.Drawing.Point(13, 102);
            this.conditionLabel.Name = "conditionLabel";
            this.conditionLabel.Size = new System.Drawing.Size(58, 12);
            this.conditionLabel.TabIndex = 5;
            this.conditionLabel.Text = "Condition";
            // 
            // conditionComboBox
            // 
            this.conditionComboBox.FormattingEnabled = true;
            this.conditionComboBox.Location = new System.Drawing.Point(24, 117);
            this.conditionComboBox.Name = "conditionComboBox";
            this.conditionComboBox.Size = new System.Drawing.Size(118, 20);
            this.conditionComboBox.TabIndex = 4;
            // 
            // categoryComboBox
            // 
            this.categoryComboBox.FormattingEnabled = true;
            this.categoryComboBox.Location = new System.Drawing.Point(172, 117);
            this.categoryComboBox.Name = "categoryComboBox";
            this.categoryComboBox.Size = new System.Drawing.Size(121, 20);
            this.categoryComboBox.TabIndex = 5;
            // 
            // minLabel
            // 
            this.minLabel.AutoSize = true;
            this.minLabel.Location = new System.Drawing.Point(23, 60);
            this.minLabel.Name = "minLabel";
            this.minLabel.Size = new System.Drawing.Size(26, 12);
            this.minLabel.TabIndex = 8;
            this.minLabel.Text = "Min";
            // 
            // minNUD
            // 
            this.minNUD.Location = new System.Drawing.Point(55, 58);
            this.minNUD.Name = "minNUD";
            this.minNUD.Size = new System.Drawing.Size(77, 21);
            this.minNUD.TabIndex = 2;
            // 
            // maxLabel
            // 
            this.maxLabel.AutoSize = true;
            this.maxLabel.Location = new System.Drawing.Point(170, 59);
            this.maxLabel.Name = "maxLabel";
            this.maxLabel.Size = new System.Drawing.Size(30, 12);
            this.maxLabel.TabIndex = 9;
            this.maxLabel.Text = "Max";
            // 
            // maxNUD
            // 
            this.maxNUD.Location = new System.Drawing.Point(215, 57);
            this.maxNUD.Name = "maxNUD";
            this.maxNUD.Size = new System.Drawing.Size(77, 21);
            this.maxNUD.TabIndex = 3;
            // 
            // DistInfoDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 185);
            this.Controls.Add(this.maxLabel);
            this.Controls.Add(this.minLabel);
            this.Controls.Add(this.categoryComboBox);
            this.Controls.Add(this.conditionComboBox);
            this.Controls.Add(this.conditionLabel);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.maxNUD);
            this.Controls.Add(this.varianceNUD);
            this.Controls.Add(this.minNUD);
            this.Controls.Add(this.meanNUD);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DistInfoDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "DistInfoDialog";
            ((System.ComponentModel.ISupportInitialize)(this.meanNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.varianceNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxNUD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown meanNUD;
        private System.Windows.Forms.NumericUpDown varianceNUD;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label conditionLabel;
        private System.Windows.Forms.ComboBox conditionComboBox;
        private System.Windows.Forms.ComboBox categoryComboBox;
        private System.Windows.Forms.Label minLabel;
        private System.Windows.Forms.NumericUpDown minNUD;
        private System.Windows.Forms.Label maxLabel;
        private System.Windows.Forms.NumericUpDown maxNUD;
    }
}