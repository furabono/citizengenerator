﻿namespace CitizenGenerator.GUI
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.schemeTreeView = new CitizenGenerator.SchemeTreeView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.schemeLoadButton = new System.Windows.Forms.Button();
            this.schemeSaveButton = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.settingTabPage = new System.Windows.Forms.TabPage();
            this.generationTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tabControl1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(884, 512);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.schemeTreeView, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(259, 506);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // schemeTreeView
            // 
            this.schemeTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.schemeTreeView.Location = new System.Drawing.Point(3, 43);
            this.schemeTreeView.Name = "schemeTreeView";
            this.schemeTreeView.Size = new System.Drawing.Size(253, 460);
            this.schemeTreeView.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.schemeSaveButton);
            this.panel1.Controls.Add(this.schemeLoadButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(253, 34);
            this.panel1.TabIndex = 1;
            // 
            // schemeLoadButton
            // 
            this.schemeLoadButton.Location = new System.Drawing.Point(4, 4);
            this.schemeLoadButton.Name = "schemeLoadButton";
            this.schemeLoadButton.Size = new System.Drawing.Size(75, 23);
            this.schemeLoadButton.TabIndex = 0;
            this.schemeLoadButton.Text = "Load";
            this.schemeLoadButton.UseVisualStyleBackColor = true;
            // 
            // schemeSaveButton
            // 
            this.schemeSaveButton.Location = new System.Drawing.Point(85, 4);
            this.schemeSaveButton.Name = "schemeSaveButton";
            this.schemeSaveButton.Size = new System.Drawing.Size(75, 23);
            this.schemeSaveButton.TabIndex = 1;
            this.schemeSaveButton.Text = "Save";
            this.schemeSaveButton.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.settingTabPage);
            this.tabControl1.Controls.Add(this.generationTabPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(268, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(613, 506);
            this.tabControl1.TabIndex = 1;
            // 
            // settingTabPage
            // 
            this.settingTabPage.Location = new System.Drawing.Point(4, 22);
            this.settingTabPage.Name = "settingTabPage";
            this.settingTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.settingTabPage.Size = new System.Drawing.Size(605, 480);
            this.settingTabPage.TabIndex = 0;
            this.settingTabPage.Text = "Settings";
            this.settingTabPage.UseVisualStyleBackColor = true;
            // 
            // generationTabPage
            // 
            this.generationTabPage.Location = new System.Drawing.Point(4, 22);
            this.generationTabPage.Name = "generationTabPage";
            this.generationTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.generationTabPage.Size = new System.Drawing.Size(605, 480);
            this.generationTabPage.TabIndex = 1;
            this.generationTabPage.Text = "Generation";
            this.generationTabPage.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 512);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private SchemeTreeView schemeTreeView;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button schemeSaveButton;
        private System.Windows.Forms.Button schemeLoadButton;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage settingTabPage;
        private System.Windows.Forms.TabPage generationTabPage;
    }
}