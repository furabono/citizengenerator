﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Citizen
{
    public abstract class Distribution
    {
        public Condition Condition
        {
            get { return condition; }
        }

        public int Min
        {
            get { return min; }
        }

        public int Max
        {
            get { return max; }
        }

        public int Mean
        {
            get { return mean; }
        }

        public float Variance
        {
            get { return variance; }
        }

        Condition condition;
        int min;
        int max;
        int mean;
        float variance;
    }

    public class Condition
    {
        public QualitativeParameterInfo Target
        {
            get { return target; }
        }

        public uint Value
        {
            get { return value; }
        }

        QualitativeParameterInfo target;
        uint value;
    }
}
