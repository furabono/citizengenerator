﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Shared.Citizen
{
    public class Civil : IEnumerable<Citizen>
    {
        public Civil(ParameterInfoCollection parameterInfoCollection, Citizen[] citizens)
        {
            this.parameterInfoCollection = parameterInfoCollection;

            MAX_AGE = 120;
            age = new List<Citizen>[MAX_AGE];
            for (int i = 0; i < MAX_AGE; ++i)
            {
                age[i] = new List<Citizen>();
            }

            foreach (Citizen c in citizens)
            {
                age[c["나이"]].Add(c);
            }

            head = 0;
            count = citizens.Length;
        }

        IEnumerator<Citizen> IEnumerable<Citizen>.GetEnumerator()
        {
            for (int i = 0; i < MAX_AGE; ++i)
            {
                foreach (Citizen c in this[i])
                {
                    yield return c;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < MAX_AGE; ++i)
            {
                foreach (Citizen c in this[i])
                {
                    yield return c;
                }
            }
        }

        public Citizen[] this[int age]
        {
            get { return this.age[(head + age + MAX_AGE - 1) % MAX_AGE].ToArray(); }
        }

        public ParameterInfoCollection Parameter
        {
            get { return parameterInfoCollection; }
        }

        public int Count
        {
            get { return count; }
        }

        readonly int MAX_AGE;
        List<Citizen>[] age;
        int head;
        ParameterInfoCollection parameterInfoCollection;
        int count;
    }
}
