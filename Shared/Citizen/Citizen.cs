﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Citizen
{
    public class Citizen
    {
        ParameterInfoCollection parameterInfoCollection;
        int[] parameters;
        Dictionary<Class.Policy, float> evaluation;

        public ParameterInfoCollection ParameterInfoCollection
        {
            get { return parameterInfoCollection; }
        }

        public dynamic this[int parameterIndex]
        {
            get
            {
                if (parameterInfoCollection[parameterIndex].Type == ParameterType.Quantitative)
                {
                    return parameters[parameterIndex];
                }
                else
                {
                    var qpi = (QualitativeParameterInfo)parameterInfoCollection[parameterIndex];
                    return qpi.Categories[parameters[parameterIndex]];
                }
            }
        }

        public dynamic this[string parameterName]
        {
            get
            {
                return parameters[parameterInfoCollection.GetIndex(parameterName)];
            }
        }

        public Citizen(ParameterInfoCollection parameterInfoCollection, params int[] parameters)
        {
            if (parameterInfoCollection.Count != parameters.Length) { throw new CitizenInitializerParameterException(); }

            this.parameterInfoCollection = parameterInfoCollection;
            this.parameters = parameters.ToArray();
        }

        public int GetRaw(int parameterIndex)
        {
            return parameters[parameterIndex];
        }

        public int GetRaw(string parameterName)
        {
            return parameters[parameterInfoCollection.GetIndex(parameterName)];
        }

        public bool SetRaw(int parameterIndex, int value)
        {
            if (parameterInfoCollection[parameterIndex].IsValueValid(value))
            {
                parameters[parameterIndex] = value;
                return true;
            }
            return false;
        }

        public bool SetRaw(string parameterName, int value)
        {
            return SetRaw(parameterInfoCollection.GetIndex(parameterName), value);
        }
    }

    public class CitizenInitializerParameterException : Exception
    {
        public CitizenInitializerParameterException() : base("Citizen Initializer Parameter Error")
        { }
    }
}