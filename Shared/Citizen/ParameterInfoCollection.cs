﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Citizen
{
    public class ParameterInfoCollection : IEnumerable<ParameterInfo>
    {
        public ParameterInfoCollection(params ParameterInfo[] parameterInfos)
        {
            nameIndexMap = new Dictionary<string, int>();
            for (int i = 0; i < parameterInfos.Length; ++i)
            {
                nameIndexMap.Add(parameterInfos[i].Name, i);
            }

            parameterInfoList = parameterInfos.ToList();
        }

        IEnumerator<ParameterInfo> IEnumerable<ParameterInfo>.GetEnumerator()
        {
            return parameterInfoList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return parameterInfoList.GetEnumerator();
        }

        public int GetIndex(string name)
        {
            return nameIndexMap[name];
        }

        public ParameterInfo this[int index]
        {
            get { return parameterInfoList[index]; }
            //set { parameterInfoList[index] = value; }
        }

        public ParameterInfo this[string name]
        {
            get { return parameterInfoList[nameIndexMap[name]]; }
        }

        public int Count
        {
            get { return parameterInfoList.Count; }
        }

        List<ParameterInfo> parameterInfoList;
        Dictionary<string, int> nameIndexMap;
    }
}
