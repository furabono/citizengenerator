﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace Shared.IO
{
    public class JsonWriter
    {
        public static void WritePolicyData(string filename, Class.Policy policy)
        {
            using (StreamWriter sw = new StreamWriter(File.Open(filename, FileMode.Create)))
            using (JsonTextWriter writer = new JsonTextWriter(sw))
            {
                PolicyToJsonObjectRev1(policy).WriteTo(writer);
            }
        }

        private static JObject PolicyToJsonObjectRev0(Class.Policy policy)
        {
            JObject root = new JObject();
            root.Add("Revision", new JValue(0));
            root.Add("Name", new JValue(policy.Name));
            root.Add("Variable", new JArray(from v in policy.Variables
                                            select new JObject(new JProperty("Name", v.Key),
                                                               new JProperty("Value", v.Value))
                                ));
            root.Add("Script", new JValue(policy.Result));

            return root;
        }

        private static JObject PolicyToJsonObjectRev1(Class.Policy policy)
        {
            JObject root = new JObject();
            root.Add("Revision", new JValue(1));
            root.Add("Name", new JValue(policy.Name));
            root.Add("Variable", new JArray(from v in policy.Variables
                                            select new JObject(new JProperty("Name", v.Key),
                                                               new JProperty("Value", v.Value))
                                ));
            root.Add("Memory1", new JValue(policy.Memory1));
            root.Add("Memory2", new JValue(policy.Memory2));
            root.Add("Memory3", new JValue(policy.Memory3));
            root.Add("Result", new JValue(policy.Result));

            return root;
        }
    }
}
