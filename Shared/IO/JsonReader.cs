﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace Shared.IO
{
    public class JsonReader
    {
        public static Citizen.Civil ReadCivilData(string filename)
        {
            JObject root;
            using (StreamReader file = File.OpenText(filename))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                root = JObject.Load(reader);
            }

            var pic = JsonObjectToParameterInfoCollection(root);
            return new Citizen.Civil(pic, JsonObjectToCitizenArray(root, pic));
        }

        public static Task<Citizen.Civil> ReadCivilDataAsync(string filename)
        {
            return Task.Run(() => { return ReadCivilData(filename); });
        }

        private static Citizen.Citizen[] JsonObjectToCitizenArray(JObject root, Citizen.ParameterInfoCollection parameterInfoCollection)
        {
            JArray citizen = (JArray)root["Citizen"];
            Citizen.Citizen[] citizens = new Citizen.Citizen[citizen.Count];
            for (int i = 0; i < citizen.Count; ++i)
            {
                citizens[i] = new Citizen.Citizen(parameterInfoCollection, (from token in citizen[i]
                                                                            select token.Value<int>()).ToArray());
            }

            return citizens;
        }

        private static Citizen.ParameterInfoCollection JsonObjectToParameterInfoCollection(JObject root)
        {
            JArray arr = (JArray)root["Parameter"];
            List<Citizen.ParameterInfo> parameters = new List<Citizen.ParameterInfo>();

            string type;
            string name;

            foreach (JObject obj in arr)
            {
                type = obj["Type"].Value<string>();
                name = obj["Name"].Value<string>();

                if (type == "Quantitative")
                {
                    parameters.Add(new Citizen.QuantitativeParameterInfo(name, obj["Min"].Value<int>(), obj["Max"].Value<int>()));
                }
                else
                {
                    parameters.Add(new Citizen.QualitativeParameterInfo(name, (from token in obj["Category"]
                                                                           select token.Value<string>()).ToArray()));
                }
            }

            return new Citizen.ParameterInfoCollection(parameters.ToArray());
        }

        public static Class.Policy ReadPolicyData(string filename)
        {
            JObject root;
            using (StreamReader file = File.OpenText(filename))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                root = JObject.Load(reader);
            }

            int revision;
            if(root["Revision"] == null)
            {
                revision = 0;
            }
            else
            {
                revision = root["Revision"].Value<int>();
            }

            Class.Policy policy;

            switch (revision)
            {
                case 0: { policy = JsonObjectToPolicyRev0(root); } break;
                case 1: { policy =  JsonObjectToPolicyRev1(root); } break;
                default: { return null; }
            }

            if (revision < 1)
            {
                JsonWriter.WritePolicyData(filename, policy);
            }

            return policy;
        }

        public static Task<Class.Policy> ReadPolicyDataAsync(string filename)
        {
            return Task.Run(() => { return ReadPolicyData(filename); });
        }

        private static Class.Policy JsonObjectToPolicyRev0(JObject root)
        {
            string name = root["Name"].Value<string>();
            var variables = (from v in root["Variable"]
                             select new KeyValuePair<string, float>(v["Name"].Value<string>(), v["Value"].Value<float>())).ToArray();
            string script = root["Script"].Value<string>();

            return new Class.Policy(name, variables, script);
        }

        private static Class.Policy JsonObjectToPolicyRev1(JObject root)
        {
            string name = root["Name"].Value<string>();
            var variables = (from v in root["Variable"]
                             select new KeyValuePair<string, float>(v["Name"].Value<string>(), v["Value"].Value<float>())).ToArray();
            string memory1 = root["Memory1"].Value<string>();
            string memory2 = root["Memory2"].Value<string>();
            string memory3 = root["Memory3"].Value<string>();
            string result = root["Result"].Value<string>();

            return new Class.Policy(name, variables, memory1, memory2, memory3, result);
        }
    }
}
