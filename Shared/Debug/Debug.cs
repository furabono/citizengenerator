﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace Shared.Debug
{
    public static class Debug
    {
        private static DebugForm df;

        public static bool UseDebug
        {
            get { return df != null; }
            set
            {
                if (df == null && value == true) { df = new DebugForm(); }
                else if(df != null && value == false)
                {
                    df.Close();
                    df = null;
                }
            }
        }

        public static void Print(string str)
        {
            if (df != null) { df.Print(str); }
        }

        public static void Print(object obj)
        {
            Print(obj.ToString());
        }

        public static void PrintLn(string str)
        {
            Print(Environment.NewLine + str);
        }

        public static void PrintLn(object obj)
        {
            PrintLn(obj.ToString());
        }

        public static void ShowDebugWindow()
        {
            if (df != null) { df.Show(); }
        }

        public static void CloseDebugWindow()
        {
            if (df != null) { df.Close(); }
        }
    }
}
