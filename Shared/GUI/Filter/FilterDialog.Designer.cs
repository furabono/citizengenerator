﻿namespace Shared.Filter
{
    partial class FilterDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView = new System.Windows.Forms.TreeView();
            this.andButton = new System.Windows.Forms.Button();
            this.orButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.removeAllButton = new System.Windows.Forms.Button();
            this.qualitativePanel = new System.Windows.Forms.Panel();
            this.categoryComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.parameterComboBox = new System.Windows.Forms.ComboBox();
            this.quantitativePanel = new System.Windows.Forms.Panel();
            this.maxNUD = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.minNUD = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.addButton = new System.Windows.Forms.Button();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.okButton = new System.Windows.Forms.Button();
            this.qualitativePanel.SuspendLayout();
            this.quantitativePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minNUD)).BeginInit();
            this.flowLayoutPanel.SuspendLayout();
            this.groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView
            // 
            this.treeView.HideSelection = false;
            this.treeView.Location = new System.Drawing.Point(12, 12);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(286, 261);
            this.treeView.TabIndex = 0;
            // 
            // andButton
            // 
            this.andButton.Location = new System.Drawing.Point(304, 12);
            this.andButton.Name = "andButton";
            this.andButton.Size = new System.Drawing.Size(75, 23);
            this.andButton.TabIndex = 1;
            this.andButton.Text = "And";
            this.andButton.UseVisualStyleBackColor = true;
            // 
            // orButton
            // 
            this.orButton.Location = new System.Drawing.Point(304, 42);
            this.orButton.Name = "orButton";
            this.orButton.Size = new System.Drawing.Size(75, 23);
            this.orButton.TabIndex = 2;
            this.orButton.Text = "OR";
            this.orButton.UseVisualStyleBackColor = true;
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(304, 93);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 3;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            // 
            // removeAllButton
            // 
            this.removeAllButton.Location = new System.Drawing.Point(304, 122);
            this.removeAllButton.Name = "removeAllButton";
            this.removeAllButton.Size = new System.Drawing.Size(75, 23);
            this.removeAllButton.TabIndex = 4;
            this.removeAllButton.Text = "RemoveAll";
            this.removeAllButton.UseVisualStyleBackColor = true;
            // 
            // qualitativePanel
            // 
            this.qualitativePanel.Controls.Add(this.categoryComboBox);
            this.qualitativePanel.Controls.Add(this.label1);
            this.qualitativePanel.Location = new System.Drawing.Point(3, 3);
            this.qualitativePanel.Name = "qualitativePanel";
            this.qualitativePanel.Size = new System.Drawing.Size(159, 41);
            this.qualitativePanel.TabIndex = 5;
            // 
            // categoryComboBox
            // 
            this.categoryComboBox.FormattingEnabled = true;
            this.categoryComboBox.Location = new System.Drawing.Point(21, 9);
            this.categoryComboBox.Name = "categoryComboBox";
            this.categoryComboBox.Size = new System.Drawing.Size(121, 20);
            this.categoryComboBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "=";
            // 
            // parameterComboBox
            // 
            this.parameterComboBox.FormattingEnabled = true;
            this.parameterComboBox.Location = new System.Drawing.Point(12, 24);
            this.parameterComboBox.Name = "parameterComboBox";
            this.parameterComboBox.Size = new System.Drawing.Size(121, 20);
            this.parameterComboBox.TabIndex = 5;
            // 
            // quantitativePanel
            // 
            this.quantitativePanel.Controls.Add(this.maxNUD);
            this.quantitativePanel.Controls.Add(this.label3);
            this.quantitativePanel.Controls.Add(this.minNUD);
            this.quantitativePanel.Controls.Add(this.label2);
            this.quantitativePanel.Location = new System.Drawing.Point(168, 3);
            this.quantitativePanel.Name = "quantitativePanel";
            this.quantitativePanel.Size = new System.Drawing.Size(224, 41);
            this.quantitativePanel.TabIndex = 7;
            // 
            // maxNUD
            // 
            this.maxNUD.Location = new System.Drawing.Point(130, 8);
            this.maxNUD.Name = "maxNUD";
            this.maxNUD.Size = new System.Drawing.Size(75, 21);
            this.maxNUD.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(110, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "~";
            // 
            // minNUD
            // 
            this.minNUD.Location = new System.Drawing.Point(26, 8);
            this.minNUD.Name = "minNUD";
            this.minNUD.Size = new System.Drawing.Size(78, 21);
            this.minNUD.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "in";
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.Controls.Add(this.qualitativePanel);
            this.flowLayoutPanel.Controls.Add(this.quantitativePanel);
            this.flowLayoutPanel.Location = new System.Drawing.Point(139, 11);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(397, 48);
            this.flowLayoutPanel.TabIndex = 8;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(251, 65);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 9;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.flowLayoutPanel);
            this.groupBox.Controls.Add(this.addButton);
            this.groupBox.Controls.Add(this.parameterComboBox);
            this.groupBox.Location = new System.Drawing.Point(13, 280);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(366, 95);
            this.groupBox.TabIndex = 10;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "Add Condition";
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(152, 389);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 10;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // FilterDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 423);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.removeAllButton);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.orButton);
            this.Controls.Add(this.andButton);
            this.Controls.Add(this.treeView);
            this.Name = "FilterDialog";
            this.Text = "FilterDialog";
            this.qualitativePanel.ResumeLayout(false);
            this.qualitativePanel.PerformLayout();
            this.quantitativePanel.ResumeLayout(false);
            this.quantitativePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minNUD)).EndInit();
            this.flowLayoutPanel.ResumeLayout(false);
            this.groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.Button andButton;
        private System.Windows.Forms.Button orButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button removeAllButton;
        private System.Windows.Forms.Panel qualitativePanel;
        private System.Windows.Forms.ComboBox categoryComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox parameterComboBox;
        private System.Windows.Forms.Panel quantitativePanel;
        private System.Windows.Forms.NumericUpDown maxNUD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown minNUD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.Button okButton;
    }
}