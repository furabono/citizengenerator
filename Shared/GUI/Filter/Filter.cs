﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shared.Filter
{
    public enum FilterElementType
    {
        Logical, QuantitativeConditional, QualitativeConditional
    }

    public enum LogicType
    {
        AND, OR
    }

    public interface IFilter
    {
        string ToFilterExpression();
    }

    public interface IFilterElement : IFilter
    {
        FilterElementType Type
        {
            get;
        }
    }

    public class FilterLogicalElement : IFilterElement
    {
        LogicType locgicType;
        List<IFilterElement> children;

        public LogicType LogicType
        {
            get { return locgicType; }
            set { LogicType = value; }
        }

        public List<IFilterElement> Children
        {
            get { return children; }
        }

        public FilterElementType Type
        {
            get
            {
                return FilterElementType.Logical;
            }
        }

        public FilterLogicalElement(LogicType logicType)
        {
            this.locgicType = logicType;
            children = new List<IFilterElement>();
        }

        public string ToFilterExpression()
        {
            if (children.Count == 0) { return ""; }

            string _operator = (locgicType == LogicType.AND) ? ("AND") : ("OR");
            string filter = "";
            foreach (IFilterElement child in children)
            {
                filter += string.Format("({0}) {1} ", child.ToFilterExpression(), _operator);
            }

            return filter.Substring(0, filter.Length - (_operator.Length + 2));
        }
    }

    public class FilterQuantitativeConditionalElement : IFilterElement
    {
        string name;
        int min;
        int max;

        public string Name
        {
            get { return name; }
        }

        public int Min
        {
            get { return min; }
        }

        public int Max
        {
            get { return max; }
        }

        public FilterElementType Type
        {
            get
            {
                return FilterElementType.QuantitativeConditional;
            }
        }

        public FilterQuantitativeConditionalElement(string name, int min, int max)
        {
            this.name = name;
            this.min = min;
            this.max = max;
        }

        public string ToFilterExpression()
        {
            if (min == max)
            {
                return string.Format("[{0}] = {1}", name, min);
            }
            else
            {
                return string.Format("[{0}] >= {1} AND [{0}] <= {2}", name, min, max);
            }
        }
    }

    public class FilterQualitativeConditionElement : IFilterElement
    {
        string name;
        string category;

        public string Name
        {
            get { return name; }
        }

        public string Category
        {
            get { return category; }
        }

        public FilterElementType Type
        {
            get
            {
                return FilterElementType.QualitativeConditional;
            }
        }

        public FilterQualitativeConditionElement(string name, string category)
        {
            this.name = name;
            this.category = category;
        }

        public string ToFilterExpression()
        {
            return string.Format("[{0}] = '{1}'", name, category);
        }
    }

    public class Filter : IFilter
    {
        string name;
        FilterLogicalElement root;

        public string Name
        {
            get { return name; }
        }

        public FilterLogicalElement Root
        {
            get { return root; }
            set { root = value; }
        }

        public Filter(string name)
        {
            this.name = name;
            root = new FilterLogicalElement(LogicType.AND);
        }

        public string ToFilterExpression()
        {
            return root.ToFilterExpression();
        }
    }
}
