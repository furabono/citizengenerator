﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// TODO

namespace Shared.GUI.Control
{
    public partial class ProbabilityDistributionQuantitativeDistributionInfoEditor 
        : UserControl, IEditor<Class.ProbabilityDistributionQuantitativeDistributionInfo>, IEditor<Class.QuantitativeDistributionInfo>
    {
        public ProbabilityDistributionQuantitativeDistributionInfoEditor()
        {
            InitializeComponent();

            addButton.Click += AddButton_Click;
            removeButton.Click += RemoveButton_Click;
            editButton.Click += EditButton_Click;

            died = new Dialog.ProbabilityDistributionInfoEditorDialog();
            distributionInfos = new List<Class.ProbabilityDistributionInfo>();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            Class.ProbabilityDistributionInfo di = null;
            while (died.ShowDialog(di) == DialogResult.OK)
            {
                di = died.Get;
                
                if (!di.isValid()) { MessageBox.Show("Invalid Value"); }
                else if (distributionInfos.Contains(di)) { MessageBox.Show("Same Distribution Already Exists"); }
                else
                {
                    distributionInfos.Add(di);
                    dataGridView.Rows.Add(DistributionInfoToString(di));
                    break;
                }
            }
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            int idx = dataGridView.SelectedRows[0].Index;
           distributionInfos.RemoveAt(idx);
            dataGridView.Rows.RemoveAt(idx);
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            int idx = dataGridView.SelectedRows[0].Index;
            Class.ProbabilityDistributionInfo di = distributionInfos[idx];
            distributionInfos.RemoveAt(idx);
            while (died.ShowDialog(di) == DialogResult.OK)
            {
                di = died.Get;

                if (!di.isValid()) { MessageBox.Show("Invalid Value"); }
                else if (distributionInfos.Contains(di)) { MessageBox.Show("Same Distribution Already Exists"); }
                else
                {
                    distributionInfos.Insert(idx, di);
                    dataGridView[0, idx].Value = DistributionInfoToString(di);
                    break;
                }
            }
        }

        Dialog.ProbabilityDistributionInfoEditorDialog died;
        //Class.ProbabilityDistributionQuantitativeGenerationInfo gi;
        List<Class.ProbabilityDistributionInfo> distributionInfos;

        // check obj is null?
        public void Init(Class.ProbabilityDistributionQuantitativeDistributionInfo obj)
        {
            distributionInfos = obj.DistributionInfos.ToList();

            dataGridView.Rows.Clear();
            foreach (Class.ProbabilityDistributionInfo di in distributionInfos)
            {
                dataGridView.Rows.Add(DistributionInfoToString(di));
            }

            targetEditor.Init(obj.Target);
        }

        // check obj is null?
        public void Init(Class.QuantitativeDistributionInfo obj)
        {
            if (obj.Type == Class.QuantitativeDistributionInfoType.ProbabilityDistribution)
            { Init((Class.ProbabilityDistributionQuantitativeDistributionInfo)obj); }
            else
            { Init(new Class.ProbabilityDistributionQuantitativeDistributionInfo(obj.Target, null)); }
        }

        public Class.ProbabilityDistributionQuantitativeDistributionInfo Get()
        {
            return new Class.ProbabilityDistributionQuantitativeDistributionInfo(targetEditor.Get(), distributionInfos.ToArray());
        }

        Class.QuantitativeDistributionInfo IEditor<Class.QuantitativeDistributionInfo>.Get()
        {
            return Get();
        }

        public System.Windows.Forms.Control AsControl()
        {
            return this;
        }

        private string DistributionInfoToString(Class.ProbabilityDistributionInfo obj)
        {
            switch(obj.Type)
            {
                case Class.ProbabilityDistributionType.Normal:
                    {
                        var ndi = (Class.NormalDistributionInfo)obj;
                        return string.Format("Normal\nMin:{0} Max:{1}\nMean:{2} Var:{3}", ndi.Min, ndi.Max, ndi.Mean, ndi.Variance);
                    }
            }

            return string.Empty;
        }
    }
}
