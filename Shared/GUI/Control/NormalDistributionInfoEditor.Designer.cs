﻿namespace Shared.GUI.Control
{
    partial class NormalDistributionInfoEditor
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.minLabel = new System.Windows.Forms.Label();
            this.minNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.maxLabel = new System.Windows.Forms.Label();
            this.maxNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.meanLabel = new System.Windows.Forms.Label();
            this.meanNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.varianceLabel = new System.Windows.Forms.Label();
            this.varNumericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.minNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meanNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.varNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // minLabel
            // 
            this.minLabel.AutoSize = true;
            this.minLabel.Location = new System.Drawing.Point(14, 16);
            this.minLabel.Name = "minLabel";
            this.minLabel.Size = new System.Drawing.Size(26, 12);
            this.minLabel.TabIndex = 0;
            this.minLabel.Text = "Min";
            // 
            // minNumericUpDown
            // 
            this.minNumericUpDown.Location = new System.Drawing.Point(59, 12);
            this.minNumericUpDown.Name = "minNumericUpDown";
            this.minNumericUpDown.Size = new System.Drawing.Size(62, 21);
            this.minNumericUpDown.TabIndex = 1;
            // 
            // maxLabel
            // 
            this.maxLabel.AutoSize = true;
            this.maxLabel.Location = new System.Drawing.Point(127, 16);
            this.maxLabel.Name = "maxLabel";
            this.maxLabel.Size = new System.Drawing.Size(30, 12);
            this.maxLabel.TabIndex = 2;
            this.maxLabel.Text = "Max";
            // 
            // maxNumericUpDown
            // 
            this.maxNumericUpDown.Location = new System.Drawing.Point(171, 12);
            this.maxNumericUpDown.Name = "maxNumericUpDown";
            this.maxNumericUpDown.Size = new System.Drawing.Size(60, 21);
            this.maxNumericUpDown.TabIndex = 3;
            // 
            // meanLabel
            // 
            this.meanLabel.AutoSize = true;
            this.meanLabel.Location = new System.Drawing.Point(14, 40);
            this.meanLabel.Name = "meanLabel";
            this.meanLabel.Size = new System.Drawing.Size(37, 12);
            this.meanLabel.TabIndex = 0;
            this.meanLabel.Text = "Mean";
            // 
            // meanNumericUpDown
            // 
            this.meanNumericUpDown.DecimalPlaces = 2;
            this.meanNumericUpDown.Location = new System.Drawing.Point(59, 36);
            this.meanNumericUpDown.Name = "meanNumericUpDown";
            this.meanNumericUpDown.Size = new System.Drawing.Size(62, 21);
            this.meanNumericUpDown.TabIndex = 1;
            // 
            // varianceLabel
            // 
            this.varianceLabel.AutoSize = true;
            this.varianceLabel.Location = new System.Drawing.Point(127, 40);
            this.varianceLabel.Name = "varianceLabel";
            this.varianceLabel.Size = new System.Drawing.Size(24, 12);
            this.varianceLabel.TabIndex = 2;
            this.varianceLabel.Text = "Var";
            // 
            // varNumericUpDown
            // 
            this.varNumericUpDown.DecimalPlaces = 2;
            this.varNumericUpDown.Location = new System.Drawing.Point(171, 36);
            this.varNumericUpDown.Name = "varNumericUpDown";
            this.varNumericUpDown.Size = new System.Drawing.Size(60, 21);
            this.varNumericUpDown.TabIndex = 3;
            // 
            // NormalDistributionInfoEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.varNumericUpDown);
            this.Controls.Add(this.maxNumericUpDown);
            this.Controls.Add(this.varianceLabel);
            this.Controls.Add(this.meanNumericUpDown);
            this.Controls.Add(this.maxLabel);
            this.Controls.Add(this.meanLabel);
            this.Controls.Add(this.minNumericUpDown);
            this.Controls.Add(this.minLabel);
            this.Name = "NormalDistributionInfoEditor";
            this.Size = new System.Drawing.Size(250, 70);
            ((System.ComponentModel.ISupportInitialize)(this.minNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meanNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.varNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label minLabel;
        private System.Windows.Forms.NumericUpDown minNumericUpDown;
        private System.Windows.Forms.Label maxLabel;
        private System.Windows.Forms.NumericUpDown maxNumericUpDown;
        private System.Windows.Forms.Label meanLabel;
        private System.Windows.Forms.NumericUpDown meanNumericUpDown;
        private System.Windows.Forms.Label varianceLabel;
        private System.Windows.Forms.NumericUpDown varNumericUpDown;
    }
}
