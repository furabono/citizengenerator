﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Shared.GUI.Control
{
    public class SchemeTreeView : TreeView
    {
        public Action<TreeNode> SelectedNodeChanged;

        public new TreeNode SelectedNode
        {
            get
            {
                return base.SelectedNode;
            }
            set
            {
                if (base.SelectedNode == null || !base.SelectedNode.Equals(value))
                {
                    base.SelectedNode = value;
                    SelectedNodeChanged(value);
                }
            }
        }
    }

    public abstract class SchemeNode : TreeNode
    {
        public abstract Class.SchemeElement SchemeElement
        {
            get;
        }

        public abstract string SchemeName
        {
            get;
            set;
        }

        public new void Remove()
        {
            base.Remove();

            SchemeElement.RemoveFromParent();
        }
    }

    public class CategoryNode : SchemeNode
    {
        Class.SchemeCategory schemeCategory;

        public CategoryNode(Class.SchemeCategory schemeCategory)
        {
            this.schemeCategory = schemeCategory;
            this.Text = schemeCategory.Name;
        }

        public override Class.SchemeElement SchemeElement
        {
            get
            {
                return schemeCategory;
            }
        }

        public override string SchemeName
        {
            get
            {
                return schemeCategory.Name;
            }

            set
            {
                schemeCategory.Name = value;
                this.Text = value;
            }
        }

        public void AddChild(SchemeNode schemeNode)
        {
            schemeCategory.AddChild(schemeNode.SchemeElement);
            this.Nodes.Add(schemeNode);
        }
    }

    public class QuantitativeParameterNode : SchemeNode
    {
        Class.SchemeQuantitativeParameter schemeParameter;

        public QuantitativeParameterNode(Class.SchemeQuantitativeParameter schemeParameter)
        {
            this.schemeParameter = schemeParameter;
            UpdateText();
        }

        public override Class.SchemeElement SchemeElement
        {
            get
            {
                return schemeParameter;
            }
        }

        private void UpdateText()
        {
            this.Text = string.Format("{0}({1},{2})", schemeParameter.Name, schemeParameter.Min, schemeParameter.Max);
        }

        public override string SchemeName
        {
            get
            {
                return schemeParameter.Name;
            }
            set
            {
                schemeParameter.Name = value;
                UpdateText();
            }
        }

        public int SchemeMin
        {
            get
            {
                return schemeParameter.Min;
            }
            set
            {
                schemeParameter.Min = value;
                UpdateText();
            }
        }

        public int SchemeMax
        {
            get
            {
                return schemeParameter.Max;
            }
            set
            {
                schemeParameter.Max = value;
                UpdateText();
            }
        }
    }

    public class QualitativeParameterNode : SchemeNode
    {
        Class.SchemeQualitativeParameter schemeQualitativeParameter;

        public override Class.SchemeElement SchemeElement
        {
            get
            {
                return schemeQualitativeParameter;
            }
        }

        public override string SchemeName
        {
            get
            {
                return schemeQualitativeParameter.Name;
            }

            set
            {
                schemeQualitativeParameter.Name = value;
                UpdateText();
            }
        }

        public QualitativeParameterNode(Class.SchemeQualitativeParameter schemeQualitativeParameter)
        {
            this.schemeQualitativeParameter = schemeQualitativeParameter;
            UpdateText();
        }

        private void UpdateText()
        {
            this.Text = schemeQualitativeParameter.Name + "(" + schemeQualitativeParameter.Categories.Length.ToString() + ")";
        }

        public void SetCategories(params string[] categories)
        {
            schemeQualitativeParameter.SetCategory(categories);
            UpdateText();
        }
    }

    public class SchemeNodeDragDropWrapper
    {
        SchemeNode schemeNode;

        public SchemeNode SchemeNode
        {
            get
            {
                return schemeNode;
            }
        }

        public SchemeNodeDragDropWrapper(SchemeNode schemeNode)
        {
            this.schemeNode = schemeNode;
        }
    }
}