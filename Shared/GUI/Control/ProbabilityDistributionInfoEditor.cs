﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shared.GUI.Control
{
    public partial class ProbabilityDistributionInfoEditor : UserControl, IEditor<Class.ProbabilityDistributionInfo>
    {
        public ProbabilityDistributionInfoEditor()
        {
            InitializeComponent();

            InitializeEditor();

            comboBox.Items.AddRange(new object[] { Class.ProbabilityDistributionType.Normal });
            comboBox.SelectedIndex = 0;
            comboBox.SelectedIndexChanged += ComboBox_SelectedIndexChanged;
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cb = (ComboBox)sender;

            IEditor<Class.ProbabilityDistributionInfo> newEditor = GetEditor((Class.ProbabilityDistributionType)cb.SelectedItem);
            newEditor.Init(currentEditor.Get());

            editorPanel.Controls.Clear();
            editorPanel.Controls.Add(newEditor.AsControl());

            currentEditor = newEditor;
        }

        Dictionary<Class.ProbabilityDistributionType, IEditor<Class.ProbabilityDistributionInfo>> editors;
        IEditor<Class.ProbabilityDistributionInfo> currentEditor;

        public void Init(Class.ProbabilityDistributionInfo obj)
        {
            comboBox.SelectedItem = obj.Type;
            currentEditor.Init(obj);
        }

        public Class.ProbabilityDistributionInfo Get()
        {
            return currentEditor.Get();
        }

        public System.Windows.Forms.Control AsControl()
        {
            return this;
        }

        private void InitializeEditor()
        {
            editors = new Dictionary<Class.ProbabilityDistributionType, IEditor<Class.ProbabilityDistributionInfo>>();
            editors[Class.ProbabilityDistributionType.Normal] = new NormalDistributionInfoEditor();

            foreach (IEditor<Class.ProbabilityDistributionInfo> editor in editors.Values)
            {
                editor.AsControl().Dock = DockStyle.Fill;
            }

            currentEditor = editors[Class.ProbabilityDistributionType.Normal];
            editorPanel.Controls.Add(currentEditor.AsControl());
        }

        private IEditor<Class.ProbabilityDistributionInfo> GetEditor(Class.ProbabilityDistributionType type)
        {
            return editors[type];
        }
    }
}
