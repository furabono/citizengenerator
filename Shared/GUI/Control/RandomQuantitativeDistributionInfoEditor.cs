﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shared.GUI.Control
{
    public partial class RandomQuantitativeDistributionInfoEditor 
        : UserControl, IEditor<Class.RandomQuantitativeDistributionInfo>, IEditor<Class.QuantitativeDistributionInfo>
    {
        public RandomQuantitativeDistributionInfoEditor()
        {
            InitializeComponent();
        }

        // check obj is null?
        public void Init(Class.RandomQuantitativeDistributionInfo obj)
        {
            targetEditor.Init(obj.Target);
        }

        // check obj is null?
        public void Init(Class.QuantitativeDistributionInfo obj)
        {
            if (obj.Type == Class.QuantitativeDistributionInfoType.Random) { Init((Class.RandomQuantitativeDistributionInfo)obj); }
            else { targetEditor.Init(obj.Target); }
        }

        public Class.RandomQuantitativeDistributionInfo Get()
        {
            return new Class.RandomQuantitativeDistributionInfo(targetEditor.Get());
        }

        Class.QuantitativeDistributionInfo IEditor<Class.QuantitativeDistributionInfo>.Get()
        {
            return Get();
        }

        public System.Windows.Forms.Control AsControl()
        {
            return this;
        }
    }
}
