﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// TODO

namespace Shared.GUI.Control
{
    public partial class ScriptQuantitativeDistributionInfoEditor 
        : UserControl, IEditor<Class.ScriptQuantitativeDistributionInfo>, IEditor<Class.QuantitativeDistributionInfo>
    {
        public ScriptQuantitativeDistributionInfoEditor()
        {
            InitializeComponent();
        }

        // check obj is null?
        public void Init(Class.ScriptQuantitativeDistributionInfo obj)
        {
            scriptEditor.Init(obj.Script);
            targetEditor.Init(obj.Target);
        }

        // check obj is null?
        public void Init(Class.QuantitativeDistributionInfo obj)
        {
            if (obj.Type == Class.QuantitativeDistributionInfoType.Script) { Init((Class.ScriptQuantitativeDistributionInfo)obj); }
            else { targetEditor.Init(obj.Target); }
        }

        public Class.ScriptQuantitativeDistributionInfo Get()
        {
            return new Class.ScriptQuantitativeDistributionInfo(targetEditor.Get(), scriptEditor.Get());
        }

        Class.QuantitativeDistributionInfo IEditor<Class.QuantitativeDistributionInfo>.Get()
        {
            return Get();
        }

        public System.Windows.Forms.Control AsControl()
        {
            return this;
        }
    }
}
