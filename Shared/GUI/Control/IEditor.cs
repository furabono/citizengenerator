﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.GUI.Control
{
    public interface IEditor<T>
    {
        void Init(T obj);
        T Get();
        System.Windows.Forms.Control AsControl();
    }
}
