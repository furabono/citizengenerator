﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shared.GUI.Control
{
    public partial class QuantitativeGenerationInfoEditor : UserControl, IEditor<Class.QuantitativeDistributionInfo[]>
    {
        public QuantitativeGenerationInfoEditor()
        {
            InitializeComponent();

            addButton.Click += AddButton_Click;
            removeButton.Click += RemoveButton_Click;
            dataGridView.SelectionChanged += DataGridView_SelectionChanged;

            dataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

            distributionInfos = new List<Class.QuantitativeDistributionInfo>();
            Unselect();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            SaveChange();

            var init = new Class.RandomQuantitativeDistributionInfo(null);
            distributionInfos.Add(init);
            dataGridView.Rows.Add(QuantitativeDistributionInfoToString(init));
            dataGridView.Rows[dataGridView.Rows.Count - 1].Selected = true;
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            if (selectedIndex.HasValue)
            {
                int index = selectedIndex.Value;

                distributionInfos.RemoveAt(index);
                dataGridView.Rows.RemoveAt(index);

                Unselect();
            }
        }

        private void DataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count > 0)
            {
                quantitativeDistributionInfoEditor.Visible = true;

                SaveChange();

                int index = dataGridView.SelectedRows[0].Index;
                quantitativeDistributionInfoEditor.Init(distributionInfos[index]);
                selectedIndex = index;
            }
        }

        List<Class.QuantitativeDistributionInfo> distributionInfos;
        int? selectedIndex;

        public void Init(Class.QuantitativeDistributionInfo[] obj)
        {
            distributionInfos = obj.ToList();

            dataGridView.Rows.Clear();
            foreach (Class.QuantitativeDistributionInfo qdi in distributionInfos)
            {
                dataGridView.Rows.Add(QuantitativeDistributionInfoToString(qdi));
            }

            Unselect();
        }

        public Class.QuantitativeDistributionInfo[] Get()
        {
            SaveChange();

            return distributionInfos.ToArray();
        }

        public System.Windows.Forms.Control AsControl()
        {
            return this;
        }

        public string QuantitativeDistributionInfoToString(Class.QuantitativeDistributionInfo obj)
        {
            string str = string.Format("Type : {0}\n", obj.Type);
            
            switch(obj.Type)
            {
                case Class.QuantitativeDistributionInfoType.Script:
                    {
                        var sqdi = (Class.ScriptQuantitativeDistributionInfo)obj;
                        str += string.Format("Script : {0}...\n", sqdi.Script.ScriptText.Substring(0, 10));
                        break;
                    }
                case Class.QuantitativeDistributionInfoType.ProbabilityDistribution:
                    {
                        var pdqdi = (Class.ProbabilityDistributionQuantitativeDistributionInfo)obj;
                        str += string.Format("With {0} Distributions\n", pdqdi.DistributionInfos.Length);
                        break;
                    }
            }

            if (obj.Target == null)
            {
                str += "Target : None";
            }
            else
            {
                str += string.Format("Target : {0}...", obj.Target.Script.ScriptText.Substring(0, 10));
            }

            return str;
        }

        private void SaveChange()
        {
            if (selectedIndex.HasValue)
            {
                int index = selectedIndex.Value;
                distributionInfos[index] = quantitativeDistributionInfoEditor.Get();
                dataGridView[0, index].Value = QuantitativeDistributionInfoToString(distributionInfos[index]);
            }
        }

        private void Unselect()
        {
            selectedIndex = null;
            quantitativeDistributionInfoEditor.Visible = false;
            dataGridView.ClearSelection();
        }
    }
}
