﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shared.GUI.Control
{
    public partial class NormalDistributionInfoEditor 
        : UserControl, IEditor<Class.NormalDistributionInfo>, IEditor<Class.ProbabilityDistributionInfo>
    {
        public NormalDistributionInfoEditor()
        {
            InitializeComponent();

            minNumericUpDown.Minimum = int.MinValue;
            minNumericUpDown.Maximum = int.MaxValue;
            maxNumericUpDown.Minimum = int.MinValue;
            maxNumericUpDown.Maximum = int.MaxValue;
        }
        
        public void Init(Class.NormalDistributionInfo obj)
        {
            minNumericUpDown.Value = obj.Min;
            maxNumericUpDown.Value = obj.Max;
            meanNumericUpDown.Value = (decimal)obj.Mean;
            varNumericUpDown.Value = (decimal)obj.Variance;
        }

        public void Init(Class.ProbabilityDistributionInfo obj)
        {
            if (obj.Type == Class.ProbabilityDistributionType.Normal) { Init((Class.NormalDistributionInfo)obj); }
            else
            {
                minNumericUpDown.Value = obj.Min;
                maxNumericUpDown.Value = obj.Max;
            }
        }

        public Class.NormalDistributionInfo Get()
        {
            return new Class.NormalDistributionInfo(
                decimal.ToInt32(minNumericUpDown.Value), 
                decimal.ToInt32(maxNumericUpDown.Value), 
                decimal.ToSingle(meanNumericUpDown.Value), 
                decimal.ToSingle(varNumericUpDown.Value));
        }

        Class.ProbabilityDistributionInfo IEditor<Class.ProbabilityDistributionInfo>.Get()
        {
            return Get();
        }

        public System.Windows.Forms.Control AsControl()
        {
            return this;
        }
    }
}
