﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shared.GUI.Control
{
    public partial class TargetEditor : UserControl, IEditor<Class.Target>
    {
        public TargetEditor()
        {
            InitializeComponent();

            checkBox.CheckedChanged += CheckBox_CheckedChanged;

            scriptEditor.Enabled = false;
        }

        private void CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            scriptEditor.Enabled = checkBox.Checked;
        }

        public void Init(Class.Target obj)
        {
            if (obj == null) { checkBox.Checked = false; }
            else
            {
                checkBox.Checked = true;
                scriptEditor.Init(obj.Script);
            }
        }

        public Class.Target Get()
        {
            Class.Script script = scriptEditor.Get();

            if (script == null) { return null; }
            else { return new Class.Target(script); }
        }

        public System.Windows.Forms.Control AsControl()
        {
            return this;
        }
    }
}
