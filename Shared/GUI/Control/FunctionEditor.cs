﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shared.GUI.Control
{
    public partial class FunctionEditor : UserControl, IEditor<string>
    {
        public FunctionEditor()
        {
            InitializeComponent();
        }

        public void Init(string obj)
        {
            richTextBox.Text = obj;
        }

        public string Get()
        {
            return richTextBox.Text;
        }

        public System.Windows.Forms.Control AsControl()
        {
            return this;
        }
    }
}
