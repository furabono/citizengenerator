﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shared.GUI.Control
{
    public partial class QuantitativeDistributionInfoEditor : UserControl, IEditor<Class.QuantitativeDistributionInfo>
    {
        public QuantitativeDistributionInfoEditor()
        {
            InitializeComponent();

            InitializeEditor();
            editorPanel.Controls.Add(currentEditor.AsControl());

            typeComboBox.Items.AddRange(new object[] {
                Class.QuantitativeDistributionInfoType.Random,
                Class.QuantitativeDistributionInfoType.Script,
                Class.QuantitativeDistributionInfoType.ProbabilityDistribution
            });
            typeComboBox.SelectedIndex = 0;
            typeComboBox.SelectedIndexChanged += TypeComboBox_SelectedIndexChanged;

            //editors = new Dictionary<Class.QuantitativeGenerationInfoType, IEditor<Class.QuantitativeGenerationInfo>>();
            //editors[Class.QuantitativeGenerationInfoType.Random] = new RandomQuantitativeGenerationInfoEditor();
        }

        private void TypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cb = (ComboBox)sender;

            IEditor<Class.QuantitativeDistributionInfo> newEditor = GetEditor((Class.QuantitativeDistributionInfoType)cb.SelectedItem);
            newEditor.Init(currentEditor.Get());

            editorPanel.Controls.Clear();
            editorPanel.Controls.Add(newEditor.AsControl());

            currentEditor = newEditor;
        }

        Dictionary<Class.QuantitativeDistributionInfoType, IEditor<Class.QuantitativeDistributionInfo>> editors;
        IEditor<Class.QuantitativeDistributionInfo> currentEditor;

        public void Init(Class.QuantitativeDistributionInfo obj)
        {
            typeComboBox.SelectedItem = obj.Type;
            currentEditor.Init(obj);
        }

        public Class.QuantitativeDistributionInfo Get()
        {
            return currentEditor.Get();
        }

        public System.Windows.Forms.Control AsControl()
        {
            return this;
        }

        private void InitializeEditor()
        {
            editors = new Dictionary<Class.QuantitativeDistributionInfoType, IEditor<Class.QuantitativeDistributionInfo>>();
            editors[Class.QuantitativeDistributionInfoType.Random] = new RandomQuantitativeDistributionInfoEditor();
            editors[Class.QuantitativeDistributionInfoType.Script] = new ScriptQuantitativeDistributionInfoEditor();
            editors[Class.QuantitativeDistributionInfoType.ProbabilityDistribution] = new ProbabilityDistributionQuantitativeDistributionInfoEditor();

            foreach (IEditor<Class.QuantitativeDistributionInfo> editor in editors.Values)
            {
                editor.AsControl().Dock = DockStyle.Fill;
            }

            currentEditor = editors[Class.QuantitativeDistributionInfoType.Random];
        }

        private IEditor<Class.QuantitativeDistributionInfo> GetEditor(Class.QuantitativeDistributionInfoType type)
        {
            return editors[type];
        }
    }
}
