﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shared.GUI.Control
{
    public partial class ScriptEditor : UserControl, IEditor<Class.Script>
    {
        public ScriptEditor()
        {
            InitializeComponent();

            //expressionRadioButton.CheckedChanged += RadioButton_CheckedChanged;
            expressionRadioButton.Click += ExpressionRadioButton_Click;
            functionRadioButton.Click += FunctionRadioButton_Click;

            expressionEditor = new ExpressionEditor();
            expressionEditor.Dock = DockStyle.Top;
            functionEditor = new FunctionEditor();
            functionEditor.Dock = DockStyle.Fill;

            expressionRadioButton.Checked = true;
            ExpressionRadioButton_Click(null, null);
        }

        private void ExpressionRadioButton_Click(object sender, EventArgs e)
        {
            editorPanel.Controls.Clear();
            editorPanel.Controls.Add(expressionEditor);
        }

        private void FunctionRadioButton_Click(object sender, EventArgs e)
        {
            editorPanel.Controls.Clear();
            editorPanel.Controls.Add(functionEditor);
        }

        //private void RadioButton_CheckedChanged(object sender, EventArgs e)
        //{
        //    var rb = (RadioButton)sender;
        //    //if (!rb.Checked) { return; }

        //    editorPanel.Controls.Clear();
        //    if (object.ReferenceEquals(rb, expressionRadioButton)) { editorPanel.Controls.Add(expressionEditor); }
        //    else { editorPanel.Controls.Add(functionEditor); }
        //}

        ExpressionEditor expressionEditor;
        FunctionEditor functionEditor;

        public void Init(Class.Script obj)
        {
            if (obj == null) { expressionRadioButton.Checked = true; }
            else
            {
                if (obj.Type == Class.ScriptType.Expression)
                {
                    expressionRadioButton.Checked = true;
                    expressionEditor.Init(obj.ScriptText);
                }
                else
                {
                    functionRadioButton.Checked = true;
                    functionEditor.Init(obj.ScriptText);
                }
            }
        }

        public Class.Script Get()
        {
            if (expressionRadioButton.Checked)
            {
                if (string.IsNullOrWhiteSpace(expressionEditor.Get())) { return null; }
                else { return new Class.Script(Class.ScriptType.Expression, expressionEditor.Get()); }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(functionEditor.Get())) { return null; }
                else { return new Class.Script(Class.ScriptType.Function, functionEditor.Get()); }
            }
        }

        public System.Windows.Forms.Control AsControl()
        {
            return this;
        }
    }
}
