﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shared.GUI.Control
{
    public partial class ExpressionEditor : UserControl, IEditor<string>
    {
        public ExpressionEditor()
        {
            InitializeComponent();

            textBox.KeyDown += TextBox_KeyDown;
            textBox.KeyPress += TextBox_KeyPress;
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            isEnter = e.KeyCode == Keys.Enter;
        }

        private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = isEnter;
        }

        bool isEnter;

        public void Init(string obj)
        {
            textBox.Text = obj;
        }

        public string Get()
        {
            return textBox.Text;
        }

        public System.Windows.Forms.Control AsControl()
        {
            return this;
        }
    }
}
