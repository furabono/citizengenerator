﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shared.GUI.Dialog
{
    public partial class ProbabilityDistributionInfoEditorDialog : Form
    {
        public ProbabilityDistributionInfoEditorDialog()
        {
            InitializeComponent();

            this.KeyDown += DistributionInfoEditorDialog_KeyDown;
            this.okButton.Click += OkButton_Click;
        }

        private void DistributionInfoEditorDialog_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) { OK(); }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            OK();
        }

        public Class.ProbabilityDistributionInfo Get
        {
            get { return distributionInfoEditor.Get(); }
        }

        public DialogResult ShowDialog(Class.ProbabilityDistributionInfo distributionInfo = null)
        {
            if (distributionInfo != null) { this.distributionInfoEditor.Init(distributionInfo); }

            return base.ShowDialog();
        }

        private void OK()
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}
