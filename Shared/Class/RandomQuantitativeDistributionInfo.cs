﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Class
{
    public class RandomQuantitativeDistributionInfo : QuantitativeDistributionInfo
    {
        public override QuantitativeDistributionInfoType Type
        {
            get { return QuantitativeDistributionInfoType.Random; }
        }

        public RandomQuantitativeDistributionInfo(Target target) : base(target)
        { }

        //public new RandomQuantitativeGenerationInfo SetCondition(Condition condition)
        //{
        //    if (condition == null) { return null; }
        //    if (condition == this.condition) { return null; }

        //    return new RandomQuantitativeGenerationInfo(condition);
        //}
    }
}
