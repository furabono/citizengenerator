﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Class
{
    public class QualitativeGenerationInfo : GenerationInfo
    {
        int?[] proportions;
        int proportionSum;
        Script[] scripts;

        public new Citizen.QualitativeParameterInfo ParameterInfo
        {
            get { return (Citizen.QualitativeParameterInfo)parameterInfo; }
        }

        public int?[] Proportions
        {
            get { return proportions; }
        }

        public int ProportionSum
        {
            get { return proportionSum; }
        }

        public Script[] Scripts
        {
            get { return scripts; }
        }

        public QualitativeGenerationInfo(Citizen.QualitativeParameterInfo parameterInfo) : base(parameterInfo)
        {
            this.proportions = new int?[parameterInfo.Categories.Length];
            this.proportionSum = 0;
        }

        // proprotions != null
        public QualitativeGenerationInfo(Citizen.QualitativeParameterInfo parameterInfo, int?[] proportions, Script[] scripts) : base(parameterInfo)
        {
            this.proportions = proportions;
            foreach (int? p in proportions)
            {
                proportionSum += p ?? 0;
            }

            this.scripts = scripts;
        }
    }
}
