﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Class
{
    public class GenerationSettings
    {
        GenerationInfo[] generationInfos;
        int[] order;

        public GenerationInfo[] GenerationInfos
        {
            get { return generationInfos; }
        }

        public IEnumerable<GenerationInfo> OrderedGenerationInfos
        {
            get { return (from i in order select generationInfos[i]); }
        }

        public GenerationSettings(GenerationInfo[] generationInfos)
        {
            this.order = Order(generationInfos);
            this.generationInfos = generationInfos;
        }

        public GenerationSettings(GenerationInfo[] generationInfos, int[] order)
        {
            if (generationInfos.Length != order.Length) { }

            this.generationInfos = generationInfos.ToArray();
            this.order = order.ToArray();
        }

        private int[] Order(GenerationInfo[] generationInfos)
        {
            var adj = new HashSet<GenerationInfo>[generationInfos.Length];

            for (int i = 0; i < generationInfos.Length; ++i)
            {
                adj[i] = new HashSet<GenerationInfo>();

                if (generationInfos[i].ParameterType == Citizen.ParameterType.Quantitative)
                {
                    var qgi = (QuantitativeGenerationInfo)generationInfos[i];

                    foreach (QuantitativeDistributionInfo qdi in qgi.DistributionInfos)
                    {
                        string script = qdi.Target.Script.ScriptText;
                        foreach (GenerationInfo gi in generationInfos)
                        {
                            if (script.Contains(gi.Name)) { adj[i].Add(gi); }
                        }
                    }
                }
                else
                {
                    var qp = (QualitativeGenerationInfo)generationInfos[i];

                    if (qp.Scripts != null)
                    {
                        foreach (Script s in qp.Scripts)
                        {
                            string script = s.ScriptText;
                            foreach (GenerationInfo gi in generationInfos)
                            {
                                if (script.Contains(gi.Name)) { adj[i].Add(gi); }
                            }
                        }
                    }
                }
            }

            var order = new List<int>();
            while (order.Count < generationInfos.Length)
            {
                int i;
                for (i = 0; i < adj.Length; ++i)
                {
                    if (adj[i].Count == 0)
                    {
                        order.Add(i);

                        for (int j = 0; j < adj.Length; ++j)
                        {
                            adj[j].Remove(generationInfos[i]);
                        }

                        adj[i].Add(generationInfos[i]);

                        break;
                    }
                }

                if (i == adj.Length)
                {
                    order = null;
                    throw new ReferenceError();
                }
            }

            return order.ToArray();
        }
    }

    public class ReferenceError : Exception
    {
        public override string Message
        {
            get { return "참조 오류"; }
        }
    }
}
