﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Class
{
    public class NormalDistributionInfo : ProbabilityDistributionInfo
    {
        float mean;
        float variance;

        public override ProbabilityDistributionType Type
        {
            get { return ProbabilityDistributionType.Normal; }
        }

        public float Mean
        {
            get { return mean; }
        }

        public float Variance
        {
            get { return variance; }
        }

        public NormalDistributionInfo(int min, int max, float mean, float variance) : base(min, max)
        {
            this.mean = mean;
            this.variance = variance;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }

            var ndi = obj as NormalDistributionInfo;
            if (ndi == null) { return false; }

            return Equals(ndi);
        }

        public bool Equals(NormalDistributionInfo normalDistributionInfo)
        {
            return (this.min == normalDistributionInfo.min
                && this.max == normalDistributionInfo.max
                && this.mean == normalDistributionInfo.mean
                && this.variance == normalDistributionInfo.variance);
        }

        public override int GetHashCode()
        {
            return (Type.GetHashCode() ^ min ^ max ^ mean.GetHashCode() ^ variance.GetHashCode());
        }

        public override bool isValid()
        {
            return variance != 0;
        }
    }
}
