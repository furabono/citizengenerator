﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Class
{
    public class QuantitativeGenerationInfo : GenerationInfo
    {
        List<QuantitativeDistributionInfo> distributionInfos;

        public new Citizen.QuantitativeParameterInfo ParameterInfo
        {
            get { return (Citizen.QuantitativeParameterInfo)parameterInfo; }
        }

        public QuantitativeDistributionInfo[] DistributionInfos
        {
            get { return distributionInfos.ToArray(); }
        }

        public QuantitativeGenerationInfo(Citizen.QuantitativeParameterInfo parameterInfo) : base(parameterInfo)
        {
            distributionInfos = new List<QuantitativeDistributionInfo>();
        }

        public QuantitativeGenerationInfo(Citizen.QuantitativeParameterInfo parameterInfo, params QuantitativeDistributionInfo[] distributionInfos) : base(parameterInfo)
        {
            this.distributionInfos = distributionInfos.ToList();
        }

        public QuantitativeGenerationInfo AddGenerationInfo(QuantitativeDistributionInfo qdi)
        {
            if (qdi == null) { return null; }

            var tmp = distributionInfos.ToList();
            tmp.Add(qdi);

            return new QuantitativeGenerationInfo(ParameterInfo, tmp.ToArray());
        }

        public QuantitativeGenerationInfo RemoveGenerationInfo(QuantitativeDistributionInfo qdi)
        {
            if (qdi == null) { return null; }

            var tmp = distributionInfos.ToList();
            if (tmp.Remove(qdi)) { return null; }

            return new QuantitativeGenerationInfo(ParameterInfo, tmp.ToArray());
        }
    }
}
