﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Class
{
    public abstract class QuantitativeDistributionInfo
    {
        //protected QuantitativeParameterInfo quantitativeParameterInfo;
        protected Target target;

        public abstract QuantitativeDistributionInfoType Type
        {
            get;
        }

        //public QuantitativeParameterInfo QuantitativeParameterInfo
        //{
        //    get { return quantitativeParameterInfo; }
        //}

        public Target Target
        {
            get { return target; }
        }

        public QuantitativeDistributionInfo(Target target)
        {
            this.target = target;
        }

        //public QuantitativeGenerationInfo SetCondition(Condition condition)
        //{

        //}
    }

    public enum QuantitativeDistributionInfoType
    {
        Random, Script, ProbabilityDistribution
    }
}
