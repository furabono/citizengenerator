﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Class
{
    public class ProbabilityDistributionQuantitativeDistributionInfo : QuantitativeDistributionInfo
    {
        ProbabilityDistributionInfo[] distributionInfos;

        public override QuantitativeDistributionInfoType Type
        {
            get { return QuantitativeDistributionInfoType.ProbabilityDistribution; }
        }

        public ProbabilityDistributionInfo[] DistributionInfos
        {
            get { return distributionInfos.ToArray(); }
        }

        public ProbabilityDistributionQuantitativeDistributionInfo(Target target, ProbabilityDistributionInfo[] distributionInfos) : base(target)
        {
            if (distributionInfos == null) { this.distributionInfos = new ProbabilityDistributionInfo[0]; }
            else { this.distributionInfos = distributionInfos.ToArray(); }
        }

        private ProbabilityDistributionQuantitativeDistributionInfo(Target target, List<ProbabilityDistributionInfo> distributionInfos) : base(target)
        {
            if (distributionInfos == null) { this.distributionInfos = new ProbabilityDistributionInfo[0]; }
            else { this.distributionInfos = distributionInfos.ToArray(); }
        }

        // return null cases -> Exception ?
        public ProbabilityDistributionQuantitativeDistributionInfo AddDistributionInfo(ProbabilityDistributionInfo distributionInfo)
        {
            //if (distributionInfos == null) { return null; }
            //if (distributionInfos.Contains(distributionInfo)) { return null; }

            var tmp = distributionInfos.ToList();
            tmp.Add(distributionInfo);
            return new ProbabilityDistributionQuantitativeDistributionInfo(this.target, tmp);
        }

        public ProbabilityDistributionQuantitativeDistributionInfo AddDistributionInfo(ProbabilityDistributionInfo distributionInfo, int index)
        {
            //if (distributionInfos == null) { return null; }
            //if (distributionInfos.Contains(distributionInfo)) { return null; }

            var tmp = distributionInfos.ToList();
            tmp.Insert(index, distributionInfo);
            return new ProbabilityDistributionQuantitativeDistributionInfo(this.target, tmp);
        }

        public ProbabilityDistributionQuantitativeDistributionInfo RemoveDistributionInfo(ProbabilityDistributionInfo distributionInfo)
        {
            //if (distributionInfos == null) { return null; }

            var tmp = distributionInfos.ToList();
            if (tmp.Remove(distributionInfo)) { return new ProbabilityDistributionQuantitativeDistributionInfo(this.target, tmp.ToArray()); }

            return null;
        }

        public ProbabilityDistributionQuantitativeDistributionInfo RemoveDistributionInfo(int index)
        {
            //if (distributionInfos == null) { return null; }

            var tmp = distributionInfos.ToList();
            tmp.RemoveAt(index);
            return new ProbabilityDistributionQuantitativeDistributionInfo(this.target, tmp);
        }
    }
}
