﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Class
{
    public class Target
    {
        Script script;

        public Script Script
        {
            get { return script; }
        }

        public Target(Script script)
        {
            this.script = script;
        }
    }
}
