﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Class
{
    public class Policy
    {
        public Policy(string name, KeyValuePair<string, float>[] variables, string result) :this(name, variables, "", "", "", result)
        { }

        public Policy(string name, KeyValuePair<string, float>[] variables, string memory1, string result) :this(name, variables, memory1, "", "", result)
        { }

        public Policy(string name, KeyValuePair<string, float>[] variables, string memory1, string memory2, string result) :this(name, variables, memory1, memory2, "", result)
        { }

        public Policy(string name, KeyValuePair<string, float>[] variables, string memory1, string memory2, string memory3, string result)
        {
            this.name = name;
            this.variables = variables;
            this.memory1 = memory1;
            this.memory2 = memory2;
            this.memory3 = memory3;
            this.result = result;
        }

        string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        KeyValuePair<string, float>[] variables;
        public KeyValuePair<string, float>[] Variables
        {
            get { return variables; }
            set { variables = value; }
        }

        string memory1;
        public string Memory1
        {
            get { return memory1; }
            set { memory1 = value; }
        }

        string memory2;
        public string Memory2
        {
            get { return memory2; }
            set { memory2 = value; }
        }

        string memory3;
        public string Memory3
        {
            get { return memory3; }
            set { memory3 = value; }
        }

        string result;
        public string Result
        {
            get { return result; }
            set { result = value; }
        }
    }
}
