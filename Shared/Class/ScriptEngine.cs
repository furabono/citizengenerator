﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Class
{
    class ScriptEngine
    {
        Microsoft.Scripting.Hosting.ScriptEngine engine;
        Microsoft.Scripting.Hosting.ScriptScope scope;

        public ScriptEngine()
        {
            engine = IronPython.Hosting.Python.CreateEngine();
            scope = engine.CreateScope();
        }

        public void SetVariable<T>(string name, T value)
        {
            scope.SetVariable(name, value);
        }

        public T GetVariable<T>(string name)
        {
            return scope.GetVariable<T>(name);
        }

        public void Run(string script)
        {
            var source = engine.CreateScriptSourceFromString(script);
            source.Execute(scope);
        }

        //public T Run<T>(Script<T> script);
    }

    enum BuildingBlock
    {
        None, Function, If, For
    }

    class PythonScriptBuilder
    {
        StringBuilder imports;
        StringBuilder body;
        string indent;
        Stack<BuildingBlock> buildingBlockStack;

        public string Script
        {
            get;
        }

        public PythonScriptBuilder()
        {
            imports = new StringBuilder();
            body = new StringBuilder();
            indent = "";
            buildingBlockStack.Push(BuildingBlock.None);
        }

        // check argument null?
        public void Import(string import)
        {
            imports.AppendFormat("import {0}\n", import);
        }

        // check argument null?
        public void Import(string from, string import)
        {
            imports.AppendFormat("from {0} import {1}\n", from, import);
        }

        // check line contains only one line
        // check argument null?
        public PythonScriptBuilder WriteLine(string line = "\n")
        {
            body.AppendLine(indent + line);
            return this;
        }

        public PythonScriptBuilder Indent()
        {
            indent = indent + "\t";
            return this;
        }

        public PythonScriptBuilder Outdent()
        {
            if (indent.Length > 0) { indent = indent.Remove(0, 1); }
            return this;
        }

        // check items of body are single/multi line
        // return null -> exception
        // check argument null?
        public PythonScriptBuilder Function(string name, string[] arguments, params string[] body)
        {
            if (buildingBlockStack.Peek() != BuildingBlock.None) { return null; }

            this.body.AppendFormat("{0}def {1}({2}):\n", indent, name, string.Join(", ", arguments));

            foreach (string line in body)
            {
                this.body.AppendFormat("{0}\t{1}\n", indent, line);
            }

            return this;
        }

        // return null -> exception
        // check argument null?
        public PythonScriptBuilder Function(string name, params string[] arguments)
        {
            if (buildingBlockStack.Peek() != BuildingBlock.None) { return null; }

            this.body.AppendFormat("{0}def {1}({2}):\n", indent, name, string.Join(", ", arguments));
            Indent();
            buildingBlockStack.Push(BuildingBlock.Function);

            return this;
        }

        // return null -> exception
        public PythonScriptBuilder EndFunction()
        {
            if (buildingBlockStack.Peek() != BuildingBlock.Function) { return null; }

            Outdent();
            buildingBlockStack.Pop();

            return this;
        }

        // check argument null?
        public PythonScriptBuilder If(string condition, params string[] body)
        {
            this.body.AppendFormat("{0}if ( {1} ):\n", indent, condition);

            foreach (string line in body)
            {
                this.body.AppendFormat("{0}\t{1}\n", indent, line);
            }

            return this;
        }

        // check argument null?
        public PythonScriptBuilder If(string condition)
        {
            this.body.AppendFormat("{0}if ( {1} ):\n", indent, condition);

            Indent();
            buildingBlockStack.Push(BuildingBlock.If);

            return this;
        }

        // return null -> exception
        // return null -> exception
        public PythonScriptBuilder EndIf()
        {
            if (buildingBlockStack.Peek() != BuildingBlock.If) { return null; }

            Outdent();
            buildingBlockStack.Pop();

            return this;
        }

        // check argument null?
        public PythonScriptBuilder For(string variable, int rangeMinInclusive, int rangeMaxExclusive, params string[] body)
        {
            this.body.AppendFormat("{0}for {1} in range({2}, {3}):\n", indent, variable, rangeMinInclusive, rangeMaxExclusive);

            foreach (string line in body)
            {
                this.body.AppendFormat("{0}\t{1}\n", indent, line);
            }

            return this;
        }

        // check argument null?
        public PythonScriptBuilder For(string variable, int rangeMinInclusive, int rangeMaxExclusive)
        {
            this.body.AppendFormat("{0}for {1} in range({2}, {3}):\n", indent, variable, rangeMinInclusive, rangeMaxExclusive);

            Indent();
            buildingBlockStack.Push(BuildingBlock.For);

            return this;
        }

        // check argument null?
        public PythonScriptBuilder For(string variable, string container, params string[] body)
        {
            this.body.AppendFormat("{0}for {1} in {2}:\n", indent, variable, container);

            foreach (string line in body)
            {
                this.body.AppendFormat("{0}\t{1}\n", indent, line);
            }

            return this;
        }

        // check argument null?
        public PythonScriptBuilder For(string variable, string container)
        {
            this.body.AppendFormat("{0}for {1} in {2}:\n", indent, variable, container);

            Indent();
            buildingBlockStack.Push(BuildingBlock.For);

            return this;
        }

        // check argument null?
        // return null -> exception
        public PythonScriptBuilder EndFor()
        {
            if (buildingBlockStack.Peek() != BuildingBlock.For) { return null; }

            Outdent();
            buildingBlockStack.Pop();

            return this;
        }
    }
}
