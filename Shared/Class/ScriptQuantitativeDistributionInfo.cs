﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Class
{
    public class ScriptQuantitativeDistributionInfo : QuantitativeDistributionInfo
    {
        Script script;

        public override QuantitativeDistributionInfoType Type
        {
            get { return QuantitativeDistributionInfoType.Script; }
        }

        public Script Script
        {
            get { return script; }
        }

        public ScriptQuantitativeDistributionInfo(Target target, Script script) : base(target)
        {
            this.script = script;
        }

        //public new ScriptQuantitativeGenerationInfo SetCondition(Condition condition)
        //{
        //    if (condition == null) { return null; }
        //    if (condition == this.condition) { return null; }

        //    return new ScriptQuantitativeGenerationInfo(condition, this.script);
        //}

        //public ScriptQuantitativeGenerationInfo SetScript(Script<int> script)
        //{
        //    if (script == null) { return null; }
        //    if (script == this.script) { return null; }

        //    return new ScriptQuantitativeGenerationInfo(this.condition, script);
        //}
    }
}
