﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Class
{
    public abstract class ProbabilityDistributionInfo
    {
        protected int min;
        protected int max;

        public abstract ProbabilityDistributionType Type
        {
            get;
        }

        public int Min
        {
            get { return min; }
        }

        public int Max
        {
            get { return max; }
        }

        public ProbabilityDistributionInfo(int min, int max)
        {
            this.min = min;
            this.max = max;
        }

        public abstract override bool Equals(object obj);
        public abstract override int GetHashCode();
        public abstract bool isValid();
    }

    public enum ProbabilityDistributionType
    {
        Normal
    }
}
