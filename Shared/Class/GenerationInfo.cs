﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Class
{
    public abstract class GenerationInfo
    {
        protected Citizen.ParameterInfo parameterInfo;

        public Citizen.ParameterType ParameterType
        {
            get { return parameterInfo.Type; }
        }

        public Citizen.ParameterInfo ParameterInfo
        {
            get { return parameterInfo; }
        }

        public string Name
        {
            get { return parameterInfo.Name; }
        }

        public GenerationInfo(Citizen.ParameterInfo parameterInfo)
        {
            this.parameterInfo = parameterInfo;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
